var searchData=
[
  ['initboard',['initBoard',['../classabstraction_1_1BoardAbstraction.html#aef1eeb472a17ef5431f5032f221083c6',1,'abstraction.BoardAbstraction.initBoard()'],['../interfacespecification_1_1BoardSpecification.html#ac6b713d2790f012d5061586c6b8d6efa',1,'specification.BoardSpecification.initBoard()']]],
  ['invariant',['invariant',['../classabstraction_1_1BoardAbstraction.html#a80a15b4d6e53df65a7f1d54a937bb858',1,'abstraction.BoardAbstraction.invariant()'],['../classabstraction_1_1PlayerAbstraction.html#a9ec58c7d0efde699f76227bb0acc94c5',1,'abstraction.PlayerAbstraction.invariant()'],['../interfacespecification_1_1BoardSpecification.html#a33cd6f7085eec02caf04f528c0f92e66',1,'specification.BoardSpecification.invariant()'],['../interfacespecification_1_1PlayerSpecification.html#a1b9618ac2a29205240f2199d97068016',1,'specification.PlayerSpecification.invariant()']]],
  ['isfull',['isFull',['../classabstraction_1_1BoardAbstraction.html#a670fc37794433a907f2f513f6002a769',1,'abstraction.BoardAbstraction.isFull()'],['../interfacespecification_1_1BoardSpecification.html#a5f9cc57fe39cd46c2d1cffffd9c830e8',1,'specification.BoardSpecification.isFull()']]],
  ['isfullcolumn',['isFullColumn',['../classmodel_1_1Connected4Board.html#a65f35976f5987c09956fcaae7684dc1e',1,'model::Connected4Board']]]
];
