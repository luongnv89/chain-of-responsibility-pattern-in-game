var searchData=
[
  ['player',['player',['../classabstraction_1_1tests_1_1RuleAbstractionTest.html#afcbde84d78d3655c981507f8e3cf9ac4',1,'abstraction.tests.RuleAbstractionTest.player()'],['../classmodel_1_1Move.html#a89f94294f919693f1cdc8f6b74ae2d34',1,'model.Move.player()']]],
  ['player1',['player1',['../classabstraction_1_1tests_1_1GameAbstractionTest.html#a4dff77546c1a1169f8ad6defbc5cb30d',1,'abstraction.tests.GameAbstractionTest.player1()'],['../classabstraction_1_1tests_1_1PlayerAbstractionTest.html#a94ebf8e08063a4d63d7f84190c75ab7a',1,'abstraction.tests.PlayerAbstractionTest.player1()']]],
  ['player1name',['player1Name',['../classabstraction_1_1tests_1_1PlayerAbstractionTest.html#a4827977093d6ccf931200f9960034f49',1,'abstraction::tests::PlayerAbstractionTest']]],
  ['player1value',['player1Value',['../classabstraction_1_1tests_1_1PlayerAbstractionTest.html#abd23a4a0c6308ebac4427dbe93ab6efd',1,'abstraction::tests::PlayerAbstractionTest']]],
  ['player2',['player2',['../classabstraction_1_1tests_1_1GameAbstractionTest.html#a10200e02b2b4610ed70ca89e4c5827d4',1,'abstraction.tests.GameAbstractionTest.player2()'],['../classabstraction_1_1tests_1_1PlayerAbstractionTest.html#a43e8aa0e8e45114cb3e92c4b893348ee',1,'abstraction.tests.PlayerAbstractionTest.player2()']]],
  ['player2name',['player2Name',['../classabstraction_1_1tests_1_1PlayerAbstractionTest.html#a0d8bcf3862a359f70b5eea2317e9de22',1,'abstraction::tests::PlayerAbstractionTest']]],
  ['player2value',['player2Value',['../classabstraction_1_1tests_1_1PlayerAbstractionTest.html#ac028e7d6dc42fdda87e69d9b8b55f0b8',1,'abstraction::tests::PlayerAbstractionTest']]],
  ['playera',['playerA',['../classabstraction_1_1GameAbstraction.html#ae4cf5a806a40951fb6ba62f5ba3216ba',1,'abstraction::GameAbstraction']]],
  ['playerb',['playerB',['../classabstraction_1_1GameAbstraction.html#a3a74b82d36baea92ab040c23f0c3006f',1,'abstraction::GameAbstraction']]],
  ['playernoinvariant',['playerNoInvariant',['../classabstraction_1_1tests_1_1PlayerAbstractionTest.html#a0461a1d2778eb218715f889f9e4af302',1,'abstraction::tests::PlayerAbstractionTest']]]
];
