var searchData=
[
  ['makemove',['makeMove',['../classabstraction_1_1PlayerAbstraction.html#ad9f18be8917a52092a0a8ee9947b0a17',1,'abstraction.PlayerAbstraction.makeMove()'],['../interfacespecification_1_1PlayerSpecification.html#aa61fd571cbbbd75822becc7b5129abca',1,'specification.PlayerSpecification.makeMove()']]],
  ['move',['Move',['../classmodel_1_1Move.html#a7160a302b276a7b8e4559110057b3ec1',1,'model.Move.Move()'],['../classmodel_1_1Connected4Board.html#a3e1ff09762c698647fa714f3535c50ed',1,'model.Connected4Board.move()'],['../classmodel_1_1XOBoard.html#aff27a107cab4169cbc6dcf33035d731a',1,'model.XOBoard.move()'],['../interfacespecification_1_1BoardSpecification.html#abdb96387ab70b5303ba0dabf8be3a3ac',1,'specification.BoardSpecification.move()']]]
];
