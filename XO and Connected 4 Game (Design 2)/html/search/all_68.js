var searchData=
[
  ['haswinner',['hasWinner',['../classabstraction_1_1BoardAbstraction.html#a5c9d03237552c54a8041e3e0e5e63397',1,'abstraction.BoardAbstraction.hasWinner()'],['../interfacespecification_1_1BoardSpecification.html#a90f62de74afea8ce3c889250a4885ae2',1,'specification.BoardSpecification.hasWinner()']]],
  ['height',['HEIGHT',['../classabstraction_1_1tests_1_1BoardAbstractionTest.html#ad3a0abe6935c63a66c21fa5a037b4cf0',1,'abstraction.tests.BoardAbstractionTest.HEIGHT()'],['../classabstraction_1_1BoardAbstraction.html#a640ef73250ebb4878c794af548960888',1,'abstraction.BoardAbstraction.height()']]],
  ['human',['Human',['../classmodel_1_1Human.html#a9ddcaf970c4f0c7a10c5e38cc8c6dc15',1,'model::Human']]],
  ['human',['Human',['../classmodel_1_1Human.html',1,'model']]],
  ['human_2ejava',['Human.java',['../Human_8java.html',1,'']]],
  ['humanabstracttest',['HumanAbstractTest',['../classmodel_1_1tests_1_1HumanAbstractTest.html',1,'model::tests']]],
  ['humanabstracttest_2ejava',['HumanAbstractTest.java',['../HumanAbstractTest_8java.html',1,'']]],
  ['humangamesimulationabstract',['HumanGameSimulationAbstract',['../classsimulations_1_1HumanGameSimulationAbstract.html',1,'simulations']]],
  ['humangamesimulationabstract_2ejava',['HumanGameSimulationAbstract.java',['../HumanGameSimulationAbstract_8java.html',1,'']]]
];
