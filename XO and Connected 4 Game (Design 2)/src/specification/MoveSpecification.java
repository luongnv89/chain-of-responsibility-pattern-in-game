/**
 * 
 */
package specification;


public interface MoveSpecification {

	/**
	 * @return x 
	 */
	public int getX();
	
	/**
	 * @return y
	 */
	public int getY();
	
	/**
	 * @return player makes the move
	 */
	public int getPlayer();
	
	/**
	 * @param other
	 * @return true if<br>
	 * <li> it is itself
	 * <li> the same player and position
	 */
	public boolean equals(Object other);
}
