/**
 * 
 */
package specification;

public interface PlayerSpecification {

	/**
	 * @return name of a player
	 */
	public String getName();

	/**
	 * @return the order of the player (is 1 0r -1)
	 */
	public int getValue();

	/**
	 * @return the move which the player makes
	 */
	public MoveSpecification getMove(BoardSpecification board);

	/**
	 * @return true if the value of a player equal to 1 or -1, else return false
	 */
	public boolean invariant();

	/**
	 * @return a String which describes a player with his name and value.
	 */
	public String toString();

	public void makeMove(MoveSpecification move, BoardSpecification board);
}
