package specification;


/**
 * Reference CSC7322ProjectNguyenVanLuong#rules
 */
public interface RuleSpecification {
	

	/**
	 * @param board
	 * @return a move for player
	 */
	public MoveSpecification getMove(BoardSpecification board);

	/**
	 * set the player who applies this rule
	 */
	public void setOwner(PlayerSpecification owner);
}
