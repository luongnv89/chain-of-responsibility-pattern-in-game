package specification;

import abstraction.PlayerAbstraction;

public interface GameSpecification {

	/**
	 * @return true if the board is full or has someone wins the game.
	 * @see {@link BoardSpecification#isFull()}
	 * @see {@link BoardSpecification#hasWinner()}
	 */
	public boolean gameOver();

	/**
	 * set the first player for the game
	 */
	public void setFirstPlayer(PlayerAbstraction firstPlayer);

	public void gameStart();

	/**
	 * change the turn for the other player. If the current player is A, the next player will be B.
	 */
	public void nextPlayer();

}
