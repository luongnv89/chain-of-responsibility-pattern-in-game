/**
 * 
 */
package specification;

import java.util.ArrayList;

import model.Connected4Board;
import model.Move;
import model.XOBoard;

/**
 * @author mainguyen<br>
 * 
 */
public interface BoardSpecification {

	/**
	 * @return the height of a board
	 */
	public int getHeight();

	/**
	 * @return the width of a board
	 */
	public int getWidth();

	/**
	 * set value for a position on a board
	 * 
	 * @param x
	 * @param y
	 * @param value
	 *            is value is set.
	 */
	public void setState(int x, int y, int value);

	/**
	 * @param x
	 * @param y
	 * @return the value of the position (x, y)
	 */
	public int getState(int x, int y);

	public boolean canMove(MoveSpecification move);

	/**
	 * make a move
	 * @param move is the move want to make<br>
	 * if this move is possible by checking {@link BoardSpecification#canMove(MoveSpecification)},<br>
	 * set the position which the move wants to go to with the wanted value by {@link BoardSpecification#setState(int, int, int)}
	 */
	public void move(MoveSpecification move);

	/**
	 * set the latest move on the position with state value = 0<br>
	 * and remove this move out of the list of history moves
	 */
	public void undoLastMove();

	/**
	 * reset a board back to initial state, this means the value of all positions on board are 0.
	 * 
	 * @see {@link BoardSpecification#initBoard()}
	 */
	public void reset();

	/**
	 * @return true if all positions on the board have value # 0 (1 or -1)
	 */
	public boolean isFull();

	/**
	 * @return true if someone wins the game
	 */
	public boolean hasWinner();

	/**
	 * set all positions on a board empty. It means their values = 0.
	 */
	public void initBoard();

	/**
	 * @return true if the width of board > minimum value of width and the
	 *         height of board > minimum value of height for the specific game
	 */
	public boolean invariant();
	
	/**
	 * @param player
	 * @return all possible moves while playing game.<br>
	 * @see {@link Connected4Board#getPossibleMoves(int)}
	 * @see {@link XOBoard#getPossibleMoves(int)}
	 */
	public ArrayList<Move> getPossibleMoves(int player);

	/**
	 * display a board on screen console.
	 */
	public void view();
}
