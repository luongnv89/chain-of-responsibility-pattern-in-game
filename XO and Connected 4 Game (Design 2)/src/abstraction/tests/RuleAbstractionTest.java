/**
 * 
 */
package abstraction.tests;

import org.junit.After;
import org.junit.Test;

import abstraction.BoardAbstraction;
import abstraction.PlayerAbstraction;
import abstraction.RuleAbstraction;

/**
 * 
 * Test for class {@link RuleAbstraction}
 * 
 */
public abstract class RuleAbstractionTest {

	protected RuleAbstraction rule;
	protected PlayerAbstraction player;
	protected BoardAbstraction board;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		rule = null;
	}

	/**
	 * Test method for
	 * {@link abstraction.RuleAbstraction#setOwner(specification.PlayerSpecification)}
	 * .
	 */
	@Test
	public void testSetOwner() {
		rule.setOwner(player);
	}

	public abstract void setUpRule();

}
