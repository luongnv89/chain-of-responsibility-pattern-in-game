/**
 * 
 */
package abstraction.tests;

import static org.junit.Assert.*;

import model.Move;

import org.junit.After;
import org.junit.Test;

import abstraction.BoardAbstraction;

/**
 * Test for class {@link BoardAbstraction}
 * 
 */
public abstract class BoardAbstractionTest {

	protected int WIDTH;
	protected int HEIGHT;
	protected BoardAbstraction board;
	protected BoardAbstraction board2;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;
	}

	/**
	 * Test method for
	 * {@link abstraction.BoardAbstraction#BoardAbstraction(int, int)}.
	 */
	@Test
	public void testBoardAbstraction() {
		assertTrue(board.getHeight() == HEIGHT);
		assertTrue(board.getWidth() == WIDTH);
		assertTrue(board.getListSavedMoves().isEmpty());
	}

	/**
	 * Test method for
	 * {@link abstraction.BoardAbstraction#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(board.equals(board));
		assertFalse(board.equals(board2));
	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#reset()}.
	 */
	@Test
	public void testReset() {
		assertTrue(board.getState(WIDTH - 1, HEIGHT - 1) == 0);
		board.setState(WIDTH - 1, HEIGHT - 1, 1);
		assertTrue(board.getState(WIDTH - 1, HEIGHT - 1) == 1);
		board.reset();
		assertTrue(board.getState(WIDTH - 1, HEIGHT - 1) == 0);
	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#initBoard()}.
	 */
	@Test
	public void testInitBoard() {
		board.initBoard();
		for (int i = 0; i < board.getWidth(); i++) {
			for (int j = 0; j < board.getHeight(); j++) {
				assertTrue(board.getState(i, j) == 0);
			}
		}
	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#getListSavedMoves()}.
	 */
	@Test
	public void testGetListSavedMoves() {
		assertTrue(board.getListSavedMoves().isEmpty());
		board.move(new Move(-1, WIDTH - 1, HEIGHT - 1));
		assertTrue(board.getListSavedMoves().size() == 1);
	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#getHeight()}.
	 */
	@Test
	public void testGetHeight() {
		assertTrue(board.getHeight() == HEIGHT);
	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#getWidth()}.
	 */
	@Test
	public void testGetWidth() {
		assertTrue(board.getWidth() == WIDTH);
	}

	/**
	 * Test method for
	 * {@link abstraction.BoardAbstraction#setState(int, int, int)}.
	 */
	@Test
	public void testSetState() {
		assertTrue(board.getState(WIDTH - 1, HEIGHT - 1) == 0);
		board.setState(WIDTH - 1, HEIGHT - 1, 1);
		assertTrue(board.getState(WIDTH - 1, HEIGHT - 1) == 1);
	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#getState(int, int)}.
	 */
	@Test
	public void testGetState() {
		assertTrue(board.getState(WIDTH - 1, HEIGHT - 1) == 0);
		board.setState(WIDTH - 1, HEIGHT - 1, 1);
		assertTrue(board.getState(WIDTH - 1, HEIGHT - 1) == 1);
	}


	/**
	 * Test method for {@link abstraction.BoardAbstraction#isFull()}.
	 */
	@Test
	public void testIsFull() {
		assertFalse(board.isFull());
		for (int i = 0; i < board.getWidth(); i++) {
			for (int j = 0; j < board.getHeight(); j++) {
				board.setState(i, j, 1);
			}
		}
		assertTrue(board.isFull());

	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(board.invariant());
		assertFalse(board2.invariant());
	}

	/**
	 * Test method for {@link abstraction.BoardAbstraction#hasWinner()}.
	 */
	@Test
	public abstract void testHasWinnerInColumn();
	
	/**
	 * Test method for {@link abstraction.BoardAbstraction#hasWinner()}.
	 */
	@Test
	public abstract void testHasWinnerOnRow();
	
	
	/**
	 * Test method for {@link abstraction.BoardAbstraction#hasWinner()}.
	 */
	@Test
	public abstract void testHasWinnerOnDiagonalLT_RB();
	
	
	/**
	 * Test method for {@link abstraction.BoardAbstraction#hasWinner()}.
	 */
	@Test
	public abstract void testHasWinnerOnDiagonalLB_RT();

	/**
	 * Test method for {@link abstraction.BoardAbstraction#view()}.
	 */
	@Test
	public void testView() {
		board.view();
		board.setState(WIDTH - 1, HEIGHT - 1, -1);
		board.setState(WIDTH - 2, HEIGHT - 2, 1);
		board.view();
	}

}
