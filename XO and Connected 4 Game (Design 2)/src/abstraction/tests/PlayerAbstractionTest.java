/**
 * 
 */
package abstraction.tests;

import static org.junit.Assert.*;

import model.Move;

import org.junit.After;
import org.junit.Test;

import abstraction.BoardAbstraction;
import abstraction.PlayerAbstraction;

/**
 * Test for class {@link PlayerAbstraction}
 * 
 */
public class PlayerAbstractionTest {
	protected String player1Name = "Player1";
	protected String player2Name = "Player2";
	protected int player1Value = 1;
	protected int player2Value = -1;

	protected PlayerAbstraction player1;
	protected PlayerAbstraction player2;
	protected PlayerAbstraction playerNoInvariant;
	protected BoardAbstraction board;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		player1 = null;
		player2 = null;
	}

	/**
	 * Test method for
	 * {@link abstraction.PlayerAbstraction#PlayerAbstraction(java.lang.String, int)}
	 * .
	 */
	@Test
	public void testPlayerAbstraction() {
		assertTrue(player1.invariant());
		assertTrue(player2.invariant());
	}

	/**
	 * Test method for {@link abstraction.PlayerAbstraction#getName()}.
	 */
	@Test
	public void testGetName() {
		assertTrue(player1.getName().equals(player1Name));
	}

	/**
	 * Test method for {@link abstraction.PlayerAbstraction#getValue()}.
	 */
	@Test
	public void testGetValue() {
		assertTrue(player1.getValue() == player1Value);
		assertTrue(player2.getValue() == player2Value);
	}

	/**
	 * Test method for
	 * {@link abstraction.PlayerAbstraction#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(player1.equals(player1));
		assertFalse(player1.equals(player2));
	}

	/**
	 * Test method for {@link abstraction.PlayerAbstraction#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(player1.invariant());
		assertTrue(player2.invariant());
		assertFalse(playerNoInvariant.invariant());
	}

	/**
	 * Test method for {@link abstraction.PlayerAbstraction#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println("Player1: " + player1.toString());
		System.out.println("Player2: " + player2.toString());
	}

	/**
	 * Test method for
	 * {@link abstraction.PlayerAbstraction#makeMove(specification.MoveSpecification, specification.BoardSpecification)}
	 * .
	 */
	@Test
	public void testMakeMove() {
		board.view();
		assertTrue(board.invariant());
		assertTrue(player1.invariant());
		assertTrue(player2.invariant());
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == 0);
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 2) == 0);
		player1.makeMove(
				new Move(player1Value, board.getWidth() - 1,
						board.getHeight() - 1), board);
		player2.makeMove(
				new Move(player2Value, board.getWidth() - 1,
						board.getHeight() - 2), board);
		board.view();
		assertTrue(board.invariant());
		assertTrue(player1.invariant());
		assertTrue(player2.invariant());
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == player1Value);
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 2) == player2Value);
	}

}
