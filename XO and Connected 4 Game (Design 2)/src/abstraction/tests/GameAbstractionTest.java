/**
 * 
 */
package abstraction.tests;

import static org.junit.Assert.*;

import model.Human;

import org.junit.After;
import org.junit.Test;

import abstraction.BoardAbstraction;
import abstraction.GameAbstraction;
import abstraction.PlayerAbstraction;

/**
 *
 */
public class GameAbstractionTest {
	protected BoardAbstraction board;
	protected PlayerAbstraction player1;
	protected PlayerAbstraction player2;
	protected GameAbstraction game;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;
		player1 = null;
		player2 = null;
		game = null;
	}

	/**
	 * Test method for {@link abstraction.GameAbstraction#GameAbstraction(abstraction.BoardAbstraction, abstraction.PlayerAbstraction, abstraction.PlayerAbstraction)}.
	 */
	@Test
	public void testGameAbstraction() {
		assertFalse(game.gameOver());
	}

	/**
	 * Test method for {@link abstraction.GameAbstraction#gameOver()}.
	 */
	@Test
	public void testGameOver() {
		assertFalse(game.gameOver());
	}

	/**
	 * Test method for {@link abstraction.GameAbstraction#getCurrentPlayer()}.
	 */
	@Test
	public void testGetCurrentPlayer() {
		game.setFirstPlayer(player1);
		assertTrue(game.getCurrentPlayer().equals(player1));
	}

	/**
	 * Test method for {@link abstraction.GameAbstraction#setFirstPlayer(abstraction.PlayerAbstraction)}.
	 */
	@Test
	public void testSetFirstPlayer() {
		game.setFirstPlayer(player1);
		assertTrue(game.getCurrentPlayer().equals(player1));
	}

	/**
	 * Test method for {@link abstraction.GameAbstraction#nextPlayer()}.
	 */
	@Test
	public void testNextPlayer() {
		game.setFirstPlayer(player1);
		assertTrue(game.getCurrentPlayer().equals(player1));
		game.nextPlayer();
		assertTrue(game.getCurrentPlayer().equals(player2));
	}

	public void setGame() {
		player1 = new Human("Human1", 1);
		player2 = new Human("Human2", -1);
		game = new GameAbstraction(board, player1, player2);
	}
}
