/**
 * 
 */
package abstraction;

import specification.GameSpecification;
import specification.MoveSpecification;

/**
 * an abstract game can:
 * <li>initialize a game by creating a board and two players
 * <li>check game over
 * <li>change the player while playing
 * <li>get the current player
 */
public class GameAbstraction implements GameSpecification {

	protected BoardAbstraction board;
	protected PlayerAbstraction playerA;
	protected PlayerAbstraction playerB;
	protected PlayerAbstraction currentPlayer;

	/**
	 * constructor with 3 parameters
	 * 
	 * @param b
	 *            is the board
	 * @param pA
	 *            is player A
	 * @param pB
	 *            is player B
	 */
	public GameAbstraction(BoardAbstraction b, PlayerAbstraction pA,
			PlayerAbstraction pB) {
		board = b;
		playerA = pA;
		playerB = pB;
	}

	public boolean gameOver() {
		return board.isFull() || board.hasWinner();
	}

	/**
	 * @return the current player
	 */
	public PlayerAbstraction getCurrentPlayer() {
		return currentPlayer;
	}

	public void setFirstPlayer(PlayerAbstraction firstPlayer) {
		currentPlayer = firstPlayer;
	}

	/**
	 * <li>always display the board
	 * <li>always check game over
	 * <li>let players make moves
	 * <li>change turn to next player
	 */
	@Override
	public void gameStart() {
		board.view();
		while (!gameOver()) {
			System.out.println(currentPlayer.getName() + " go:");
			MoveSpecification move = currentPlayer.getMove(board);
			currentPlayer.makeMove(move, board);
			board.view();
			nextPlayer();
		}
		if (board.isFull()) {
			System.out.println("Game drawn!");
		} else {
			System.out.println(currentPlayer.getName() + " win!");
		}
	}

	@Override
	public void nextPlayer() {
		if (currentPlayer == playerA) {
			currentPlayer = playerB;
		} else {
			currentPlayer = playerA;
		}
	}
}
