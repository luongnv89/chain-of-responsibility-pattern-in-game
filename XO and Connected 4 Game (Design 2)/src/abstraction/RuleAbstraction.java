/**
 * 
 */
package abstraction;

import model.RuleBlockWin;
import model.RuleRandom;
import model.RuleWin;
import abstraction.tests.RuleAbstractionTest;
import specification.PlayerSpecification;
import specification.RuleSpecification;

/**
 * Reference CSC7322ProjectNguyenVanLuong#rules
 * is extended by {@link RuleWin}, {@link RuleBlockWin}, {@link RuleRandom}
 * Test {@link RuleAbstractionTest}
 */
public abstract class RuleAbstraction implements RuleSpecification {
	
	protected PlayerSpecification owner;
	protected String ruleName;


	public void setOwner(PlayerSpecification owner) {
		this.owner = owner;
	}

	/**
	 * @return the name of the rule
	 */
	public String getRuleName() {
		return ruleName;
	}
	
	
}
