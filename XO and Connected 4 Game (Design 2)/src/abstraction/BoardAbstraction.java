/**
 * 
 */
package abstraction;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import specification.BoardSpecification;
import specification.MoveSpecification;

/**
 * an abstract board for games.
 * 
 */
public abstract class BoardAbstraction implements BoardSpecification {

	/**
	 * list of all history moves.
	 */
	protected List<MoveSpecification> listSavedMoves;
	
	/**
	 * number of connected pieces to win:
	 * <li> XO game: 3 pieces
	 * <li> Connected 4: 4 pieces
	 */
	protected int nbOfConnectedPiecesToWin;
	
	/**
	 * the minimum value of width and height of a board:
	 * <li> XO game: 3 
	 * <li> Connected 4: 4 
	 */
	protected int minHeight;
	protected int minWidth;

	/**
	 * the value of width and height of a board
	 */
	private int height;
	private int width;

	/**
	 * current state of all positions on a board
	 */
	private int[][] listStates;

	/**
	 * constructor default with 2 integers as parameters
	 * 
	 * @param w
	 *            is the width of a board
	 * @param h
	 *            is the height of a board
	 */
	public BoardAbstraction(int h, int w) {
		height = h;
		width = w;
		listSavedMoves = new LinkedList<>();
		listStates = new int[width][height];
		initBoard();
	}

	/**
	 * return true if,<br>
	 * <li>it's itself <li>two board have the same height, width and all states
	 * on boards are the values
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		BoardAbstraction other = (BoardAbstraction) obj;
		if (height != other.height)
			return false;
		if (!Arrays.deepEquals(listStates, other.listStates))
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	public void reset() {
		initBoard();
		listSavedMoves.clear();
	}

	public void initBoard() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++)
				listStates[i][j] = 0;
		}
	}

	/**
	 * @return the listSavedMoves
	 */
	public List<MoveSpecification> getListSavedMoves() {
		return listSavedMoves;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public void setState(int x, int y, int value) {
		listStates[x][y] = value;
	}

	public int getState(int x, int y) {
		return listStates[x][y];
	}


	public boolean isFull() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (getState(i, j) == 0)
					return false;
			}
		}
		return true;
	}

	public boolean invariant() {
		return (this.getHeight() >= minHeight) && (this.getWidth() >= minWidth);
	}

	/**
	 * someone wins the game when there is one direction which he has {@code nbOfConnectedPiecesToWin} pieces:
	 * <li> in a column
	 * <li> in a row
	 * <li> in a diagonal from Left-Top to Right-Bottom
	 * <li> in a diagonal from Left-Bottom to Right-Top 
	 */
	public boolean hasWinner() {
		if (checkWinColumns())
			return true;
		else if (checkWinRows())
			return true;
		else if (checkWinDiagonalLT_RB())
			return true;
		else if (checkWinDiagonalLB_RT())
			return true;
		return false;
	}

	/**
	 * Direction: <br>
	 * check from Left-Bottom to Right-Top whether has {@code nbOfConnectedPiecesToWin} pieces of one player
	 * @return true if there is one meets the above requirement.
	 * @see {@link BoardAbstraction#winDiagonalLB_RT(int, int)}
	 */
	private boolean checkWinDiagonalLB_RT() {
		for (int i = nbOfConnectedPiecesToWin - 1; i < getWidth(); i++) {
			if (winDiagonalLB_RT(i, 0))
				return true;
		}

		for (int i = 0; i < getHeight() - nbOfConnectedPiecesToWin + 1; i++) {
			if (winDiagonalLB_RT(getWidth() - 1, i))
				return true;
		}
		return false;
	}

	/**
	 * check from Left-Bottom to Right-Top
	 * @param x is the column
	 * @param y is the row
	 * @return true if the number of adjacent pieces of a player = {@code nbOfConnectedPiecesToWin}
	 */
	private boolean winDiagonalLB_RT(int x, int y) {
		int total = 0;
		int i = 1;
		while ((x - i) >= 0 && (y + i) < getHeight()) {
			if (getState(x - i, y + i) != 0
					&& getState(x - i + 1, y + i - 1) == getState(x - i, y + i))
				total += 1;
			else
				total = 0;
			if (total == nbOfConnectedPiecesToWin - 1)
				return true;
			i++;
		}
		return false;
	}

	/**
	 * Direction: <br>
	 * check from Left-Top to Right-Bottom whether has {@code nbOfConnectedPiecesToWin} pieces of one player
	 * @return true if there is one meets the above requirement.
	 * @see {@link BoardAbstraction#winDiagonalLT_RB(int, int)}
	 */
	private boolean checkWinDiagonalLT_RB() {
		for (int i = 0; i < getWidth(); i++) {
			if (winDiagonalLT_RB(i, 0))
				return true;
		}

		for (int i = 0; i < getHeight(); i++) {
			if (winDiagonalLT_RB(0, i))
				return true;
		}
		return false;
	}

	/**
	 * check from Left-Top to Right-Bottom
	 * @param x is the column
	 * @param y is the row
	 * @return true if the number of adjacent pieces of a player = {@code nbOfConnectedPiecesToWin}
	 */
	private boolean winDiagonalLT_RB(int x, int y) {
		int total = 0;
		int i = 1;
		while ((x + i) < getWidth() && (y + i) < getHeight()) {
			if (getState(x + i, y + i) != 0
					&& getState(x + i, y + i) == getState(x + i - 1, y + i - 1))
				total += 1;
			else
				total = 0;
			if (total == nbOfConnectedPiecesToWin - 1)
				return true;
			i++;
		}
		return false;
	}

	/**
	 * Direction: <br>
	 * traverse all row and 
	 * check each row whether has the number of adjacent pieces of the player = {@code nbOfConnectedPiecesToWin} 
	 * @return true if find one row contains winner
	 * @see {@link BoardAbstraction#winRow(int)}
	 */
	private boolean checkWinRows() {
		for (int i = 0; i < getHeight(); i++) {
			if (winRow(i))
				return true;
		}
		return false;
	}

	/**
	 * @param x is a row on a board
	 * @return true if the number of adjacent pieces of a player = {@code nbOfConnectedPiecesToWin}
	 */
	private boolean winRow(int y) {
		int total = 0;
		for (int i = 1; i < getWidth(); i++) {
			if (getState(i, y) != 0 && getState(i, y) == getState(i - 1, y))
				total += 1;
			else
				total = 0;
			if (total == nbOfConnectedPiecesToWin - 1)
				return true;
		}
		return false;
	}

	/**
	 * Direction: <br>
	 * traverse all columns and 
	 * check each column whether has the number of adjacent pieces of the player = {@code nbOfConnectedPiecesToWin} 
	 * @return true if find one column contains winner
	 * @see {@link BoardAbstraction#winColumn(int)}
	 */
	private boolean checkWinColumns() {
		for (int i = 0; i < getWidth(); i++) {
			if (winColumn(i))
				return true;
		}
		return false;
	}

	/**
	 * @param x is a column on a board
	 * @return true if the number of adjacent pieces of a player = {@code nbOfConnectedPiecesToWin}
	 */
	private boolean winColumn(int x) {
		int total = 0;
		for (int i = 0; i < getHeight() - 1; i++) {
			if (getState(x, i) != 0 && getState(x, i) == getState(x, i + 1))
				total += 1;
			else
				total = 0;
			if (total == nbOfConnectedPiecesToWin-1)
				return true;
		}
		return false;
	}

	public void view() {
		for (int i = -1; i < this.getHeight(); i++) {
			if (i == -1) {
				for (int j = -1; j < this.getWidth(); j++) {
					if (j != -1)
						System.out.print(j + " ");
					else
						System.out.print(j + "  ");
				}
				System.out.println();
			} else {
				for (int j = -1; j < this.getWidth(); j++) {

					if (j == -1) {
						if (i != 10)
							System.out.print(i + "   ");
						else {
							System.out.print(i + "  ");
						}

					} else {
						int state = this.getState(j, i);
						switch (state) {
						case 0:
							System.out.print("-" + " ");
							break;
						case 1:
							System.out.print("x" + " ");
							break;
						case -1:
							System.out.print("0" + " ");
							break;
						}
					}
				}
			}
			System.out.println();
		}
	}

	
}
