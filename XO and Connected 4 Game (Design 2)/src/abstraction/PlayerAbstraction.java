package abstraction;

import model.Connected4Board;
import model.XOBoard;
import specification.BoardSpecification;
import specification.MoveSpecification;
import specification.PlayerSpecification;

/**
 *         an abstract player has name and value which represent for him.
 *         {@code PlayerAbstraction#value} is 1 or -1 to distinct player A and
 *         player B on the board.
 */
public abstract class PlayerAbstraction implements PlayerSpecification {

	/**
	 * name of player
	 */
	protected String name;
	/**
	 * value of player (1 or -1)
	 */
	protected int value;

	/**
	 * default constructor player
	 * 
	 * @param n
	 *            is name of player
	 * @param or
	 *            is value of player (1 or -1)
	 */
	public PlayerAbstraction(String n, int val) {
		name = n;
		value = val;
	} 

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerAbstraction other = (PlayerAbstraction) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value != other.value)
			return false;
		return true;
	}

	public boolean invariant() {
		return (this.getValue() == 1 || this.getValue() == -1);
	}

	public String toString() {
		String str = "";
		str = str + "Player's name: " + this.getName() + " and his value is : "
				+ this.getValue();
		return str;
	}

	/**
	 *  player can make a move on a board need to:
	 *  <li>check on the board the move is possible or not {@link BoardAbstraction#canMove(MoveSpecification)}
	 *  <li>let make a move on the specific board ({@link XOBoard} or {@link Connected4Board})
	 *  @see XOBoard#move(MoveSpecification)
	 *  @see Connected4Board#move(MoveSpecification)
	 */
	public void makeMove(MoveSpecification move, BoardSpecification board) {
		if (board.canMove(move)) {
			board.move(move);
		} else {
			System.out.println("Player cannot make move");
		}
	}
}
