/**
 * 
 */
package model.connected4.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import model.Connected4Board;
import model.Move;

import org.junit.Before;
import org.junit.Test;

import abstraction.tests.BoardAbstractionTest;

/**
 * 
 * Test for {@link Connected4Board}
 * 
 */
public class Connected4BoardTest extends BoardAbstractionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		WIDTH = 9;
		HEIGHT = 9;
		board = new Connected4Board(WIDTH, HEIGHT);
		board2 = new Connected4Board(3, 3);
	}

	/**
	 * Test method for {@link model.Connected4Board#Connected4Board(int, int)}.
	 */
	@Test
	public void testConnected4Board() {
		assertTrue(board.invariant());
	}

	/**
	 * Test method for
	 * {@link model.Connected4Board#move(specification.MoveSpecification)}.
	 */
	@Test
	public void testMove() {
		assertTrue(board.getState(0, HEIGHT - 1) == 0);
		board.move(new Move(1, 0, 0));
		assertTrue(board.invariant());
		assertTrue(board.getState(0, HEIGHT - 1) == 1);
		board.move(new Move(1, 0, 0));
		assertTrue(board.invariant());
		assertTrue(board.getState(0, HEIGHT - 2) == 1);
	}

	/**
	 * Test method for
	 * {@link model.Connected4Board#canMove(specification.MoveSpecification)}.
	 */
	@Test
	public void testCanMove() {
		assertTrue(board.canMove(new Move(-1, 0, 0)));
		for (int i = 0; i < board.getHeight(); i++) {
			board.move(new Move(1, 0, 0));
		}
		assertFalse(board.canMove(new Move(1, 0, 0)));
	}

	/**
	 * Test method for {@link model.Connected4Board#undoLastMove()}.
	 */
	@Test
	public void testUndoLastMove() {
		assertTrue(board.getListSavedMoves().isEmpty());
		board.move(new Move(1, 0, 0));
		board.move(new Move(1, 0, 0));
		assertTrue(board.invariant());
		assertTrue(board.getListSavedMoves().size() == 2);
		assertTrue(board.getState(0, HEIGHT - 2) == 1);
		board.undoLastMove();
		board.view();
		assertTrue(board.invariant());
		assertTrue(board.getListSavedMoves().size() == 1);
		assertTrue(board.getState(0, HEIGHT - 2) == 0);

	}


	@Override
	@Test
	public void testHasWinnerInColumn() {
		assertTrue(board.invariant());
		assertFalse(board.hasWinner());
		board.move(new Move(1, 0, 0));
		board.move(new Move(1, 0, 0));
		board.move(new Move(1, 0, 0));
		board.move(new Move(1, 0, 0));
		board.view();
		assertTrue(board.invariant());
		assertTrue(board.hasWinner());

	}

	@Override
	@Test
	public void testHasWinnerOnRow() {
		assertTrue(board.invariant());
		assertFalse(board.hasWinner());
		board.move(new Move(1, 0, 0));
		board.move(new Move(1, 1, 0));
		board.move(new Move(1, 2, 0));
		board.move(new Move(1, 3, 0));
		board.view();
		assertTrue(board.invariant());
		assertTrue(board.hasWinner());

	}

	@Override
	@Test
	public void testHasWinnerOnDiagonalLT_RB() {
		assertTrue(board.invariant());
		assertFalse(board.hasWinner());

		board.move(new Move(-1, 0, 0));
		board.move(new Move(-1, 1, 0));
		board.move(new Move(1, 2, 0));
		board.move(new Move(1, 3, 0));

		board.move(new Move(1, 0, 0));
		board.move(new Move(-1, 1, 0));
		board.move(new Move(1, 2, 0));

		board.move(new Move(-1, 0, 0));
		board.move(new Move(1, 1, 0));

		board.move(new Move(1, 0, 0));

		board.view();
		assertTrue(board.invariant());
		assertTrue(board.hasWinner());

	}

	@Override
	@Test
	public void testHasWinnerOnDiagonalLB_RT() {

		assertTrue(board.invariant());
		assertFalse(board.hasWinner());

		board.move(new Move(1, 0, 0));
		board.move(new Move(-1, 1, 0));
		board.move(new Move(1, 2, 0));
		board.move(new Move(1, 3, 0));

		board.move(new Move(1, 1, 0));
		board.move(new Move(-1, 2, 0));
		board.move(new Move(1, 3, 0));

		board.move(new Move(1, 2, 0));
		board.move(new Move(-1, 3, 0));

		board.move(new Move(1, 3, 0));

		board.view();
		assertTrue(board.invariant());
		assertTrue(board.hasWinner());
	}

}
