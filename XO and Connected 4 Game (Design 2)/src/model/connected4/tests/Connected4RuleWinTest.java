/**
 * 
 */
package model.connected4.tests;

import static org.junit.Assert.assertNotNull;
import model.Connected4Board;
import model.tests.RuleWinAbstractTest;

import org.junit.Before;
import org.junit.Test;

import specification.MoveSpecification;

/**
 * 
 *
 */
public class Connected4RuleWinTest extends RuleWinAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connected4Board(9, 9);
		setUpRule();
	}

	@Override
	@Test
	public void testGetMove() {
		board.setState(0, board.getHeight() - 1, -1);
		board.setState(0, board.getHeight() - 2, -1);
		board.setState(0, board.getHeight() - 3, -1);
		MoveSpecification move = rule.getMove(board);
		assertNotNull(move);
		System.out.println(move.toString());
	}

}
