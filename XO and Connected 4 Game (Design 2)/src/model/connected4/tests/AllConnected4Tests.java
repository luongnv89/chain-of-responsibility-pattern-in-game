package model.connected4.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Connected4AIRuleManagerTest.class, Connected4AITest.class,
		Connected4BoardTest.class, Connected4GameTest.class,
		Connected4HumanTest.class, Connected4RuleBlockWinTest.class,
		Connected4RuleRandomTest.class, Connected4RuleWinTest.class })
public class AllConnected4Tests {

}
