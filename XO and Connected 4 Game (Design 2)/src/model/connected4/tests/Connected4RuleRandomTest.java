/**
 * 
 */
package model.connected4.tests;

import model.Connected4Board;
import model.tests.RuleRandomAbstractTest;

import org.junit.Before;

/**
 * 
 *
 */
public class Connected4RuleRandomTest extends RuleRandomAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connected4Board(9, 9);
		setUpRule();
	}

}
