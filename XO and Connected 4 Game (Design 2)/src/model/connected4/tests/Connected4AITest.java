/**
 * 
 */
package model.connected4.tests;

import model.AI;
import model.Connected4Board;
import model.tests.AIAbstractTest;

import org.junit.Before;

/**
 * Test for class {@link AI} with {@link Connected4Board}
 * 
 * 
 */
public class Connected4AITest extends AIAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connected4Board(9, 9);
		setUpPlayer();
	}
}
