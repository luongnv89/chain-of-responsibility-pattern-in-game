/**
 * 
 */
package model.connected4.tests;

import model.Connected4Board;

import org.junit.Before;

import abstraction.tests.GameAbstractionTest;

/**
 *
 */
public class Connected4GameTest extends GameAbstractionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connected4Board(9, 9);
		setGame();
	}

}
