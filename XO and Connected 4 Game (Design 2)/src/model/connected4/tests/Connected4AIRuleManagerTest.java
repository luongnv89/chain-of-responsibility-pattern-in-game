/**
 * 
 */
package model.connected4.tests;

import model.Connected4Board;
import model.tests.AIRuleManagerAbstractTest;

import org.junit.Before;

/**
 * 
 *
 */
public class Connected4AIRuleManagerTest extends AIRuleManagerAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connected4Board(9, 9);
		setUpRule();
	}
}
