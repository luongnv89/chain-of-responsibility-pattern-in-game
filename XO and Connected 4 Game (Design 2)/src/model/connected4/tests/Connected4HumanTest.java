/**
 * 
 */
package model.connected4.tests;

import model.Connected4Board;
import model.Human;
import model.tests.HumanAbstractTest;

import org.junit.Before;

/**
 * 
 * Test for class {@link Human} with {@link Connected4Board}
 * 
 */
public class Connected4HumanTest extends HumanAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connected4Board(9, 9);
		setUpPlayer();
	}

}
