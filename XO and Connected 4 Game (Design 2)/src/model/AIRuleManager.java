/**
 * 
 */
package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import model.tests.AIRuleManagerAbstractTest;
import specification.BoardSpecification;
import specification.MoveSpecification;
import specification.PlayerSpecification;
import specification.RuleSpecification;
import abstraction.RuleAbstraction;

/**
 * Reference CSC7322ProjectNguyenVanLuong#rules.<br>
 * Test {@link AIRuleManagerAbstractTest}.
 * There is the list of rules<br>
 * To define the way to play, AI player uses rules by ordering them. This order is the adding order of rules:
 * <li>{@link AIRuleManager#createRuleManual()}
 * <li>{@link AIRuleManager#createRuleRandom()}
 * <li>{@link AIRuleManager#createRulesFromFile(String)}
 */
public class AIRuleManager extends RuleAbstraction {
	List<RuleSpecification> listUseRules;
	List<RuleSpecification> listAllRules;

	public AIRuleManager() {
		ruleName = "Manager";
		listUseRules = new LinkedList<RuleSpecification>();
		listAllRules = new LinkedList<RuleSpecification>();
		listAllRules.addAll(getAllPossibleRules());
	}

	/**
	 * collect all available rules
	 * @return all available rules
	 */
	private Collection<? extends RuleSpecification> getAllPossibleRules() {
		List<RuleAbstraction> allRules = new LinkedList<RuleAbstraction>();
		allRules.add(new RuleWin());
		allRules.add(new RuleBlockWin());
		allRules.add(new RuleRandom());
		return allRules;
	}

	@Override
	public MoveSpecification getMove(BoardSpecification board) {
		for (int i = 0; i < listUseRules.size(); i++) {
			MoveSpecification move = listUseRules.get(i).getMove(board);
			if (move != null)
				return move;
		}
		return null;
	}

	/**
	 * create order of rules to play in a file. 
	 * Then AI applies rules by reading this file (use {@link AIRuleManager#findRuleByString(String)})
	 * @param path is the path of rules file
	 */
	public void createRulesFromFile(String path) {
		listUseRules.clear();
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(path));
			String ruleString = buffer.readLine();
			while (ruleString != null) {
				RuleSpecification rule = findRuleByString(ruleString);
				if (rule != null)
					listUseRules.add(rule);
				else
					System.out.println("Cannot find the rule with name : "
							+ ruleString);
				ruleString = buffer.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param ruleString is the name of rule
	 * @return a rule if the input string matches to the name of rule.<br>
	 * else, return null.
	 */
	private RuleSpecification findRuleByString(String ruleString) {
		for (int i = 0; i < listAllRules.size(); i++) {
			if (((RuleAbstraction) listAllRules.get(i)).getRuleName().equals(
					ruleString))
				return listAllRules.get(i);
		}
		return null;
	}

	/**
	 * create order of rules randomly
	 */
	public void createRuleRandom() {
		listUseRules.clear();
		Random ran = new Random();
		int nbOfRule = ran.nextInt(listAllRules.size());
		if (nbOfRule == 0)
			nbOfRule = 1;
		boolean hasRandom = false;
		for (int i = 0; i < nbOfRule; i++) {
			int indexOfRule = ran.nextInt(listAllRules.size());
			RuleSpecification rule = listAllRules.get(indexOfRule);
			if (!listUseRules.contains(rule))
				listUseRules.add(rule);
			if (indexOfRule == listAllRules.size() - 1)
				hasRandom = true;
		}
		if (!hasRandom) {
			listUseRules.add(listAllRules.get(listAllRules.size() - 1));
		}

	}

	/**
	 * create order of rules from the keyboard
	 */
	public void createRuleManual() {
		listUseRules.clear();
		Scanner input = new Scanner(System.in);

		showAllRules();
		System.out.println("Number of rules (>0 and <=" + listAllRules.size()
				+ " ): ");
		int nbOfRule = input.nextInt();
		while (nbOfRule <= 0 || nbOfRule > listAllRules.size()) {
			System.out
					.println("Number of rules must be bigger than 0 and smaller than "
							+ listAllRules.size());
			System.out.println("Input again: ");
			nbOfRule = input.nextInt();
		}

		for (int i = 0; i < nbOfRule; i++) {
			System.out.println("Index of rule number " + i);
			int indexOfRule = input.nextInt();
			while (indexOfRule < 0 || indexOfRule > listAllRules.size() - 1) {
				System.out
						.println("Number of rules must be bigger than or equals 0 and smaller than "
								+ (listAllRules.size() - 1));
				System.out.println("Input again: ");
				indexOfRule = input.nextInt();
				RuleSpecification rule = listAllRules.get(indexOfRule);
				if (listUseRules.contains(rule)) {
					System.out.println("Rule already exists. Index: "
							+ indexOfRule);
					System.out.println("Input another index: ");
					indexOfRule = -1;
				} else {
					listUseRules.add(rule);
				}
			}

		}
	}

	/**
	 * show all rules of this AI player
	 */
	public void showAllRules() {
		for (int i = 0; i < listAllRules.size(); i++) {
			System.out.println("[ " + i + "] "
					+ ((RuleAbstraction) listAllRules.get(i)).getRuleName());
		}

	}

	/**
	 * show order of rules which is applied
	 */
	public void showUseRules() {
		for (int i = 0; i < listUseRules.size(); i++) {
			System.out.println("[ " + i + "] "
					+ ((RuleAbstraction) listUseRules.get(i)).getRuleName());
		}

	}

	@Override
	public void setOwner(PlayerSpecification owner) {
		for (int i = 0; i < listAllRules.size(); i++) {
			listAllRules.get(i).setOwner(owner);
		}
	}
}
