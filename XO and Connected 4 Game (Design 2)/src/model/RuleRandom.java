/**
 * 
 */
package model;

import java.util.ArrayList;
import java.util.Random;

import model.connected4.tests.Connected4RuleRandomTest;
import model.xo.tests.XORuleRandomTest;

import specification.BoardSpecification;
import specification.MoveSpecification;
import abstraction.BoardAbstraction;
import abstraction.RuleAbstraction;

/**
 * Reference CSC7322ProjectNguyenVanLuong#rules
 * Test:
 * <li> {@link Connected4RuleRandomTest}
 * <li> {@link XORuleRandomTest}
 */
public class RuleRandom extends RuleAbstraction {

	public RuleRandom() {
		ruleName = "Choose one of the remaining free positions randomly";
	}


	/** 
	 * chose a move from the list of possible moves randomly
	 */
	@Override
	public MoveSpecification getMove(BoardSpecification board) {
		ArrayList<Move> listPossibleMoves = ((BoardAbstraction) board)
				.getPossibleMoves(owner.getValue());
		if (listPossibleMoves.isEmpty())
			return null;
		Random ran = new Random();
		int indexOfMove = ran.nextInt(listPossibleMoves.size());
		return listPossibleMoves.get(indexOfMove);
	}

}
