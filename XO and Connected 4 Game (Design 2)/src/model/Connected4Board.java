package model;

import java.util.ArrayList;

import specification.BoardSpecification;
import specification.MoveSpecification;
import abstraction.BoardAbstraction;

public class Connected4Board extends BoardAbstraction implements
		BoardSpecification {

	/**
	 * @param h is the height
	 * @param w is the width 
	 *<li> set {@code nbOfConnectedPiecesToWin} = 4
	 *<li> set {@code minHeight} = 4
	 *<li> set {@code minWidth} = 4
	 */
	public Connected4Board(int w, int h) {
		super(h, w);
		this.nbOfConnectedPiecesToWin = 4;
		this.minHeight = 4;
		this.minWidth = 4;
	}

	public void move(MoveSpecification move) {
		if (canMove(move)) {
			updateColumn(move.getPlayer(), move.getX());
			listSavedMoves.add(move);
		}
	}

	/**
	 * Update state of a column by setting a value on a column equals to {@code value}<br>
	 * use when make a move
	 * @param value is the value of the player who makes this move
	 * @param column where make the move
	 * @see Connected4Board#move(MoveSpecification)
	 */
	private void updateColumn(int value, int column) {
		for (int i = getHeight() - 1; i >= 0; i--) {
			if (getState(column, i) == 0) {
				setState(column, i, value);
				break;
			}
		}

	}

	/**
	 * Update state of a column by setting a value on a column equals to {@code 0}<br>
	 * use when undo the last move
	 * @param column where the move was made
	 * @see Connected4Board#undoLastMove()
	 */
	private void undoColumn(int column) {
		for (int i = getHeight() - 1; i >= 0; i--) {
			if (getState(column, i) == 0) {
				setState(column, i + 1, 0);
				break;
			}
		}

	}

	/**
	 * check a move is possible or not?<br>
	 * get the column where the move is made and check whether this column is full or not?
	 * the full column is all pieces on the column have values != 0
	 */
	@Override
	public boolean canMove(MoveSpecification move) {
		for (int i = getHeight() - 1; i >= 0; i--) {
			if (getState(move.getY(), i) == 0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void undoLastMove() {
		if (!listSavedMoves.isEmpty()) {
			int lastMoveIndex = listSavedMoves.size() - 1;
			MoveSpecification lastMove = listSavedMoves.get(lastMoveIndex);
			undoColumn(lastMove.getX());
			listSavedMoves.remove(lastMoveIndex);
		}

	}

	/**
	 * get all column is not full
	 * @see Connected4Board#isFullColumn(int)
	 */
	@Override
	public ArrayList<Move> getPossibleMoves(int player) {
		ArrayList<Move> listMoves = new ArrayList<Move>();
		for (int i = 0; i < getWidth(); i++) {
			if (!isFullColumn(i))
				listMoves.add(new Move(player, i, 0));
		}
		return listMoves;
	}

	/**
	 * check a column is full or not?
	 * @param column
	 * @return true if all pieces on the column have values != 0
	 */
	private boolean isFullColumn(int column) {
		for (int i = 0; i < getHeight(); i++) {
			if (getState(column, i) == 0)
				return false;
		}
		return true;
	}

}
