package model;

import java.util.ArrayList;

import specification.BoardSpecification;
import specification.MoveSpecification;
import abstraction.BoardAbstraction;

public class XOBoard extends BoardAbstraction implements BoardSpecification {

	/**
	 * @param h is the height
	 * @param w is the width 
	 *<li> set {@code nbOfConnectedPiecesToWin} = 3
	 *<li> set {@code minHeight} = 3
	 *<li> set {@code minWidth} = 3
	 */
	public XOBoard(int h, int w) {
		super(h, w);
		this.nbOfConnectedPiecesToWin = 3;
		this.minHeight = 3;
		this.minWidth = 3;
	}

	public void move(MoveSpecification move) {
		if (canMove(move)) {
			setState(move.getX(), move.getY(), move.getPlayer());
			listSavedMoves.add(move);
		}
	}

	/**
	 * check a move is possible or not?<br>
	 * get the position where the move is made and check whether this position is empty or not?
	 * a position is empty if its value = 0
	 */
	@Override
	public boolean canMove(MoveSpecification move) {
		if (move.getX() >= 0 && move.getX() < getWidth() && move.getY() >= 0
				&& move.getY() < getHeight()
				&& getState(move.getX(), move.getY()) == 0)
			return true;
		return false;
	}

	public void undoLastMove() {
		if (!listSavedMoves.isEmpty()) {
			int lastMoveIndex = listSavedMoves.size() - 1;
			MoveSpecification lastMove = listSavedMoves.get(lastMoveIndex);
			setState(lastMove.getX(), lastMove.getY(), 0);
			listSavedMoves.remove(lastMoveIndex);
		}
	}

	/**
	 * get all positions have values = 0
	 */
	@Override
	public ArrayList<Move> getPossibleMoves(int player) {
		ArrayList<Move> listMoves = new ArrayList<Move>();
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				if (getState(i, j) == 0)
					listMoves.add(new Move(player, i, j));
			}
		}
		return listMoves;
	}

}
