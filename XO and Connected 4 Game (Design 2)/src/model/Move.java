package model;

import specification.MoveSpecification;

public class Move implements MoveSpecification {

	/**
	 * who makes the move
	 */
	protected int player;

	protected int x;
	protected int y;

	public Move(int player, int x, int y) {
		super();
		this.player = player;
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getPlayer() {
		return player;
	}

	
	public String toString() {
		return "Move [player=" + player + ", x=" + x + ", y=" + y + "]";
	}

	
}
