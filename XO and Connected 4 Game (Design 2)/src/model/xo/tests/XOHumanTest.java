package model.xo.tests;

import model.Human;
import model.XOBoard;
import model.tests.HumanAbstractTest;

import org.junit.Before;

/**
 * 
 * Test for class {@link Human} with {@link XOBoard}
 * 
 */
public class XOHumanTest extends HumanAbstractTest {

	@Before
	public void setUp() throws Exception {
		board = new XOBoard(6, 6);
		setUpPlayer();
	}

}
