/**
 * 
 */
package model.xo.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import model.Move;
import model.XOBoard;

import org.junit.Before;
import org.junit.Test;

import abstraction.tests.BoardAbstractionTest;

/**
 * Test for class {@link XOBoard}
 * 
 * 
 */
public class XOBoardTest extends BoardAbstractionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		WIDTH = 5;
		HEIGHT = 5;
		board = new XOBoard(WIDTH, HEIGHT);
		board2 = new XOBoard(2, 2);
	}

	/**
	 * Test method for {@link model.XOBoard#XOBoard(int, int)}.
	 */
	@Test
	public void testXOBoard() {
		assertTrue(board.invariant());
	}

	/**
	 * Test method for
	 * {@link model.XOBoard#move(specification.MoveSpecification)}.
	 */
	@Test
	public void testMove() {
		assertTrue(board.invariant());
		board.view();
		board.move(new Move(1, 0, 0));
		assertTrue(board.invariant());
		board.view();
		board.move(new Move(-1, 2, 2));
		assertTrue(board.invariant());
		board.view();
	}

	/**
	 * Test method for
	 * {@link model.XOBoard#canMove(specification.MoveSpecification)}.
	 */
	@Test
	public void testCanMove() {
		assertTrue(board.invariant());
		board.view();
		assertTrue(board.canMove(new Move(1, 0, 0)));
		assertFalse(board.canMove(new Move(1, WIDTH, HEIGHT)));
		assertFalse(board.canMove(new Move(1, -1, 0)));
		assertFalse(board.canMove(new Move(1, 0, -1)));
	}

	/**
	 * Test method for {@link model.XOBoard#undoLastMove()}.
	 */
	@Test
	public void testUndoLastMove() {
		assertTrue(board.invariant());
		board.view();
		assertTrue(board.getState(0, 0) == 0);
		board.move(new Move(1, 0, 0));
		assertTrue(board.invariant());
		board.view();
		assertTrue(board.getState(0, 0) == 1);
		assertTrue(board.getState(2, 2) == 0);
		board.move(new Move(-1, 2, 2));
		assertTrue(board.invariant());
		board.view();
		assertTrue(board.getState(2, 2) == -1);
		board.undoLastMove();
		assertTrue(board.invariant());
		board.view();
		assertTrue(board.getState(2, 2) == 0);

	}


	@Override
	@Test
	public void testHasWinnerInColumn() {
		assertFalse(board.hasWinner());
		board.move(new Move(1, 0, 0));
		board.move(new Move(1, 0, 1));
		board.move(new Move(1, 0, 2));
		board.view();
		assertTrue(board.hasWinner());

	}

	@Override
	@Test
	public void testHasWinnerOnRow() {
		assertFalse(board.hasWinner());
		board.move(new Move(-1, 0, 0));
		board.move(new Move(-1, 1, 0));
		board.move(new Move(-1, 2, 0));
		board.view();
		assertTrue(board.hasWinner());
	}

	@Override
	@Test
	public void testHasWinnerOnDiagonalLT_RB() {
		assertFalse(board.hasWinner());
		board.move(new Move(-1, 0, 0));
		board.move(new Move(-1, 1, 1));
		board.move(new Move(-1, 2, 2));
		board.view();
		assertTrue(board.hasWinner());
	}

	@Override
	@Test
	public void testHasWinnerOnDiagonalLB_RT() {
		assertFalse(board.hasWinner());
		board.move(new Move(-1, 0, 2));
		board.move(new Move(-1, 1, 1));
		board.move(new Move(-1, 2, 0));
		board.view();
		assertTrue(board.hasWinner());

	}

}
