/**
 * 
 */
package model.xo.tests;

import model.AI;
import model.XOBoard;
import model.tests.AIAbstractTest;

import org.junit.Before;

/**
 * 
 * Test for class {@link AI} with {@link XOBoard}
 * 
 */
public class XOAITest extends AIAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard(5, 5);
		setUpPlayer();
	}

}
