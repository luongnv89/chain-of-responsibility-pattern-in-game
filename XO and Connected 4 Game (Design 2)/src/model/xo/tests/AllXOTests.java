package model.xo.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ XOAIRuleManagerTest.class, XOAITest.class, XOBoardTest.class,
		XOGameTest.class, XOHumanTest.class, XORuleBlockWinTest.class,
		XORuleRandomTest.class, XORuleWinTest.class })
public class AllXOTests {

}
