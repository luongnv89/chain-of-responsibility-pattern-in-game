/**
 * 
 */
package model.xo.tests;

import model.XOBoard;

import org.junit.Before;

import abstraction.tests.GameAbstractionTest;

/**
 *
 */
public class XOGameTest extends GameAbstractionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard(5, 5);
		setGame();
	}

}
