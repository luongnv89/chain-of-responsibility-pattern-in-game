/**
 * 
 */
package model.xo.tests;

import static org.junit.Assert.*;

import model.XOBoard;
import model.tests.RuleBlockWinAbstractTest;

import org.junit.Before;
import org.junit.Test;

import specification.MoveSpecification;

/**
 * 
 *
 */
public class XORuleBlockWinTest extends RuleBlockWinAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard(5, 5);
		setUpRule();
	}
 
	@Override
	@Test
	public void testGetMove() {
		board.setState(0, 0, 1);
		board.setState(0, 1, 1);
		MoveSpecification move = rule.getMove(board);
		assertNotNull(move);
		System.out.println(move.toString());

	}

}
