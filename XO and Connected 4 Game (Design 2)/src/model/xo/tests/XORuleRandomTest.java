/**
 * 
 */
package model.xo.tests;

import model.XOBoard;
import model.tests.RuleRandomAbstractTest;

import org.junit.Before;

/**
 * 
 *
 */
public class XORuleRandomTest extends RuleRandomAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard(5, 5);
		setUpRule();
	}

}
