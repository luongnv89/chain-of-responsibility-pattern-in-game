/**
 * 
 */
package model.xo.tests;

import model.XOBoard;
import model.tests.AIRuleManagerAbstractTest;

import org.junit.Before;

/**
 * 
 *
 */
public class XOAIRuleManagerTest extends AIRuleManagerAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard(5, 5);
		setUpRule();
	}
}
