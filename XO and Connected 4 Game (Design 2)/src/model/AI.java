package model;

import specification.BoardSpecification;
import specification.MoveSpecification;
import specification.PlayerSpecification;
import abstraction.PlayerAbstraction;

/**
 * an AI player plays by using rules.
 * {@link AIRuleManager}
 */
public class AI extends PlayerAbstraction implements PlayerSpecification {
	AIRuleManager rule;

	public AI(String n, int or, AIRuleManager rule) {
		super(n, or);
		rule.setOwner(this);
		this.rule = rule;
	}

	@Override
	public MoveSpecification getMove(BoardSpecification board) {
		return rule.getMove(board);
	}

}
