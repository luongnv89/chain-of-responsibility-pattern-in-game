/**
 * 
 */
package model;

import java.util.ArrayList;

import model.connected4.tests.Connected4RuleBlockWinTest;
import model.xo.tests.XORuleBlockWinTest;

import specification.BoardSpecification;
import specification.MoveSpecification;
import abstraction.BoardAbstraction;
import abstraction.RuleAbstraction;

/**
 * Reference CSC7322ProjectNguyenVanLuong#rules
 * Test:
 * <li> {@link Connected4RuleBlockWinTest}
 * <li> {@link XORuleBlockWinTest}
 */
public class RuleBlockWin extends RuleAbstraction {

	public RuleBlockWin() {
		ruleName = "If you can block a win then block a win";
	}

	 /** get a move in the list of possible moves.<br>
	 * try to move with the value of his competitor
	 * <li>if this move make his competitor wins, return this move but with his value
	 * <li>else, return null
	 * */
	@Override
	public MoveSpecification getMove(BoardSpecification board) {
		ArrayList<Move> listPossibleMoves = ((BoardAbstraction) board)
				.getPossibleMoves(owner.getValue() == -1 ? 1 : -1);
		if (listPossibleMoves.isEmpty())
			return null;
		for (int i = 0; i < listPossibleMoves.size(); i++) {
			Move move = listPossibleMoves.get(i);
			board.move(move);
			if (board.hasWinner()) {
				board.undoLastMove();
				return move;
			}
			board.undoLastMove();
		}
		return null;
	}

}
