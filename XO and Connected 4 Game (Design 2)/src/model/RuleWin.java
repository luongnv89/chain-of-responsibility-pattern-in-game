/**
 * 
 */
package model;

import java.util.ArrayList;

import model.connected4.tests.Connected4RuleWinTest;
import model.xo.tests.XORuleWinTest;

import specification.BoardSpecification;
import specification.MoveSpecification;
import abstraction.BoardAbstraction;
import abstraction.RuleAbstraction;

/**
 * Reference CSC7322ProjectNguyenVanLuong#rules
 * Test:
 * <li>{@link Connected4RuleWinTest}
 * <li>{@link XORuleWinTest}
 */
public class RuleWin extends RuleAbstraction {

	public RuleWin() {
		ruleName = "If you can win then win";
	}

	/**
	 * get a move in the list of possible moves.<br>
	 * try to move with his value and check is it possible to win?<br>
	 * <li>if can win, return this move
	 * <li>else, return null
	 */
	@Override
	public MoveSpecification getMove(BoardSpecification board) {
		ArrayList<Move> listPossibleMoves = ((BoardAbstraction) board)
				.getPossibleMoves(owner.getValue());
		if (listPossibleMoves.isEmpty())
			return null;
		for (int i = 0; i < listPossibleMoves.size(); i++) {
			Move move = listPossibleMoves.get(i);
			board.move(move);
			if (board.hasWinner()) {
				board.undoLastMove();
				return move;
			}
			board.undoLastMove();
		}
		return null;
	}
}
