package model;

import java.util.Scanner;

import abstraction.PlayerAbstraction;
import specification.BoardSpecification;
import specification.MoveSpecification;
import specification.PlayerSpecification;

/**
 * a human player plays (makes a move) by inputing values from the keyboard
 */
public class Human extends PlayerAbstraction implements PlayerSpecification {

	public Human(String n, int or) {
		super(n, or);
	}

	/**
	 * input the position you want to move from the keyboard
	 */
	public MoveSpecification getMove(BoardSpecification board) {
		Scanner input = new Scanner(System.in);
		int x = -1;
		int y = -1;
		boolean canmove = false;
		while (!canmove) {
			System.out.println("Input coordinate of move: ");
			System.out.print("x = ");
			x = input.nextInt();
			System.out.print("y = ");
			y = input.nextInt();

			canmove = x >= 0 && x < board.getWidth() && y >= 0
					&& y < board.getHeight();
			if (!canmove) {
				System.out
						.println("The position is out of board. Please input again");
			} else {
				canmove = board.getState(x, y) == 0;
				if (!canmove) {
					System.out
							.println("The input position isn't empty. Please input again:");
				}
			}
		}
		return new Move(getValue(), x, y);
	}

}
