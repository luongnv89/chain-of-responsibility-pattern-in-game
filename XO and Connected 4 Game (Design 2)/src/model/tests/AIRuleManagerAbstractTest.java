/**
 * 
 */
package model.tests;

import static org.junit.Assert.*;

import model.AIRuleManager;
import model.Human;

import org.junit.Test;

import specification.MoveSpecification;

import abstraction.tests.RuleAbstractionTest;

/**
 * 
 *
 */
public abstract class AIRuleManagerAbstractTest extends RuleAbstractionTest {
	String path = "rule1";

	/**
	 * Test method for {@link model.AIRuleManager#AIRuleManager()}.
	 */
	@Test
	public void testAIRuleManager() {
		assertTrue(rule.getRuleName().equals("Manager"));
		((AIRuleManager) rule).showAllRules();
	}

	/**
	 * Test method for {@link model.AIRuleManager#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMove() {
		((AIRuleManager) rule).createRulesFromFile(path);
		((AIRuleManager) rule).showUseRules();
		MoveSpecification move = rule.getMove(board);
		assertNotNull(move);
		System.out.println(move.toString());
	}

	/**
	 * Test method for {@link model.AIRuleManager#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMoveFullBoard() {
		for (int i = 0; i < board.getWidth(); i++) {
			for (int j = 0; j < board.getHeight(); j++) {
				board.setState(i, j, 1);
			}
		}
		MoveSpecification move = rule.getMove(board);
		assertNull(move);
	}

	/**
	 * Test method for {@link model.AIRuleManager#createRulesFromFile(java.lang.String)}.
	 */
	@Test
	public void testCreateRulesFromFile() {
		((AIRuleManager) rule).showUseRules();
		((AIRuleManager) rule).createRulesFromFile(path);
		((AIRuleManager) rule).showUseRules();
	}

	/**
	 * Test method for {@link model.AIRuleManager#createRuleRandom()}.
	 */
	@Test
	public void testCreateRuleRandom() {
		((AIRuleManager) rule).showUseRules();
		((AIRuleManager) rule).createRuleRandom();
		((AIRuleManager) rule).showUseRules();
	}

	/**
	 * Test method for {@link model.AIRuleManager#createRuleManual()}.
	 */
	/*
	 * @Test public void testCreateRuleManual() { ((AIRuleManager)
	 * rule).showUseRules(); ((AIRuleManager) rule).createRuleManual();
	 * ((AIRuleManager) rule).showUseRules(); }
	 */

	/**
	 * Test method for {@link model.AIRuleManager#showAllRules()}.
	 */
	@Test
	public void testShowAllRules() {
		((AIRuleManager) rule).showAllRules();
	}

	/**
	 * Test method for {@link model.AIRuleManager#showUseRules()}.
	 */
	@Test
	public void testShowUseRules() {
		((AIRuleManager) rule).createRuleRandom();
		((AIRuleManager) rule).showUseRules();
	}

	@Override
	public void setUpRule() {
		rule = new AIRuleManager();
		player = new Human("Human", -1);
		rule.setOwner(player);
	}

}
