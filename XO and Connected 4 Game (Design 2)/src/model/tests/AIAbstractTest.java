/**
 * 
 */
package model.tests;

import model.AI;
import model.AIRuleManager;

import org.junit.Test;

import specification.MoveSpecification;
import abstraction.tests.PlayerAbstractionTest;

/**
 * 
 * Test for class {@link AI}
 * 
 */
public abstract class AIAbstractTest extends PlayerAbstractionTest {

	public void setUpPlayer() throws Exception {
		AIRuleManager rule1 = new AIRuleManager();
		rule1.createRuleRandom();

		AIRuleManager rule2 = new AIRuleManager();
		rule2.createRuleRandom();

		player1 = new AI(player1Name, player1Value, rule1);
		player2 = new AI(player2Name, player2Value, rule2);
		playerNoInvariant = new AI("PlayerNoInvariant", 2, rule1);
	}

	/**
	 * Test method for
	 * {@link model.AI#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMove() {
		MoveSpecification move = player1.getMove(board);
		System.out.println(move.toString());
	}

}
