/**
 * 
 */
package model.tests;

import model.Human;

import org.junit.Test;

import specification.MoveSpecification;
import abstraction.tests.PlayerAbstractionTest;

/**
 * Test for class {@link Human}
 * 
 * 
 */
public abstract class HumanAbstractTest extends PlayerAbstractionTest {

	public void setUpPlayer() throws Exception {
		player1 = new Human(player1Name, player1Value);
		player2 = new Human(player2Name, player2Value);
		playerNoInvariant = new Human("PlayerNoInvariant", 2);
	}

	/**
	 * Test method for
	 * {@link model.Human#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMove() {
		MoveSpecification move = player1.getMove(board);
		System.out.println(move.toString());
	}

}
