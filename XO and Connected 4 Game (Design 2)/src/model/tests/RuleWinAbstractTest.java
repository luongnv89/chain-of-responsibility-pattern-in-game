/**
 * 
 */
package model.tests;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import model.Human;
import model.RuleWin;

import org.junit.Test;

import specification.MoveSpecification;
import abstraction.tests.RuleAbstractionTest;

/**
 *
 *
 */
public abstract class RuleWinAbstractTest extends RuleAbstractionTest {

	public void setUpRule(){
		player = new Human("Human", -1);
		rule = new RuleWin();
		rule.setOwner(player);
	}

	/**
	 * Test method for {@link model.RuleWin#RuleWin()}.
	 */
	@Test
	public void testRuleWin() {
		assertTrue(rule.getRuleName().equals("If you can win then win"));
	}

	/**
	 * Test method for {@link model.RuleWin#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMoveNull() {
		MoveSpecification move = rule.getMove(board);
		assertNull(move);
	}

	/**
	 * Test method for {@link model.RuleWin#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMoveFullBoard() {
		for (int i = 0; i < board.getWidth(); i++) {
			for (int j = 0; j < board.getHeight(); j++) {
				board.setState(i, j, 1);
			}
		}
		MoveSpecification move = rule.getMove(board);
		assertNull(move);
	}

	/**
	 * Test method for {@link model.RuleWin#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public abstract void testGetMove();

}
