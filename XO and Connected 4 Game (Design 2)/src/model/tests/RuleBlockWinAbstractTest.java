/**
 * 
 */
package model.tests;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import model.Human;
import model.RuleBlockWin;

import org.junit.Test;

import specification.MoveSpecification;
import abstraction.tests.RuleAbstractionTest;

/**
 *
 *
 */
public abstract class RuleBlockWinAbstractTest extends RuleAbstractionTest {

	/**
	 * Test method for {@link model.RuleBlockWin#RuleBlockWin()}.
	 */
	@Test
	public void testRuleBlockWin() {
		assertTrue(rule.getRuleName().equals(
				"If you can block a win then block a win"));
	}

	/**
	 * Test method for {@link model.RuleBlockWin#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMoveNull() {
		MoveSpecification move = rule.getMove(board);
		assertNull(move);
	}

	/**
	 * Test method for {@link model.RuleBlockWin#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMoveFullBoard() {
		for (int i = 0; i < board.getWidth(); i++) {
			for (int j = 0; j < board.getHeight(); j++) {
				board.setState(i, j, 1);
			}
		}
		MoveSpecification move = rule.getMove(board);
		assertNull(move);
	}

	/**
	 * Test method for {@link model.RuleBlockWin#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public abstract void testGetMove();

	public void setUpRule() {
		player = new Human("Human", -1);
		rule = new RuleBlockWin();
		rule.setOwner(player);
	}

}
