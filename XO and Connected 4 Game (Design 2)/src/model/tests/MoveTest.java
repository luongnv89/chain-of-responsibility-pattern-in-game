/**
 * 
 */
package model.tests;

import static org.junit.Assert.*;

import model.Move;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * Test for class {@link Move}
 * 
 */
public class MoveTest {
	Move move;
	int x;
	int y;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		x = 0;
		y = 1;
		move = new Move(1, x, y);
	}

	/**
	 * Test method for {@link model.Move#Move(int, int, int)}.
	 */
	@Test
	public void testMove() {
		assertTrue(move.getX() == x);
		assertTrue(move.getY() == y);
		assertTrue(move.getPlayer() == 1);

	}

	/**
	 * Test method for {@link model.Move#getX()}.
	 */
	@Test
	public void testGetX() {
		assertTrue(move.getX() == x);
	}

	/**
	 * Test method for {@link model.Move#getY()}.
	 */
	@Test
	public void testGetY() {
		assertTrue(move.getY() == y);
	}

	/**
	 * Test method for {@link model.Move#getPlayer()}.
	 */
	@Test
	public void testGetPlayer() {
		assertTrue(move.getPlayer() == 1);
	}
}
