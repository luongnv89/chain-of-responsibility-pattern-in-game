/**
 * 
 */
package model.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import model.Human;
import model.RuleRandom;

import org.junit.Test;

import specification.MoveSpecification;
import abstraction.tests.RuleAbstractionTest;

/**
 * 
 *
 */
public abstract class RuleRandomAbstractTest extends RuleAbstractionTest {

	public void setUpRule() {
		player = new Human("Human", -1);
		rule = new RuleRandom();
		rule.setOwner(player);
	}

	/**
	 * Test method for {@link model.RuleRandom#RuleRandom()}.
	 */
	@Test
	public void testRuleRandom() {
		assertTrue(rule.getRuleName().equals(
				"Choose one of the remaining free positions randomly"));
	}

	/**
	 * Test method for {@link model.RuleRandom#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMoveFullBoard() {
		for (int i = 0; i < board.getWidth(); i++) {
			for (int j = 0; j < board.getHeight(); j++) {
				board.setState(i, j, 1);
			}
		}
		MoveSpecification move = rule.getMove(board);
		assertNull(move);
	}

	/**
	 * Test method for {@link model.RuleRandom#getMove(specification.BoardSpecification)}.
	 */
	@Test
	public void testGetMove() {
		MoveSpecification move = rule.getMove(board);
		assertNotNull(move);
		System.out.println(move.toString());
	}

}
