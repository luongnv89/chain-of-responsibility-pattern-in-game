package simulations;

import model.connected4.tests.Connected4AIRuleManagerTest;
import model.connected4.tests.Connected4AITest;
import model.connected4.tests.Connected4BoardTest;
import model.connected4.tests.Connected4GameTest;
import model.connected4.tests.Connected4HumanTest;
import model.connected4.tests.Connected4RuleBlockWinTest;
import model.connected4.tests.Connected4RuleRandomTest;
import model.connected4.tests.Connected4RuleWinTest;
import model.tests.MoveTest;
import model.xo.tests.XOAIRuleManagerTest;
import model.xo.tests.XOAITest;
import model.xo.tests.XOBoardTest;
import model.xo.tests.XOGameTest;
import model.xo.tests.XOHumanTest;
import model.xo.tests.XORuleBlockWinTest;
import model.xo.tests.XORuleRandomTest;
import model.xo.tests.XORuleWinTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MoveTest.class, XOAIRuleManagerTest.class, XOAITest.class,
		XOBoardTest.class, XOGameTest.class, XORuleBlockWinTest.class,
		XORuleRandomTest.class, XORuleWinTest.class,
		Connected4AIRuleManagerTest.class, Connected4AITest.class,
		Connected4BoardTest.class, Connected4GameTest.class,
		Connected4RuleBlockWinTest.class, Connected4RuleRandomTest.class,
		Connected4RuleWinTest.class, Connected4AIGameSimulation.class,
		XOAIGameSimulation.class })
public class AllTests {

}
