/**
 * 
 */
package simulations;

import model.AI;
import model.AIRuleManager;
import model.Human;

/**
 *
 */
public abstract class AIvsHumanGameSimulationAbstract extends
		GameSimulationAbstract {

	@Override
	public void setUpPlayer() {
		AIRuleManager rule1 = new AIRuleManager();
		rule1.createRulesFromFile("rule1");
		player1 = new AI("AI", 1, rule1);
		player2 = new Human("Human", -1);
	}

}
