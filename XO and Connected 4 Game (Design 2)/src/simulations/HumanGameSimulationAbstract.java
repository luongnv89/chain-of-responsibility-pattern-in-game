/**
 * 
 */
package simulations;

import model.Human;

/**
 *
 */
public abstract class HumanGameSimulationAbstract extends
		GameSimulationAbstract {

	@Override
	public void setUpPlayer() {
		player1 = new Human("AIPlayer1", 1);
		player2 = new Human("AIPlayer2", -1);
	}

}
