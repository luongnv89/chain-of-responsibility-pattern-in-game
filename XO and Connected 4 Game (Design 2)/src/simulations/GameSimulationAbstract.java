/**
 * 
 */
package simulations;

import org.junit.Test;

import abstraction.tests.GameAbstractionTest;

/**
 * 
 *
 */
public abstract class GameSimulationAbstract extends GameAbstractionTest {

	public abstract void setUpPlayer();

	@Test
	public void testGameStart() {
		game.gameStart();
	}

}
