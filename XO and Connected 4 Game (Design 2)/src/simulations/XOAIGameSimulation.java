/**
 * 
 */
package simulations;

import model.XOBoard;

import org.junit.Before;

import abstraction.GameAbstraction;

/**
 * @author crocode
 *
 */
public class XOAIGameSimulation extends AIGameSimulationAbstract {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard(5, 5);
		setUpPlayer();
		game = new GameAbstraction(board, player1, player2);
		game.setFirstPlayer(player1);
	}

}
