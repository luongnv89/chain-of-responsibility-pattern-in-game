/**
 * 
 */
package simulations;

import model.AI;
import model.AIRuleManager;

/**
 *
 */
public abstract class AIGameSimulationAbstract extends GameSimulationAbstract {

	@Override
	public void setUpPlayer() {
		AIRuleManager rule1 = new AIRuleManager();
		rule1.createRulesFromFile("rule1");
		player1 = new AI("AIPlayer1", 1, rule1); 

		AIRuleManager rule2 = new AIRuleManager();
		rule2.createRuleRandom();
		player2 = new AI("AIPlayer2", -1, rule2);
	}

}
