/**
 * 
 */
package simulations;

import model.Connected4Board;

import org.junit.Before;

import abstraction.GameAbstraction;

/**
 * @author crocode
 *
 */
public class Connected4AIvsHumanGameSimulation extends
		AIvsHumanGameSimulationAbstract {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connected4Board(9, 9);
		setUpPlayer();
		game = new GameAbstraction(board, player1, player2);
		game.setFirstPlayer(player1);
	}

}
