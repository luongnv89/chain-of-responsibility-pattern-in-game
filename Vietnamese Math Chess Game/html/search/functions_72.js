var searchData=
[
  ['resetboard',['resetBoard',['../classabstractions_1_1BoardAbstract.html#a78e333d833c7bce6e3c394cad02f30e1',1,'abstractions.BoardAbstract.resetBoard()'],['../interfacespecifications_1_1BoardInterface.html#a5263f52ced17aafe991c02d424423645',1,'specifications.BoardInterface.resetBoard()']]],
  ['resetcaculator',['resetCaculator',['../classevaluator_1_1MiniMaxCalculator.html#ac8446ee2d4c36b639c005f2a0a8c7fd1',1,'evaluator::MiniMaxCalculator']]],
  ['resetchessman',['resetChessman',['../classmathchess_1_1Chessman.html#a60210fb6e1ed2392d6a71d521fac34c3',1,'mathchess::Chessman']]],
  ['resetgame',['resetGame',['../classabstractions_1_1GameAbstract.html#a811d025d425cf68465b58875bb814ffb',1,'abstractions.GameAbstract.resetGame()'],['../interfacespecifications_1_1GameInterface.html#a228e72011b3e7cf3feaa0fe9e80e81c6',1,'specifications.GameInterface.resetGame()']]],
  ['resetgraphic',['resetGraphic',['../classabstractions_1_1GraphicAbstract.html#a8f7e327abc9fcb01b1a1c66c7a8329a6',1,'abstractions.GraphicAbstract.resetGraphic()'],['../interfacespecifications_1_1GraphicInterface.html#ac0c65bcd5c69fa5bee2d2fa1fb7afbde',1,'specifications.GraphicInterface.resetGraphic()']]],
  ['resetplayer',['resetPlayer',['../classmathchess_1_1PlayerMathChess.html#a952721f7a7561ee7746e2c4d9639ce4e',1,'mathchess::PlayerMathChess']]],
  ['rule_5fmanger',['Rule_Manger',['../classrules_1_1Rule__Manger.html#abd58cca6b5659b4b5acd7dc4a06f1a43',1,'rules::Rule_Manger']]],
  ['rule_5fminimax',['Rule_Minimax',['../classrules_1_1Rule__Minimax.html#a8bb72dec8fa3e7c85e13bb1af94643b6',1,'rules.Rule_Minimax.Rule_Minimax()'],['../classrules_1_1Rule__Minimax.html#a8a09d23ee91da840496a374a1028d8e5',1,'rules.Rule_Minimax.Rule_Minimax(int depth)']]]
];
