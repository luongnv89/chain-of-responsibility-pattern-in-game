var searchData=
[
  ['undolastmove',['undoLastMove',['../classmathchess_1_1Board.html#ae74d05be5b7eb3a669666ebf8a13405e',1,'mathchess.Board.undoLastMove()'],['../classmathchess_1_1PlayerMathChess.html#ac607d667754be929c465cb52a699c4a1',1,'mathchess.PlayerMathChess.undoLastMove()'],['../interfacespecifications_1_1BoardInterface.html#aa3584e3d660966c546c11744a87b98b0',1,'specifications.BoardInterface.undoLastMove()'],['../interfacespecifications_1_1PlayerInterface.html#a0915ace8d71b753b0a82127ce3fc827f',1,'specifications.PlayerInterface.undoLastMove()']]],
  ['update',['update',['../classabstractions_1_1ChessmanAbstract.html#af0599c6db2ad1e346f33faa19055288b',1,'abstractions.ChessmanAbstract.update()'],['../interfacespecifications_1_1ChessmanInterface.html#ab9c53b869b98c29893d0f164b4c2d419',1,'specifications.ChessmanInterface.update()']]],
  ['updatechessman',['updateChessman',['../classmathchess_1_1PlayerMathChess.html#a0475e13bd2dbe1354e4aaa3ffee3184f',1,'mathchess::PlayerMathChess']]],
  ['updatechessmancaught',['updateChessmanCaught',['../classmathchess_1_1PlayerMathChess.html#ab1a3b73a9b75f945d4268f5af50bf302',1,'mathchess::PlayerMathChess']]]
];
