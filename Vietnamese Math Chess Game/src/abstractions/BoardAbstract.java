package abstractions;

import specifications.BoardInterface;

/**
 * Represent an abstract board. Board stores all moves in an
 * <code>ArrayList</code>. {@code BoardAbstract#states} is the 2 dimensional
 * array, with the value = 0 if this position is empty, else the value = the
 * value of the chess-man at the position. This class allows to be reset a board
 * back to the initial one.
 */
public abstract class BoardAbstract implements BoardInterface {

	/**
	 * The state of board
	 */
	private int[][] states;

	/**
	 * The width of board
	 */
	private int width;

	/**
	 * The height of board
	 */
	private int height;

	/**
	 * Constructor a board with the height and the width as integers.
	 * 
	 * @param width
	 * @param height
	 */
	public BoardAbstract(int width, int height) {
		this.width = width;
		this.height = height;
		this.states = new int[width][height];
		this.initial();
	}

	public void resetBoard() {
		initial();
	}

	/**
	 * Check the position (x,y) is empty or not?
	 * 
	 * @param x
	 * @param y
	 * @return true if the state of board at position (x,y) is 0. else return
	 *         false.
	 */
	public boolean isEmpty(int x, int y) {
		return this.getState(x, y) == 0;
	}

	/**
	 * Check a position is inside a board or not?
	 * 
	 * @param x
	 * @param y
	 * @return true if the position (x,y): <li>0 <= x <
	 *         {@link BoardAbstract#getWidth()} and <li>0 <= y <
	 *         {@link BoardAbstract#getHeight()}.
	 */
	public boolean insideBoard(int x, int y) {
		return x >= 0 && x < getWidth() && y >= 0 && y < getHeight();
	}

	/**
	 * Set value for position (x,y) on a board
	 * 
	 * @param x
	 * @param y
	 * @param value
	 *            to set
	 */
	public void setState(int x, int y, int value) {
		states[x][y] = value;
	}

	/**
	 * Get the value of position (x,y) on a board
	 * 
	 * @param x
	 * @param y
	 * @return the value of position (x,y) on a board
	 */
	public int getState(int x, int y) {
		return states[x][y];
	}

	/**
	 * @return the width of board
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the height of board
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * just display value of all positions on the board as an array of values of
	 * each position.
	 */
	public String toString() {
		String str = "";
		for (int i = 0; i < getHeight(); i++) {
			for (int j = 0; j < getWidth(); j++) {
				str = str + String.valueOf(this.getState(j, i));
			}
			str = str + "\n";
		}
		return str;
	}
}
