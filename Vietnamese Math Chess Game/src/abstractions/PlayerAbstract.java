package abstractions;

import java.util.ArrayList;

import mathchess.PlayerMathChess;

import specifications.MoveInterface;
import specifications.PlayerInterface;

/**
 * {@link PlayerAbstract} represent a player abstraction in a game <li>
 * {@link PlayerAbstract#playerName} The name of player <li>
 * {@link PlayerAbstract#blue} The color represent for player in a game which
 * has two player against each other <li>true if the player is a blue player in
 * a game <li>false if the player is a red player in a game
 * {@link PlayerAbstract#listAllPossibleMoves} List all possible move can be
 * done by player
 * 
 * 
 */
public abstract class PlayerAbstract implements PlayerInterface {
	/**
	 * The name of player
	 */
	protected String playerName;
	/**
	 * The color represent for player in a game which has two player against each other
	 * <li> true if the player is a blue player in a game
	 * <li> false if the player is a red player in a game
	 */
	protected boolean blue;
	/**
	 * List all possible move can be done by player
	 */
	protected ArrayList<MoveInterface> listAllPossibleMoves;

	/**
	 * Create a new player with name and decide it is blue or not. 
	 * @param playerName
	 * @param blue
	 */
	public PlayerAbstract(String playerName, boolean blue) {
		this.playerName = playerName;
		this.blue = blue;
		this.listAllPossibleMoves = new ArrayList<MoveInterface>();
	}

	/**
	 * @return the name of a player
	 */
	public String getPlayerName() {
		return playerName;
	}

	public boolean isBlue() {
		return blue;
	}

	/**
	 * Get all the possible move of player. 
	 * List of possible moves will be calculated by 
	 * {@link PlayerMathChess#calculateMove(specifications.BoardInterface)}
	 * each time the player play.
	 * @return the listAllPossibleMoves
	 */
	public ArrayList<MoveInterface> getListAllPossibleMoves() {
		return listAllPossibleMoves;
	}
}
