package abstractions;

import specifications.ChessmanInterface;

/**
 * each chess-man has.
 * <br>value depends on the game which it belongs to
 * <br>(x,y): the position of chess-man on a board.
 * An invariant chess-man when it's on an invariant board,
 * and its value is also the state of the position where it's on the board.
 *
 */
public abstract class ChessmanAbstract implements ChessmanInterface {
	/**
	 * Value present a chessman
	 */
	protected int value;
	/**
	 * The x coordinate of chessman
	 */
	protected int x;
	/**
	 * The y coordinate of chessman
	 */
	protected int y;

	/**
	 * Constructor with its value and position on a board.
	 * @param value of the chess-man
	 * @param x
	 * @param y
	 */
	public ChessmanAbstract(int value, int x, int y) {
		this.value = value;
		this.x = x;
		this.y = y;
	}

	public void update(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * display a chess-man with its value and its position on a board.
	 */
	public String toString() {
		return "Chessman [value=" + value + ", x=" + x + ", y=" + y + "]";
	}

	
	public int getValue() {
		return value;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
