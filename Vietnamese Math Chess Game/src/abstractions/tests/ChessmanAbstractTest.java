/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.*;

import mathchess.tests.ChessmanTest;

import org.junit.After;
import org.junit.Test;

import abstractions.ChessmanAbstract;

/**
 * Test {@link ChessmanAbstract} class.
 * an abstract class which is extended by {@link ChessmanTest} class.
 *
 */
public abstract class ChessmanAbstractTest {
	
	protected ChessmanAbstract chessmanOk;
	protected ChessmanAbstract chessman0;

	protected int valueOk;
	protected int valueKo;
	
	protected int xOk;
	protected int yOk;
	
	protected int xKo;
	protected int yKo;
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		chessmanOk = null;
		chessman0 = null;
	}

	/**
	 * Test method for {@link abstractions.ChessmanAbstract#ChessmanAbstract(int, int, int)}.
	 */
	@Test
	public void testConstructor() {
		assertTrue(chessmanOk.invariant());
		assertEquals(valueOk, chessmanOk.getValue());
		
		assertFalse(chessman0.invariant());
	}

	/**
	 * Test method for {@link abstractions.ChessmanAbstract#update(int, int)}.
	 * Change the position of {@code ChessmanAbstractTest#chessmanOk},
	 * then check the new position by using getX, getY methods.
	 */
	@Test
	public void testUpdate() {
		chessmanOk.update(xKo+1, yKo+1);
		assertTrue(chessmanOk.invariant());
		assertEquals(xKo+1, chessmanOk.getX());
		assertEquals(yKo+1, chessmanOk.getY());
	}

	/**
	 * Test method for {@link abstractions.ChessmanAbstract#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(chessmanOk.toString());
	} 

	/**
	 * Test method for {@link abstractions.ChessmanAbstract#getValue()}.
	 */
	@Test
	public void testGetValue() {
		assertEquals(valueOk, chessmanOk.getValue());
	}

	/**
	 * Test method for {@link abstractions.ChessmanAbstract#getX()}.
	 */
	@Test
	public void testGetX() {
		assertEquals(xOk, chessmanOk.getX());
	}

	/**
	 * Test method for {@link abstractions.ChessmanAbstract#getY()}.
	 */
	@Test
	public void testGetY() {
		assertEquals(yOk, chessmanOk.getY());
	}
}
