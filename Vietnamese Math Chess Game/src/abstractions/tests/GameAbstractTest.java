/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mathchess.Game;
import mathchess.tests.GameTest;

import org.junit.After;
import org.junit.Test;

import specifications.BoardInterface;
import specifications.PlayerInterface;
import abstractions.GameAbstract;

/**
 * Test {@link GameAbstract} class.
 * an abstract test class which is extended by {@link GameTest}.
 */
public abstract class GameAbstractTest {

	protected BoardInterface board;

	protected PlayerInterface blueHumanPlayer;
	protected PlayerInterface redHumanPlayer;


	protected GameAbstract defaultGame;
	protected GameAbstract gameKo;


	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;
		defaultGame = null;
		gameKo = null;

	}

	/**
	 * Test {@link Game#Game(BoardInterface, PlayerInterface, PlayerInterface)}.
	 * checks invariant of games.
	 */
	@Test
	public void testConstructor() {
		assertTrue(defaultGame.invariant());
		assertFalse(gameKo.invariant());
	}

	/**
	 * Test method for {@link abstractions.GameAbstract#setFirstPlayer(specifications.PlayerInterface)}.
	 */
	@Test
	public void testSetCurrentPlayer() {
		((GameAbstract) defaultGame).setFirstPlayer(true);
		assertTrue(blueHumanPlayer.equals(((GameAbstract) defaultGame)
				.getCurrentPlayer()));
	}

	/**
	 * Test method for {@link abstractions.GameAbstract#resetGame()}.
	 * After reset game, the board becomes an initial board (defaultBoard).
	 */
	@Test
	public void testResetGame() {
		defaultGame.resetGame();
		assertTrue(board.equals(((GameAbstract) defaultGame).getBoard()));
	}

	/**
	 * Test {@link GameAbstract#getBoard()}.
	 */
	@Test
	public void testGetBoard(){
		assertTrue(board.equals(defaultGame.getBoard()));
	}
	
	/**
	 * Test {@link GameAbstract#invariant()}.
	 */
	@Test
	public void testInvariant(){
		assertTrue(defaultGame.invariant());
		assertFalse(gameKo.invariant());
	}
}
