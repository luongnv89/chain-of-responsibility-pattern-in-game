/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;

import specifications.BoardInterface;
import specifications.GraphicInterface;
import abstractions.BoardAbstract;
import abstractions.GraphicAbstract;

/**
 * Test for {@link GraphicAbstract}
 *
 */
public class GraphicAbstractTest {

	protected GraphicInterface graphic;
	protected BoardInterface board;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		graphic = null;
		board = null;
	}

	/**
	 * Test method for {@link abstractions.GraphicAbstract#resetGraphic()}.
	 */
	@Test
	public void testResetGraphic() {
		((BoardAbstract) board).setState(5, 5, 5);
		graphic.view();
		graphic.resetGraphic();
		graphic.view();
	}

	/**
	 * Test method for {@link abstractions.GraphicAbstract#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(graphic.invariant());
	}

}
