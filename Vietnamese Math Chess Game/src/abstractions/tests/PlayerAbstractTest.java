/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mathchess.tests.PlayerMathChessTest;

import org.junit.After;
import org.junit.Test;

import specifications.BoardInterface;
import abstractions.PlayerAbstract;

/**
 * Test {@link PlayerAbstract} class.
 * an abstract class which is extended by {@link PlayerMathChessTest}.
 */
public abstract class PlayerAbstractTest {

	/**
	 * players for tests
	 */
	protected PlayerAbstract blue;
	protected PlayerAbstract red;
	protected PlayerAbstract playerKO;

	/**
	 * names of players
	 */
	protected String blueName = "Blue";
	protected String redName = "Red";

	/**
	* board for test
	*/
	protected BoardInterface board;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		blue = null;
		red = null;
		playerKO = null;
		board = null;
	}

	/**
	 * Test method for
	 * {@link abstractions.PlayerAbstract#PlayerAbstract(java.lang.String, boolean)}
	 */
	@Test
	public void testConstructor() {
		//test blue player
		assertTrue(blue.invariant()); 
		assertTrue(blue.isBlue());
		assertEquals(blueName.hashCode(), blue.getPlayerName().hashCode());

		//test red player
		assertTrue(red.invariant());
		assertFalse(red.isBlue());
		assertEquals(redName.hashCode(), red.getPlayerName().hashCode());
	}

	/**
	 * test {@link PlayerAbstract#isBlue()}.
	 */
	@Test
	public void testIsBlue() {
		assertTrue(blue.isBlue());
		assertFalse(red.isBlue());
	}

	/**
	 * Test method for {@link abstractions.PlayerAbstract#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(blue.toString());
	}

	/**
	 * Test method for {@link abstractions.PlayerAbstract#getPlayerName()}.
	 */
	@Test
	public void testGetPlayerName() {
		assertTrue(blueName.equals(blue.getPlayerName()));
	}

}
