/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mathchess.Board;
import mathchess.tests.BoardTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import abstractions.BoardAbstract;

/**
 * Test {@link BoardAbstract} class.
 * an abstract class which is extended by {@link BoardTest} class.
 */
public abstract class BoardAbstractTest {

	protected BoardAbstract defaultBoard;
	protected BoardAbstract nonDefaultBoard;

	protected int width;
	protected int heigh;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		defaultBoard = null;
		nonDefaultBoard = null;
	}

	/**
	 * Test {@link Board#ChessBoard()}. The result should look like Board
	 * default: 
	 */
	@Test
	public void testConstructorDefault() {
		assertTrue(defaultBoard.invariant());
		Assert.assertEquals(width, defaultBoard.getWidth());
		Assert.assertEquals(heigh, defaultBoard.getHeight());
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#resetBoard()}. After
	 * reset a board, it becomes a default board.
	 */
	@Test
	public void testResetBoard() {
		System.out.println("Initial board: \n" + defaultBoard.toString());
		defaultBoard.setState(5, 5, 6);
		defaultBoard.setState(5, 4, 7);
		defaultBoard.setState(6, 9, 3);
		System.out.println("\nChanged some pieces of board: \n"
				+ defaultBoard.toString());
		defaultBoard.resetBoard();
		System.out.println("Reset board: \n" + defaultBoard.toString());
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#isEmpty(int, int)}.
	 */
	@Test
	public void testIsEmpty() {
		assertTrue(defaultBoard.isEmpty(1, 1));
		assertFalse(defaultBoard.isEmpty(1, 0));
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#insideBoard(int, int)}.
	 */
	@Test
	public void testInsideBoard() {
		System.out.println(defaultBoard.toString());
		assertTrue(defaultBoard.insideBoard(0, 0));
		assertTrue(defaultBoard.insideBoard(width - 1, heigh - 1));
		assertTrue(defaultBoard.insideBoard(0, heigh - 1));
		assertTrue(defaultBoard.insideBoard(width - 1, 0));
		assertTrue(defaultBoard.insideBoard(width / 2, heigh / 2));

		assertFalse(defaultBoard.insideBoard(-1, -1));
		assertFalse(defaultBoard.insideBoard(width, heigh));
		assertFalse(defaultBoard.insideBoard(width, heigh / 2));
		assertFalse(defaultBoard.insideBoard(width / 2, heigh));

	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#setState(int, int, int)}.
	 */
	@Test
	public void testSetState() {
		defaultBoard.setState(0, 0, 7);
		assertTrue(defaultBoard.getState(0, 0) == 7);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#getState(int, int)}.
	 */
	@Test
	public void testGetState() {
		assertTrue(defaultBoard.getState(1, 1) == 0);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#getWidth()}.
	 */
	@Test
	public void testGetWidth() {
		assertTrue(defaultBoard.getWidth() == width);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#getHeight()}.
	 */
	@Test
	public void testGetHeight() {
		assertTrue(defaultBoard.getHeight() == heigh);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(defaultBoard.toString());
	}

}
