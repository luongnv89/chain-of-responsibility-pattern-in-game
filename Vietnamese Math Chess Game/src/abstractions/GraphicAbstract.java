/**
 * 
 */
package abstractions;

import specifications.BoardInterface;
import specifications.GraphicInterface;

/**
 * {@link GraphicAbstract} represent the {@link GraphicAbstract#board} state by graphic
 *
 */
public abstract class GraphicAbstract implements GraphicInterface {
	/**
	 * Board will be showed by graphic
	 */
	protected BoardInterface board;

	public GraphicAbstract(BoardInterface board) {
		this.board = board;
	}

	@Override
	public void resetGraphic() {
		board.resetBoard();
	}

	@Override
	public boolean invariant() {
		return board.invariant();
	}
}
