package abstractions;

import mathchess.Board;
import mathchess.Game;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.GameInterface;
import specifications.GraphicInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import evaluator.MiniMaxCalculator;

/**
 * An abstract class which is extended by {@link Game}.
 * To reset a game,reset the board and players of this game back to the initial state.
 * It also allows to get the current player {@link GameAbstract#getCurrentPlayer()} at the specific state.
 * Defines invariant() method when the board and players invariant, and two players are different.
 */
public abstract class GameAbstract implements GameInterface {

	/**
	 * The board game
	 */
	protected BoardInterface board;

	/**
	 * The graphic will represent the board game
	 */
	protected GraphicInterface graphic;

	/**
	 * Blue player in game
	 */
	protected PlayerInterface bluePlayer;
	/**
	 * The red player in game
	 */
	protected PlayerInterface redPlayer;
	/**
	 * The current player in game
	 */
	protected PlayerInterface currentPlayer;

	/**
	 * constructor a game with a board and two players
	 * @param board
	 * @param bluePlayer
	 * @param redPlayer
	 */
	public GameAbstract(BoardInterface board, PlayerInterface bluePlayer,
			PlayerInterface redPlayer) {
		this.board = board;
		this.bluePlayer = bluePlayer;
		this.redPlayer = redPlayer;
	}

	/**
	 * Set the first player for a game,
	 * set the player before starting the game.
	 * <li> if blueFirst is true, the blue player will be the first player of game
	 * <li> if blueFirst is false, the red player will be the first player of game
	 * @param blueFirst
	 */
	public void setFirstPlayer(boolean blueFirst) {
		if (blueFirst) {
			this.currentPlayer = bluePlayer;
		} else {
			this.currentPlayer = redPlayer;
		}
	}

	/**
	 * Get the current player of a game
	 * @return
	 */
	public PlayerInterface getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * @return board of this game.
	 */
	public BoardInterface getBoard() {
		return board;
	}

	public void resetGame() {
		board.resetBoard();
		((PlayerMathChess) bluePlayer).resetPlayer();
		((PlayerMathChess) redPlayer).resetPlayer();
		graphic.resetGraphic();

	}

	public void startGame() throws Exception {
		graphic.view();
		calculateMoveOfPlayers();
		while (!isGameOver() && this.invariant()) {
			MiniMaxCalculator calculator = new MiniMaxCalculator(bluePlayer,
					redPlayer, board);
			MessageConsole.inforMessage("Minimax Score: "
					+ calculator.calculateStateScore());
			MessageConsole.inforMessage("Blue Score: "
					+ ((Board) board).getScore(true));
			MessageConsole.inforMessage("Red Score: "
					+ ((Board) board).getScore(false));
			MessageConsole.inforMessage(((PlayerAbstract) currentPlayer)
					.getPlayerName()
					+ " go: ("
					+ (currentPlayer.isBlue() ? "blue" : "red") + ")");
			MoveInterface move = currentPlayer.getMove(board);
			if (board.canMakeMove(move)) {
				int chessman2 = ((Board) board).getDestMove(move);
				if (currentPlayer.makeMove(move, board)) {
					MessageConsole
							.inforMessage(((PlayerAbstract) currentPlayer)
									.getPlayerName()
									+ " made move: "
									+ move.toString());
					graphic.view();
					currentPlayer = nextPlayer();
					boolean updateOK = true;
					if (chessman2 != 0)
						updateOK = ((PlayerMathChess) currentPlayer)
								.updateChessmanCaught(chessman2);
					if (updateOK)
						calculateMoveOfPlayers();
					else {
						MessageConsole.errorMessage("Update error!");
					}
				}
			} else {
				throw (new GameExceptions(this,
						((PlayerAbstract) currentPlayer).getPlayerName()
								+ " Invalid move: " + move.toString()));
			}
		}

	}

	/**
	 * Calculate moves for all players.
	 * @throws Exception 
	 * @see 
	 */
	// TODO not use child class methods in parent class
	private void calculateMoveOfPlayers() throws Exception {
		((PlayerMathChess) bluePlayer).calculateMove(board);
		((PlayerMathChess) redPlayer).calculateMove(board);
	}

	/**
	 * Change the player turn
	 * @return if the current player is blue, then the next one is red.
	 * if the current player is red, then the next one is blue.
	 */
	private PlayerInterface nextPlayer() {
		if (this.getCurrentPlayer() == bluePlayer) {
			return redPlayer;
		} else
			return bluePlayer;
	}

	/**
	 *  @return false 
	 *  <li>If the position of chess-man is out of the board
	 *  <li> There is two Chess-men at the same position 
	 *  <br>(In this case, at that position, the state of board is only one value 
	 *  <br> -> the first condition will return false 
	 *  <br>=> we don't need to check this case)
	 * */
	@Override
	public boolean invariant() {
		// Check invariant of board
		if (!this.board.invariant()) {
			MessageConsole
					.errorMessage("@GameAbstract#invariant: Board is invalid");
			return false;
		}

		// Check graphic
		if (!graphic.invariant()) {
			MessageConsole
					.errorMessage("@GameAbstract#invariant: Graphic is invalid");
			return false;
		}

		// Check kind of player
		if (this.bluePlayer.isBlue() == this.redPlayer.isBlue()) {
			MessageConsole.debugMessage("Two player are same color");
			return false;
		}
		return true;
	}

}
