/**
 * 
 */
package rules;

import mathchess.Board;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.MessageConsole;
import abstractions.PlayerAbstract;

/**
 * {@link Rule_Catch} extends {@link RuleAbstract} represent a rule of AI player
 * <li> AI player will get the move which can catch the biggest chess-man of enemy
 * 
 *
 */
public class Rule_Catch extends RuleAbstract {
	public Rule_Catch() {
		ruleName = "If you can capture than capture";
	}

	@Override
	public MoveInterface getMove(BoardInterface board) throws Exception {
		if (((PlayerMathChess) player).getListAllPossibleCaught() != null
				&& !((PlayerMathChess) player).getListAllPossibleCaught()
						.isEmpty()) {
			int maxChessman2 = 0;
			MoveInterface moveMax = null;
			for (int i = 0; i < ((PlayerMathChess) player)
					.getListAllPossibleCaught().size(); i++) {
				MoveInterface move = ((PlayerMathChess) player)
						.getListAllPossibleCaught().get(i);
				int destMove = ((Board) board).getDestMove(move);
				if (player.makeMoveSafe(move, board, enemy)) {
					if (destMove > maxChessman2 && board.canMakeMove(move)) {
						maxChessman2 = destMove;
						moveMax = move;
					}
				}
			}
			if (maxChessman2 > 0) {
				MessageConsole.inforMessage(((PlayerAbstract) player)
						.getPlayerName()
						+ ": Catching move: "
						+ moveMax.toString());
				return moveMax;
			}
		}
		return null;
	}
}
