/**
 * 
 */
package rules;

import java.util.ArrayList;
import java.util.Random;

import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.MessageConsole;
import abstractions.PlayerAbstract;

/**
 * {@link Rule_Random} extends {@link RuleAbstract} represent a rule of AI Player.
 * <li> The AI player get a move randomly from list possible move
 *
 */
public class Rule_Random extends RuleAbstract {
	public Rule_Random() {
		ruleName = "Choose a random move";
	}
	@Override
	public MoveInterface getMove(BoardInterface board) throws Exception {
		ArrayList<MoveInterface> listAllPossibleMove = new ArrayList<MoveInterface>();
		listAllPossibleMove.addAll(((PlayerAbstract) player)
				.getListAllPossibleMoves());
		if (listAllPossibleMove.isEmpty()) {
			System.out.println(((PlayerAbstract) player).getPlayerName()
					+ " doesn't have any possible move!");
			return null;
		} else { 
			Random ran = new Random();
			MoveInterface move;
			do {
				int moveIndex = ran.nextInt(listAllPossibleMove.size());
				move = listAllPossibleMove.get(moveIndex);
				listAllPossibleMove.remove(moveIndex);
			} while (!player.makeMoveSafe(move, board, enemy)
					&& !listAllPossibleMove.isEmpty());
			MessageConsole.inforMessage(((PlayerAbstract) player)
					.getPlayerName() + ": Random move: " + move.toString());
			return move;
		}
	}
}
