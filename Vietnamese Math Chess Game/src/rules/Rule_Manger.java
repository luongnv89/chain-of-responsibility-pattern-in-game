/**
 * 
 */
package rules;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import mathchess.HumanPlayer;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.GameExceptions;

/**
 * {@link Rule_Manger} manager the rule will be used by AI player
 *
 */
public class Rule_Manger extends RuleAbstract {

	ArrayList<RuleInterface> listRules;
	ArrayList<RuleInterface> allRules;

	/**
	 * Create a rule manager
	 */
	public Rule_Manger() {
		ruleName = "Manager";
		listRules = new ArrayList<RuleInterface>();
		allRules = new ArrayList<RuleInterface>();
		loadAllRules();
	}

	/**
	 * Load all the rules of AI player in ordering:
	 * <li> {@link Rule_Win}
	 * <li> {@link Rule_DefenceLost}
	 * <li> {@link Rule_Catch}
	 * <li> {@link Rule_DefenceCatch}
	 * <li> {@link Rule_Random}
	 * 
	 * <br> When the AI player get move:
	 * <li>The rule manager will use the rule in list in ordering to get move.  
	 * @return a move
	 */
	private void loadAllRules() {

		allRules.add(new Rule_Win());

		allRules.add(new Rule_DefenceLost());

		allRules.add(new Rule_Catch());

		allRules.add(new Rule_Random());
	}

	/** 
	 * @param otherPlayer set enemy player for all the rule
	 */
	public void setEnemy(PlayerInterface enemy) throws GameExceptions {
		if (this.enemy != null) {
			throw new GameExceptions(this,
					"@Rule_Manager#setEnemy. The enemy player already exists!");
		} else {
			if (player.isBlue() != enemy.isBlue()) {
				this.enemy = (PlayerMathChess) enemy;
				for (int i = 0; i < listRules.size(); i++) {
					((RuleAbstract) listRules.get(i)).setEnemy(enemy);
				}
			} else {
				throw new GameExceptions(this,
						"@Rule_Manager#setEnemy. Two player are same color");
			}
		}

	}

	/**
	 * Set the AI player will use the rule
	 * @param player the player to set
	 * @throws GameExceptions 
	 */
	@Override
	public void setPlayer(PlayerInterface player)
			throws IllegalArgumentException, GameExceptions {
		if (player instanceof HumanPlayer) {
			throw (new IllegalArgumentException(
					"Cannot apply a playing rule for a Human player: "
							+ ((PlayerMathChess) player).getPlayerName()));
		} else {
			this.player = (PlayerMathChess) player;
			for (int i = 0; i < listRules.size(); i++) {
				listRules.get(i).setPlayer(player);
			}
		}
	}

	/**
	 * {@link Rule_Manger} using the rule in ordering in {@link Rule_Manger#listRules} to get move for player
	 * @return
	 * <li> null if there isn't any move can get move for player
	 * <li> A possible move can be done by player
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws Exception {
		if (listRules.isEmpty())
			throw new GameExceptions(this,
					"@Rule_Manager#getMove: No rule to manager");

		for (int i = 0; i < listRules.size(); i++) {
			MoveInterface move = listRules.get(i).getMove(board);
			if (move != null)
				return move;
		}
		return null;
	}

	/**
	 * set rule from text file
	 * @param ruleFilePath
	 * @throws GameExceptions 
	 */
	public void setRules(String ruleFilePath) throws GameExceptions {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(ruleFilePath));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				RuleInterface rule = getRuleByName(line);
				addRule(rule);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set rules for AI player 
	 * @param index
	 * <li> index = 0, get randomly number of rules and set randomly ordering of rules for AI player
	 * <li> index = 1, choose and set rules for AI player manually
	 * <li> index = 2, set rules for player in order: Rule_Win -> Rule_DefenceLost -> Rule_Catch > Rule_Random
	 * <li> index = 3, set rules for player only : Rule_Random
	 * <li> index = 4, set rule Minimax
	 * @throws GameExceptions 
	 */
	public void setRules(int index) throws GameExceptions {
		switch (index) {
		case 0:
			setRandomly();
			break;
		case 1:
			setManually();
			break;
		case 2:
			setGoodRules();
			break;
		case 3:
			setBadRules();
			break;
		case 4:
			setMiniMaxrules();
			break;
		default:
			throw new IllegalArgumentException(
					"@Rule_Manager#setRules(int): Invalid input: " + index);
		}
	}

	/**
	 * Set the minimax rule for AI player
	 * @throws GameExceptions 
	 */
	private void setMiniMaxrules() throws GameExceptions {
		listRules.clear();
		Rule_Minimax rule = new Rule_Minimax();
		addRule(rule);
	}

	/**
	 * Set the bad rule for AI player
	 * <bre> There is only {@link Rule_Random} in listRules 
	 * @throws GameExceptions 
	 */
	private void setBadRules() throws GameExceptions {
		listRules.clear();
		Rule_Random rule = new Rule_Random();
		addRule(rule);
	}

	/**
	 * Set a good rule ordering for AIplayer
	 * <li> The first rule is {@link Rule_Win}
	 * <li> The second rule is {@link Rule_BlockWin}
	 * <li> The final rule is {@link Rule_Random}
	 * @throws GameExceptions 
	 */
	private void setGoodRules() throws GameExceptions {
		listRules.clear();

		Rule_Win win = new Rule_Win();
		addRule(win);

		Rule_DefenceLost block = new Rule_DefenceLost();
		addRule(block);

		Rule_Catch catching = new Rule_Catch();
		addRule(catching);

		Rule_Random rule = new Rule_Random();
		addRule(rule);
	}

	/**
	 * Set manually rule and order of rules
	 * @throws GameExceptions 
	 */
	private void setManually() throws GameExceptions {
		listRules.clear();
		System.out.println("Total rules of AI player:");
		showRules(false);

		int totalRules = allRules.size();

		// Get the number of rules
		int numberOfUseRules = getNumberOfUseRules(totalRules);
		Scanner input = new Scanner(System.in);
		// Get the list index of rules
		ArrayList<Integer> listRuleIndex = new ArrayList<Integer>();
		for (int i = 0; i < numberOfUseRules; i++) {
			int ruleIndex = -1;
			while (ruleIndex < 0 || ruleIndex > totalRules - 1) {
				System.out
						.println("[ "
								+ i
								+ " ] Index of rule will be added (Each rule can be selected only one time): ");
				try {
					ruleIndex = Integer.parseInt(input.nextLine());
					if (!listRuleIndex.contains(ruleIndex)) {
						listRuleIndex.add(ruleIndex);
					} else {
						System.out.println("Rule has selected: " + ruleIndex);
						ruleIndex = -1;
					}
				} catch (NumberFormatException e) {
					System.out.println(e.toString());
					ruleIndex = -1;
				}
			}
		}

		// Add list rules
		for (int i = 0; i < listRuleIndex.size(); i++) {
			addRule(allRules.get(listRuleIndex.get(i)));
		}

	}

	/**
	 * Get the number of ruled will use
	 * @param totalRules
	 * @return
	 */
	private int getNumberOfUseRules(int totalRules) {
		Scanner input = new Scanner(System.in);
		int numberOfUseRules = -1;
		while (numberOfUseRules < 1 || numberOfUseRules > totalRules) {
			System.out
					.println("Number of rules will be used (must be possitive and less than "
							+ totalRules + " :");
			try {
				numberOfUseRules = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println(e.toString());
				numberOfUseRules = -1;
			}
		}
		return numberOfUseRules;
	}

	/**
	 * <li> Get randomly number of rules will be use
	 * <li> Set randomly order of rules
	 * <li> To avoid the rule_manager cannot get any move, the listRules must contains Rule_Random
	 * @throws GameExceptions 
	 */
	private void setRandomly() throws GameExceptions {
		listRules.clear();
		Random ran = new Random();
		int numberOfRules = ran.nextInt(allRules.size());
		if (numberOfRules == 0)
			numberOfRules = 1;
		ArrayList<RuleInterface> allrules = new ArrayList<RuleInterface>();
		allrules.addAll(allRules);
		boolean existRandom = false;
		for (int i = 0; i < numberOfRules; i++) {
			int index = ran.nextInt(allrules.size());
			RuleInterface rule = allrules.get(index);
			if (((RuleAbstract) rule).getRuleName().equals(
					"Choose a random move"))
				existRandom = true;
			addRule(rule);
			allrules.remove(index);
		}
		if (!existRandom)
			addRule(new Rule_Random());
	}

	/**
	 * Add new rule in the list rules of AI player
	 * 
	 * @param rule
	 * @throws GameExceptions 
	 */
	public void addRule(RuleInterface rule) throws GameExceptions {
		if (listRules.isEmpty()) {
			setPlayerForRule((RuleAbstract) rule);
			listRules.add(rule);
		} else {
			boolean exist = false;
			for (int i = 0; i < listRules.size(); i++) {
				if (((RuleAbstract) rule).getRuleName().equals(
						((RuleAbstract) listRules.get(i)).getRuleName())) {
					System.out.println("The rule already exist!");
					exist = true;
					break;
				}
			}
			if (!exist) {
				setPlayerForRule((RuleAbstract) rule);
				listRules.add(rule);
			}
		}
	}

	/**
	 * Get the rule in listRules by name of rule
	 * @param ruleName
	 * @return <li> A rule has name equals input name
	 * <li> null if there isn't any move has name like input name
	 */
	public RuleInterface getRuleByName(String ruleName) {
		for (int i = 0; i < allRules.size(); i++) {
			if (((RuleAbstract) allRules.get(i)).getRuleName().equals(ruleName)) {
				return allRules.get(i);
			}
		}
		System.out.println("Cannot found the rule with name = " + ruleName);
		return null;
	}

	/**
	 * @param useRule
	 * <li> useRule is true, show the list using rules of AI player
	 * <li> useRule is false, show all rules of AI player
	 */
	public void showRules(boolean useRule) {
		ArrayList<RuleInterface> showRules = useRule ? listRules : allRules;
		for (int i = 0; i < showRules.size(); i++) {
			System.out.println("[" + i + "]: "
					+ ((RuleAbstract) showRules.get(i)).getRuleName());
		}
	}

	/**
	 * Set player for rule.
	 * <br> When the Rule_Manager already has player and enemy, each rule add more in the listRules will have to set player and enemy automatically
	 * @param rule
	 * @throws GameExceptions
	 */
	private void setPlayerForRule(RuleAbstract rule) throws GameExceptions {
		if (player != null)
			rule.setPlayer(player);
		if (enemy != null)
			rule.setEnemy(enemy);
	}
}
