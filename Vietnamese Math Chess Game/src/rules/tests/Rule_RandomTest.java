/**
 * 
 */
package rules.tests;

import org.junit.Before;
import org.junit.Test;

import rules.Rule_Random;
import utils.MessageConsole;

/**
 * Test for class{@link Rule_Random}
 *
 */
public class Rule_RandomTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		blueRule = new Rule_Random();
		redRule = new Rule_Random();
		setUpGame();
	}

	@Test
	public void testGetMoveRandom() throws Exception {
		MessageConsole.disableAllMessages();
		graphic.view();
		testGetMoves();
	}

}
