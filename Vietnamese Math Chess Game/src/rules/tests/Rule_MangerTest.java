/**
 * 
 */
package rules.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import mathchess.Board;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import rules.Rule_Manger;
import rules.Rule_Win;
import specifications.MoveInterface;
import utils.GameExceptions;

/**
 * 
 *Test for class {@link Rule_Manger}
 *
 */
public class Rule_MangerTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		blueRule = new Rule_Manger();
		((Rule_Manger) blueRule).setRules(2);
		redRule = new Rule_Manger();
		((Rule_Manger) redRule).setRules(2);
		setUpGame();
	}

	/**
	 * Test method for {@link rules.Rule_Manger#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(blueRule.invariant());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#Rule_Manger()}.
	 */
	@Test
	public void testRule_Manger() {
		assertTrue(blueRule.invariant());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#setRules(java.lang.String)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testSetRulesString() throws GameExceptions {
		((Rule_Manger) blueRule).setRules("goodrule.rules");
		((Rule_Manger) blueRule).showRules(true);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#setRules(int)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testSetRulesInt() throws GameExceptions {
		((Rule_Manger) blueRule).setRules(0);
		((Rule_Manger) blueRule).showRules(true);

		((Rule_Manger) blueRule).setRules(2);
		((Rule_Manger) blueRule).showRules(true);

		((Rule_Manger) blueRule).setRules(3);
		((Rule_Manger) blueRule).showRules(true);

		((Rule_Manger) blueRule).setRules(4);
		((Rule_Manger) blueRule).showRules(true);

	}

	/**
	 * Test for method {@link Rule_Manger#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 */
	@Test
	public void testGetMove() throws Exception {
		for (int i = 0; i < 100; i++) {

			((PlayerMathChess) blue).calculateMove(board);
			((PlayerMathChess) red).calculateMove(board);
			graphic.view();

			MoveInterface blueMove = blue.getMove(board);
			assertNotNull(blueMove);
			System.out.println(blueMove.toString());
			int dstMove = ((Board) board).getDestMove(blueMove);
			assertTrue(blue.makeMove(blueMove, board));
			if (dstMove != 0)
				((PlayerMathChess) red).updateChessmanCaught(dstMove);

			((PlayerMathChess) blue).calculateMove(board);
			((PlayerMathChess) red).calculateMove(board);
			graphic.view();

			MoveInterface redMove = red.getMove(board);
			assertNotNull(redMove);
			System.out.println(redMove.toString());
			int dstMove2 = ((Board) board).getDestMove(redMove);
			assertTrue(red.makeMove(redMove, board));
			if (dstMove2 != 0)
				((PlayerMathChess) blue).updateChessmanCaught(dstMove2);

		}
	}

	/**
	 * Test method for {@link rules.Rule_Manger#showRules(boolean)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testShowRules() throws GameExceptions {
		((Rule_Manger) blueRule).showRules(true);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#addRule(specifications.RuleInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testAddRule() throws GameExceptions {
		((Rule_Manger) blueRule).setRules(2);
		((Rule_Manger) blueRule).showRules(true);
		((Rule_Manger) blueRule).addRule(new Rule_Win());
		((Rule_Manger) blueRule).showRules(true);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#getRuleByName(java.lang.String)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testGetRuleByName() throws GameExceptions {
		((Rule_Manger) blueRule).setRules(2);
		System.out.println(((Rule_Manger) blueRule).getRuleByName(
				"If you can win then win").toString());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#getRuleByName(java.lang.String)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testGetRuleByNameNull() throws GameExceptions {
		((Rule_Manger) blueRule).setRules(2);
		assertNull(((Rule_Manger) blueRule)
				.getRuleByName("If you can win then winn"));
	}

}
