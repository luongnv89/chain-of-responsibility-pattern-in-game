/**
 * 
 */
package rules.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import mathchess.Move;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import rules.Rule_Win;
import specifications.MoveInterface;

/**
 * Test for class {@link Rule_Win}
 *
 */
public class Rule_WinTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		blueRule = new Rule_Win();
		redRule = new Rule_Win();
		setUpGame();
	}

	/**
	 * Test for method {@link Rule_Win#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveWinBycatchZero() throws Exception {

		//Create board state for test
		blue.makeMove(new Move(3, 0, 3, 6), board);
		blue.makeMove(new Move(3, 6, 3, 8), board);
		blue.makeMove(new Move(2, 0, 2, 7), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = blue.getMove(board);
		assertNotNull(blueMove);
		System.out.println(blueMove.toString());

	}

	/**
	 * Test for method {@link Rule_Win#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveWinByGetScore() throws Exception {

		((PlayerMathChess) blue).setMAX_LOST_SCORE(5);
		//Create board state for test
		blue.makeMove(new Move(4, 0, 5, 1), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = blue.getMove(board);
		assertNotNull(blueMove);
		System.out.println(blueMove.toString());

	}

	/**
	 * Test for method {@link Rule_Win#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveWinByNullCaughtPossible() throws Exception {

		((PlayerMathChess) blue).setMAX_LOST_SCORE(5);
		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = blue.getMove(board);
		assertNull(blueMove);
	}

	/**
	 * Test for method {@link Rule_Win#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveWinNullNoGetMaxScore() throws Exception {

		//Create board state for test
		blue.makeMove(new Move(4, 0, 5, 1), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = blue.getMove(board);
		assertNull(blueMove);
	}

}
