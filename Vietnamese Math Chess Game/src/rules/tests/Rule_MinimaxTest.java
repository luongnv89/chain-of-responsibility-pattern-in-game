/**
 * 
 */
package rules.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import rules.Rule_Minimax;
import utils.MessageConsole;

/**
 * Test for class {@link Rule_Minimax}
 *
 */
public class Rule_MinimaxTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		blueRule = new Rule_Minimax();
		redRule = new Rule_Minimax();

		setUpGame();
	}

	/**
	 * Test method for {@link rules.Rule_Minimax#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(blueRule.invariant());
		assertTrue(redRule.invariant());
	}

	/**
	 * Test method for {@link rules.Rule_Minimax#getDepth()}.
	 */
	@Test
	public void testGetDepth() {
		assertEquals(((Rule_Minimax) blueRule).getDepth(), 1);
	}

	/**
	 * Test method for {@link rules.Rule_Minimax#setDepth(int)}.
	 */
	@Test
	public void testSetDepth() {
		((Rule_Minimax) blueRule).setDepth(4);
		assertEquals(((Rule_Minimax) blueRule).getDepth(), 4);
	}

	/**
	 * Test for method {@link Rule_Minimax#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 */
	@Test
	public void testGetMove() throws Exception {
		MessageConsole.disableAllMessages();
		testGetMoves();
	}
}
