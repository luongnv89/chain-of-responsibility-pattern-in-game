package rules.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Rule_CatchTest.class, Rule_DefenceCatchTest.class,
		Rule_DefenceLostTest.class, Rule_MangerTest.class,
		Rule_MinimaxTest.class, Rule_RandomTest.class, Rule_WinTest.class })
public class AllRulesTests {

}
