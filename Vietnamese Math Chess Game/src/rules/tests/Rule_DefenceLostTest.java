/**
 * 
 */
package rules.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import mathchess.Move;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import rules.Rule_DefenceLost;
import rules.Rule_Win;
import specifications.MoveInterface;

/**
 * Test for {@link Rule_DefenceLost}
 *
 */
public class Rule_DefenceLostTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		blueRule = new Rule_DefenceLost();
		redRule = new Rule_DefenceLost();

		setUpGame(); 

	}

	/**
	 * Test for method {@link Rule_DefenceLost#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveDefenceLostBycatchZeroNull() throws Exception {

		//Create board state for test
		blue.makeMove(new Move(3, 0, 3, 6), board);
		blue.makeMove(new Move(3, 6, 3, 8), board);
		blue.makeMove(new Move(2, 0, 2, 7), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = red.getMove(board);
		assertNull(blueMove);
	}

	/**
	 * Test for method {@link Rule_DefenceLost#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveDefenceLostNull() throws Exception {

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = red.getMove(board);
		assertNull(blueMove);
	}

	/**
	 * Test for method {@link Rule_DefenceLost#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveDefenceLostBycatchZeroNotNull()
			throws Exception {

		//Create board state for test
		blue.makeMove(new Move(3, 0, 3, 6), board);
		blue.makeMove(new Move(3, 6, 3, 8), board);
		blue.makeMove(new Move(2, 0, 2, 7), board);
		red.makeMove(new Move(2, 10, 3, 9), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = red.getMove(board);
		assertNotNull(blueMove);
	}

	/**
	 * Test for method {@link Rule_Win#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveDefenceLostByGetScore() throws Exception {

		((PlayerMathChess) blue).setMAX_LOST_SCORE(5);
		//Create board state for test
		blue.makeMove(new Move(4, 0, 5, 1), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = red.getMove(board);
		assertNotNull(blueMove);
		System.out.println(blueMove.toString());

	}

}
