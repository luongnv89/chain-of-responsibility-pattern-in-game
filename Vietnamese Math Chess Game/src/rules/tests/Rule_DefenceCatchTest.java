/**
 * 
 */
package rules.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import mathchess.Move;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import rules.Rule_DefenceCatch;
import specifications.MoveInterface;

/**
 * Test for {@link Rule_DefenceCatch}
 *
 */
public class Rule_DefenceCatchTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		blueRule = new Rule_DefenceCatch();
		redRule = new Rule_DefenceCatch();
		setUpGame();
	}

	/**
	 * Test for method {@link Rule_DefenceCatch#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveDefenceCatchNullNoCatchToBreak()
			throws Exception {

		graphic.view();
		MoveInterface blueMove = red.getMove(board);
		assertNull(blueMove);
	}

	/**
	 * Test for method {@link Rule_DefenceCatch#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveDefenceCatchNotNull() throws Exception {

		blue.makeMove(new Move(4, 0, 5, 1), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = red.getMove(board);
		assertNotNull(blueMove);
		System.out.println(blueMove.toString());
	}
}
