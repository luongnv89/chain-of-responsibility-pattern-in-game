/**
 * 
 */
package rules.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import mathchess.Move;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import rules.Rule_Catch;
import specifications.MoveInterface;

/**
 * Test for {@link Rule_Catch}
 *
 */
public class Rule_CatchTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		blueRule = new Rule_Catch();
		redRule = new Rule_Catch();
		setUpGame();
	}

	/**
	 * Test for method {@link Rule_Catch#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveCatchNull() throws Exception {
		graphic.view();
		MoveInterface blueMove = blue.getMove(board);
		assertNull(blueMove);
	}

	/**
	 * Test for method {@link Rule_Catch#getMove(specifications.BoardInterface)}
	 * @throws Exception 
	 * 
	 * */
	@Test
	public void testGetMoveCatchNotNull() throws Exception {

		blue.makeMove(new Move(4, 0, 5, 1), board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		graphic.view();

		MoveInterface blueMove = blue.getMove(board);
		assertNotNull(blueMove);
		System.out.println(blueMove.toString());
	}

}
