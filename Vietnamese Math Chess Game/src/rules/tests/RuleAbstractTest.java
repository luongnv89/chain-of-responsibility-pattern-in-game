/**
 * 
 */
package rules.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import mathchess.AIPlayer;
import mathchess.Board;
import mathchess.GraphicConsole;
import mathchess.HumanPlayer;
import mathchess.PlayerMathChess;

import org.junit.After;
import org.junit.Test;

import rules.RuleAbstract;
import rules.Rule_Random;
import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.GameExceptions;

/**
 * test {@link RuleAbstract} class.
 *
 */
public abstract class RuleAbstractTest {

	protected BoardInterface board;
	protected GraphicConsole graphic;
	protected PlayerInterface blue;
	protected PlayerInterface red;
	protected RuleInterface blueRule;
	protected RuleInterface redRule;
	protected RuleInterface rule;

	public void setUpGame() throws Exception {
		blue = new AIPlayer("AI Blue player", true, blueRule);
		red = new AIPlayer("AI Red player", false, redRule);
		((AIPlayer) blue).setEnemy(red);
		((AIPlayer) red).setEnemy(blue);
 
		board = new Board();
		graphic = new GraphicConsole(board);

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
	}

	/**
			 * @throws java.lang.Exception
			 */
	@After
	public void tearDown() throws Exception {
		board = null;
		blue = null;
		red = null;
		blueRule = null;
		redRule = null;
		rule = null;
	}

	/**
	 * Test for method {@link RuleAbstract#setPlayer(PlayerInterface)}
	 * @throws GameExceptions 
	 * */
	@Test
	public void testSetPlayer() throws GameExceptions {
		rule = new Rule_Random();
		rule.setPlayer(blue);
	}

	/**
	 * Test for method {@link RuleAbstract#setPlayer(PlayerInterface)}
	 * @throws GameExceptions 
	 * */
	@Test(expected = GameExceptions.class)
	public void testSetPlayerExist() throws GameExceptions {
		rule = new Rule_Random();
		rule.setPlayer(blue);
		rule.setPlayer(red);
	}

	/**
	 * Test for method {@link RuleAbstract#setPlayer(PlayerInterface)}
	 * @throws GameExceptions 
	 * */
	@Test(expected = GameExceptions.class)
	public void testSetPlayerHumanPlayer() throws GameExceptions {
		HumanPlayer human = new HumanPlayer("Human", false);
		rule = new Rule_Random();
		rule.setPlayer(human);
	}

	/**
	 * Test for method {@link RuleAbstract#setEnemy(PlayerInterface)}
	 * @throws GameExceptions 
	 */
	@Test
	public void testSetEnemy() throws GameExceptions {
		rule = new Rule_Random();
		rule.setPlayer(blue);
		((RuleAbstract) rule).setEnemy(red);
	}

	/**
	 * Test for method {@link RuleAbstract#setEnemy(PlayerInterface)}
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testSetEnemyPlayerDoesntExists() throws GameExceptions {
		rule = new Rule_Random();
		((RuleAbstract) rule).setEnemy(red);
	}

	/**
	 * Test for method {@link RuleAbstract#setEnemy(PlayerInterface)}
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testSetEnemyExist() throws GameExceptions {
		rule = new Rule_Random();
		HumanPlayer human = new HumanPlayer("Human", false);
		rule.setPlayer(blue);
		((RuleAbstract) rule).setEnemy(red);
		((RuleAbstract) rule).setEnemy(human);
	}

	/**
	 * Test for method {@link RuleAbstract#setEnemy(PlayerInterface)}
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testSetEnemySameColor() throws GameExceptions {
		rule = new Rule_Random();
		((RuleAbstract) rule).setPlayer(red);
		((RuleAbstract) rule).setEnemy(red);
	}

	/**
	 * @throws Exception 
	 */
	protected void testGetMoves() throws Exception {
		for (int i = 0; i < 4; i++) {

			((PlayerMathChess) blue).calculateMove(board);
			((PlayerMathChess) red).calculateMove(board);
			graphic.view();

			MoveInterface blueMove = blue.getMove(board);
			assertNotNull(blueMove);
			System.out.println(blueMove.toString());
			assertTrue(blue.makeMove(blueMove, board));

			((PlayerMathChess) blue).calculateMove(board);
			((PlayerMathChess) red).calculateMove(board);
			graphic.view();

			MoveInterface redMove = red.getMove(board);
			assertNotNull(redMove);
			System.out.println(redMove.toString());
			assertTrue(red.makeMove(redMove, board));
		}
	}

}
