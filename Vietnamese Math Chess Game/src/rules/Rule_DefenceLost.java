/**
 * 
 */
package rules;

import java.util.ArrayList;

import mathchess.Board;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.GameExceptions;

/**
 * {@link Rule_DefenceLost} extends {@link RuleAbstract} represent a rule of AI player
 * <li> The AI player get the move can be done which defence the player become losser
 *
 */
public class Rule_DefenceLost extends RuleAbstract {
	
	public Rule_DefenceLost() {
		ruleName="If you can block win then block win";
	}

	/**
	 * The rule will look at all possible catching move of enemy player.
	 * <li> If The rule find a move of enemy can catch the Zero chessman of player, the player will find the move can break this move
	 * <li> If The rule find a move of enemy can catch the chessman of player and after the move has done, the player will be lost, the player will find the move can break this move
	 * @return
	 * <li> null if the list all possible catching move of enemy player is empty
	 * <li> null if there isn't any move of player can break the win-move of enemy player
	 * <li> a move can break the win-move of enemy player
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws GameExceptions {
		ArrayList<MoveInterface> listAllPossibleCaughtOfEnemy = new ArrayList<MoveInterface>();
		listAllPossibleCaughtOfEnemy.addAll(((PlayerMathChess) enemy)
				.getListAllPossibleCaught());
		if (listAllPossibleCaughtOfEnemy.isEmpty()) 
			return null;
		for (int i = 0; i < listAllPossibleCaughtOfEnemy.size(); i++) {
			MoveInterface move = listAllPossibleCaughtOfEnemy.get(i);
			int destMove = ((Board) board).getDestMove(move);
			//Lost the Zero chess-men by enemy
			if (destMove != 0 && destMove % 10 == 0) {
				return player.getDefenceMove(move, board, enemy);
			}

			//Win by get the MAX_SCORE 
			if ((((Board) board).getScore(enemy.isBlue()) + destMove) >= ((PlayerMathChess) enemy)
					.getMAX_LOST_SCORE())
				return player.getDefenceMove(move, board, enemy);
		}
		return null;
	}
}