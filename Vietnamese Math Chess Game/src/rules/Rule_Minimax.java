/**
 * 
 */
package rules;

import java.util.ArrayList;

import mathchess.Board;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import evaluator.MiniMaxCalculator;

/**
 * {@link Rule_Minimax} extend {@link RuleAbstract} represent the Minimax alpha beta cut rule of an AI Player.
 * <br> An AI Player will use {@link Rule_Minimax} to calculate move
 * 
 */
public class Rule_Minimax extends RuleAbstract {

	/**
	 * The depth of minimax alpha-beta cut algorithms.
	 */
	int depth;
	/**
	 * The calculator will calculate the value of board at a sate
	 */
	MiniMaxCalculator calculator;

	/**
	 * Create new {@link Rule_Minimax} with the value of depth is default
	 */
	public Rule_Minimax() {
		super();
		ruleName = "Using minimax algorithms";
		depth = 1;
	}

	/**
	 * Create new {@link Rule_Minimax} with the value of depth is input
	 * @param depth
	 */
	public Rule_Minimax(int depth) {
		super();
		this.depth = depth;
	}

	/**
	 * @return a move depend on the player using rule is a blue or red player
	 * <li> If the player using rule is a blue player, that means he is a max player
	 * <br> {@link Rule_Minimax#getMove(BoardInterface)} return a move of max player
	 * <li> If the player using rule is a red player, that means he is a min player
	 * <br> {@link Rule_Minimax#getMove(BoardInterface)} return a move of min player
	 * @throws Exception 
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws Exception {
		calculator = new MiniMaxCalculator(player, enemy, board);
		if (!calculator.invariant()) {
			throw new GameExceptions(this,
					"@Rule_Minimax#getMove. Calculator isn't invariant!");
		}
		calculator.resetCaculator();
		MessageConsole.debugMessage("Get move for: " + player.toString());
		calculator.addNewVisitedNode(board);
		int playerValue = player.isBlue() ? Integer.MIN_VALUE
				: Integer.MAX_VALUE;
		int moveIndex = 0;

		ArrayList<MoveInterface> listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(((PlayerMathChess) player)
				.getListAllPossibleCaught());
		listAllPossibleMoves.addAll(((PlayerMathChess) player)
				.getListAllPossibleMoves());

		for (int i = 0; i < listAllPossibleMoves.size(); i++) {
			MoveInterface currentMove = listAllPossibleMoves.get(i);
			int destMove = ((Board) board).getDestMove(currentMove);
			player.makeMove(currentMove, board);
			if (destMove != 0)
				((PlayerMathChess) enemy).updateChessmanCaught(destMove);
			if (!calculator.isVisited(board)) {
				calculator.addNewVisitedNode(board);
				((PlayerMathChess) player).calculateMove(board);
				((PlayerMathChess) enemy).calculateMove(board);
				if (player.isBlue()) {
					int maxValue = calculator.expandMinNode(depth - 1,
							playerValue);
					if (maxValue > playerValue) {
						playerValue = maxValue;
						moveIndex = i;
					}
				} else {
					int minValue = calculator.expandMaxNode(depth - 1,
							playerValue);
					if (minValue < playerValue) {
						playerValue = minValue;
						moveIndex = i;
					}
				}
			}
			enemy.undoLastMove(board);
			player.undoLastMove(board);
			board.undoLastMove();
		}
		// graphic.view();
		System.out.println("Depth: " + depth);
		System.out.println("Number of visited nodes: "
				+ calculator.getListVisitedNode().size());
		System.out.println("Number of calculated states: "
				+ MiniMaxCalculator.listCalculatedStates.size());
		return listAllPossibleMoves.get(moveIndex);
	}

	/**
	 * @return the depth
	 */
	public int getDepth() {
		return depth;
	}

	/**
	 * @param depth
	 *            the depth to set
	 */
	public void setDepth(int depth) {
		if (depth <= 0) {
			MessageConsole
					.inforMessage("Depth cannot less than 1. Depth will be default = 1");
		} else
			this.depth = depth;
	}

	@Override
	public boolean invariant() {
		if (depth < 1 || player.isBlue() == enemy.isBlue())
			return false;
		else
			return super.invariant();
	}
}
