/**
 * 
 */
package rules;

import mathchess.HumanPlayer;
import mathchess.PlayerMathChess;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.GameExceptions;

/**
 * {@link RuleAbstract} represent the rule which the AI {@link RuleAbstract#player} will use.
 * <br> To calculate move for a player, a Rule needs player, enemy 
 * 
 * 
 */
public abstract class RuleAbstract implements RuleInterface {

	/**
	 * The AI player will use this rule
	 */
	protected PlayerMathChess player;
	protected PlayerMathChess enemy;
	protected String ruleName;

	/**
	 * Set the AI player will use the rule
	 * @param player the player to set
	 */
	public void setPlayer(PlayerInterface player) throws GameExceptions {
		if (this.player != null) {
			throw new GameExceptions(this,
					"@RuleAbstract#setPlayer: The player of rule already exists");
		} else {
			if (player instanceof HumanPlayer) {
				throw new GameExceptions(this,
						"Cannot apply a playing rule for a Human player: "
								+ ((PlayerMathChess) player).getPlayerName());
			} else {
				this.player = (PlayerMathChess) player;
			}
		}
	}

	/*
	 * Check the invariant of rule. <li>return false if the rule doesn't have
	 * any AI player who will use it
	 */
	public boolean invariant() {
		return player != null && enemy != null;
	}

	/**
	 * Set the enemy player of rule
	 * @param otherPlayer the otherPlayer to set
	 * @throws GameExceptions 
	 */
	public void setEnemy(PlayerInterface enemy) throws GameExceptions {
		if (this.enemy != null) {
			throw new GameExceptions(this,
					"@RuleAbstract#setEnemy. The enemy player of rule already exists");
		} else {
			if (player == null) {
				throw new GameExceptions(this,
						"@RuleAbstract#setEnemy. The player of rule doesn't exist");
			} else {
				if (player.isBlue() != enemy.isBlue()) {
					this.enemy = (PlayerMathChess) enemy;
				} else {
					throw new GameExceptions(this,
							"@RuleAbstract#setEnemy. Two player are same color");
				}
			}
		}

	}

	/**
	 * @return the ruleName
	 */
	public String getRuleName() {
		return ruleName;
	}

}
