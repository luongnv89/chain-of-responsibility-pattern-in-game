/**
 * 
 */
package rules;

import java.util.ArrayList;

import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.MessageConsole;
import abstractions.PlayerAbstract;

/**
 * {@link Rule_DefenceCatch} extends {@link RuleAbstract} represent a rule of AI player
 * <li> AI player get the move to prevent the enemy catch some its chess-men 
 *
 */
public class Rule_DefenceCatch extends RuleAbstract {
	public Rule_DefenceCatch() {
		ruleName = "If you can block capture then block capture";
	}
	/**
	 * The rule will look at list catching moves of enemy player
	 * <br> The rule will get the move to break a catching move of enemy player
	 * @return <li> null if the list catching moves of enemy player is empty
	 * <li> null if there isn't any move can break all catching moves of enemy player
	 * <li> a move can break a catching move of enemy player
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws Exception {
		ArrayList<MoveInterface> listAllPossibleCaughtOfEnemy = new ArrayList<MoveInterface>();
		listAllPossibleCaughtOfEnemy.addAll(((PlayerMathChess) enemy)
				.getListAllPossibleCaught());
		if (listAllPossibleCaughtOfEnemy.isEmpty())
			return null;
		for (int i = 0; i < listAllPossibleCaughtOfEnemy.size(); i++) {
			MoveInterface move = ((PlayerMathChess) enemy)
					.getListAllPossibleCaught().get(i);

			MoveInterface moveDefence = player.getDefenceMove(move, board,
					enemy);
			if (moveDefence != null) {
				if (player.makeMoveSafe(moveDefence, board, enemy)) {
					MessageConsole.inforMessage(((PlayerAbstract) player)
							.getPlayerName()
							+ ": Defence catch : "
							+ moveDefence.toString());
					return moveDefence;
				}
			}
		}
		return null;
	}

}
