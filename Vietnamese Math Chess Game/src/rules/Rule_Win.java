/**
 * 
 */
package rules;

import mathchess.Board;
import mathchess.Game;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import abstractions.PlayerAbstract;

/**
 * {@link Rule_Win} extends {@link RuleAbstract} represent a rule of AI player
 * <li> The AI player get the move which can get the player become the winner
 *
 */
public class Rule_Win extends RuleAbstract {
	public Rule_Win() {
		ruleName = "If you can win then win";
	}

	/**
	 * @return
	 * <li> null if the player doesn't have any catching move possible
	 * <li> A move can catch the Zero chessman of enemy
	 * <li> A move can catch a chessman of enemy and after the move has done, the player will get the score >= {@link Game#getWinScore()}
	 * <li> null if there isn't any move can lead the player to win
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws GameExceptions {
		if (((PlayerMathChess) player).getListAllPossibleCaught().isEmpty())
			return null;
		else {
			for (int i = 0; i < ((PlayerMathChess) player)
					.getListAllPossibleCaught().size(); i++) {
				MoveInterface move = ((PlayerMathChess) player)
						.getListAllPossibleCaught().get(i);
				int destMove = ((Board) board).getDestMove(move);
				// Win by catch the Zero chess-men of enemy
				if (destMove != 0 && destMove % 10 == 0
						&& board.canMakeMove(move)) {
					MessageConsole.inforMessage(((PlayerAbstract) player)
							.getPlayerName()
							+ ": Win move by catch Zero: "
							+ move.toString());
					return move;
				}

				// Win by get the MAX_SCORE
				if ((((Board) board).getScore(player.isBlue()) + (destMove % 10)) >= ((PlayerMathChess) player)
						.getMAX_LOST_SCORE() && board.canMakeMove(move)) {
					MessageConsole
							.inforMessage(((PlayerAbstract) player)
									.getPlayerName()
									+ ": Win move: "
									+ move.toString());
					return move;
				}
			}
		}
		return null;
	}
}
