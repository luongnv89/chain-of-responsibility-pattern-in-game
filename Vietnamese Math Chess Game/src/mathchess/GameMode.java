/**
 * 
 */
package mathchess;

	/**
	 * Game mode
	 * <br> ZERO_CHECK: game finish when one zero chess-man is checked and cannot defence
	 * <br> MAX_SCORE: game finish when one player get score >= MAX_SCORE
	 * <br> in this case, player has higher score is the winner
	 *
	 *
	 */
public enum GameMode {
	ZERO_CHECK, MAX_SCORE,
}
