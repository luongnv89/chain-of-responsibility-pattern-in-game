package mathchess;

import specifications.BoardInterface;
import specifications.PlayerInterface;
import utils.MessageConsole;
import abstractions.GameAbstract;
import abstractions.PlayerAbstract;

/**
 * {@link Game} Represent a math chess game
 * <li> The rule of game can be reference here: <a href="http://cotoan.vnvista.com/rules-en.html"> Game rules</a>
 * <li> The introduction about project reference here: <a href="https://docs.google.com/file/d/0B9EBg3UKBX8VMDlhcXhPVlNIbmc/edit">Presentation about project</a>
 *
 */
public class Game extends GameAbstract {
	/**
	 * The win score of game.
	 * <br> When one of two player get score equals or bigger than WIN_SCORE, the game is finish
	 */
	private int WIN_SCORE = 20;

	/**
	 * game mode
	 */
	private GameMode gameMode = GameMode.ZERO_CHECK;

	public Game(BoardInterface board, PlayerInterface bluePlayer,
			PlayerInterface redPlayer) {
		super(board, bluePlayer, redPlayer);
		// this.events = new Events(board);
		this.graphic = new GraphicConsole(board);
	}

	/**
	 * Get the Win score which if the player can get, the player will win
	 * @return {@link Game#WIN_SCORE}
	 */
	public int getWinScore() {
		return WIN_SCORE;
	}

	/**
	 * Set the win score of game
	 * @param wIN_SCORE the wIN_SCORE to set
	 */
	public void setWinScore(int wIN_SCORE) {
		WIN_SCORE = wIN_SCORE;
		((PlayerMathChess) this.bluePlayer).setMAX_LOST_SCORE(wIN_SCORE);
		((PlayerMathChess) this.bluePlayer).setMAX_LOST_SCORE(wIN_SCORE);
	}

	/**
	 * Get the game mode of game
	 * @return the gameMode
	 * @see GameMode
	 */
	public GameMode getGameMode() {
		return gameMode;
	}

	/**
	 * Set mode of game
	 * @param gameMode
	 * @see GameMode
	 */
	public void setGameMode(GameMode gameMode) {
		this.gameMode = gameMode;
	}

	/**
	 * @return true if:
	 * <li> currentPlayer can caught the Zero chessman of enemy
	 * <li> If gameMode is MAX_SCORE and the score of red player or blue player is bigger than {@link Game#WIN_SCORE}
	 * 
	 * */
	@Override
	public boolean isGameOver() {
		if (((PlayerMathChess) this.currentPlayer).canCatchZero((Board) board)) {
			MessageConsole.inforMessage(((PlayerAbstract) this.currentPlayer)
					.getPlayerName() + " win!");
			return true;
		}
		if (((Board) board).getScore(true) >= getWinScore()) {
			MessageConsole.inforMessage(((PlayerAbstract) bluePlayer)
					.getPlayerName() + " win!");
			return true;
		}
		if (((Board) board).getScore(false) >= getWinScore()) {
			MessageConsole.inforMessage(((PlayerAbstract) redPlayer)
					.getPlayerName() + " win!");
			return true;
		}
		return false;
	}

}
