/**
 * 
 */
package mathchess;

import java.util.Scanner;

import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.MessageConsole;

/**
 * Represent a human player. Human player wants to get a move, he needs to input coordinates from the keyboard
 * It extends from {@link PlayerMathChess}.
 */
public class HumanPlayer extends PlayerMathChess {

	/**
	 * constructor with 
	 * @param playerName is the name of player
	 * @param blue is to define player is the blue one or the red one:
	 *<br> is the blue one if blue = true, 
	 *<br> is the red one if blue = false.
	 */
	public HumanPlayer(String playerName, boolean blue) {
		super(playerName, blue);
	}

	@Override
	public MoveInterface getMove(BoardInterface board) {
		return readInput(board);
	}

	/**
	 * Read the input for a move from keyboard
	 * @return 
	 */
	private MoveInterface readInput(BoardInterface board) {
		MoveInterface move = null;
		Scanner input = new Scanner(System.in);
		int x1, x2, chessman, y1, y2;
		boolean possibleMove = false;
		while (!possibleMove) {
			System.out.print("x1 = ");
			x1 = input.nextInt();
			System.out.print("y1 = ");
			y1 = input.nextInt();
			System.out.print("x2 = ");
			x2 = input.nextInt();
			System.out.print("y2 = ");
			y2 = input.nextInt();

			chessman = findIndexOfChessman(board.getState(x1, y1));
			if (chessman != -1) {
				move = new Move(x1, y1, x2, y2);
				possibleMove = board.canMakeMove(move);
			} else {
				MessageConsole.inforMessage(this.getPlayerName()
						+ " don't have any chessman at (" + x1 + ", " + y1
						+ ")!");
				possibleMove = false;
			}
			if (!possibleMove) {
				MessageConsole.inforMessage("Please input again!");
			}
		}
		return move;
	}

}
