/**
 * 
 */
package mathchess;

import specifications.BoardInterface;
import abstractions.GraphicAbstract;

/**
 * {@link GraphicConsole} extends from {@link GraphicAbstract} display the state of board on the console 
 *
 */
public class GraphicConsole extends GraphicAbstract {

	public GraphicConsole(BoardInterface board) {
		super(board);
	}

	@Override
	public void view() {
		for (int i = -1; i < board.getHeight(); i++) {
			if (i == -1) {
				showTopRows();
			} else {
				for (int j = -1; j < board.getWidth(); j++) {
					showAPiece(i, j);
				}
			}
			System.out.println();
		}
	}

	/**
	 * Represent the piece at (i,j)
	 * @param i
	 * @param j
	 * 
	 */
	private void showAPiece(int i, int j) {
		if (j == -1) {
			if (i != 10)
				System.out.print(i + "   ");
			else {
				System.out.print(i + "  ");
			}

		} else {
			int state = board.getState(j, i);
			System.out.print(showByValue(state));
		}
	}

	/**
	 * Show the top rows which represent the column index of board
	 */
	private void showTopRows() {
		for (int j = -1; j < board.getWidth(); j++) {
			if (j != -1)
				System.out.print(j + "  ");
			else
				System.out.print(j + "  ");
		}
		System.out.println();
	}

	/**
	 * Represent a piece of board by state of them
	 * @param state
	 * @return
	 * <li> "x  " if the piece is an empty piece
	 * <li> the number represent for the value of chess-men of player
	 * <li> If the chess-men is a chess-man of red player, it will be represented by a number and a *
	 */
	private String showByValue(int state) {
		if (state == 0)
			return "x  ";
		else if (state <= 10) {
			int value = state % 10;
			return value + "  ";
		} else {
			int value = state % 10;
			return value + "* ";
		}
	}

}
