/**
 * 
 */
package mathchess;

import specifications.HasInvariant;

/**
 * {@link Piece} represent one piece on the board.
 * Each piece has two coordinates x & y
 */
public class Piece implements HasInvariant {
	private int x, y;

	/**
	 * constructor with 2 integers as parameters
	 * @param x
	 * @param y
	 */
	public Piece(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Get the coordinate of piece
	 * @param index
	 * @return <li> x coordinate if index = 0
	 * <li> y coordinate if index = 1
	 * @throws IllegalArgumentException if index !=0 && index !=1
	 */
	public int getCoordinate(int index) throws IllegalArgumentException {
		if (index < 0 || index > 1)
			throw new IllegalArgumentException(
					"@Piece#getCoordinate: Invalid index. Index must be in 0 or 1. Index : "
							+ index);
		if (index == 0)
			return x;
		else
			return y;
	}

	/**
	 * @param other
	 * @return true if x and y of both {@link Piece}
	 */
	public boolean equals(Piece other) {
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public boolean invariant() {
		return true;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}

}
