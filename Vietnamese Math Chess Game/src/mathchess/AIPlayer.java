/**
 * 
 */
package mathchess;

import rules.RuleAbstract;
import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.GameExceptions;

/**
 *{@link AIPlayer} extends {@link PlayerMathChess} represent an AI player in Vietnammese Math Chess game
 *<br>AI player have a rule to calculate move
 */
public class AIPlayer extends PlayerMathChess {

	RuleInterface rule;

	public AIPlayer(String playerName, boolean blue, RuleInterface rule)
			throws IllegalArgumentException, GameExceptions {
		super(playerName, blue);
		if (rule == null)
			throw new IllegalArgumentException(
					"@AIplayer#Constructor: Rule is null");
		this.rule = rule;
		this.rule.setPlayer(this);
	}

	@Override
	public MoveInterface getMove(BoardInterface board) throws Exception {
		if (rule.invariant())
			return rule.getMove(board);
		else {
			throw new GameExceptions(this,
					"@AIPlayer#getMove: Rule isn't invariant");
		}
	}

	/**
	 * Set enemy for player
	 * @param enemy
	 * @throws GameExceptions 
	 */
	public void setEnemy(PlayerInterface enemy) throws GameExceptions {
		((RuleAbstract) rule).setEnemy(enemy);
	}
}
