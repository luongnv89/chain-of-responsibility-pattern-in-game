/**
 * 
 */
package mathchess;

import specifications.MoveInterface;

/**
 * {@link SavedMove} represent a move has done by player.
 * <br> Use MoveSaved to undo the last move of game
 *
 */
public class SavedMove {

	/**
	 * The last move has done
	 */
	private MoveInterface move;
	/**
	 * The destination state of last move before the move has done
	 */
	private int dstMove;

	/**
	 * @param move
	 * @param dstMove
	 */
	public SavedMove(MoveInterface move, int dstMove) {
		this.move = move;
		this.dstMove = dstMove;
	}

	/**
	 * @return the move
	 */
	public MoveInterface getMove() {
		return move;
	}

	/**
	 * @return the dstMove
	 */
	public int getDstMove() {
		return dstMove;
	}

}
