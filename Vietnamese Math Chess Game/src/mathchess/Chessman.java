package mathchess;

import java.util.ArrayList;

import specifications.BoardInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import abstractions.BoardAbstract;
import abstractions.ChessmanAbstract;

/**
 * {@link Chessman} extends {@link ChessmanAbstract} represent a chessman in Vietnammese Math Chess game
 *
 */
public class Chessman extends ChessmanAbstract {

	/**
	 * List possible move can be done by chess-man
	 */
	private ArrayList<Move> listMove = new ArrayList<Move>();
	/**
	 * List catch can be done by chess-man 
	 */
	private ArrayList<Move> listCatch = new ArrayList<Move>();
	/**
	 * List trap create by chess-man
	 */
	private ArrayList<Move> listTrap = new ArrayList<Move>();

	public Chessman(int value, int x, int y) {
		super(value, x, y);
	}

	/**
	 * Reset list possible moves of chessman before calculate move on new board state
	 */
	public void resetChessman() {
		listMove.clear();
		listTrap.clear();
		listCatch.clear();
	}

	/**
	 * Check the chess-man is Zero or not 
	 * @return true if chess-man is Zero chess-man
	 */
	public boolean isZero() {
		return getValue() != 0 && getValue() % 10 == 0;
	}

	/**
	 * Check valid of a chess-man
	 * @return false if these case:
	 * <br>the value of chess-man is out of value of chess-man in game
	 * <br>if chess-man is Zero chess-man
	 * <br>if chess-man is at (4,1) or (4,9) (isZero=true) but the value isn't 0
	 * <br>if the position (x,y) of chess-man is out of board
	 * 
	 */
	@Override
	public boolean invariant() {
		// Check value
		if (getValue() < 1 || getValue() > 20)
			return false;

		// Check the position of Zero chess-man
		if (isZero()) {
			if (x != 4)
				return false;
			else if ((y != 1) && y != 9)
				return false;
		}

		return true;
	}

	/**
	 * Get list possible move of chessman
	 * @param index
	 * @return <li> list possible move of chessman if index =0
	 * <li> list all the trap of chessman if index =1.
	 * <br><b>A trap</b> of a chessman is a position on a board where if the chessman of enemy go there, it can be caught by chessman
	 * <li> list all catch possible of chessman if index =2 
	 * @throws IllegalArgumentException if the input index isn't in {0,1,2}
	 */
	public ArrayList<Move> getListMove(int index)
			throws IllegalArgumentException {
		if (index < 0 || index > 2)
			throw new IllegalArgumentException(
					"@Chessman#getListMove: Index is invalid. Index must be in {0,1,2}. Input index: "
							+ index);
		if (index == 0)
			return listMove;
		else if (index == 1)
			return listTrap;
		else
			return listCatch;
	}

	/**
	 * Calculate moves of a chessman on a board
	 * <li> Calculate all possible normal move of chessman - (move to empty piece)
	 * <li> Calculate all possible catch of chessman - (move and catch another chessman of enemy player)
	 * <li> Calculate all "trap" of chessman - pieces on the board where if enemy chessmen move to there, enemy chessmen can be caught by chessman
	 * <li> If the chessman is Zero chessman, it cannot be moved.
	 * <li> if the value of chessman is different with the state of board at the position of chessman, return exception
	 * @param board
	 * @throws Exception 
	 */
	public void calculateMove(Board board) throws Exception {
		resetChessman();
		if (getValue() % 10 == 0) {
			MessageConsole
					.debugMessage("This is the Zero chess-man. Cannot move");
		} else if (board.getState(x, y) != getValue()) {
			throw new GameExceptions(this,
					"@Chessman#calculateMove: Chessman isn't on the board "
							+ toString());
		} else {
			calculateMoveInColumn(board, false);
			calculateMoveInColumn(board, true);
			calculateMoveOnRow(board, true);
			calculateMoveOnRow(board, false);
			calculateMoveOnDiagonal(board, true, true);
			calculateMoveOnDiagonal(board, false, false);
			calculateMoveOnDiagonal(board, true, false);
			calculateMoveOnDiagonal(board, false, true);
		}
	}

	/**
	 * Calculate all possible moves of chessman on a row of board
	 * 
	 * @param board
	 * @param leftToRight
	 * <li> LeftToRight is true, calculate all possible moves of chessman on a row of board from left to right
	 * <li> LeftToRight is false, calculate all possible moves of chessman on a row of board from right to left
	 * <br> Only calculate all possible moves of chessman on the path if the adjacent piece of chessman (on the path which is considering) is inside the board
	 * <li> if the adjacent piece of chessman is an empty piece, calculate all possible normal moves of chessman (chessman move to an empty piece) by {@link Chessman#calculateNextMove(int, int, BoardInterface)}
	 * <li> If the adjacent piece of chessman is a chessman same player, calculate all the trap of chessman by {@link Chessman#calculateTrapOnRows(Board, boolean)}
	 */
	private void calculateMoveOnRow(Board board, boolean leftToRight) {
		int index = leftToRight ? 1 : (-1);
		if (board.insideBoard(x + index, y)) {
			int adValue = board.getState(x + index, y);
			//Get normal move
			if (board.isEmpty(x + index, y)) {
				int i = 1;
				boolean nonStop = true;
				while (nonStop) {
					nonStop = calculateNextMove(x + i * index, y, board);
					i++;
				}
			}
			//Get a catch or trap
			else {
				if (board.samePlayer(adValue, getValue())) {
					calculateTrapOnRows(board, leftToRight);
				}
			}
		}

	}

	/**
	 * @param board
	 * @param leftToRight
	 */
	private void calculateTrapOnRows(Board board, boolean leftToRight) {
		if (leftToRight) {
			for (int i = x + 2; i <= 8; i++) {
				boolean next = calculateTrap(i, y, board);
				if (!next)
					break;
			}
		} else {
			for (int i = x - 2; i >= 0; i--) {
				boolean next = calculateTrap(i, y, board);
				if (!next)
					break;
			}
		}
	}

	/**
	 * Calculate all possible moves of chessman on a row of board
	 * 
	 * @param board
	 * @param leftToRight
	 * <li> xIncrease is true and yIncrease is true, calculate all possible moves of chessman on a diagonal of board from left-top to right-bottom
	 * <li> xIncrease is false and yIncrease is false, calculate all possible moves of chessman on a diagonal of board from right-bottom to left-top 
	 * <li> xIncrease is true and yIncrease is false, calculate all possible moves of chessman on a diagonal of board from left-bottom to right-top
	 * <li> xIncrease is false and yIncrease is true, calculate all possible moves of chessman on a diagonal of board from right-top to left-bottom
	 * <br> Only calculate all possible moves of chessman on the path if the adjacent piece of chessman (on the path which is considering) is inside the board
	 * <li> if the adjacent piece of chessman is an empty piece, calculate all possible normal moves of chessman (chessman move to an empty piece) by {@link Chessman#calculateNextMove(int, int, BoardInterface)}
	 * <li> If the adjacent piece of chessman is a chessman same player, calculate all the trap of chessman by {@link Chessman#calculateTrapOnDiagonal(Board, int, int)}
	 * 
	 */
	private void calculateMoveOnDiagonal(Board board, boolean xIncrease,
			boolean yIncrease) throws Exception {
		int xIndex = xIncrease ? 1 : (-1);
		int yIndex = yIncrease ? 1 : (-1);
		if (board.insideBoard(x + xIndex, y + yIndex)) {
			int adValue = board.getState(x + xIndex, y + yIndex);
			//Get normal move
			if (board.isEmpty(x + xIndex, y + yIndex)) {
				int i = 1;
				boolean nonStop = true;
				while (nonStop) {
					nonStop = calculateNextMove(x + i * xIndex, y + i * yIndex,
							board);
					i++;
				}
			}
			//Get a catch or trap
			else {
				if (board.samePlayer(adValue, getValue())) {
					calculateTrapOnDiagonal(board, xIncrease, yIncrease);
				}
			}
		}

	}

	/**
	 * Calculate all traps of chessman on diagonal of board by:
	 * <li> {@link Chessman#calculateTrapOnDiagonalLTRB(Board)} if xIncrease is true and yIncrease is true
	 * <li> {@link Chessman#calculateTrapOnDiagonalRBLT(Board)} if xIncrease is false and yIncrease is false
	 * <li> {@link Chessman#calculateTrapOnDiagonalLBRT(Board)(Board)} if xIncrease is true and yIncrease is false
	 * <li> {@link Chessman#calculateTrapOnDiagonalRTLB(Board)(Board)} if xIncrease is false and yIncrease is true
	 * @param board
	 * @param xIncrease
	 * @param yIncrease
	 * @throws Exception 
	 */
	private void calculateTrapOnDiagonal(Board board, boolean xIncrease,
			boolean yIncrease) throws Exception {
		int xIndex = xIncrease ? 1 : (-1);
		int yIndex = yIncrease ? 2 : (-2);
		int value = xIndex + yIndex;
		switch (value) {
		case 3:
			//Left-top to Right-bottom
			calculateTrapOnDiagonalLTRB(board);
			break;
		case -3:
			//Right-bottom to left-top
			calculateTrapOnDiagonalRBLT(board);
			break;
		case -1:
			//Left-bottom to right-top
			calculateTrapOnDiagonalLBRT(board);
			break;
		case 1:
			//Right-top to left-bottom
			calculateTrapOnDiagonalRTLB(board);
			break;

		default:
			throw new Exception("@Chessman#calculateTrapOnDiagonal: Exception!");
		}

	}

	/**
	 * Calculate all traps of chessman on diagonal path from right-top to left-bottom on the board
	 * @param board
	 */
	private void calculateTrapOnDiagonalRTLB(Board board) {
		for (int i = x - 2, j = y + 2; i >= 0 && j <= 8; i--, j++) {
			if (!calculateTrap(i, j, board))
				break;
		}
	}

	/**
	 * Calculate all traps of chessman on diagonal path from left-bottom to right-top on the board
	 * @param board
	 */
	private void calculateTrapOnDiagonalLBRT(Board board) {
		for (int i = x + 2, j = y - 2; i <= 8 && j >= 0; i++, j--) {
			if (!calculateTrap(i, j, board))
				break;
		}
	}

	/**Calculate all traps of chessman on diagonal path from right-bottom to left-top on the board
	 * @param board
	 */
	private void calculateTrapOnDiagonalRBLT(Board board) {
		for (int i = x - 2, j = y - 2; i >= 0 && j >= 0; i--, j--) {
			if (!calculateTrap(i, j, board))
				break;
		}
	}

	/**
	 * Calculate all traps of chessman on diagonal path from left-top to right-bottom on the board
	 * @param board
	 */
	private void calculateTrapOnDiagonalLTRB(Board board) {
		for (int i = x + 2, j = y + 2; i <= 8 && j <= 10; i++, j++) {
			if (!calculateTrap(i, j, board))
				break;
		}
	}

	/**
	 * Calculate all possible moves of chessman in a column of board
	 * 
	 * @param board
	 * @param isUp
	 * <li> isUp is true, calculate all possible moves of chessman in a column of board from bottom to top
	 * <li> isUp is false, calculate all possible moves of chessman in a column of board from top to bottom
	 * <br> Only calculate all possible moves of chessman on the path if the adjacent piece of chessman (on the path which is considering) is inside the board
	 * <li> if the adjacent piece of chessman is an empty piece, calculate all possible normal moves of chessman (chessman move to an empty piece) by {@link Chessman#calculateNextMove(int, int, BoardInterface)}
	 * <li> If the adjacent piece of chessman is a chessman same player, calculate all the trap of chessman by {@link Chessman#calculateTrapInColumn(Board, int)}
	 */
	private void calculateMoveInColumn(Board board, boolean isUp) {
		int index = isUp ? (-1) : 1;
		if (board.insideBoard(x, y + index)) {
			int adValue = board.getState(x, y + index);
			//Get normal move
			if (board.isEmpty(x, y + index)) {
				int i = 1;
				boolean nonStop = true;
				while (nonStop) {
					nonStop = calculateNextMove(x, y + i * index, board);
					i++;
				}
			}
			//Get a catch or trap
			else {
				if (board.samePlayer(adValue, getValue())) {
					calculateTrapInColumn(board, isUp);
				}
			}
		}

	}

	/**
	 * Calculate all traps of chessman in a colum
	 * @param board
	 * @param isUp
	 * <li> if isUp is true, calculate all traps of chessman in a column from bottom to top
	 * <li> if isUp is false, calculate all traps of chessman in a column from top to bottom
	 */
	private void calculateTrapInColumn(Board board, boolean isUp) {
		if (isUp) {
			//Go up
			for (int i = y - 2; i >= 0; i--) {
				boolean next = calculateTrap(x, i, board);
				if (!next)
					break;
			}
		} else {
			//Go down
			for (int i = y + 2; i <= 10; i++) {
				boolean next = calculateTrap(x, i, board);
				if (!next)
					break;
			}
		}
	}

	/**
	 * Check the position (x2,y2)
	 * <li> return false -> don't continue considering the next trap
	 * @param x2
	 * @param y2
	 * @param board
	 * @return <li> (x2,y2) is out of board, return false;
	 * <li> (x2,y2) is a trap position and (x2,y2) is empty, add new trap of chessman, go to next position
	 * <li> (x2,y2) is a trap position and (x2,y2) is the position of enemy player chessman , add new catch of chessman and return false;
	 * <li> if (x2,y2) isn't empty, return false;
	 */
	private boolean calculateTrap(int x2, int y2, Board board) {
		if (!board.insideBoard(x2, y2))
			return false;
		//Consider special positions
		if (board.isATrap(x, y, x2, y2)) {
			if (board.isEmpty(x2, y2)) {
				listTrap.add(new Move(x, y, x2, y2));
				return true;
			} else {
				int dstMove = board.getState(x2, y2);
				if (!board.samePlayer(getValue(), dstMove))
					listCatch.add(new Move(x, y, x2, y2));
				return false;
			}
		}
		if (!board.isEmpty(x2, y2))
			return false;
		return true;
	}

	/**
	 * Check the position (x,y) can be the destination move of chessman
	 * @param x
	 * @param i
	 * @param board
	 * @return <li> if the position (x,y) is out of board, return false;
	 * <li> if the position (x,y) isn't empty, return false;
	 * <li> if the number of steps from (x,y) to the position of chessman is bigger than the value of chessman return false;
	 */
	private boolean calculateNextMove(int x2, int y2, BoardInterface board) {
		int numberOfStep = ((Board) board).getNumberOfSteps(x, y, x2, y2);
		if (((BoardAbstract) board).insideBoard(x2, y2)
				&& ((BoardAbstract) board).isEmpty(x2, y2)
				&& numberOfStep <= (getValue() % 10)) {
			listMove.add(new Move(x, y, x2, y2));
			return true;
		}
		return false;
	}

}
