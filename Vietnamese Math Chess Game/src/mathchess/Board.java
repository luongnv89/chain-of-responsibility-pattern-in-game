package mathchess;

import java.util.ArrayList;

import specifications.MoveInterface;
import utils.MessageConsole;
import abstractions.BoardAbstract;

/**
 * extends {@link BoardAbstract} class.
 * represents the board for only Vietnamese mathematical chess.
 * The board has 9x11 pieces (width = 9, height = 11).
 */
public class Board extends BoardAbstract {

	/**
	 * list all the successful move on the board
	 */
	public ArrayList<SavedMove> historyMoves;
	/**
	 * The default width of Vietnammese mathematical chess.
	 */
	final public static int WIDTH = 9;
	/**
	 * The default height of Vietnammese mathematical chess.
	 */
	final public static int HEIGHT = 11;

	/**
	 * constructor default to initialize board's width and height and
	 * set state for all positions on the board.
	 */
	public Board() {
		super(WIDTH, HEIGHT);
		this.initial();
		historyMoves = new ArrayList<SavedMove>();
	}

	/**
	 * Check the invariant of a mathchess board game
	 * 
	 * @return false if:
	 * <li> There are some positions on the board have state is out of range of value (0-20)
	 * <li> There are two chessman same value on the board
	 */
	@Override
	public boolean invariant() {

		ArrayList<Integer> listValue = new ArrayList<Integer>();

		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				int value = getState(i, j);

				// Check the range of value of chessman
				if (value < 0 || value > 20) {
					System.out.println("The value is out of range at (" + i
							+ ", " + j + "): " + value);
					return false;
				}
				// Check if board has two chessmans same value (except the blank
				// piece)
				else if (value != 0) {
					if (listValue.contains(value)) {
						System.out
								.println("There are two chessman same value: "
										+ value);
						return false;
					} else
						listValue.add(value);
				}
			}
		}

		return true;

	}

	/**
	 * Check an empty path from (x1,y1) to (x2,y2) (except the position (x1,y1) and (x2,y2))
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return true if there isn't any chess-man in the path (except the
	 * position (x1,y1) and (x2,y2))
	 * <br> false if:
	 * <li> The position (x1,y1) or (x2,y2) is out of board
	 * <li> The path isn't a correct path
	 * <br>
	 * <li> If x1=x2, consider path in column x1 by {@link Board#isEmptyColumn(int, int, int)}
	 * <li> If y1=y2, consider path on row y1 by {@link Board#isEmptyRow(int, int, int)}
	 * <li> If x1+y1 = x2+y2, consider path on the diagonal from right-top to left-bottom of board by {@link Board#isEmptyCrossRTtoLB(int, int, int, int)}
	 * <li> If x1-x2 = y1-y2, consider path on the diagonal from right-bottom to left-top of board by {@link Board#isEmptyCrossRBtoLT(int, int, int, int)}
	 *         
	 */
	public boolean isEmptyPath(int x1, int y1, int x2, int y2) {

		int numberOfStep = getNumberOfSteps(x1, y1, x2, y2);
		int direction = getDirection(x1, y1, x2, y2);

		if (numberOfStep <= 0)
			return false;
		switch (direction) {
		case -1:
			return checkEmptyColumnBT(x1, y1, numberOfStep);

		case 1:
			return checkEmptyColumnTB(x1, y1, numberOfStep);

		case 2:
			return checkEmptyRowLR(x1, y1, numberOfStep);

		case -2:
			return checkEmptyRowRL(x1, y1, numberOfStep);

		case 3:
			return checkEmptyDiagonalLBRT(x1, y1, numberOfStep);

		case -3:
			return checkEmptyDiagonalRTLB(x1, y1, numberOfStep);

		case 4:
			return checkEmptyDiagonalLTRB(x1, y1, numberOfStep);

		case -4:
			return checkEmptyDiagonalRBLT(x1, y1, numberOfStep);

		default:
			MessageConsole.debugMessage("@Board#isEmptyPath: Wrong direction "
					+ direction);
			return false;
		}
	}

	/**
	 * Check the empty of path on a diagonal from right-bottom to left-top, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyDiagonalRBLT(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1 - i, y1 - i)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ (x1 - i) + ", " + (y1 - i) + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check the empty of path on a diagonal from left-top to right-bottom, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyDiagonalLTRB(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1 + i, y1 + i)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ (x1 + i) + ", " + (y1 + i) + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check the empty of path on a diagonal from right-top to left-bottom, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyDiagonalRTLB(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1 - i, y1 + i)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ (x1 - i) + ", " + (y1 + i) + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check the empty of a path on a diagonal from left-bottom to right-top, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyDiagonalLBRT(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1 + i, y1 - i)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ (x1 + i) + ", " + (y1 - i) + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check the empty of a path on a row from right to left, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyRowRL(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1 - i, y1)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ (x1 - i) + ", " + y1 + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check the empty of a path on a row from left to right, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyRowLR(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1 + i, y1)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ (x1 + i) + ", " + y1 + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check the empty of a path in a column from top to bottom, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyColumnTB(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1, y1 + i)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ x1 + ", " + (y1 + i) + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check the empty of a path in a column from bottom to top, start at (x1,y1) and move numberOfSteps steps 
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return <li> false if there are some non-empty positions on the path
	 * <li>true if there isn't any non-empty position on the path
	 */
	private boolean checkEmptyColumnBT(int x1, int y1, int numberOfStep) {
		for (int i = 1; i < numberOfStep; i++) {
			if (!isEmpty(x1, y1 - i)) {
				MessageConsole
						.debugMessage("@Board#isEmptyPath: There is a chess-man at ("
								+ x1 + ", " + (y1 - i) + ")");
				return false;
			}
		}
		return true;
	}

	/**
	 * Create new game board
	 * <li> The position of chess-men of blue player 
	 * <li> The position of chess-men of red player 
	 */
	@Override
	public void initial() {
		// Set initial state of player 1
		for (int i = 0; i < WIDTH; i++) {
			setState(i, 0, WIDTH - i);
		}
		setState(4, 1, 10);

		// Set initial state of player 2
		for (int i = 0; i < WIDTH; i++) {
			setState(i, HEIGHT - 1, HEIGHT + i);
		}
		setState(4, 9, 20);
	}

	/**
	 * The board make the move.
	 * <li> Update the board state at the source and destination positions of move
	 * <li> Save the move into the {@link Board#historyMoves}
	 * @return false if {@link Board#canMakeMove(MoveInterface)} return false
	 * <li> true in other case
	 * */
	@Override
	public boolean makeMove(MoveInterface move) {
		if (!canMakeMove(move))
			return false;
		else {
			int x1 = move.getCoordinate(0);
			int y1 = move.getCoordinate(1);
			int fromState = getState(x1, y1);

			int x2 = move.getCoordinate(2);
			int y2 = move.getCoordinate(3);
			int dstMove = getState(x2, y2);
			setState(x2, y2, fromState);
			setState(x1, y1, 0);
			historyMoves.add(new SavedMove(move, dstMove));
			return true;
		}
	}

	/**
	 * The board undo the last move
	 * <li> Get the last move from {@link Board#historyMoves}
	 * <li> Update the board state back the state before the last move was done
	 * <li> Remove the last move from {@link Board#historyMoves}
	 * @return
	 * <li> false If {@link Board#getLastMoveSaved()} return null
	 * <li> true in other case 
	 * 
	 * */
	@Override
	public boolean undoLastMove() {
		SavedMove moveSaved = getLastMoveSaved();
		if (moveSaved != null) {
			MoveInterface move = moveSaved.getMove();
			int x1 = move.getCoordinate(0);
			int y1 = move.getCoordinate(1);
			int dstMove = moveSaved.getDstMove();

			int x2 = move.getCoordinate(2);
			int y2 = move.getCoordinate(3);
			int srcMove = getState(x2, y2);
			setState(x1, y1, srcMove);
			setState(x2, y2, dstMove);
			historyMoves.remove(moveSaved);
			return true;
		}
		return false;
	}

	/**
	 * Get the last move on the board
	 * @return <li> null if there isn't any move has done
	 * <li> the last move on the board
	 */
	public SavedMove getLastMoveSaved() {
		if (!historyMoves.isEmpty()) {
			return historyMoves.get(historyMoves.size() - 1);
		} else {
			MessageConsole.inforMessage("History is empty!");
			return null;
		}
	}

	/**
	 * Calculate all the trap values from valueOfChessman1 and valueOfChessmanAdjacent
	 * <br> There are five possible values:
	 * <li> add : get the module of result with 10
	 * <li> multi: get the module of result with 10
	 * <li> minus : if valueOfChessman1 > valueOfChessmanAdjacent
	 * <li> div : If valueOfChessman1 > valueOfChessmanAdjacent and valueOfChessmanAdjacent isn't 10 and 20
	 * <li> mode :If valueOfChessmanAdjacent isn't 10 and 20
	 * <li> A trap value is bigger than 0 and smaller than 10.
	 * @param valueOfChessman1 the value of chess-man who make the move
	 * @param valueOfChessmanAdjacent the value of adjacent chess-man 
	 * @return <li> List all trap values
	 */
	private ArrayList<Integer> calculateTrapValues(int valueOfChessman1,
			int valueOfChessmanAdjacent) {
		ArrayList<Integer> listTrapValue = new ArrayList<Integer>();
		// Add operator
		listTrapValue.add((valueOfChessman1 + valueOfChessmanAdjacent) % 10);
		// Multi operator
		listTrapValue.add((valueOfChessman1 * valueOfChessmanAdjacent) % 10);
		// Minus operator
		listTrapValue.add(valueOfChessman1 - valueOfChessmanAdjacent);

		if (valueOfChessmanAdjacent != 0) {
			// Div operator
			listTrapValue.add(valueOfChessman1 / valueOfChessmanAdjacent);
			// Module operator
			listTrapValue.add(valueOfChessman1 % valueOfChessmanAdjacent);
		}

		//filter the trap values
		for (int i = 0; i < listTrapValue.size(); i++) {
			if (listTrapValue.get(i) <= 0)
				listTrapValue.remove(i);
		}
		return listTrapValue;
	}

	/**
	 * Check two chess-mans has value chessman12 and chessman22 belong to same
	 * player
	 * 
	 * @param chessman12
	 * @param chessman22
	 * @return false if:
	 * <li> chessman12 is out of range of value
	 * <li> chessman22 is out of range of value
	 * <li> chessman12 is smaller than or equal 10 but the chessman22 is bigger than 10
	 * <li> chessman22 is smaller than or equal 10 but the chessman12 is bigger than 10
	 * <br> true in other cases
	 */
	public boolean samePlayer(int chessman12, int chessman22) {
		if (chessman12 < 0 || chessman12 > 20)
			return false;

		if (chessman22 < 0 || chessman22 > 20)
			return false;

		if (chessman12 <= 10 && chessman22 > 10)
			return false;
		if (chessman12 > 10 && chessman22 <= 10)
			return false;
		return true;
	}

	/**
	 * Get the score of player.
	 * <br> It is calculated by the total value of chess-men which the player caught from enemy
	 * @param blueScore
	 * @return <li> The score of blue player if blueScore is true
	 * <li> The score of red player if blueScore is false
	 */
	public int getScore(boolean blueScore) {
		int score = 0;
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				//Total value of chess-men of red player
				if (blueScore) {
					if (getState(i, j) > 10)
						score += getState(i, j) % 10;
				}
				//Total value of chess-men of blue player
				else {
					if (getState(i, j) < 10)
						score += getState(i, j) % 10;
				}
			}
		}
		return 45 - score;
	}

	/**
	 * Check a move can be done on a math chess game board
	 * @return false if:
	 * <li> The source position is out of board
	 * <li> The destination position is out of board
	 * <li> The source position is empty -> there isn't any chess-man will be moved.
	 * <li> Move the zero chess-man
	 * <li> The path of move isn't valid 
	 * <br> If the destination of move is empty, return {@link Board#canMakeNormalMove(MoveInterface)}
	 * <li> If the destination of move isn't empty, return {@link Board#canMakeCatch(MoveInterface)}
	 * */
	public boolean canMakeMove(MoveInterface move) {

		int x1 = move.getCoordinate(0);
		int y1 = move.getCoordinate(1);
		int x2 = move.getCoordinate(2);
		int y2 = move.getCoordinate(3);

		if (!insideBoard(x1, y1)) {
			MessageConsole
					.debugMessage("@Board#canMakeMove: The source position is out of board. ("
							+ x1 + ", " + y1 + ")");
			return false;
		}

		if (!insideBoard(x2, y2)) {
			MessageConsole
					.debugMessage("@Board#canMakeMove: The destination position is out of board. ("
							+ x2 + ", " + y2 + ")");
			return false;
		}

		int fromState = getState(x1, y1);

		int toState = getState(x2, y2);

		if (isEmpty(x1, y1)) {
			MessageConsole
					.debugMessage("@Board#canMakeMove: There is no chess-man to move!");
			return false;
		}

		if (fromState != 0 && fromState % 10 == 0) {
			MessageConsole
					.debugMessage("@Board#canMakeMove: Cannot move the Zero chess-man. Value of chess-man wants to move: "
							+ fromState);
			return false;
		}
		if (getDirection(x1, y1, x2, y2) == 0) {
			MessageConsole
					.debugMessage("@Board#canMakeMove: The path of move isn't valid. Move: "
							+ move.toString());
			return false;
		}

		if (toState == 0)
			return canMakeNormalMove(x1, y1, x2, y2);
		else
			return canMakeCatch(x1, y1, x2, y2);

	}

	/**
	 * Check the position (x2,y2) can be a trap of chess-man who is the position (x1,y1)
	 * <br> A trap of a chessman A: - The position on board where if the enemy's chess-man is there, it can be caught by chess-man A
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return<li> false if {@link Board#getAdjacientPieceOnPath(int, int, int, int)} return NULL
	 * <li> false if there isn't any adjacent - same player chess-man of moved chess-man on the path (x1,y1) to (x2,y2)
	 * <li> false if the the path from the position of adjacent chess-man of moved chess-man to (x2,y2) isn't an empty path 
	 * <li> false if the distance from the position of adjacent chess-man of moved chess-man to (x2,y2) isn't a trap value of move chess-man
	 * <li> true in other case
	 */
	public boolean isATrap(int x1, int y1, int x2, int y2) {
		int movedChessman = getState(x1, y1);
		Piece adPiece = getAdjacientPieceOnPath(x1, y1, x2, y2);
		if (adPiece == null) {
			MessageConsole
					.debugMessage("@Board#canMakeCatch:@Board#getAdjacientPieceOnPath return NULL ");
			return false;
		}

		int adX = adPiece.getCoordinate(0);
		int adY = adPiece.getCoordinate(1);

		if (isEmpty(adX, adY)) {
			MessageConsole
					.debugMessage("@Board#canMakeCatch:@Board#Adjacent piece is empty ");
			return false;
		}

		int adjChessman = getState(adX, adY);

		if (!samePlayer(movedChessman, adjChessman)) {
			MessageConsole
					.debugMessage("@Board#canMakeCatch: There isn't any adjacent - same player chess-man of moved chess-man on the path "
							+ "\nChessman 1: "
							+ movedChessman
							+ "\nAdjacent state of chessman 1:" + adPiece);
			return false;
		}

		if (!isEmptyPath(adX, adY, x2, y2)) {
			MessageConsole
					.debugMessage("@Board#canMakeCatch: the the path from the position of adjacent chess-man "
							+ adPiece.toString()
							+ " of moved chess-man to ("
							+ x2 + ", " + y2 + ") isn't an empty path");
			return false;
		}

		int numberOfSteps = getNumberOfSteps(x1, y1, x2, y2) - 1;
		ArrayList<Integer> listTrapValues = calculateTrapValues(movedChessman,
				adjChessman);
		if (!listTrapValues.contains(numberOfSteps)) {
			MessageConsole
					.debugMessage("@Board#canMakeCatch: the distance from the position of adjacent chess-man of moved chess-man to (x2,y2) isn't a trap value of move chess-man"
							+ "\nDistance: " + numberOfSteps);
			return false;
		}

		return true;
	}

	/**
	 * Check a catch of a chess-man at (x1,y1) with a chess-man at (x2,y2)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return <li> false if the chess-man at (x1,y1) and the chess-man at (x2,y2) belong to the same player
	 * <li> true if (x2,y2) is a trap of chess-man who is at (x1,y1) 
	 *  {@link Board#isATrap(int, int, int, int)}
	 */
	public boolean canMakeCatch(int x1, int y1, int x2, int y2) {
		int movedChessman = getState(x1, y1);
		int caughtChessman = getState(x2, y2);

		if (samePlayer(movedChessman, caughtChessman)) {
			MessageConsole
					.debugMessage("@Board#canMakeCatch: Two chessmans belong to the same player: "
							+ "\nChessman 1: "
							+ movedChessman
							+ "\nChessman 2:" + caughtChessman);
			return false;
		}
		return isATrap(x1, y1, x2, y2);
	}

	/**
	 * Get the number of steps from (x1,y1) to (x2,y2) on the board
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return the number of pieces from (x1,y1) to (x2,y2)
	 * <li> -1 if (x1,y1) and (x2,y2) are same position on the board
	 * <li> -1 if the path from (x1,y1) to (x2,y2) isn't a correct path
	 */
	public int getNumberOfSteps(int x1, int y1, int x2, int y2) {
		if (getDirection(x1, y1, x2, y2) == 0)
			return -1;
		int xStep = Math.abs(x2 - x1);
		int yStep = Math.abs(y2 - y1);
		return (xStep > yStep ? xStep : yStep);
	}

	/**
	 * Get a position where adjacent with (x1,y1) on the path from (x1,y1) to (x2,y2)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2 
	 * @return <li> NULL if the path from (x1,y1) to (x2,y2) isn't valid
	 * <li> the piece adjacent with (x1,y1) on the path from (x1,y1) to (x2,y2)
	 */
	public Piece getAdjacientPieceOnPath(int x1, int y1, int x2, int y2) {
		int direction = getDirection(x1, y1, x2, y2);
		switch (direction) {
		case 1:
			return new Piece(x1, y1 + 1);
		case -1:
			return new Piece(x1, y1 - 1);
		case 2:
			return new Piece(x1 + 1, y1);
		case -2:
			return new Piece(x1 - 1, y1);
		case 3:
			return new Piece(x1 + 1, y1 - 1);
		case -3:
			return new Piece(x1 - 1, y1 + 1);
		case 4:
			return new Piece(x1 + 1, y1 + 1);
		case -4:
			return new Piece(x1 - 1, y1 - 1);
		default:
			return null;
		}
	}

	/**
	 * Check a move of a chess-man from (x1,y1) to an empty position (x2,y2)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 * <li> false if the number of steps of move is bigger than the value of moved chess-man
	 * <li> in other case, return {@link Board#isEmptyPath(int, int, int, int)}
	 */
	public boolean canMakeNormalMove(int x1, int y1, int x2, int y2) {
		int numberOfStep = getNumberOfSteps(x1, y1, x2, y2);
		if (getState(x1, y1) % 10 < numberOfStep) {
			MessageConsole
					.debugMessage("The number of steps of move is bigger than the value of moved chessman");
			return false;
		}
		return isEmptyPath(x1, y1, x2, y2);
	}

	/**
	 * Get the direction from (x1,y1) to (x2,y2) on the board<br>
	 * -4 -1  3<br>
	 * -2  0  2<br>
	 * -3  1  4<br>
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return 
	 * <li>  0 : if (x1,y1) and (x2,y2) is a position on the board
	 * <li>  0 : there isn't a valid path on the board
	 * <li>  0 : if (x1,y1) or (x2,y2) is out of board
	 * <li>  1 : in a column, from top to bottom
	 * <li> -1 : in a column, from bottom to top
	 * <li>  2 : on a row, from left to right
	 * <li> -2 : on a row, from right to left
	 * <li>  3 : on a diagonal, from left-bottom to right-top
	 * <li> -3 : on a diagonal, from right-top to left-bottom
	 * <li>  4 : on a diagonal, from left-top to right-bottom
	 * <li> -4 : on a diagonal, from right-bottom to left-top
	 * 
	 */
	public int getDirection(int x1, int y1, int x2, int y2) {
		if (!insideBoard(x1, y1) || !insideBoard(x2, y2))
			return 0;
		if (x1 == x2 && y1 == y2)
			return 0;
		else if (x1 == x2) {
			return getValueDirection(y1, y2);
		} else if (y1 == y2) {
			return 2 * getValueDirection(x1, x2);
		} else if (x1 + y1 == x2 + y2) {
			return 3 * getValueDirection(x1, x2);
		} else if (x1 - x2 == y1 - y2) {
			return 4 * getValueDirection(x1, x2);
		} else
			return 0;
	}

	/**
	 * Get value of direction from x1 to y1
	 * @param x1
	 * @param x2
	 * @return <li> 1: if x1<x2
	 * <li> -1: if x1>x2
	 */
	private int getValueDirection(int x1, int x2) {
		if (x1 < x2)
			return 1;
		else
			return -1;
	}

	/**
	 * Get the destination state of a move on the board before the move has been done
	 * @param move
	 * @return 
	 * <li> the state of board at the destination of move where the chess-man wants to move to
	 */
	public int getDestMove(MoveInterface move) throws IllegalArgumentException {
		int x2 = move.getCoordinate(2);
		int y2 = move.getCoordinate(3);

		if (!insideBoard(x2, y2))
			throw new IllegalArgumentException(
					"@Board#getDestMove: Invalid input! move: "
							+ move.toString());

		return getState(x2, y2);
	}

	/**
	 * Get all the empty position where can break a move from (x1,y1) to (x2,y2)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return <li> If the number of step equals Zero, there is only one piece can break the move. The first piece
	 * <li> if the path from (x1,y1) to (x2,y2) in a column, from bottom to top, return {@link Board#getListEmptyPositionColumnBT(int, int, int)}
	 * <li> if the path from (x1,y1) to (x2,y2) in a column, from top to bottom, return {@link Board#getListEmptyPositionColumnTB(int, int, int)}
	 * <li> if the path from (x1,y1) to (x2,y2) on a row, from left to right, return {@link Board#getListEmptyPositionRowLR(int, int, int)}
	 * <li> if the path from (x1,y1) to (x2,y2) on a row, from right to left, return {@link Board#getListEmptyPositionRowRL(int, int, int)}
	 * <li> if the path from (x1,y1) to (x2,y2) on a diagonal, from left-top to right-bottom, return {@link Board#getListEmptyPositionDiagonalLTRB(int, int, int)}
	 * <li> if the path from (x1,y1) to (x2,y2) on a diagonal, from right-bottom to left-top, return {@link Board#getListEmptyPositionDiagonalRBLT(int, int, int)}
	 * <li> if the path from (x1,y1) to (x2,y2) on a diagonal, from right-top to left-bottom {@link Board#getListEmptyPositionDiagonalRTLB(int, int, int)}
	 * <li> if the path from (x1,y1) to (x2,y2) on a diagonal, from left-bottom to right-top, return {@link Board#getListEmptyPositionDiagonalLBRT(int, int, int)}
	 * 
	 */
	public ArrayList<Piece> getListEmptyPosition(int x1, int y1, int x2, int y2) {
		int numberOfStep = getNumberOfSteps(x1, y1, x2, y2);
		int direction = getDirection(x1, y1, x2, y2);

		if (numberOfStep <= 0)
			return null;
		switch (direction) {
		case -1:
			return getListEmptyPositionColumnBT(x1, y1, numberOfStep);

		case 1:
			return getListEmptyPositionColumnTB(x1, y1, numberOfStep);

		case 2:
			return getListEmptyPositionRowLR(x1, y1, numberOfStep);

		case -2:
			return getListEmptyPositionRowRL(x1, y1, numberOfStep);

		case 3:
			return getListEmptyPositionDiagonalLBRT(x1, y1, numberOfStep);

		case -3:
			return getListEmptyPositionDiagonalRTLB(x1, y1, numberOfStep);

		case 4:
			return getListEmptyPositionDiagonalLTRB(x1, y1, numberOfStep);

		case -4:
			return getListEmptyPositionDiagonalRBLT(x1, y1, numberOfStep);

		default:
			MessageConsole.debugMessage("@Board#isEmptyPath: Wrong direction "
					+ direction);
			return null;
		}
	}

	/**
	 * Get a list empty positions where can break a move on path Right-bottom to left-top from (x1,y1) with the number of steps is numberOfStep  
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionDiagonalRBLT(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1 - i, y1 - i)) {
				listEmptyPosition.add(new Piece(x1 - i, y1 - i));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

	/**Get a list empty positions where can break a move on path left-top to Right-bottom from (x1,y1) with the number of steps is numberOfStep
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionDiagonalLTRB(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1 + i, y1 + i)) {
				listEmptyPosition.add(new Piece(x1 + i, y1 + i));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

	/**Get a list empty positions where can break a move on path right-top to left-bottom from (x1,y1) with the number of steps is numberOfStep
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionDiagonalRTLB(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1 - i, y1 + i)) {
				listEmptyPosition.add(new Piece(x1 - i, y1 + i));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

	/**
	 * Get a list empty positions where can break a move on path left-bottom to right-top from (x1,y1) with the number of steps is numberOfStep
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionDiagonalLBRT(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1 + i, y1 - i)) {
				listEmptyPosition.add(new Piece(x1 + i, y1 - i));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

	/**
	 * Get a list empty positions where can break a move on a row from right to left from (x1,y1) with the number of steps is numberOfStep
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionRowRL(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1 - i, y1)) {
				listEmptyPosition.add(new Piece(x1 - i, y1));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

	/**
	 * Get a list empty positions where can break a move on a row from left to right from (x1,y1) with the number of steps is numberOfStep
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionRowLR(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1 + i, y1)) {
				listEmptyPosition.add(new Piece(x1 + i, y1));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

	/**
	 * Get a list empty positions where can break a move in a column from top to bottom from (x1,y1) with the number of steps is numberOfStep
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionColumnTB(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1, y1 + i)) {
				listEmptyPosition.add(new Piece(x1, y1 + i));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

	/**
	 * Get a list empty positions where can break a move in a column from bottom to top from (x1,y1) with the number of steps is numberOfStep
	 * @param x1
	 * @param y1
	 * @param numberOfStep
	 * @return
	 */
	private ArrayList<Piece> getListEmptyPositionColumnBT(int x1, int y1,
			int numberOfStep) {
		ArrayList<Piece> listEmptyPosition = new ArrayList<Piece>();
		listEmptyPosition.add(new Piece(x1, y1));
		for (int i = 1; i < numberOfStep; i++) {
			if (isEmpty(x1, y1 - i)) {
				listEmptyPosition.add(new Piece(x1, y1 - i));
			} else {
				break;
			}
		}
		return listEmptyPosition;
	}

}
