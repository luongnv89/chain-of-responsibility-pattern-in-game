/**
 * 
 */
package mathchess;

import java.util.ArrayList;

import specifications.BoardInterface;
import specifications.ChessmanInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import abstractions.BoardAbstract;
import abstractions.PlayerAbstract;

/**
 * a player in Vietnamese math chess:
 * <li>{@link PlayerMathChess#listChessmen} is a list all current chess-men.
 * <li>{@link PlayerMathChess#listAllPossibleCaught} is a list all the possible catch.
 * <li>{@link PlayerMathChess#MAX_LOST_SCORE} is the maximum score can be lost the game.
 * <li>If the player lost chess-men with total point of them is equal or greater than {@link PlayerMathChess#MAX_LOST_SCORE}, he will lose the game.
 * <li>{@link PlayerMathChess#caughtZero} true if the player caught the chess-man zero of the competitor. 
 */
public abstract class PlayerMathChess extends PlayerAbstract {

	/**
	 * List all the chess-men which player have currently
	 * <br>Both blue and red players have the same number of chess-men at the beginning.
	 * <br>For Vietnamese math chess each player has 10 chess-men.
	 */
	protected ArrayList<ChessmanInterface> listChessmen;

	/**
	 * List all current possible catch
	 */
	ArrayList<MoveInterface> listAllPossibleCaught = new ArrayList<MoveInterface>();

	/**
	 * The maximum score to lose the game.
	 * If the player lost chess-men with total point of them is equal or greater than {@link PlayerMathChess#MAX_LOST_SCORE}, he will lose the game.
	 */
	int MAX_LOST_SCORE = 45;

	/**
	 * tell that the player caught the Zero chess-man of the competitor.
	 */
	protected boolean caughtZero = false;

	/**
	 * Create a new player with name and decide it is blue or not. 
	 * Set the initial chess-men for the player.
	 * @param playerName
	 * @param blue
	 */
	public PlayerMathChess(String playerName, boolean blue) {
		super(playerName, blue);
		listChessmen = new ArrayList<ChessmanInterface>();
		intitialPlayer();
	}

	/**
	 * Get list of current chess-men.
	 * @return the listChessmans
	 */
	public ArrayList<ChessmanInterface> getListChessmen() {
		return listChessmen;
	}

	/**
	 * @return true if the player caught the chess-man zero of the competitor.
	 */
	public boolean isWinner() {
		return caughtZero;
	}

	/**
	 * @return true if the player lost chess-men which the sum points of these chess-men is equal or greater than {@link PlayerMathChess#MAX_LOST_SCORE}
	 */
	public boolean lostByScore() {
		return getLostScore() >= MAX_LOST_SCORE;
	}

	/**
	 *  Update the position of chess-man which was moved by current player
	 * @param x new x 
	 * @param y new y
	 * @param value of chess-man
	 * @return true if found the chess-man in list chess-men of player
	 * @throws GameExceptions if cannot found the chess-man in list chess-men of player
	 */
	public boolean updateChessman(int x, int y, int value)
			throws GameExceptions {
		int indexChessman = findIndexOfChessman(value);
		if (indexChessman != -1) {
			// The move is done by current player
			listChessmen.get(indexChessman).update(x, y);
			return true;
		} else {
			throw new GameExceptions(this,
					"@PlayerMathChess#updateListChessman:"
							+ "Cannot update the position of chessman: "
							+ value);
		}

	}

	/**
	 * Find the index of chess-man  in the list of chess-men.
	 * @param value of chess-man that need to find
	 * @return 
	 * <li> if cannot find the chess-man in the list chess-men of player, return -1
	 * <li> else return the chess-man's index in the list.
	 */
	public int findIndexOfChessman(int value) throws IllegalArgumentException {
		if (value < 0 || value > 20)
			throw new IllegalArgumentException(
					"@PlayerMathChess#findIndexOfChessman: Invalid input. Index must be in 0-20");
		for (int i = 0; i < listChessmen.size(); i++) {
			if (listChessmen.get(i).getValue() == value)
				return i;
		}
		return -1;
	}

	/**
	 * remove the chess-man is caught of the player out of the list current chess-men.
	 * @param caughtChessman is value of caught chess-man.
	 * @return true if found the chess-man in the list caught chess-men.
	 * @throws GameExceptions
	 */
	public boolean updateChessmanCaught(int caughtChessman)
			throws GameExceptions {
		if (caughtChessman < 1 || caughtChessman > 20) {
			throw new GameExceptions(
					this,
					"@PlayerMathChess#updateChessmanCaught:"
							+ "Invalid input. The value of chess-man must be in 1-20. But input value is: "
							+ caughtChessman);
		}
		int indexOfChessman = findIndexOfChessman(caughtChessman);
		if (indexOfChessman != -1) {
			listChessmen.remove(indexOfChessman);
			return true;
		} else {
			throw new GameExceptions(this,
					"@PlayerMathChess#updateChessmanCaught:"
							+ "Cannot find the chessman is caught: "
							+ caughtChessman);
		}
	}

	/**
	 * Check the invariant of a Math Chess player
	 * @return false if:
	 *  <li> Player has more than 10 chessman
	 *  <li> Player has an invalid chess-man in list current chess-men
	 *  <li> Player has a chess-man does NOT belong to the player.
	 *  <li> Player has two chess-man same value
	 *  <br>
	 *  <li> return true in other cases
	 * */
	@Override
	public boolean invariant() {
		if (listChessmen.size() > 10) {
			MessageConsole.debugMessage(this.getPlayerName()
					+ "has too much chessmen " + listChessmen.size());
			return false;
		}
		ArrayList<Integer> listVisited = new ArrayList<Integer>();
		for (int i = 0; i < listChessmen.size(); i++) {
			int value = listChessmen.get(i).getValue();
			// Check same value
			if (listVisited.contains(value)) {
				MessageConsole.debugMessage(this.getPlayerName()
						+ " has two chess-mans same value: " + value);
				return false;
			} else {
				listVisited.add(value);
			}
			// Check the invariant of chess-men
			if (!listChessmen.get(i).invariant()) {
				MessageConsole.debugMessage(this.getPlayerName()
						+ " has an invalid chess-man: "
						+ listChessmen.get(i).toString());
				return false;
			}
			// Check the value of chess-men
			if (isBlue() && listChessmen.get(i).getValue() > 10) {
				MessageConsole
						.debugMessage(this.getPlayerName()
								+ " is"
								+ (isBlue() ? " " : "n't ")
								+ "blue player. Player has a chess-man doesn't belong to him "
								+ listChessmen.get(i).getValue());
				return false;
			}
		}
		return true;
	}

	public String toString() {
		return "\nPlayerMathChess [\nlistChessmen=" + listChessmen
				+ ", \nlistAllPossibleCatchs=" + listAllPossibleCaught
				+ ", \nMAX_LOST_SCORE=" + MAX_LOST_SCORE + ", \ncaughtZero="
				+ caughtZero + "]";
	}

	/**
	 * @return the MAX_LOST_SCORE
	 */
	public int getMAX_LOST_SCORE() {
		return MAX_LOST_SCORE;
	}

	/**
	 * @param mAX_LOST_SCORE the mAX_LOST_SCORE to set
	 */
	public void setMAX_LOST_SCORE(int mAX_LOST_SCORE)
			throws IllegalArgumentException {
		if (mAX_LOST_SCORE < 1 || mAX_LOST_SCORE > 45)
			throw new IllegalArgumentException(
					"@PlayerMathChess#setMAX_LOST_SCORE: Invalid value. The max_lost_score must be in 1-45");
		MAX_LOST_SCORE = mAX_LOST_SCORE;
	}

	/**
	 * Initialize the list chess-men of player: 
	 * <li>each player has 10 chess-men
	 * <li>chess-men of the blue have value from 1 - 10.
	 * <li>chess-men of the red have value from 11 - 20.
	 * <li>fix the positions for two chess-men zero: blue-10(4,1), red-20(4,9)
	 */
	protected void intitialPlayer() {
		listChessmen.clear();
		if (blue) {
			// Set initial state of player 1
			for (int i = 0; i < Board.WIDTH; i++) {
				Chessman chessman = new Chessman(Board.WIDTH - i, i, 0);
				listChessmen.add(chessman);
			}
			Chessman zeroChessman = new Chessman(10, 4, 1);
			listChessmen.add(zeroChessman);
		} else {
			// Set initial state of player 2
			for (int i = 0; i < Board.WIDTH; i++) {
				Chessman chessman = new Chessman(Board.HEIGHT + i, i,
						Board.HEIGHT - 1);
				listChessmen.add(chessman);
			}
			Chessman zeroChessman = new Chessman(20, 4, 9);
			listChessmen.add(zeroChessman);
		}
	}

	/**
	 * @return
	 * <li> false if the player isn't invariant
	 * <li> false if {@link Board#canMakeMove(MoveInterface)} return false;
	 * <li> false if {@link Board#makeMove(MoveInterface)} return false;
	 * <li> false if {@link PlayerMathChess#updateChessman(int, int, int)} return false;
	 * <li> true in other cases
	 * */
	public boolean makeMove(MoveInterface move, BoardInterface board)
			throws GameExceptions {
		int x2 = move.getCoordinate(2);
		int y2 = move.getCoordinate(3);
		int x1 = move.getCoordinate(0);
		int y1 = move.getCoordinate(1);

		if (!((BoardAbstract) board).insideBoard(x1, y1)) {
			MessageConsole
					.errorMessage("@PlayerMathChess#makeMove: the source position of move is out of board"
							+ "\nMove: " + move.toString());
			return false;
		}

		if (!((BoardAbstract) board).insideBoard(x2, y2)) {
			MessageConsole
					.errorMessage("@PlayerMathChess#makeMove: the destination position of move is out of board"
							+ "\nMove: " + move.toString());
			return false;
		}

		int movedChessman = board.getState(x1, y1);
		if (!invariant()) {
			MessageConsole
					.errorMessage("@PlayerMathChess#makeMove: The player isn't invariant"
							+ "\nPlayer is blue player: "
							+ String.valueOf(isBlue())
							+ "\nChessman: "
							+ movedChessman);
			return false;
		}
		if (board.canMakeMove(move)) {
			return (board.makeMove(move) && updateChessman(x2, y2,
					movedChessman));

		}
		return false;
	}

	/**
	 * Get the losing score of the player.
	 * <li> at the beginning, the total of value of chess-men is 45
	 * @return 45 - The total of value of chess-men in list current chess-men 
	 */
	public int getLostScore() {
		int score = 0;
		for (int i = 0; i < listChessmen.size(); i++) {
			score += listChessmen.get(i).getValue() % 10;
		}
		return (45 - score);
	}

	/**
	 * get all chess-men are caught by the competitor.
	 * @return the listAllPossibleCatchs
	 */
	public ArrayList<MoveInterface> getListAllPossibleCaught() {
		return listAllPossibleCaught;
	}

	/**
	 * @param board in game
	 * @return true if the player in the situation which can catch the chess-man zero of his competitor.
	 *else false.
	 */
	public boolean canCatchZero(Board board) {
		for (int j = 0; j < this.getListAllPossibleCaught().size(); j++) {
			MoveInterface move = this.getListAllPossibleCaught().get(j);

			int toChessman = board.getDestMove(move);
			if (toChessman != 0 && toChessman % 10 == 0)
				return true;
		}
		return false;
	}

	/**
	 * Calculate the all possible move of each chess-man in the list current chess-men.
	 * Traverse each chess-man in the list and use {@link Chessman#calculateMove(BoardInterface)} 
	 * to calculate moves for the working chess-man. 
	 * To get <code>listAllPossibleCaught</code> and <code>listAllPossibleMoves</code> for the player.
	 * @param board
	 * @throws Exception 
	 * @see Chessman#calculateMove(BoardInterface)
	 */
	public void calculateMove(BoardInterface board) throws Exception {
		// Reset
		listAllPossibleCaught.clear();
		listAllPossibleMoves.clear();
		for (int i = 0; i < listChessmen.size(); i++) {
			calculateMoveChessman(board, i);
		}
	}

	/**
	 * Calculate move of a chessman of player
	 * @param board 
	 * @param i
	 * @throws Exception 
	 */
	private void calculateMoveChessman(BoardInterface board, int i)
			throws Exception {
		Chessman currentChessman = (Chessman) listChessmen.get(i);
		if (!currentChessman.isZero()) {
			currentChessman.calculateMove((Board) board);
			listAllPossibleMoves.addAll(currentChessman.getListMove(0));
			listAllPossibleCaught.addAll(currentChessman.getListMove(2));
		}
	}

	/**	/**
	 * to undo the last move, we check the last move is a normal move of a chess-man 
	 * or it is a catch.
	 * 
	 * <li> If the last move of game was done by current player, restore the position of chess-man of player who made in the last move
	 * <br>
	 * <li> If the last move of game was done by enemy of current player:
	 * <br>- If the last move is a caught move, read the chess-man that was caught by enemy in the last move into the list chess-men of current player
	 * <br>- If the last move is a normal move (Move to empty piece), do nothing.
	 *	@see {@link PlayerMathChess#undoCaughtByEnemy(int, int, int)}
	 * 	@see {@link PlayerMathChess#updateChessman(int, int, int)}
	 *
	 */
	@Override
	public boolean undoLastMove(BoardInterface board) throws GameExceptions {
		SavedMove moveSaved = ((Board) board).getLastMoveSaved();
		MoveInterface move = moveSaved.getMove();
		int x1 = move.getCoordinate(0);
		int y1 = move.getCoordinate(1);
		int dstMove = moveSaved.getDstMove();
		int x2 = move.getCoordinate(2);
		int y2 = move.getCoordinate(3);
		int srcMove = board.getState(x2, y2);

		int chessmanIndex = findIndexOfChessman(srcMove);
		if (chessmanIndex != -1) {
			return updateChessman(x1, y1, srcMove);
		} else {
			if (dstMove != 0) {
				return undoCaughtByEnemy(x2, y2, dstMove);
			} else {
				MessageConsole
						.debugMessage("This is normal move which is done by competitor of player. Move "
								+ move.toString());
				MessageConsole.debugMessage("Current player: "
						+ getPlayerName());
				return true;
			}
		}
	}

	/**
	 * apply this method only if we know a move is a catch.
	 * undo by creating a new chess-man at the same position and value as the chess-man was caught.
	 * @param x of the caught chess-man
	 * @param y of the caught chess-man
	 * @param caughtChessman is value of the caught chess-man
	 * @return true if the move is a catch and add a new chess-man into the list of current chess-men successfully.
	 * @throws GameExceptions
	 */
	private boolean undoCaughtByEnemy(int x, int y, int caughtChessman)
			throws GameExceptions {
		int chessman2Index = findIndexOfChessman(caughtChessman);
		if (chessman2Index != -1) {
			throw new GameExceptions(
					this,
					"#undoCaughtByEnemy.The chess-man which is caught in move still alive. Caught chessman "
							+ caughtChessman);

		} else {
			// REcuse the chessman which is caught by enemy
			Chessman recuseChessman = new Chessman(caughtChessman, x, y);
			listChessmen.add(recuseChessman);
			return true;
		}

	}

	/**
	 *<li> Clear all the list of possible moves and the list of possible catch.
	 *<li> Initialize 10 chess-men for the player as the beginning.
	 *<li> Reset the state of catch the chess-man zero of the competitor becomes false
	 */
	public void resetPlayer() {
		listAllPossibleCaught.clear();
		listAllPossibleMoves.clear();
		caughtZero = false;
		intitialPlayer();
	}

	/**
	 * Get all the possible move of player which can "break" a move of enemy player.
	 * @param move
	 * @return If enemy player's move is a normal move, it can be break by :
	 * <li>catch the chess-man who will be moved (the chessman is at (x1,y1)) 
	 * <li> or insert a chess-man between (x1,y1) and (x2,y2) (both (x2,y2))
	 * 
	 * <br> If enemy player's move is a catching move, it can be break by:
	 * <li> catch the chess-man who will be moved (the chessman is at (x1,y1))
	 * <li> catch the chess-man who is adjacent of the chess-man at (x1,y1)
	 * <li> move the chess-man who will be caught (the chessman is at (x2,y2))
	 * <li> insert a chess-man between the chessman adjacent of chessman at (x1,y1) and chessman at (x2,y2)
	 * @throws GameExceptions 
	 */
	public MoveInterface getDefenceMove(MoveInterface move,
			BoardInterface board, PlayerMathChess enemy) throws GameExceptions {
		MoveInterface moveDefence = null;
		int x1 = move.getCoordinate(0);
		int y1 = move.getCoordinate(1);
		int x2 = move.getCoordinate(2);
		int y2 = move.getCoordinate(3);

		// Break by catching the source chessman of move
		moveDefence = getMoveDefenceByCatch(x1, y1);
		if (moveDefence != null) {
			return moveDefence;
		}
		// Break a normal move
		if (((BoardAbstract) board).isEmpty(x2, y2)) {
			moveDefence = getMoveDefenceByInsert(x1, y1, x2, y2, board);
		}
		// Break a catching move
		else {
			moveDefence = getBreakACatch(board, x1, y1, x2, y2);
		}
		return moveDefence;
	}

	/**
	 * Get the move can "break" a catch of the competitor
	 * <br>a catch can break a move of the competitor:
	 * <li> catch the chess-man of the competitor - which will be moved 
	 * <li> catch the adjacent of the chess-man above
	 * <li> move the chess-man who will be caught (the chessman is at (x2,y2))
	 * <li> insert a chess-man between the chessman adjacent of chessman at (x1,y1) and chessman at (x2,y2)
	 * @param move
	 * @return null if cannot find any move possible can break the move of the competitor
	 * @throws GameExceptions 
	 * @see {@link PlayerMathChess#getMoveDefenceByCatch(int, int)}
	 * @see {@link PlayerMathChess#getMoveDefenceByInsert(int, int, int, int, BoardInterface)}
	 * @see {@link PlayerMathChess#getMoveHasSource(int, int}
	 */
	private MoveInterface getBreakACatch(BoardInterface board, int x1, int y1,
			int x2, int y2) {
		MoveInterface moveDefence;
		Piece adjPiece = ((Board) board)
				.getAdjacientPieceOnPath(x1, y1, x2, y2);
		int adX = adjPiece.getCoordinate(0);
		int adY = adjPiece.getCoordinate(1);
		// Break move by catch the adjacent chessman
		moveDefence = getMoveDefenceByCatch(adX, adY);
		if (moveDefence != null)
			return moveDefence;
		else {
			// Break move by move the destination chessman of move
			moveDefence = getMoveHasSource(x2, y2);
			if (moveDefence != null)
				return moveDefence;
			// break move by insert
			else {
				Piece adjPiece2 = ((Board) board).getAdjacientPieceOnPath(x2,
						y2, x1, y1);
				moveDefence = getMoveDefenceByInsert(adX, adY,
						adjPiece2.getCoordinate(0), adjPiece2.getCoordinate(1),
						board);
				if (moveDefence != null)
					return moveDefence;
			}

		}
		return null;
	}

	/**
	 * Get a move has source position is (x2,y2)
	 * @param x2
	 * @param y2
	 * @return 
	 * <li> a move has source is (x2,y2)
	 * <li> null if the player don't have any move has source is (x2,y2)
	 */
	private MoveInterface getMoveHasSource(int x2, int y2) {
		for (int i = 0; i < listAllPossibleMoves.size(); i++) {
			MoveInterface move = listAllPossibleMoves.get(i);
			int x1 = move.getCoordinate(0);
			int y1 = move.getCoordinate(1);
			if (x1 == x2 && y1 == y2)
				return move;

		}
		return null;
	}

	/**
	 * Get a move defense by insert a chess-man into the path from (x1,y1) to (x2,y2) - except the position (x1,y1)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param board
	 * @return <li> a normal move to a position in the path from (x1,y1) to (x2,y2) (except the position (x1,y1))
	 * <li> null if there isn't any move who has the destination move in the path from (x1,y1) to (x2,y2) (except the position (x1,y1))
	 */
	private MoveInterface getMoveDefenceByInsert(int x1, int y1, int x2,
			int y2, BoardInterface board) {
		if (x1 == x2 && y1 == y2)
			return null;
		ArrayList<Piece> listBreakPosition = ((Board) board)
				.getListEmptyPosition(x1, y1, x2, y2);
		for (int i = 0; i < listAllPossibleMoves.size(); i++) {
			MoveInterface moveInsert = listAllPossibleMoves.get(i);
			int x2Insert = moveInsert.getCoordinate(2);
			int y2Insert = moveInsert.getCoordinate(3);
			for (int j = 0; j < listBreakPosition.size(); j++) {
				if (x2Insert == listBreakPosition.get(j).getCoordinate(0)
						&& y2Insert == listBreakPosition.get(j)
								.getCoordinate(1))
					return moveInsert;
			}
		}
		return null;
	}

	/**
	 * <li> catch the chess-man of the competitor - which will be moved 
	 * <li> catch the adjacent chess-men of the competitor's chess-man which will catch the chess-man of current player
	 * @param listBreakPosition
	 * @return 
	 * <li> a move can catch one of two chess-men next to each other of the competitor.
	 * <li> null if there isn't any move has destination move is (x1,y1)
	 */
	private MoveInterface getMoveDefenceByCatch(int x1, int y1) {
		for (int i = 0; i < listAllPossibleCaught.size(); i++) {
			MoveInterface caught = listAllPossibleCaught.get(i);
			if (caught.getCoordinate(2) == x1 && caught.getCoordinate(3) == y1)
				return caught;
		}
		return null;
	}

	/**
	 * Check a move is safe or not?
	 * @param movePlayer is a move need to check
	 * @param board
	 * @return 
	 * <li> true if after making the move , the player won't lose the game
	 * <li> false if after making the move , the player will lose the game
	 * @throws Exception 
	 */
	public boolean makeMoveSafe(MoveInterface movePlayer, BoardInterface board,
			PlayerMathChess enemy) throws Exception {
		boolean safe = true;
		int destMove = ((Board) board).getDestMove(movePlayer);
		if (makeMove(movePlayer, board)) {
			if (destMove != 0)
				enemy.updateChessmanCaught(destMove);

			calculateMove(board);
			enemy.calculateMove(board);
			for (int i = 0; i < enemy.getListAllPossibleCaught().size(); i++) {
				MoveInterface move = enemy.getListAllPossibleCaught().get(i);
				int destMove2 = ((Board) board).getDestMove(move);
				// Lost the Zero chess-men by enemy
				if (destMove2 != 0 && destMove2 % 10 == 0) {
					safe = false;
					break;
				}
			}
			undoLastMove(board);
			enemy.undoLastMove(board);
			board.undoLastMove();
			calculateMove(board);
			enemy.calculateMove(board);
		} else {
			throw new GameExceptions(this,
					"@PlayerMathChess#makeMoveSafe: Cannot make move! move: "
							+ movePlayer.toString());
		}
		return safe;
	}

}
