package mathchess;

import specifications.MoveInterface;

/**
 * {@link Move} implement of {@link MoveInterface} represent a move in game
 * <li> A move in game can be a normal move: a chess-man move to an empty position
 * <li> A move in game also can be a catching: a chess-man move and catch a chess-man of enemy.
 * 
 * 
 */
public class Move implements MoveInterface {

	/**
	 * (x1,y1) is the current position of chess-man
	 */
	private int x1, y1;
	/**
	 * (x2,y2) is the move destination 
	 */
	private int x2, y2;

	/**
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public Move(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	/**
	 * Get the coordinates of move
	 * @return
	 * <li> the coordinate x1 of move if index = 0
	 * <li> the coordinate y1 of move if index = 1
	 * <li> the coordinate x2 of move if index = 2
	 * <li> the coordinate y2 of move if index = 3
	 * 
	 * */
	public int getCoordinate(int index) throws IllegalArgumentException {
		switch (index) {
		case 0:
			return x1;
		case 1:
			return y1;
		case 2:
			return x2;
		case 3:
			return y2;
		default:
			throw new IllegalArgumentException("Invalid input: index = "
					+ index);
		}
	}

	@Override
	public boolean invariant() {
		return true;
	}


	public String toString() {
		return "Move [x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2
				+ "]";
	}

}
