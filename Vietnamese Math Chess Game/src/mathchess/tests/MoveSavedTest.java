/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.*;

import mathchess.Move;
import mathchess.SavedMove;

import org.junit.Before;
import org.junit.Test;

import specifications.MoveInterface;

/**
 * Test for {@link SavedMove}
 *
 */
public class MoveSavedTest {
	SavedMove moveSaved;
	MoveInterface move;
	int dst = 4;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		move = new Move(4, 4, 5, 5);
		moveSaved = new SavedMove(move, dst);
	}

	/**
	 * Test method for {@link mathchess.SavedMove#getMove()}.
	 */
	@Test
	public void testGetMove() {
		assertTrue(move.equals(moveSaved.getMove()));
	}

	/**
	 * Test method for {@link mathchess.SavedMove#getDstMove()}.
	 */
	@Test
	public void testGetDstMove() {
		assertTrue(dst == moveSaved.getDstMove());
	}

}
