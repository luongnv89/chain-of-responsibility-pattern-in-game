/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import mathchess.Move;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test methods in {@link Move} class.
 * Distinct a move and a catch and their invariants.
 * 
 */
public class MoveTest {

	/**
	 * move the chess-man zero -> not allow to move it.
	 */
	protected Move moveKo;
	/**
	 * move OK
	 */
	protected Move moveOK;

	/**
	 * catches a chess-man which does not exist.
	 */
	protected Move catchKo;
	/**
	 * catch OK
	 */
	protected Move catchOK;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		moveOK = new Move(2, 0, 2, 7);
		moveKo = new Move(4, 1, 4, 2);

		catchOK = new Move(2, 7, 4, 9);
		catchKo = new Move(0, 0, 0, 10);

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		moveOK = null;
		catchOK = null;
		moveKo = null;
		catchKo = null;
	}

	/**
	 * Test method for {@link mathchess.Move#Move(int, int, int, int, int)}.
	 */
	@Test
	public void testConstructorMove() {
		assertTrue(moveOK.invariant());
	}

	/**
	 * Test method for {@link mathchess.objects.Move#getCoordinate(2)}.
	 */
	@Test
	public void testgetCoordinate2() {
		assertEquals(2, moveOK.getCoordinate(2));
	}

	/**
	 * Test method for {@link mathchess.objects.Move#getCoordinate(3)}.
	 */
	@Test
	public void testgetCoordinate3() {
		assertEquals(7, moveOK.getCoordinate(3));
	}

	/**
	 * Test method for {@link mathchess.objects.Move#getCoordinate(0)}.
	 */
	@Test
	public void testgetCoordinate0() {
		assertEquals(2, moveOK.getCoordinate(0));
	}

	/**
	 * Test method for {@link mathchess.objects.Move#getCoordinate(1)}.
	 */
	@Test
	public void testgetCoordinate1() {
		assertEquals(0, moveOK.getCoordinate(1));
	}

	/**
	 * Test method for {@link mathchess.objects.Move#getCoordinate(-1)}.
	 * 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetInvalidIndex() {
		moveOK.getCoordinate(-1);
	}

	/**
	 * Test method for {@link mathchess.Move#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println("Move test----> " + moveOK.toString());
	}

}
