/**
 * 
 */
package mathchess.tests;

import mathchess.Board;
import mathchess.HumanPlayer;

import org.junit.Before;

/**
 *Extends {@link PlayerMathChessTest} class.
 *Test a human player behaviors.
 */
public class HumanPlayerTest extends PlayerMathChessTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Board();

		blue = new HumanPlayer(blueName, true);
		red = new HumanPlayer(redName, false);
	}
	
}
