package mathchess.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AIPlayerTest.class, BoardTest.class, ChessmanTest.class,
		GameTest.class, GraphicConsoleTest.class, HumanPlayerTest.class,
		MoveSavedTest.class, MoveTest.class, PieceTest.class, })
public class AllMathChessObjectTests {

}
