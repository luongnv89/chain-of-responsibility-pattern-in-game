/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.*;

import mathchess.Board;
import mathchess.GraphicConsole;

import org.junit.Before;
import org.junit.Test;

import abstractions.tests.GraphicAbstractTest;

/**
 *Test for class {@link GraphicConsole} 
 *
 */
public class GraphicConsoleTest extends GraphicAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Board();
		graphic = new GraphicConsole(board);
	}

	/**
	 * Test method for {@link mathchess.GraphicConsole#GraphicConsole(specifications.BoardInterface)}.
	 */
	@Test
	public void testGraphicConsole() {
		assertTrue(graphic.invariant());
	}

	/**
	 * Test method for {@link mathchess.GraphicConsole#view()}.
	 */
	@Test
	public void testView() {
		graphic.view();
	}

}
