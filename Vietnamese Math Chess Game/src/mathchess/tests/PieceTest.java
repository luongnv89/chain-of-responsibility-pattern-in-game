/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.*;

import mathchess.Piece;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link Piece}
 *
 */
public class PieceTest {
	Piece p;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		p = new Piece(5, 5);
	}

	/**
	 * Test method for {@link mathchess.Piece#Piece(int, int)}.
	 */
	@Test
	public void testPiece() {
		assertTrue(p.invariant());
	}

	/**
	 * Test method for {@link mathchess.Piece#getCoordinate(int)}.
	 */
	@Test
	public void testGetCoordinate0() {
		assertTrue(p.getCoordinate(0) == 5);
	}

	/**
	 * Test method for {@link mathchess.Piece#getCoordinate(int) }.
	 */
	@Test
	public void testGetCoordinate1() {
		assertTrue(p.getCoordinate(1) == 5);
	}
 
	/**
	 * Test method for {@link mathchess.Piece#getCoordinate(int) }.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCoordinateException1() {
		p.getCoordinate(-1);
	}

	/**
	 * Test method for {@link mathchess.Piece#getCoordinate(int) }.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCoordinateException2() {
		p.getCoordinate(2);
	}

	/**
	 * Test method for {@link mathchess.Piece#equals(mathchess.Piece)}.
	 */
	@Test
	public void testEqualsPiece() {
		Piece p2 = new Piece(5, 5);
		assertTrue(p.equals(p));
		assertTrue(p.equals(p2));
		assertFalse(p.equals(new Piece(3, 3)));
		assertFalse(p.equals(new Piece(5, 3)));
	}

	/**
	 * Test method for {@link mathchess.Piece#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(p.invariant());
	}

	/**
	 * Test method for {@link mathchess.Piece#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(p.toString());
	}

}
