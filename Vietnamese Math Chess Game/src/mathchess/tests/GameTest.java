/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mathchess.Board;
import mathchess.Game;
import mathchess.GameMode;
import mathchess.GraphicConsole;
import mathchess.HumanPlayer;
import mathchess.Move;

import org.junit.Before;
import org.junit.Test;

import simulations.AIPlayingGameTest;
import simulations.HumanPlayingGameTest;
import utils.GameExceptions;

import abstractions.GameAbstract;
import abstractions.tests.GameAbstractTest;

/**
 * test {@link Game} class.
 * test the validation of methods in the class. There is also another test class to test playing a game:
 * {@link AIPlayingGameTest} and {@link HumanPlayingGameTest}.
 */
public class GameTest extends GameAbstractTest {

	private GraphicConsole graphic;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Board();

		graphic = new GraphicConsole(board);

		blueHumanPlayer = new HumanPlayer("blueHumanPlayer", true);
		redHumanPlayer = new HumanPlayer("redHumanPlayer", false);

		defaultGame = new Game(board, blueHumanPlayer, redHumanPlayer);
		((GameAbstract) defaultGame).setFirstPlayer(true);
		((Game) defaultGame).setGameMode(GameMode.MAX_SCORE);
		((Game) defaultGame).setWinScore(10);

		gameKo = new Game(board, blueHumanPlayer, blueHumanPlayer);

	}

	/**
	 * Test method for {@link mathchess.Game#getWinScore()}.
	 */
	@Test
	public void testGetWIN_SCORE() {
		assertEquals(((Game) defaultGame).getWinScore(), 10);
	}

	/**
	 * Test method for {@link mathchess.Game#setWinScore(int)}.
	 */
	@Test
	public void testSetWIN_SCORE() {
		((Game) defaultGame).setWinScore(20);
		assertEquals(((Game) defaultGame).getWinScore(), 20);
	}

	/**
	 * Test method for {@link mathchess.Game#getGameMode()}.
	 */
	@Test
	public void testGetGameMode() {
		assertTrue(((Game) defaultGame).getGameMode() == GameMode.MAX_SCORE);
	}

	/**
	 * Test method for
	 * {@link mathchess.Game#setGameMode(mathchess.GameMode)}.
	 */
	@Test
	public void testSetGameMode() {
		((Game) defaultGame).setGameMode(GameMode.MAX_SCORE);
		assertTrue(((Game) defaultGame).getGameMode() == GameMode.MAX_SCORE);
	}

	/**
	 * Test method for {@link mathchess.Game#isGameOver()}.
	 */
	@Test
	public void testIsGameOver() {
		assertFalse(defaultGame.isGameOver());
	}

	/**
	 * Test method for {@link mathchess.Game#startGame()}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testCatchZero() throws GameExceptions {
		graphic = new GraphicConsole(board);
		graphic.view();
		boolean makeMoveOK = true;
		makeMoveOK = blueHumanPlayer.makeMove(new Move(6, 0, 7, 1), board);
		if (makeMoveOK) {
			graphic.view();
			makeMoveOK = redHumanPlayer.makeMove(new Move(6, 10, 6, 5), board);
		}
		if (makeMoveOK) {
			graphic.view();
			makeMoveOK = redHumanPlayer.makeMove(new Move(6, 5, 3, 5), board);
		}
		if (makeMoveOK) {
			graphic.view();
			System.out.println(board.canMakeMove(new Move(8, 0, 3, 5)));
			makeMoveOK = blueHumanPlayer.makeMove(new Move(2, 0, 4, 2), board);
		}
		if (makeMoveOK) {
			graphic.view();
			makeMoveOK = blueHumanPlayer.makeMove(new Move(4, 2, 5, 2), board);
		}
		graphic.view();
		System.out.println(board.canMakeMove(new Move(7, 4, 4, 1)));
	}
}
