package mathchess.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mathchess.Board;
import mathchess.Chessman;
import mathchess.GraphicConsole;
import mathchess.Move;

import org.junit.Before;
import org.junit.Test;

import utils.GameExceptions;
import utils.MessageConsole;
import abstractions.tests.ChessmanAbstractTest;

/**
 * 
 *	extends {@link ChessmanAbstractTest}, test validation of methods of the class {@link Chessman}.
 *	Test invariant of a chess-man:
 * <ul>
 * <li> an invariant chess-man with value = 5 and its position is on the board.
 * <li> an invariant chess-man zero with value = 10 and its position is x = 4, y = 1;
 * <li> a not invariant chess-man with value = 0 (value = 1 -> 20).
 * <li> a not invariant chess-man zero with value = 20 and its position is x = 5, y = 9;
 * <ul>
 */
public class ChessmanTest extends ChessmanAbstractTest {
	Board board;
	Chessman zeroOK;
	Chessman zeroWrongPosition;
	Chessman zeroWrongPosition2;
	Chessman chessman21;
	Chessman blue5;
	Chessman blue4;
	Chessman isntOnBoard;
	GraphicConsole graphic;
	private Chessman red4;
	private Chessman red5;

	@Before
	public void setUp() throws Exception {
		board = new Board();
		graphic = new GraphicConsole(board);
		valueOk = 5;
		xOk = 4;
		yOk = 0;
		chessmanOk = new Chessman(valueOk, xOk, yOk);

		valueKo = 0;
		xKo = 4;
		yKo = 2;
		chessman0 = new Chessman(valueKo, xKo, yKo);
		chessman21 = new Chessman(21, 5, 5);

		zeroOK = new Chessman(10, 4, 1);
		zeroWrongPosition = new Chessman(20, 5, 9);
		zeroWrongPosition2 = new Chessman(10, 4, 2);

		board.makeMove(new Move(4, 0, 5, 1));
		blue5 = new Chessman(5, 5, 1);
		blue4 = new Chessman(4, 5, 0);
		red4 = new Chessman(14, 3, 10);
		red5 = new Chessman(15, 4, 10);

		isntOnBoard = new Chessman(5, 2, 2);
		MessageConsole.disableAllMessages();
	}

	/**
	 * Test {@link Chessman#resetChessman()}.
	 * After reset, all lists of this chess-man are empty.
	 */
	@Test
	public void testResetChessman1() {
		((Chessman) chessmanOk).resetChessman();
		assertEquals(0, ((Chessman) chessmanOk).getListMove(2).size());
		assertEquals(0, ((Chessman) chessmanOk).getListMove(1).size());
		assertEquals(0, ((Chessman) chessmanOk).getListMove(0).size());
	}

	/**
	 * Test {@link Chessman#isZero()}.
	 * check a chess-man is the chess-man zero or not?
	 */
	@Test
	public void testIsZero() {
		assertFalse(((Chessman) chessmanOk).isZero());
		assertTrue(zeroOK.isZero());
	}

	/**
	 * Test {@link Chessman#invariant()}.
	 * 
	 */
	@Test
	public void testInvariant() {
		assertTrue(chessmanOk.invariant());
		assertTrue(zeroOK.invariant());

		assertFalse(chessman0.invariant());
		assertFalse(zeroWrongPosition.invariant());
		assertFalse(zeroWrongPosition2.invariant());
		assertFalse(chessman21.invariant());
	}

	/**
	 * Test method for {@link mathchess.objects.Chessman#getListMove(0)}.
	 * Display all possible moves.
	 * @throws Exception 
	 */
	@Test
	public void testgetListMove0() throws Exception {
		graphic.view();

		blue4.calculateMove(board);
		for (int i = 0; i < ((Chessman) blue4).getListMove(0).size(); i++) {
			System.out.println(((Chessman) blue4).getListMove(0).get(i)
					.toString());
		}

		red4.calculateMove(board);
		for (int i = 0; i < ((Chessman) red4).getListMove(0).size(); i++) {
			System.out.println(((Chessman) red4).getListMove(0).get(i)
					.toString());
		}

		red5.calculateMove(board);
		for (int i = 0; i < ((Chessman) red5).getListMove(0).size(); i++) {
			System.out.println(((Chessman) red5).getListMove(0).get(i)
					.toString());
		}

		board.setState(5, 3, 18);
		blue5.calculateMove(board);
		graphic.view();
		for (int i = 0; i < ((Chessman) blue5).getListMove(0).size(); i++) {
			System.out.println(((Chessman) blue5).getListMove(0).get(i)
					.toString());
		}
		
		blue4.calculateMove(board);
		for (int i = 0; i < ((Chessman) blue4).getListMove(0).size(); i++) {
			System.out.println(((Chessman) blue4).getListMove(0).get(i)
					.toString());
		}
	}

	/**
	 * Test method for {@link mathchess.objects.Chessman#getListMove(1)}.
	 * Display all possible traps.
	 * @throws Exception 
	 */
	@Test
	public void testgetListMove1() throws Exception {
		graphic.view();

		blue4.calculateMove(board);
		for (int i = 0; i < ((Chessman) blue4).getListMove(1).size(); i++) {
			System.out.println(((Chessman) blue4).getListMove(1).get(i)
					.toString());
		}
	}

	/**
	 * Test method for {@link mathchess.objects.Chessman#getListMove(2)}.
	 * Display all possible catches.
	 * @throws Exception 
	 */
	@Test
	public void testgetListMove2() throws Exception {
		blue4.calculateMove(board);
		for (int i = 0; i < ((Chessman) blue4).getListMove(2).size(); i++) {
			System.out.println(((Chessman) blue4).getListMove(2).get(i)
					.toString());
		}
	}

	/**
	 * Test method for {@link mathchess.objects.Chessman#getListMove(2)}.
	 * index is bigger than 1
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testgetListMoveException1() throws Exception {
		blue4.calculateMove(board);
		blue4.getListMove(-1);
	}

	/**
	 * Test method for {@link mathchess.objects.Chessman#getListMove(2)}.
	 * Index is bigger than 2
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testgetListMoveException2() throws Exception {
		blue4.calculateMove(board);
		blue4.getListMove(3);
	}

	/**
	 * Test method for {@link mathchess.Chessman#calculateMove(specifications.BoardInterface)}.
	 * Display all possible catches.
	 * @throws Exception 
	 */
	@Test
	public void testCalculateMove() throws Exception {
		graphic.view();
		assertTrue(blue5.getListMove(2).isEmpty());
		assertTrue(blue5.getListMove(0).isEmpty());
		assertTrue(blue5.getListMove(1).isEmpty());
		blue5.calculateMove(board);
		blue4.calculateMove(board);
		assertTrue(blue5.getListMove(2).isEmpty());
		assertFalse(blue4.getListMove(2).isEmpty());
		assertFalse(blue5.getListMove(0).isEmpty());
		assertTrue(blue5.getListMove(1).isEmpty());
	}

	/**
	 * Test method for {@link mathchess.Chessman#calculateMove(specifications.BoardInterface)}.
	 * Display all possible catches.
	 * @throws Exception 
	 */
	@Test(expected = GameExceptions.class)
	public void testCalculateMoveChessmanisntOnBoard() throws Exception {
		graphic.view();
		assertTrue(isntOnBoard.getListMove(2).isEmpty());
		assertTrue(isntOnBoard.getListMove(0).isEmpty());
		assertTrue(isntOnBoard.getListMove(1).isEmpty());
		isntOnBoard.calculateMove(board);
	}

	/**
	 * Test method for {@link mathchess.Chessman#calculateMove(specifications.BoardInterface)}.
	 * Display all possible catches.
	 * @throws Exception 
	 */
	@Test
	public void testCalculateMoveZeroChessman() throws Exception {
		graphic.view();
		assertTrue(zeroOK.getListMove(2).isEmpty());
		assertTrue(zeroOK.getListMove(0).isEmpty());
		assertTrue(zeroOK.getListMove(1).isEmpty());
		zeroOK.calculateMove(board);
		assertTrue(zeroOK.getListMove(2).isEmpty());
		assertTrue(zeroOK.getListMove(0).isEmpty());
		assertTrue(zeroOK.getListMove(1).isEmpty());
	}

}
