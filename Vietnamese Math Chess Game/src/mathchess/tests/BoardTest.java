/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import mathchess.Board;
import mathchess.Move;
import mathchess.Piece;
import mathchess.SavedMove;

import org.junit.Before;
import org.junit.Test;

import specifications.MoveInterface;
import abstractions.tests.BoardAbstractTest;

/**
 * 
 * Extends {@link BoardAbstractTest} class.
 * check validation of methods in the class {@link Board}.
 */
public class BoardTest extends BoardAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		width = Board.WIDTH;
		heigh = Board.HEIGHT;
		defaultBoard = new Board();
		nonDefaultBoard = new Board();
		nonDefaultBoard.setState(0, 0, 21);
		nonDefaultBoard.setState(8, 0, 0);
	}

	/**
	 * Test method for {@link mathchess.Board#Board()}.
	 */
	@Test
	public void testBoard() { 
		assertTrue(defaultBoard.invariant());
		assertTrue(defaultBoard.getHeight() == 11);
		assertTrue(defaultBoard.getWidth() == 9);

		assertTrue(defaultBoard.getState(0, 0) == 9);
		assertTrue(defaultBoard.getState(8, 0) == 1);
		assertTrue(defaultBoard.getState(4, 1) == 10);

		assertTrue(defaultBoard.getState(8, 10) == 19);
		assertTrue(defaultBoard.getState(0, 10) == 11);
		assertTrue(defaultBoard.getState(4, 9) == 20);
	}

	/**
	 * Test method for {@link mathchess.Board#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(defaultBoard.invariant());
		assertFalse(nonDefaultBoard.invariant());

		defaultBoard.setState(1, 1, 9);
		assertFalse(defaultBoard.invariant());

		defaultBoard.setState(1, 1, 0);
		defaultBoard.setState(1, 1, -1);
		assertFalse(defaultBoard.invariant());

		defaultBoard.setState(1, 1, 0);
		defaultBoard.setState(1, 1, 21);
		assertFalse(defaultBoard.invariant());
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathWrongDirectionSamePosition() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(0, 0, 0, 0));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathWrongDirectionOutofBoard() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(-1, 0, 2, 3));
		assertFalse(((Board) defaultBoard).isEmptyPath(1, 0, 2, -3));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathWrongDirectionInvalidPath() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(1, 0, 2, 4));
		assertFalse(((Board) defaultBoard).isEmptyPath(1, 1, 8, 0));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathZeroPath() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(4, 4, 4, 4));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathInColumnBT() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(4, 10, 4, 0));
		assertTrue(((Board) defaultBoard).isEmptyPath(4, 9, 4, 1));
		assertTrue(((Board) defaultBoard).isEmptyPath(0, 10, 0, 0));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathInColumnTB() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(4, 0, 4, 10));
		assertTrue(((Board) defaultBoard).isEmptyPath(4, 1, 4, 9));
		assertTrue(((Board) defaultBoard).isEmptyPath(0, 0, 0, 9));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathOnRowLR() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(0, 1, 8, 1));
		assertFalse(((Board) defaultBoard).isEmptyPath(3, 9, 6, 9));
		assertTrue(((Board) defaultBoard).isEmptyPath(0, 2, 8, 2));
		assertTrue(((Board) defaultBoard).isEmptyPath(0, 8, 8, 8));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathOnRowRL() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(8, 1, 0, 1));
		assertFalse(((Board) defaultBoard).isEmptyPath(6, 9, 3, 9));
		assertTrue(((Board) defaultBoard).isEmptyPath(8, 2, 0, 2));
		assertTrue(((Board) defaultBoard).isEmptyPath(8, 8, 0, 8));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathInDiangonalLBRT() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(3, 10, 5, 8));
		assertFalse(((Board) defaultBoard).isEmptyPath(2, 3, 5, 0));
		assertTrue(((Board) defaultBoard).isEmptyPath(0, 10, 8, 2));
		assertTrue(((Board) defaultBoard).isEmptyPath(4, 10, 8, 6));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathInDiangonalRTLB() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(5, 8, 3, 10));
		assertFalse(((Board) defaultBoard).isEmptyPath(5, 0, 2, 3));
		assertTrue(((Board) defaultBoard).isEmptyPath(8, 2, 0, 10));
		assertTrue(((Board) defaultBoard).isEmptyPath(8, 6, 4, 10));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathInDiangonaLTRB() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(3, 0, 6, 3));
		assertFalse(((Board) defaultBoard).isEmptyPath(2, 7, 5, 10));
		assertTrue(((Board) defaultBoard).isEmptyPath(4, 1, 8, 5));
		assertTrue(((Board) defaultBoard).isEmptyPath(0, 5, 4, 9));
	}

	/**
	 * Test method for {@link mathchess.Board#isEmptyPath(int, int, int, int)}.
	 */
	@Test
	public void testIsEmptyPathInDiangonaRBLT() {
		System.out.println(defaultBoard.toString());
		assertFalse(((Board) defaultBoard).isEmptyPath(6, 3, 3, 0));
		assertFalse(((Board) defaultBoard).isEmptyPath(5, 10, 2, 7));
		assertTrue(((Board) defaultBoard).isEmptyPath(8, 5, 4, 1));
		assertTrue(((Board) defaultBoard).isEmptyPath(4, 9, 0, 5));
	}

	/**
	 * Test method for {@link mathchess.Board#initial()}.
	 */
	@Test
	public void testInitial() {
		defaultBoard.initial();
		assertTrue(defaultBoard.invariant());
		assertTrue(defaultBoard.getHeight() == 11);
		assertTrue(defaultBoard.getWidth() == 9);

		assertTrue(defaultBoard.getState(0, 0) == 9);
		assertTrue(defaultBoard.getState(8, 0) == 1);
		assertTrue(defaultBoard.getState(4, 1) == 10);

		assertTrue(defaultBoard.getState(8, 10) == 19);
		assertTrue(defaultBoard.getState(0, 10) == 11);
		assertTrue(defaultBoard.getState(4, 9) == 20);
	}

	/**
	 * Test method for {@link mathchess.Board#makeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testMakeMoveCouldnotBeDone() {
		MoveInterface cannotBeDone = new Move(0, 0, 3, 4);
		assertFalse(defaultBoard.makeMove(cannotBeDone));
	}

	/**
	 * Test method for {@link mathchess.Board#makeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testMakeMoveOK() {
		MoveInterface canBeDone = new Move(0, 0, 4, 4);
		int srcMove = defaultBoard.getState(0, 0);
		assertTrue(defaultBoard.makeMove(canBeDone));
		assertTrue(defaultBoard.getState(0, 0) == 0);
		assertTrue(defaultBoard.getState(4, 4) == srcMove);
	}

	/**
	 * Test method for {@link mathchess.Board#undoLastMove()}.
	 */
	@Test
	public void testUndoLastMove() {
		assertFalse(defaultBoard.undoLastMove());
		MoveInterface move1 = new Move(0, 0, 4, 4);
		assertTrue(defaultBoard.makeMove(move1));
		MoveInterface move2 = new Move(8, 10, 4, 6);
		int srcLastMove = defaultBoard.getState(8, 10);
		int dstLastMove = defaultBoard.getState(4, 6);
		assertTrue(defaultBoard.makeMove(move2));
		assertTrue(defaultBoard.getState(4, 6) == srcLastMove);
		assertTrue(defaultBoard.getState(8, 10) == 0);
		assertTrue(defaultBoard.undoLastMove());
		assertTrue(defaultBoard.getState(4, 6) == dstLastMove);
		assertTrue(defaultBoard.getState(8, 10) == srcLastMove);
	}

	/**
	 * Test method for {@link mathchess.Board#getLastMoveSaved()}.
	 */
	@Test
	public void testGetLastMoveSaved() {
		assertNull(((Board) defaultBoard).getLastMoveSaved());
		MoveInterface move1 = new Move(0, 0, 4, 4);
		assertTrue(defaultBoard.makeMove(move1));
		MoveInterface move2 = new Move(8, 10, 4, 6);
		assertTrue(defaultBoard.makeMove(move2));
		SavedMove lastMoveSaved = ((Board) defaultBoard).getLastMoveSaved();
		MoveInterface move = lastMoveSaved.getMove();
		assertTrue(move.equals(move2));
		assertTrue(lastMoveSaved.getDstMove() == 0);
	}

	/**
	 * Test method for {@link mathchess.Board#samePlayer(int, int)}.
	 */
	@Test
	public void testSamePlayer() {
		assertTrue(((Board) defaultBoard).samePlayer(13, 14));
		assertTrue(((Board) defaultBoard).samePlayer(3, 4));
		assertTrue(((Board) defaultBoard).samePlayer(10, 4));
		assertTrue(((Board) defaultBoard).samePlayer(11, 20));

		assertFalse(((Board) defaultBoard).samePlayer(0, 14));
		assertFalse(((Board) defaultBoard).samePlayer(-9, 14));
		assertFalse(((Board) defaultBoard).samePlayer(3, 24));
		assertFalse(((Board) defaultBoard).samePlayer(3, 14));
		assertFalse(((Board) defaultBoard).samePlayer(13, 4));
		assertFalse(((Board) defaultBoard).samePlayer(10, 11));
		assertFalse(((Board) defaultBoard).samePlayer(20, 1));

	}

	/**
	 * Test method for {@link mathchess.Board#getScore(boolean)}.
	 */
	@Test
	public void testGetScore() {
		assertTrue(((Board) defaultBoard).getScore(true) == 0);
		assertTrue(((Board) defaultBoard).getScore(false) == 0);
		defaultBoard.setState(0, 0, 0);
		defaultBoard.setState(0, 10, 0);
		assertTrue(((Board) defaultBoard).getScore(true) == 1);
		assertTrue(((Board) defaultBoard).getScore(false) == 9);
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testCanMakeMoveOutOfBoard() {
		MoveInterface outOfBoard = new Move(0, 0, 10, 10);
		assertFalse(defaultBoard.canMakeMove(outOfBoard));

		MoveInterface outOfBoard2 = new Move(-2, -1, 8, 8);
		assertFalse(defaultBoard.canMakeMove(outOfBoard2));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testCanMakeMoveSourceEmpty() {
		MoveInterface emptySource = new Move(1, 1, 3, 3);
		assertFalse(defaultBoard.canMakeMove(emptySource));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testCanMakeMoveInvalidPath() {
		MoveInterface invalidPath = new Move(0, 0, 2, 3);
		assertFalse(defaultBoard.canMakeMove(invalidPath));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testCanMakeMoveNormalMove() {
		MoveInterface normalMove = new Move(0, 0, 4, 4);
		assertTrue(defaultBoard.canMakeMove(normalMove));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testCanMakeMoveZero() {
		assertFalse(defaultBoard.makeMove(new Move(4, 1, 4, 6)));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testCanMakeMoveCaughtMove() {
		defaultBoard.makeMove(new Move(8, 10, 4, 6));
		MoveInterface catchMove = new Move(4, 0, 4, 6);
		assertTrue(defaultBoard.canMakeMove(catchMove));
	}

	/**
	 * Test method for {@link mathchess.Board#isATrap(int, int, int, int)}.
	 */
	@Test
	public void testIsATrapAjacentNULL() {

		assertFalse(((Board) defaultBoard).isATrap(0, 0, -2, -2));

		assertFalse(((Board) defaultBoard).isATrap(0, 0, 2, 4));
	}

	/**
	 * Test method for {@link mathchess.Board#isATrap(int, int, int, int)}.
	 */
	@Test
	public void testIsATrapAjacentIsnotTrapValue() {

		assertFalse(((Board) defaultBoard).isATrap(4, 0, 4, 3));

		assertFalse(((Board) defaultBoard).isATrap(4, 10, 4, 3));
	}

	/**
	 * Test method for {@link mathchess.Board#isATrap(int, int, int, int)}.
	 */
	@Test
	public void testIsATrapAjacentIsntSamePlayer() {
		defaultBoard.setState(1, 1, 19);
		assertFalse(((Board) defaultBoard).isATrap(0, 0, 2, 2));
	}

	/**
	 * Test method for {@link mathchess.Board#isATrap(int, int, int, int)}.
	 */
	@Test
	public void testIsATrapIsnotEmptyPath() {
		defaultBoard.makeMove(new Move(6, 0, 4, 2));
		assertFalse(((Board) defaultBoard).isATrap(4, 0, 4, 6));
	}

	/**
	 * Test method for {@link mathchess.Board#isATrap(int, int, int, int)}.
	 */
	@Test
	public void testIsATrapOK() {
		assertTrue(((Board) defaultBoard).isATrap(4, 0, 4, 6));
		assertTrue(((Board) defaultBoard).isATrap(4, 10, 4, 4));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeCatch(int, int, int, int)}.
	 */
	@Test
	public void testCanMakeCatchSamePlayer() {
		assertFalse(((Board) defaultBoard).canMakeCatch(0, 0, 2, 0));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeCatch(int, int, int, int)}.
	 */
	@Test
	public void testCanMakeCatchIsnotATrap() {
		defaultBoard.makeMove(new Move(8, 10, 4, 6));
		defaultBoard.makeMove(new Move(4, 6, 4, 4));
		assertFalse(((Board) defaultBoard).canMakeCatch(4, 0, 4, 4));
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeCatch(int, int, int, int)}.
	 */
	@Test
	public void testCanMakeCatchOK() {
		defaultBoard.makeMove(new Move(8, 10, 4, 6));
		assertTrue(((Board) defaultBoard).canMakeCatch(4, 0, 4, 6));
	}

	/**
	 * Test method for {@link mathchess.Board#getNumberOfSteps(int, int, int, int)}.
	 */
	@Test
	public void testGetNumberOfSteps() {
		assertTrue(((Board) defaultBoard).getNumberOfSteps(0, 0, 8, 7) == -1);
		assertTrue(((Board) defaultBoard).getNumberOfSteps(0, 0, 8, 8) == 8);
	}

	/**
	 * Test method for {@link mathchess.Board#canMakeNormalMove(int, int, int, int)}.
	 */
	@Test
	public void testCanMakeNormalMoveBiggerStep() {
		MoveInterface moveBiggerStep = new Move(3, 0, 3, 8);
		assertFalse(defaultBoard.canMakeMove(moveBiggerStep));

		MoveInterface moveInvalidPath = new Move(3, 0, 2, 3);
		assertFalse(defaultBoard.canMakeMove(moveInvalidPath));

		MoveInterface moveUnEmptyPath = new Move(3, 0, 5, 2);
		assertFalse(defaultBoard.canMakeMove(moveUnEmptyPath));

		MoveInterface moveInColumnBT = new Move(3, 10, 3, 8);
		assertTrue(defaultBoard.canMakeMove(moveInColumnBT));

		MoveInterface moveInColumnTB = new Move(3, 0, 3, 5);
		assertTrue(defaultBoard.canMakeMove(moveInColumnTB));

		defaultBoard.makeMove(new Move(3, 0, 3, 3));

		MoveInterface moveOnRowLR = new Move(3, 3, 5, 3);
		assertTrue(defaultBoard.canMakeMove(moveOnRowLR));

		MoveInterface moveOnRowRL = new Move(3, 3, 0, 3);
		assertTrue(defaultBoard.canMakeMove(moveOnRowRL));

		MoveInterface moveOnDiagonalLTRB = new Move(0, 0, 2, 2);
		assertTrue(defaultBoard.canMakeMove(moveOnDiagonalLTRB));

		MoveInterface moveOnDiagonalRBLT = new Move(8, 10, 5, 7);
		assertTrue(defaultBoard.canMakeMove(moveOnDiagonalRBLT));

		MoveInterface moveOnDiagonalRTLB = new Move(6, 0, 4, 2);
		assertTrue(defaultBoard.canMakeMove(moveOnDiagonalRTLB));

		MoveInterface moveOnDiagonalLBRT = new Move(5, 10, 7, 8);
		assertTrue(defaultBoard.canMakeMove(moveOnDiagonalLBRT));
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionSamePosition() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(0, 0, 0, 0) == 0);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionOutOfBoard() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(-1, 0, 2, 3) == 0);
		assertTrue(((Board) defaultBoard).getDirection(1, 0, 2, -3) == 0);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionInvalidPath() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(1, 0, 2, 4) == 0);
		assertTrue(((Board) defaultBoard).getDirection(1, 1, 8, 0) == 0);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionInColumnBT() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(4, 10, 4, 1) == -1);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionInColumnTB() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(4, 1, 4, 10) == 1);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionOnRowLR() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(0, 1, 8, 1) == 2);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionOnRowRL() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(8, 1, 0, 1) == -2);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionOnDiagonalLBRT() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(3, 10, 5, 8) == 3);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionOnDiagonalRTLB() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(5, 8, 3, 10) == -3);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionOnDiagonalLTRB() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(3, 0, 6, 3) == 4);
	}

	/**
	 * Test method for {@link mathchess.Board#getDirection(int, int, int, int)}.
	 */
	@Test
	public void testGetDirectionOnDiagonalRBLT() {
		System.out.println(defaultBoard.toString());
		assertTrue(((Board) defaultBoard).getDirection(6, 3, 3, 0) == -4);
	}

	/**
	 * Test method for {@link mathchess.Board#getDestMove(specifications.MoveInterface)}.
	 */
	@Test
	public void testGetDestMove() {
		MoveInterface move = new Move(0, 0, 8, 8);
		MoveInterface move2 = new Move(3, 7, 3, 10);
		assertTrue(((Board) defaultBoard).getDestMove(move) == 0);
		assertTrue(((Board) defaultBoard).getDestMove(move2) == 14);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionZeroSteps() {
		assertNull(((Board) defaultBoard).getListEmptyPosition(0, 0, 0, 0));

	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionWrongDirection() {
		assertNull(((Board) defaultBoard).getListEmptyPosition(1, 0, 2, 4));

	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionInColumnBT() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(8, 10, 8, 2)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(8, 10, 8, 2)
				.size() == 8);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionInColumnTB() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(0, 0, 0, 8)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(0, 0, 0, 8)
				.size() == 8);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionOnRowLR() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(0, 3, 8, 3)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(0, 3, 8, 3)
				.size() == 8);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionOnRowRL() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(8, 3, 0, 3)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(8, 3, 0, 3)
				.size() == 8);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionOnDiagonalLTRB() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(0, 5, 5, 10)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(0, 5, 5, 10)
				.size() == 4);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionOnDiagonalRBLT() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(8, 5, 3, 0)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(8, 5, 3, 0)
				.size() == 4);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionOnDiagonalLBRT() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(0, 5, 5, 0)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(0, 5, 5, 0)
				.size() == 4);
	}

	/**
	 * Test method for {@link mathchess.Board#getListEmptyPosition(int, int, int, int)}.
	 */
	@Test
	public void testGetListEmptyPositionOnDiagonalRTLB() {
		assertFalse(((Board) defaultBoard).getListEmptyPosition(8, 5, 3, 10)
				.isEmpty());
		assertTrue(((Board) defaultBoard).getListEmptyPosition(8, 5, 3, 10)
				.size() == 4);
	}

	/**
	 * Test method for {@link mathchess.Board#getAdjacientPieceOnPath(int, int, int, int)}.
	 */
	@Test
	public void testgetAdjacientPieceOnPath() {
		Piece bottomUp = new Piece(5, 4);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 5, 0)
				.equals(bottomUp));

		Piece topDown = new Piece(5, 6);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 5, 10)
				.equals(topDown));

		Piece leftRight = new Piece(6, 5);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 8, 5)
				.equals(leftRight));

		Piece rightLeft = new Piece(4, 5);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 0, 5)
				.equals(rightLeft));

		Piece lTRB = new Piece(6, 6);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 8, 8)
				.equals(lTRB));

		Piece rBLT = new Piece(4, 4);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 0, 0)
				.equals(rBLT));

		Piece lBRT = new Piece(6, 4);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 8, 2)
				.equals(lBRT));

		Piece rTLB = new Piece(4, 6);
		assertTrue(((Board) defaultBoard).getAdjacientPieceOnPath(5, 5, 0, 10)
				.equals(rTLB));
	}
}
