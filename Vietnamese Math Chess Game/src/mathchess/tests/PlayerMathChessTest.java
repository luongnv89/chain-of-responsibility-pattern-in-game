/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import mathchess.Board;
import mathchess.Chessman;
import mathchess.GraphicConsole;
import mathchess.Move;
import mathchess.PlayerMathChess;

import org.junit.Test;

import specifications.ChessmanInterface;
import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import abstractions.BoardAbstract;
import abstractions.tests.PlayerAbstractTest;

/**
 * Test for {@link PlayerMathChess}
 *
 */
public abstract class PlayerMathChessTest extends PlayerAbstractTest {

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getListChessmen()}.
	 */
	@Test
	public void testGetListChessmen() {
		assertEquals(10, ((PlayerMathChess) blue).getListChessmen().size());
		assertEquals(10, ((PlayerMathChess) red).getListChessmen().size());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#isWinner()}.
	 */
	@Test
	public void testIsWinner() {
		assertFalse(((PlayerMathChess) red).isWinner());
		assertFalse(((PlayerMathChess) blue).isWinner());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#lostByScore()}.
	 */
	@Test
	public void testLostByScore() {
		assertFalse(((PlayerMathChess) blue).lostByScore());
		((PlayerMathChess) blue).setMAX_LOST_SCORE(20);
		for (int i = 0; i < 9; i++) {
			((PlayerMathChess) blue).getListChessmen().remove(0);
		}
		assertTrue(((PlayerMathChess) blue).lostByScore());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#updateChessman(int, int, int)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testUpdateChessmanOK() throws GameExceptions {
		int chessmanValue = ((PlayerMathChess) blue).getListChessmen().get(0)
				.getValue();
		int xOld = ((PlayerMathChess) blue).getListChessmen().get(0).getX();
		int yOld = ((PlayerMathChess) blue).getListChessmen().get(0).getY();
		((PlayerMathChess) blue).updateChessman(4, 6, chessmanValue);
		int xNew = ((PlayerMathChess) blue).getListChessmen().get(0).getX();
		int yNew = ((PlayerMathChess) blue).getListChessmen().get(0).getY();

		assertFalse(xOld == 4);
		assertFalse(yOld == 6);
		assertTrue(xNew == 4);
		assertTrue(yNew == 6);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#updateChessman(int, int, int)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testUpdateChessmanException() throws GameExceptions {
		((PlayerMathChess) blue).updateChessman(4, 6, 11);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#findIndexOfChessman(int)}.
	 */
	@Test
	public void testFindIndexOfChessman() {
		int chessmanValue = ((PlayerMathChess) blue).getListChessmen().get(0)
				.getValue();
		assertTrue(0 == ((PlayerMathChess) blue)
				.findIndexOfChessman(chessmanValue));
		assertTrue(-1 == ((PlayerMathChess) blue).findIndexOfChessman(11));
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#findIndexOfChessman(int)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testFindIndexOfChessmanExceptionLessThan1() {
		((PlayerMathChess) blue).findIndexOfChessman(-1);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#findIndexOfChessman(int)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testFindIndexOfChessmanExceptionBiggerThan20() {
		((PlayerMathChess) blue).findIndexOfChessman(21);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#updateChessmanCaught(int)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testUpdateChessmanCaughtOK() throws GameExceptions {
		int chessmanValue = ((PlayerMathChess) blue).getListChessmen().get(0)
				.getValue();
		assertTrue(0 == ((PlayerMathChess) blue)
				.findIndexOfChessman(chessmanValue));
		assertTrue(((PlayerMathChess) blue).updateChessmanCaught(chessmanValue));
		assertTrue(-1 == ((PlayerMathChess) blue)
				.findIndexOfChessman(chessmanValue));
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#updateChessmanCaught(int)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testUpdateChessmanCaughtInvalidValue1() throws GameExceptions {
		((PlayerMathChess) blue).updateChessmanCaught(0);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#updateChessmanCaught(int)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testUpdateChessmanCaughtInvalidValue2() throws GameExceptions {
		((PlayerMathChess) blue).updateChessmanCaught(21);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#updateChessmanCaught(int)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testUpdateChessmanCaughtNotFound() throws GameExceptions {
		assertTrue(-1 == ((PlayerMathChess) blue).findIndexOfChessman(11));
		((PlayerMathChess) blue).updateChessmanCaught(11);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(blue.invariant());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#invariant()}.
	 */
	@Test
	public void testInvariantMoreThan10Chessman() {
		((PlayerMathChess) blue).getListChessmen().add(new Chessman(3, 3, 3));
		assertFalse(blue.invariant());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#invariant()}.
	 */
	@Test
	public void testInvariantSameValue() {
		((PlayerMathChess) blue).getListChessmen().remove(0);
		ChessmanInterface addChessman = ((PlayerMathChess) blue)
				.getListChessmen().get(0);
		((PlayerMathChess) blue).getListChessmen().add(addChessman);
		assertFalse(blue.invariant());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#invariant()}.
	 */
	@Test
	public void testInvariantInvalidChessman() {
		((PlayerMathChess) blue).getListChessmen().remove(0);
		((PlayerMathChess) blue).getListChessmen().add(new Chessman(0, 1, 2));
		assertFalse(blue.invariant());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#invariant()}.
	 */
	@Test
	public void testInvariantChessmanDoesntBelong() {
		((PlayerMathChess) blue).getListChessmen().remove(0);
		((PlayerMathChess) blue).getListChessmen().add(new Chessman(11, 1, 2));
		assertFalse(blue.invariant());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(blue.toString());
		System.out.println(red.toString());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getMAX_LOST_SCORE()}.
	 */
	@Test
	public void testGetMAX_LOST_SCORE() {
		assertTrue(((PlayerMathChess) blue).getMAX_LOST_SCORE() == 45);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#setMAX_LOST_SCORE(int)}.
	 */
	@Test
	public void testSetMAX_LOST_SCORE() {
		assertTrue(((PlayerMathChess) blue).getMAX_LOST_SCORE() == 45);
		((PlayerMathChess) blue).setMAX_LOST_SCORE(20);
		assertTrue(((PlayerMathChess) blue).getMAX_LOST_SCORE() == 20);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#setMAX_LOST_SCORE(int)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetMAX_LOST_SCOREExceptionLessThan1() {
		assertTrue(((PlayerMathChess) blue).getMAX_LOST_SCORE() == 45);
		((PlayerMathChess) blue).setMAX_LOST_SCORE(0);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#setMAX_LOST_SCORE(int)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetMAX_LOST_SCOREExceptionMoreThan45() {
		assertTrue(((PlayerMathChess) blue).getMAX_LOST_SCORE() == 45);
		((PlayerMathChess) blue).setMAX_LOST_SCORE(50);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#intitialPlayer()}.
	 */
	@Test
	public void testIntitialPlayer() {
		assertTrue(blue.invariant());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#makeMove(specifications.MoveInterface, specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testMakeMove() throws GameExceptions {
		assertTrue(blue.makeMove(new Move(0, 0, 1, 1), board));
		assertFalse(blue.makeMove(new Move(4, 1, 4, 2), board));
		assertFalse(blue.makeMove(new Move(-1, 0, 0, 2), board));
		assertFalse(blue.makeMove(new Move(1, 0, 0, 21), board));
		assertFalse(blue.makeMove(new Move(0, 10, 5, 9), board));
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getLostScore()}.
	 */
	@Test
	public void testGetLostScore() {
		assertTrue(((PlayerMathChess) blue).getLostScore() == 0);
		int value = ((PlayerMathChess) blue).getListChessmen().get(0)
				.getValue();
		((PlayerMathChess) blue).getListChessmen().remove(0);
		assertTrue(((PlayerMathChess) blue).getLostScore() == value);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getListAllPossibleCaught()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetListAllPossibleCaught() throws Exception {
		assertTrue(blue.invariant());
		assertTrue(((PlayerMathChess) blue).getListAllPossibleCaught()
				.isEmpty());
		assertTrue(blue.makeMove(new Move(4, 0, 5, 1), board));
		((PlayerMathChess) blue).calculateMove(board);
		assertFalse(((PlayerMathChess) blue).getListAllPossibleCaught()
				.isEmpty());

	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#canCatchZero(mathchess.Board)}.
	 * @throws Exception 
	 */
	@Test
	public void testCanCatchZero() throws Exception {
		assertTrue(blue.invariant());
		((PlayerMathChess) blue).calculateMove(board);
		assertFalse(((PlayerMathChess) blue).canCatchZero((Board) board));
		assertTrue(blue.makeMove(new Move(3, 0, 3, 6), board));
		assertTrue(blue.makeMove(new Move(3, 6, 3, 8), board));
		assertTrue(blue.makeMove(new Move(2, 0, 2, 7), board));

		((PlayerMathChess) blue).calculateMove(board);
		assertTrue(((PlayerMathChess) blue).canCatchZero((Board) board));

	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#calculateMove(specifications.BoardInterface)}.
	 * @throws Exception 
	 */
	@Test
	public void testCalculateMove() throws Exception {
		assertTrue(blue.invariant());
		assertTrue(((PlayerMathChess) blue).getListAllPossibleCaught()
				.isEmpty());
		assertTrue(((PlayerMathChess) blue).getListAllPossibleMoves().isEmpty());
		assertTrue(blue.makeMove(new Move(3, 0, 3, 6), board));
		assertTrue(blue.makeMove(new Move(3, 6, 3, 8), board));
		assertTrue(blue.makeMove(new Move(2, 0, 2, 7), board));
		((PlayerMathChess) blue).calculateMove(board);
		assertFalse(((PlayerMathChess) blue).getListAllPossibleCaught()
				.isEmpty());
		assertFalse(((PlayerMathChess) blue).getListAllPossibleMoves()
				.isEmpty());
	} 

	/**
	 * Test method for {@link mathchess.PlayerMathChess#undoLastMove(specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testUndoLastMoveDoneByPlayer() throws GameExceptions {
		MessageConsole.disableAllMessages();
		GraphicConsole graphic = new GraphicConsole(board);
		graphic.view();
		assertTrue(blue.invariant());
		MoveInterface move = new Move(0, 0, 2, 2);
		int value = board.getState(0, 0);
		assertTrue(blue.makeMove(move, board));
		graphic.view();
		assertTrue(((BoardAbstract) board).isEmpty(0, 0));
		assertTrue(board.getState(2, 2) == value);

		assertTrue(blue.undoLastMove(board));
		assertTrue(board.undoLastMove());
		graphic.view();
		assertTrue(((BoardAbstract) board).isEmpty(2, 2));
		assertTrue(board.getState(0, 0) == value);

	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#undoLastMove(specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testUndoLastMoveNotDoneByPlayerNoCaught() throws GameExceptions {
		assertTrue(blue.invariant());
		MoveInterface move = new Move(0, 0, 2, 2);
		int value = board.getState(0, 0);
		assertTrue(blue.makeMove(move, board));
		assertTrue(((BoardAbstract) board).isEmpty(0, 0));
		assertTrue(board.getState(2, 2) == value);

		assertTrue(red.undoLastMove(board));

	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#undoLastMove(specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testUndoLastMoveNotDoneByPlayerWithCaught()
			throws GameExceptions {
		assertTrue(blue.invariant());
		MoveInterface move = new Move(4, 0, 5, 1);
		MoveInterface caught = new Move(5, 0, 5, 10);
		int caughtChessman = board.getState(5, 10);
		assertTrue(blue.makeMove(move, board));
		assertTrue(blue.makeMove(caught, board));
		assertTrue(((PlayerMathChess) red).updateChessmanCaught(caughtChessman));
		assertTrue(((PlayerMathChess) red).getListChessmen().size() == 9);
		assertTrue(red.undoLastMove(board));
		assertTrue(((PlayerMathChess) red).getListChessmen().size() == 10);

	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#resetPlayer()}.
	 */
	@Test
	public void testResetPlayer() {
		assertTrue(blue.invariant());
		assertTrue(((PlayerMathChess) blue).getListChessmen().size() == 10);
		((PlayerMathChess) blue).getListChessmen().remove(0);
		assertTrue(((PlayerMathChess) blue).getListChessmen().size() == 9);
		((PlayerMathChess) blue).resetPlayer();
		assertTrue(((PlayerMathChess) blue).getListChessmen().size() == 10);
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getDefenceMove(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetDefenceMoveNormalMoveByCatch() throws Exception {
		assertTrue(blue.invariant());
		assertTrue(red.invariant());

		assertTrue(blue.makeMove(new Move(4, 0, 5, 1), board));

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);

		Move normalMove = new Move(5, 10, 6, 9);

		Move defence = (Move) ((PlayerMathChess) blue).getDefenceMove(
				normalMove, board, (PlayerMathChess) red);
		assertNotNull(defence);
		System.out.println(defence.toString());

	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getDefenceMove(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetDefenceMoveNormalMoveByInsert() throws Exception {
		assertTrue(blue.invariant());
		assertTrue(red.invariant());

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);

		Move normalMove = new Move(6, 10, 0, 4);

		Move defence = (Move) ((PlayerMathChess) blue).getDefenceMove(
				normalMove, board, (PlayerMathChess) red);
		assertNotNull(defence);
		System.out.println(defence.toString());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getDefenceMove(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetDefenceMoveCaughtMoveByCatch() throws Exception {
		setUpBlueCatchZero();
		assertTrue(red.makeMove(new Move(3, 10, 2, 9), board));

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);

		Move catchZero = new Move(2, 7, 4, 9);

		Move defence = (Move) ((PlayerMathChess) red).getDefenceMove(catchZero,
				board, (PlayerMathChess) blue);
		assertNotNull(defence);
		System.out.println(defence.toString());

	}

	/**
	 * @throws GameExceptions
	 */
	private void setUpBlueCatchZero() throws GameExceptions {
		assertTrue(blue.invariant());
		assertTrue(red.invariant());

		assertTrue(blue.makeMove(new Move(3, 0, 3, 6), board)); 
		assertTrue(blue.makeMove(new Move(3, 6, 3, 8), board));
		assertTrue(blue.makeMove(new Move(2, 0, 2, 7), board));
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getDefenceMove(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetDefenceMoveCaughtMoveByCatchAdjacent()
			throws Exception {
		setUpBlueCatchZero();

		assertTrue(red.makeMove(new Move(2, 10, 3, 9), board));

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);

		Move catchZero = new Move(2, 7, 4, 9);

		Move defence = (Move) ((PlayerMathChess) red).getDefenceMove(catchZero,
				board, (PlayerMathChess) blue);
		assertNotNull(defence);
		System.out.println(defence.toString());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getDefenceMove(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetDefenceMoveCaughtMoveByMoveDestination()
			throws Exception {
		assertTrue(blue.invariant());
		assertTrue(red.invariant());

		assertTrue(blue.makeMove(new Move(3, 0, 3, 6), board));
		assertTrue(blue.makeMove(new Move(3, 6, 3, 8), board));
		assertTrue(blue.makeMove(new Move(3, 8, 2, 9), board));
		assertTrue(blue.makeMove(new Move(2, 0, 2, 7), board));
		assertTrue(blue.makeMove(new Move(2, 7, 2, 8), board));

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);

		Move catch3 = new Move(2, 8, 2, 10);

		Move defence = (Move) ((PlayerMathChess) red).getDefenceMove(catch3,
				board, (PlayerMathChess) blue);
		assertNotNull(defence);
		System.out.println(defence.toString());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#getDefenceMove(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetDefenceMoveCaughtMoveByInsert() throws Exception {
		assertTrue(blue.invariant());
		assertTrue(red.invariant());

		assertTrue(blue.makeMove(new Move(3, 0, 3, 6), board));
		assertTrue(blue.makeMove(new Move(2, 0, 2, 7), board));
		assertTrue(blue.makeMove(new Move(3, 6, 4, 6), board));

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);

		Move catch3 = new Move(3, 6, 3, 10);

		Move defence = (Move) ((PlayerMathChess) red).getDefenceMove(catch3,
				board, (PlayerMathChess) blue);
		assertNotNull(defence);
		System.out.println(defence.toString());
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#makeMoveSafe(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test
	public void testMakeMoveSafe() throws Exception {
		assertTrue(blue.invariant());
		assertTrue(red.invariant());
		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		assertTrue(((PlayerMathChess) red).makeMoveSafe(new Move(8, 10, 8, 5),
				board, (PlayerMathChess) blue));
		assertTrue(blue.makeMove(new Move(3, 0, 3, 6), board));
		assertTrue(blue.makeMove(new Move(3, 6, 3, 8), board));
		assertTrue(blue.makeMove(new Move(2, 0, 2, 7), board));

		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		assertTrue(((PlayerMathChess) blue).canCatchZero((Board) board));
		assertFalse(((PlayerMathChess) red).makeMoveSafe(new Move(8, 10, 8, 5),
				board, (PlayerMathChess) blue));
	}

	/**
	 * Test method for {@link mathchess.PlayerMathChess#makeMoveSafe(specifications.MoveInterface, specifications.BoardInterface, mathchess.PlayerMathChess)}.
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testMakeMoveSafeException() throws Exception {
		assertTrue(blue.invariant());
		assertTrue(red.invariant());
		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);
		assertTrue(((PlayerMathChess) red).makeMoveSafe(new Move(8, 10, 8, -2),
				board, (PlayerMathChess) blue));
	}

}
