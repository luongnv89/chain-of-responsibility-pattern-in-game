package mathchess.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import rules.tests.Rule_CatchTest;
import rules.tests.Rule_DefenceCatchTest;
import rules.tests.Rule_DefenceLostTest;
import rules.tests.Rule_MangerTest;
import rules.tests.Rule_MinimaxTest;
import rules.tests.Rule_RandomTest;
import rules.tests.Rule_WinTest;
import simulations.AIPlayingGameTest;
import evaluator.tests.EvaluatorDominateTest;
import evaluator.tests.EvaluatorManagerTest;
import evaluator.tests.EvaluatorValueTest;
import evaluator.tests.EvaluatorZeroTest;
import evaluator.tests.MiniMaxCalculatorTest;

@RunWith(Suite.class)
@SuiteClasses({ AIPlayerTest.class, 
		BoardTest.class, ChessmanTest.class, GameTest.class,
		GraphicConsoleTest.class, HumanPlayerTest.class, MoveSavedTest.class,
		MoveTest.class, PieceTest.class, Rule_CatchTest.class,
		Rule_DefenceCatchTest.class, Rule_DefenceLostTest.class,
		Rule_MangerTest.class, Rule_MinimaxTest.class, Rule_RandomTest.class,
		Rule_WinTest.class, EvaluatorDominateTest.class,
		EvaluatorManagerTest.class, EvaluatorValueTest.class,
		EvaluatorZeroTest.class, MiniMaxCalculatorTest.class,
		AIPlayingGameTest.class })
public class AllTests {

}
