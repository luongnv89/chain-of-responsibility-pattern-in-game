/**
 * 
 */
package mathchess.tests;

import static org.junit.Assert.assertNotNull;
import mathchess.AIPlayer;
import mathchess.Board;
import mathchess.Move;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import rules.RuleAbstract;
import rules.Rule_Random;
import utils.GameExceptions;

/**
 * Test for {@link AIPlayer}
 *
 */
public class AIPlayerTest extends PlayerMathChessTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Board();

		Rule_Random blueRule = new Rule_Random();
		Rule_Random redRule = new Rule_Random();

		blue = new AIPlayer(blueName, true, blueRule);
		red = new AIPlayer(redName, false, redRule);

		blueRule.setEnemy(red);
		redRule.setEnemy(blue);

	}

	/**
	 * Test method for {@link mathchess.AIPlayer#AIPlayer(java.lang.String, boolean, specifications.RuleInterface)}.
	 * @throws GameExceptions 
	 * @throws IllegalArgumentException 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAIPlayerException() throws IllegalArgumentException, GameExceptions {
		playerKO = new AIPlayer("AI_NoInvariant", false, null);
	}

	/**
	 * Test method for {@link mathchess.AIPlayer#getMove(specifications.BoardInterface)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetMove() throws Exception {
		((PlayerMathChess) blue).calculateMove(board);
		((PlayerMathChess) red).calculateMove(board);

		Move blueMove = (Move) blue.getMove(board);

		Move redMove = (Move) red.getMove(board);
		assertNotNull(blueMove);
		assertNotNull(redMove);
		System.out.println(blueName + " get move: " + blueMove.toString());
		System.out.println(redName + " get move: " + redMove.toString());
	}

	/**
	 * Test method for {@link mathchess.AIPlayer#getMove(specifications.BoardInterface)}.
	 * @throws Exception 
	 */
	@Test(expected = GameExceptions.class)
	public void testGetMoveException() throws Exception {
		RuleAbstract random = new Rule_Random();
		playerKO = new AIPlayer("Rule isnt invariant", false, random);
		playerKO.getMove(board);
	}

}
