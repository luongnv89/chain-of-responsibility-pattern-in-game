/**
 * 
 */
package simulations;

import mathchess.AIPlayer;
import mathchess.Board;
import mathchess.Game;
import mathchess.HumanPlayer;

import org.junit.After;
import org.junit.Before;

import rules.Rule_Manger;
import rules.Rule_Minimax;
import specifications.BoardInterface;
import specifications.GameInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;

/**
 * {@link PlayingGameAbstractTest} test for {@link Game#startGame()}
 *
 */
public abstract class PlayingGameAbstractTest {

	protected BoardInterface board;

	// Human player
	protected PlayerInterface blueHumanPlayer;
	protected PlayerInterface redHumanPlayer;

	// Minimax player
	protected RuleInterface blueMiniMaxRule;
	protected RuleInterface redMiniMaxRule;
	protected PlayerInterface blueAIMinimaxPlayer;
	protected PlayerInterface redAIMinimaxPlayer;

	// Rule Manager player
	protected RuleInterface blueManageRule;
	protected RuleInterface redManageRule;
	protected PlayerInterface redAIManagePlayer;
	protected PlayerInterface blueAIManagePlayer;

	// Game
	protected GameInterface game;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		board = new Board();

		// AI - Blue - Minimax - depth 1
		blueMiniMaxRule = new Rule_Minimax();
		blueAIMinimaxPlayer = new AIPlayer("blueAIMinimaxPlayer", true,
				blueMiniMaxRule);

		// AI - Red - minimax - depth 2
		redMiniMaxRule = new Rule_Minimax();
		redAIMinimaxPlayer = new AIPlayer("redAIMinimaxPlayer", false,
				redMiniMaxRule);

		// AI - Blue - Manage 
		blueManageRule = new Rule_Manger();
		blueAIManagePlayer = new AIPlayer("blueAIRandomPlayer", true,
				blueManageRule);

		// AI - red - Manage
		redManageRule = new Rule_Manger();
		redAIManagePlayer = new AIPlayer("redAIRandomPlayer", false,
				redManageRule);

		blueHumanPlayer = new HumanPlayer("blueHumanPlayer", true);
		redHumanPlayer = new HumanPlayer("redHumanPlayer", false);

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;

		// Human player
		blueHumanPlayer = null;
		redHumanPlayer = null;

		// Minimax player
		blueMiniMaxRule = null;
		redMiniMaxRule = null;
		blueAIMinimaxPlayer = null;
		redAIMinimaxPlayer = null;

		// Winfirst Random player
		blueManageRule = null;
		redManageRule = null;
		redAIManagePlayer = null;
		blueAIManagePlayer = null;

		// Game
		game = null;
	}
}
