/**
 * 
 */
package simulations;

import mathchess.Game;
import mathchess.GameMode;

import org.junit.Test;

import rules.Rule_Manger;
import rules.Rule_Minimax;
import utils.MessageConsole;
import abstractions.GameAbstract;

/**
 * Test AI players automatic play with each other.
 * Two AIs have different algorithms or rules.
 * 
 */
public class AIPlayingGameTest extends PlayingGameAbstractTest {

	/**
	 * Test method for {@link mathchess.Game#startGame()}.
	 * <li> Game create by two AI Minimax player
	 * @throws Exception 
	 */
	@Test
	public void testStartGameAIMinimaxvsAIMiniMax() throws Exception {
		MessageConsole.disableAllMessages();
		((Rule_Minimax) blueMiniMaxRule).setEnemy(redAIMinimaxPlayer);
		((Rule_Minimax) redMiniMaxRule).setEnemy(blueAIMinimaxPlayer);
		game = new Game(board, redAIMinimaxPlayer, blueAIMinimaxPlayer);
		((GameAbstract) game).setFirstPlayer(true);
		((Game) game).setGameMode(GameMode.MAX_SCORE);
		((Game) game).setWinScore(25);

		game.startGame();
	}

	/**
	 * Test method for {@link mathchess.Game#startGame()}.
	 * <li> Game create by two AI Random player
	 * @throws Exception 
	 */
	@Test
	public void testStartGameAIManagervsAIManager() throws Exception {
		MessageConsole.disableAllMessages();
		MessageConsole.setINFOR(true);
		((Rule_Manger) blueManageRule).setEnemy(redAIManagePlayer);
		((Rule_Manger) redManageRule).setEnemy(blueAIManagePlayer);
		((Rule_Manger) blueManageRule).setRules(0);
		((Rule_Manger) redManageRule).setRules(3);
		game = new Game(board, blueAIManagePlayer, redAIManagePlayer);
		((GameAbstract) game).setFirstPlayer(true);
		((Game) game).setGameMode(GameMode.MAX_SCORE);
		((Game) game).setWinScore(30);
		game.startGame();
	}

	/**
	 * Test method for {@link mathchess.Game#startGame()}.
	 * <li> Game create by an AI Minimax player and an AI Random player
	 * @throws Exception 
	 */
	@Test
	public void testStartGameAIMinmaxvsAIManager() throws Exception {

		MessageConsole.disableAllMessages();
		((Rule_Minimax) blueMiniMaxRule).setDepth(1);
		((Rule_Minimax) blueMiniMaxRule).setEnemy(redAIManagePlayer);
		((Rule_Manger) redManageRule).setRules(3);
		((Rule_Manger) redManageRule).setEnemy(blueAIMinimaxPlayer);

		game = new Game(board, blueAIMinimaxPlayer, redAIManagePlayer);
		((GameAbstract) game).setFirstPlayer(false);
		((Game) game).setGameMode(GameMode.MAX_SCORE);
		((Game) game).setWinScore(45);
		game.startGame();
	}

}
