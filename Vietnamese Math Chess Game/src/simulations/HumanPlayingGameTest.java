/**
 * 
 */
package simulations;

import mathchess.Game;
import mathchess.GameMode;

import org.junit.Test;

import rules.Rule_Manger;
import rules.Rule_Minimax;
import utils.MessageConsole;
import abstractions.GameAbstract;

/**
 * Test <br>
 * <li>a human player plays with another human
 * <li>a human players plays with AI player.
 * 
 */
public class HumanPlayingGameTest extends PlayingGameAbstractTest {

	/**
	 * Test method for {@link mathchess.Game#startGame()}.
	 * <li> Game create with an AI Minimax player and a Human player
	 * @throws Exception 
	 */
	@Test
	public void testStartGameAIMinmaxvsHuman() throws Exception {
		MessageConsole.disableAllMessages();
		((Rule_Minimax) blueMiniMaxRule).setEnemy(redHumanPlayer);
		game = new Game(board, blueAIMinimaxPlayer, redHumanPlayer);
		((GameAbstract) game).setFirstPlayer(true);
		((Game) game).setGameMode(GameMode.MAX_SCORE);
		((Game) game).setWinScore(45);
		game.startGame();
	}

	/**
	 * Test method for {@link mathchess.Game#startGame()}.
	 * <li> Game create with an AI Random player and a Human player
	 * @throws Exception 
	 */
	@Test
	public void testStartGameAIRandomvsHuman() throws Exception {
		MessageConsole.disableAllMessages();
		// MessageConsole.setINFOR(true);
		((Rule_Manger) blueManageRule).setRules(0);
		((Rule_Manger) blueManageRule).setEnemy(redHumanPlayer);
		game = new Game(board, blueAIManagePlayer, redHumanPlayer);
		((GameAbstract) game).setFirstPlayer(true);
		((Game) game).setGameMode(GameMode.MAX_SCORE);
		((Game) game).setWinScore(45);
		game.startGame();
	}

	/**
	 * Test method for {@link mathchess.Game#startGame()}.
	 * <li> Game create with a human player and another Human player
	 * @throws Exception 
	 */
	@Test
	public void testStartGameHumanvsHuman() throws Exception {
		game = new Game(board, blueHumanPlayer, redHumanPlayer);
		((GameAbstract) game).setFirstPlayer(true);
		((Game) game).setGameMode(GameMode.MAX_SCORE);
		((Game) game).setWinScore(45);
		game.startGame();
	}

}
