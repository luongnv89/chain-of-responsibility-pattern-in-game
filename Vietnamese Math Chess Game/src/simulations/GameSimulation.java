/**
 * 
 */
package simulations;

import java.util.Scanner;

import mathchess.AIPlayer;
import mathchess.Board;
import mathchess.Game;
import mathchess.HumanPlayer;
import rules.Rule_Manger;
import specifications.BoardInterface;
import specifications.GameInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.MessageConsole;
import abstractions.GameAbstract;
import abstractions.PlayerAbstract;

/**
 * {@link GameSimulation} simulate a game application
 * <br> Present a game as an application. When the application start:
 * <br> Config game:
 * <li> Choose game mode {@link GameSimulation#chooseGameMode()}
 * <br> - AI vs AI
 * <br> - Human vs AI
 * <br> - Human vs Human
 * <br>
 * <br> To setting a Human player: {@link GameSimulation#createHumanPlayer()} in case this is the first time create player
 * <br> To setting a Human player: {@link GameSimulation#createHumanPlayer()} in case this is the second time create player (already know about the color of player)
 * <br>
 * <br> To setting an AI player: {@link GameSimulation#createAIPlayer(boolean)}
 * <br> With an AI player, to setting the rule which AI player will use in game, {@link GameSimulation#chooseWayLoadRules()}
 * <br>
 * <br> Before start game, need to set the first player of game.
 *
 */
public class GameSimulation {
	private static BoardInterface board;
	private static PlayerInterface player1;
	private static PlayerInterface player2;
	private static GameInterface game;

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		System.out
				.println("***WELCOME TO OBJECT ORIENTED DEVELOPMENT PROJECT ***");
		configGame();
		board = new Board();
		game = new Game(board, player1, player2);
		System.out.println("\n***Choose player will go first ");
		System.out.println("[0]" + ((PlayerAbstract) player1).getPlayerName());
		System.out.println("[1]" + ((PlayerAbstract) player2).getPlayerName());
		int firstPlayer = -1;
		Scanner input = new Scanner(System.in);
		while (firstPlayer < 0 || firstPlayer > 1) {
			System.out.println("Player go first:");
			try {
				firstPlayer = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println(e.toString());
				firstPlayer = -1;
			}
		}
		if (firstPlayer == 1)
			((GameAbstract) game).setFirstPlayer(player1.isBlue());
		else {
			((GameAbstract) game).setFirstPlayer(player2.isBlue());
		}
		MessageConsole.disableAllMessages();
		game.startGame();
	}

	/**
	 *Config parameters of game 
	 * @throws Exception 
	 */
	private static void configGame() throws Exception {

		// Choose game mode
		int gameMode = chooseGameMode();
		switch (gameMode) {
		case 0:
			System.out.println("Game mode: AI vs AI");
			player1 = createAIPlayer(true);
			player2 = createAIPlayer(false);
			((AIPlayer) player1).setEnemy(player2);
			((AIPlayer) player2).setEnemy(player1);
			break;
		case 1:
			System.out.println("Game mode: AI vs Human");
			player1 = createHumanPlayer();
			player2 = createAIPlayer(!player1.isBlue());
			((AIPlayer) player2).setEnemy(player1);
			break;
		case 2:
			System.out.println("Game mode: Human vs Human");
			player1 = createHumanPlayer();
			player2 = createHumanPlayer(!player1.isBlue());
			break;
		default:
			throw new Exception("Error! Cannot choose game mode!");
		}
	}

	/**
	 * Create a new human player with the color is known
	 * @param b
	 * @return
	 */
	private static PlayerInterface createHumanPlayer(boolean b) {
		Scanner input = new Scanner(System.in);
		// Name
		System.out.println("Player name: ");
		String name = input.nextLine();
		return new HumanPlayer(name, b);
	}

	/**
	 * Setting mode of game:
	 * 
	 * @return
	 * <li> 0 : AI vs AI
	 * <li> 1 : Human vs AI
	 * <li> 2 : Human vs Human
	 */
	private static int chooseGameMode() {
		System.out.println("\n***GAME MODE: ");
		System.out.println("[0] AI vs AI");
		System.out.println("[1] Human vs AI");
		System.out.println("[2] Human vs Human");
		int mode = -1;
		Scanner input = new Scanner(System.in);
		while (mode < 0 || mode > 2) {
			System.out.println("Choose mode of game:");
			try {
				mode = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println(e.toString());
				mode = -1;
			}
		}
		return mode;
	}

	/**
	 * Create a human player
	 * <li> Create name of player
	 * <li> Create color of player
	 * @return a Human player
	 */
	private static PlayerInterface createHumanPlayer() {
		Scanner input = new Scanner(System.in);
		// Name
		System.out.println("Player name: ");
		String name = input.nextLine();

		// Color
		System.out.println("\n***Player color");
		System.out.println("[0] Blue");
		System.out.println("[1] Red");
		int color = -1;
		while (color < 0 || color > 1) {
			System.out.println("Choose player color: ");
			try {
				color = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println(e.toString());
				color = -1;
			}
		}
		boolean isBlue = color == 0 ? true : false;
		return new HumanPlayer(name, isBlue);
	}

	/**
	 * Create an AIplayer 
	 * @param isBlue
	 * @param player22 
	 * @return an AIplayer
	 * @throws Exception
	 */
	private static PlayerInterface createAIPlayer(boolean isBlue)
			throws Exception {
		// Name
		String name = (isBlue ? "Blue" : "Red") + " AIPlayer";

		// Color
		RuleInterface rule = new Rule_Manger();
		int wayLoadRule = chooseWayLoadRules();

		switch (wayLoadRule) {
		case 0:
			System.out.println("Choose and order rules randomly");
			((Rule_Manger) rule).setRules(0);
			break;
		case 1:
			System.out.println("Choose and order rules manually");
			((Rule_Manger) rule).setRules(1);
			break;
		case 2:
			System.out.println("Load rules from text file");
			System.out.println("Path to rules file: ");
			Scanner input = new Scanner(System.in);
			String path = input.nextLine();
			((Rule_Manger) rule).setRules(path);
			break;
		case 3:
			System.out.println("GoodRules");
			((Rule_Manger) rule).setRules(2);
			break;
		case 4:
			System.out.println("BadRules");
			((Rule_Manger) rule).setRules(3);
			break;
		case 5:
			System.out.println("Minimax rule");
			((Rule_Manger) rule).setRules(4);
			break;
		default:
			throw new Exception("Error! Cannot load rule for AI player");
		}
		AIPlayer player = new AIPlayer(name, isBlue, rule);
		System.out.println("Rules of player: ");
		((Rule_Manger) rule).showRules(true);
		return player;
	}

	private static int chooseWayLoadRules() {
		System.out.println("\n***Ways to load rules for AI player");
		System.out.println("[0] Choose and order rules randomly");
		System.out.println("[1] Choose and order rules manually");
		System.out.println("[2] Load rules from text file");
		System.out.println("[3] GoodRules");
		System.out.println("[4] BadRules");
		System.out.println("[5] Minimax rule");
		Scanner input = new Scanner(System.in);
		int wayLoadRule = -1;
		while (wayLoadRule < 0 || wayLoadRule > 5) {
			System.out.println("Choose the way to load rules: ");
			try {
				wayLoadRule = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println(e.toString());
				wayLoadRule = -1;
			}
		}
		return wayLoadRule;
	}

}
