/**
 * 
 */
package specifications;

/**
 * An interface which represents a move in a game.
 * It allows to check the validation of a move
 */
public interface MoveInterface extends HasInvariant {

	/**
	 * Get the coordinate of move
	 * @param index
	 * @return
	 * <li> the coordinate x1 of move if index = 0
	 * <li> the coordinate y1 of move if index = 1
	 * <li> the coordinate x2 of move if index = 2
	 * <li> the coordinate y2 of move if index = 3
	 */
	public int getCoordinate(int index);
}
