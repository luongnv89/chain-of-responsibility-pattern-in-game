package specifications;

import utils.GameExceptions;


/**
 * {@link GameInterface} represent interfaces of Game
 *
 */
public interface GameInterface extends HasInvariant {

	/**
	 * Start a game
	 * @throws GameExceptions 
	 * @throws Exception 
	 */
	public void startGame() throws GameExceptions, Exception;

	/**
	 * Reset a game means renew a game:
	 * <li> reset the board {@link BoardInterface#resetBoard()}
	 */
	public void resetGame();

	/**
	 * Check the status of game is over or not
	 * @return <li> true if game is over
	 * <li> false if game isn't over
	 */
	public boolean isGameOver();

}
