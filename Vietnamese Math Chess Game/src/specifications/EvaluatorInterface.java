/**
 * 
 */
package specifications;

/**
 * {@link EvaluatorInterface} represent the interface of evaluator which use to evaluate the value of player in game
 *
 */
public interface EvaluatorInterface extends HasInvariant {
	/**
	 * Calculate the value of a player in the board game
	 * @param player
	 * @param board
	 * @return an Integer number represent the value of player in board game 
	 */
	public int evaluate(PlayerInterface player, BoardInterface board);
}
