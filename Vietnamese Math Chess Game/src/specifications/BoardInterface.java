package specifications;

/**
 * BoardSpecification represent a board game.
 * <br> Board game is represented by integer 2D array.
 */
public interface BoardInterface extends HasInvariant {

	/**
	 * The board make a move
	 * @param move
	 * @return <li> true if the move has been done successfully
	 * <li> false if the move couldn't be done
	 */
	public boolean makeMove(MoveInterface move);

	/**
	 * Create a board with the state of starting the game.
	 */
	public void initial();

	/**
	 * Reset a board then the values of all the positions on a board are 0.
	 */
	public void resetBoard();

	/**
	 * Get the width of a board
	 * @return
	 */
	public int getWidth();

	/**
	 * Get the height of a board
	 * @return
	 */
	public int getHeight();

	/**
	 * Get the value of the position (x, y) on a board.
	 * @param i
	 * @param j
	 * @return an integer value of the state of the position.
	 */
	public int getState(int x, int y);

	/**
	 * Undo the last move of the game
	 * @return <li> true if undo successfully
	 * <li> false if there is some problem when try to undo
	 */
	public boolean undoLastMove();

	/**
	 * Check a move on the board can be done or cannot
	 * @param move
	 * @return <li> true if the move can be done on the board
	 * <li> false if the move cannot be done on the board
	 */
	public boolean canMakeMove(MoveInterface move);

}
