/**
 * 
 */
package specifications;

/**
 * {@link ChessmanInterface} represent a Chess-men in a game.
 */
public interface ChessmanInterface extends HasInvariant {
	
	/**
	 * Get the value of chess-man
	 * @return
	 */
	public int getValue();

	/**
	 * Get the x coordinate of chess-man 
	 * @return
	 */
	public int getX();

	/**
	 * Get the y coordinate of chess-man
	 * @return
	 */
	public int getY();

	/**
	 * Update the position of chess-man on a board
	 * @param x
	 * @param y
	 */
	public void update(int x, int y);
}
