/**
 * 
 */
package specifications;

import utils.GameExceptions;

/**
 * {@link RuleInterface} Represent the interface of Rule in game which the AI player will use to calculate move
 *
 */
public interface RuleInterface extends HasInvariant {
	
	/**
	 * Set the player will use the rule
	 * @param player
	 * @throws GameExceptions 
	 */
	public void setPlayer(PlayerInterface player) throws GameExceptions;
	
	/**
	 * Get a move by using rule
	 * @param board
	 * @return a possible move
	 * @throws GameExceptions
	 * @throws Exception
	 */
	MoveInterface getMove(BoardInterface board) throws GameExceptions, Exception;

	
}
