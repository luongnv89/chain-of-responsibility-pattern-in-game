/**
 * 
 */
package specifications;


/**
 * {@link GraphicInterface} represent the interface of a graphic object of game which will represent the board state in game
 *
 */
public interface GraphicInterface extends HasInvariant {
	/**
	 * display the board game on the console
	 */
	public void view();

	/**
	 * Reset graphic when game reset
	 */
	public void resetGraphic();
}
