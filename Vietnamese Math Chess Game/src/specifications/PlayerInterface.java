package specifications;

import utils.GameExceptions;

public interface PlayerInterface extends HasInvariant {

	/**
	 * <li> AI player will use algorithms to calculate and return the best move.
	 * <li> Human player inputs a move from the keyboard.
	 * @return a new move of player
	 * @throws GameExceptions 
	 * @throws Exception 
	 */
	public MoveInterface getMove(BoardInterface board) throws GameExceptions,
			Exception;

	/**
	 * Make a move  on a board:
	 * @param move
	 * @param board
	 * @return <li> true if make move successfully
	 * <li> false if make move unsuccessfully
	 * @throws GameExceptions
	 */
	public boolean makeMove(MoveInterface move, BoardInterface board)
			throws GameExceptions;

	/**
	 * undo the last move of game
	 * @param board
	 * @return
	 * <li> true if undo successfully
	 * <li> false if undo unsuccessfully
	 * @throws GameExceptions 
	 */
	public boolean undoLastMove(BoardInterface board) throws GameExceptions;

	/**
	 * a game has two players: blue and red
	 * @return 
	 * <li> true if the player is the blue player
	 * <li> false if the player is the red player 
	 */
	public boolean isBlue();

}
