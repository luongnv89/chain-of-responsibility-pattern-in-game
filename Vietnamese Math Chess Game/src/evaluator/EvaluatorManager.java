/**
 * 
 */
package evaluator;

import java.util.ArrayList;

import specifications.BoardInterface;
import specifications.EvaluatorInterface;
import specifications.PlayerInterface;
import utils.MessageConsole;

/**
 * {@link EvaluatorManager} manage evaluators which will be used to calculate the value of player in game.
 * Evaluators are contained in a list {@link EvaluatorManager#listEvaluators} 
 *
 */
public class EvaluatorManager implements EvaluatorInterface {

	/**
	 * List evaluators will be used 
	 */
	ArrayList<EvaluatorAbstract> listEvaluators = new ArrayList<EvaluatorAbstract>();

	/**
	 * Add new evaluator into the list evaluators
	 * <li> If the evaluator already exists in the list, it won't be added into list.
	 * @param eva
	 */
	public void addEvaluator(EvaluatorInterface eva) {
		boolean invalid = false;

		for (int i = 0; i < listEvaluators.size(); i++) {
			if (listEvaluators.get(i).getId() == ((EvaluatorAbstract) eva)
					.getId()) {
				MessageConsole
						.debugMessage("The evaluator already exists. Id: "
								+ ((EvaluatorAbstract) eva).getId());
				invalid = true;
				break;
			}
		}
		if (!invalid)
			listEvaluators.add((EvaluatorAbstract) eva);
	}
 
	@Override
	public int evaluate(PlayerInterface player, BoardInterface board) {
		int total = 0;
		for (int i = 0; i < listEvaluators.size(); i++) {
			total = total + listEvaluators.get(i).evaluate(player, board)
					* listEvaluators.get(i).getCoef();
		}
		return total;
	}

	/**
	 * Load all Evaluators
	 * <li> It's possible to add new evaluator for player in the board game
	 */
	public void loadAllEvaluator() {
		listEvaluators.add(new EvaluatorValue());
		listEvaluators.add(new EvaluatorDominate());
		listEvaluators.add(new EvaluatorZero());
	}

	/**
	 * @return false if the EvaluatorManager doesn't have any Evaluator
	 * */
	@Override
	public boolean invariant() {
		return !listEvaluators.isEmpty();
	}

	/**
	 * Get the list of evaluators which are used by the AI player
	 * @return the listEvaluators
	 */
	public ArrayList<EvaluatorAbstract> getListEvaluators() {
		return listEvaluators;
	}

}
