/**
 * 
 */
package evaluator;

import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.PlayerInterface;

/**
 * {@link EvaluatorValue} evaluate the value of player by the total value of all the chess-men which the player has currently
 *
 */
public class EvaluatorValue extends EvaluatorAbstract {

	public EvaluatorValue() {
		id = 1;
		coef = 1;
	}

	@Override
	public int evaluate(PlayerInterface player, BoardInterface board) {
		int value = 0;
		for (int i = 0; i < ((PlayerMathChess) player).getListChessmen().size(); i++) {
			value = value
					+ ((PlayerMathChess) player).getListChessmen().get(i)
							.getValue() % 10;
		}
		return value * coef;
	}
}
