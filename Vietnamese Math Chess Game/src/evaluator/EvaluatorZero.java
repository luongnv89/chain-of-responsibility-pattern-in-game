/**
 * 
 */
package evaluator;

import mathchess.Board;
import mathchess.Chessman;
import mathchess.Move;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.ChessmanInterface;
import specifications.PlayerInterface;
import utils.MessageConsole;

/**
 * In the Vietnam math chess game, the Zero chessman is the most important chessman on the board.
 * <li> If any player catch the Zero chessman of enemy, he win!
 * 
 * {@link EvaluatorZero} evaluator the attaching enemy's Zero chess-man potential of player.
 * <br> It is evaluated by total potential of all chess-men which the player currently has.
 *
 */
public class EvaluatorZero extends EvaluatorAbstract {

	/**
	 * The value of Zero chessman
	 */
	private static int ZERO_WEIGHT = 10000;
	/**
	 * The coef of value of chessman when chessman can see the Zero chessman of enemy
	 */
	private int coefSeeZero = 10;

	public EvaluatorZero() {
		id = 3;
		coef = 5;
	}

	@Override
	public int evaluate(PlayerInterface player, BoardInterface board) {
		int evaluate = 0;
		for (int i = 0; i < ((PlayerMathChess) player).getListChessmen().size(); i++) {
			evaluate = evaluate
					+ evaluateChessmanZero(((PlayerMathChess) player)
							.getListChessmen().get(i), board);
		}
		return evaluate;
	}

	/**
	 * Evaluate the Zero catching potential of a chess-man of player
	 * <br> It is evaluated by two value:
	 * <li> If the chess-man can catch the enemy's Zero chess-man: it will get {@link EvaluatorZero#ZERO_WEIGHT} point 
	 * <li> If the chess-man is at position where can see the enemy's Zero chess-man in the region where the chess-man can see: it will get {@link EvaluatorZero#ZERO_WEIGHT}/ {@link EvaluatorZero#coefSeeZero} point   
	 * @param chessman
	 * @param board
	 * @return 
	 */
	private int evaluateChessmanZero(ChessmanInterface chessman,
			BoardInterface board) {
		int evalCatch = 0;

		evalCatch += zeroCaughtEvaluate(chessman, board);

		evalCatch += seeZeroEvaluate(chessman, board);
		return evalCatch;
	}

	/**
	 * Check if chessman can see the Zero chessman of enemy
	 * @param chessman
	 * @param board
	 * @param evalCatch
	 * @return <li> 0 if the chessman cannot see the Zero chessman of enemy
	 * <li> ZERO_WEIGHT / coefSeeZero if the chessman can see the Zero chessman of enemy
	 */
	private int seeZeroEvaluate(ChessmanInterface chessman, BoardInterface board) {
		int evalCatch = 0;
		int value = chessman.getValue();
		if (value < 10) {
			if (((Board) board).isEmptyPath(chessman.getX(), chessman.getY(),
					4, 9)
					&& (Math.abs(chessman.getX() - 4) <= value)
					|| (Math.abs(chessman.getY() - 9) <= value)) {
				evalCatch = evalCatch + ZERO_WEIGHT / coefSeeZero;
			}
		} else {
			value = value % 10;
			if (((Board) board).isEmptyPath(chessman.getX(), chessman.getY(),
					4, 1)
					&& (Math.abs(chessman.getX() - 4) <= value)
					|| (Math.abs(chessman.getY() - 1) <= value)) {
				evalCatch = evalCatch + ZERO_WEIGHT / coefSeeZero;
			}
		}
		return evalCatch;
	}

	/**
	 * Evaluate value of chessman when the chessman can catch the Zero chessman of enemy
	 * @param chessman
	 * @param evalCatch
	 * @return <li> 0 if the chessman cannot catch the Zero chessman of enemy
	 * <li> ZERO_WEIGHT if the chessman can catch the Zero chessman of enemy
	 */
	private int zeroCaughtEvaluate(ChessmanInterface chessman,
			BoardInterface board) {
		int evalCatch = 0;
		for (int i = 0; i < ((Chessman) chessman).getListMove(2).size(); i++) {
			Move catchMove = ((Chessman) chessman).getListMove(2).get(i);
			int caughtChessman = ((Board) board).getDestMove(catchMove);
			if ((caughtChessman != 0 && caughtChessman % 10 == 0)) {
				MessageConsole.debugMessage("Can catch the Zero chessman!!!"
						+ catchMove.toString());
				evalCatch = ZERO_WEIGHT;
			}

		}
		return evalCatch; 
	}
}
