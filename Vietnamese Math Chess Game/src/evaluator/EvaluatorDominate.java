/**
 * 
 */
package evaluator;

import mathchess.Board;
import mathchess.Chessman;
import mathchess.Move;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.ChessmanInterface;
import specifications.PlayerInterface;

/**
 * {@link EvaluatorDominate} evaluate the domination of player on the board.
 * <br> It is evaluated by three value:
 * <li> The number of move can be done
 * <li> The number of position trap where if the enemy's chess-man is there, it can be caught by player
 * <li> The total value of enemy's chess-men which can be caught by player.
 * <br> It is the total result of evaluating all the chess-men of player
 *
 */
public class EvaluatorDominate extends EvaluatorAbstract {

	/**
	 * The coef of movable
	 */
	int moveCoef = 1;
	/**
	 * The coef of trap
	 */
	int trapCoef = 10;
	/**
	 * The coef of catching
	 */
	int catchCoef = 20;

	public EvaluatorDominate() {
		id = 2;
		coef = 1;
	}

	@Override
	public int evaluate(PlayerInterface player, BoardInterface board) {
		int evaluate = 0;

		for (int i = 0; i < ((PlayerMathChess) player).getListChessmen().size(); i++) {
			ChessmanInterface currentChessman = ((PlayerMathChess) player)
					.getListChessmen().get(i);
			int evalChesman = evaluateChessmanDominate(currentChessman, board);
			evaluate = evaluate + evalChesman;
		}

		return evaluate;
	}

	/**
	 * Evaluate the domination value of a chess-man of player 
	 * @param chessman
	 * @param board
	 * @return
	 */
	private int evaluateChessmanDominate(ChessmanInterface chessman,
			BoardInterface board) {
		int evalMove = ((Chessman) chessman).getListMove(0).size();
		int evalTrap = ((Chessman) chessman).getListMove(1).size();
		int evalCatch = 0;
		for (int i = 0; i < ((Chessman) chessman).getListMove(2).size(); i++) {
			Move caughtMove = ((Chessman) chessman).getListMove(2).get(i);
			int caughtChessman = ((Board) board).getDestMove(caughtMove);
			evalCatch = evalCatch + caughtChessman % 10;
		}

		return evalMove * moveCoef + evalCatch * catchCoef + evalTrap
				* trapCoef;
	}

	/**
	 * Set the coef of parameters
	 * @param moveCoef
	 * @param trapCoef
	 * @param catchCoef
	 */
	public void setCoefs(int moveCoef, int trapCoef, int catchCoef) {
		this.moveCoef = moveCoef;
		this.trapCoef = trapCoef;
		this.catchCoef = catchCoef;
	}
 
}
