/**
 * 
 */
package evaluator.tests;

import static org.junit.Assert.*;

import mathchess.Board;
import mathchess.GraphicConsole;
import mathchess.HumanPlayer;
import mathchess.Move;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import specifications.BoardInterface;
import specifications.GraphicInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;
import utils.MessageConsole;

import evaluator.MiniMaxCalculator;

/**
 * Test for class {@link MiniMaxCalculator}
 *
 */
public class MiniMaxCalculatorTest {

	MiniMaxCalculator calOK1;
	MiniMaxCalculator calOK2;
	MiniMaxCalculator calKO;

	PlayerInterface bluePlayer;
	PlayerInterface redPlayer;
	BoardInterface board;
	GraphicInterface graphic;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Board();
		graphic = new GraphicConsole(board);
		bluePlayer = new HumanPlayer("BluePlayer", true);
		redPlayer = new HumanPlayer("RedPlayer", false);
		calOK1 = new MiniMaxCalculator(bluePlayer, redPlayer, board);
		calOK2 = new MiniMaxCalculator(redPlayer, bluePlayer, board);
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#MiniMaxCalculator(specifications.PlayerInterface, specifications.PlayerInterface, specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testMiniMaxCalculator() throws GameExceptions {
		assertTrue(calOK1.invariant());
		assertTrue(calOK2.invariant());
		assertTrue(calOK1.getListVisitedNode().isEmpty());
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#MiniMaxCalculator(specifications.PlayerInterface, specifications.PlayerInterface, specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testMiniMaxCalculatorExceptionTwoRed() throws GameExceptions {
		calKO = new MiniMaxCalculator(redPlayer, redPlayer, board);
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#MiniMaxCalculator(specifications.PlayerInterface, specifications.PlayerInterface, specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testMiniMaxCalculatorExceptionTwoBlue() throws GameExceptions {
		calKO = new MiniMaxCalculator(bluePlayer, bluePlayer, board);
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#isVisited(specifications.BoardInterface)}.
	 */
	@Test
	public void testIsVisited() {
		assertFalse(calOK1.isVisited(board));
		calOK1.addNewVisitedNode(board);
		assertTrue(calOK1.isVisited(board));
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#resetCaculator()}.
	 */
	@Test
	public void testResetCaculator() {
		assertFalse(calOK1.isVisited(board));
		calOK1.addNewVisitedNode(board);
		assertTrue(calOK1.isVisited(board));
		calOK1.resetCaculator();
		assertFalse(calOK1.isVisited(board));

	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#addNewVisitedNode(specifications.BoardInterface)}.
	 */
	@Test
	public void testAddNewVisitedNode() {
		assertFalse(calOK1.isVisited(board));
		calOK1.addNewVisitedNode(board);
		assertTrue(calOK1.isVisited(board));
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#calculateStateScore()}.
	 * @throws Exception 
	 */
	@Test
	public void testCalculateStateScore() throws Exception {
		assertTrue(calOK1.calculateStateScore() == 0);
		System.out.println(calOK1.calculateStateScore());
		assertTrue(bluePlayer.makeMove(new Move(4, 0, 5, 1), board));
		((PlayerMathChess) bluePlayer).calculateMove(board);
		((PlayerMathChess) redPlayer).calculateMove(board);
		assertTrue(calOK1.calculateStateScore() != 0);
		System.out.println(calOK1.calculateStateScore());
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#expandMinNode(int, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testExpandMinNode() throws Exception {
		MessageConsole.disableAllMessages();
		((PlayerMathChess) bluePlayer).calculateMove(board);
		((PlayerMathChess) redPlayer).calculateMove(board);

		int MAX = Integer.MAX_VALUE;
		System.out.println(calOK1.expandMinNode(1, MAX));
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#expandMaxNode(int, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testExpandMaxNode() throws Exception {
		MessageConsole.disableAllMessages();
		((PlayerMathChess) bluePlayer).calculateMove(board);
		((PlayerMathChess) redPlayer).calculateMove(board);

		int MIN = Integer.MIN_VALUE;
		System.out.println(calOK1.expandMinNode(1, MIN));
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#getListVisitedNode()}.
	 */
	@Test
	public void testGetListVisitedNode() {
		assertTrue(calOK1.getListVisitedNode().isEmpty());
		calOK1.addNewVisitedNode(board);
		assertTrue(calOK1.getListVisitedNode().size() == 1);
	}

	/**
	 * Test method for {@link evaluator.MiniMaxCalculator#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(calOK1.invariant());
		assertTrue(calOK2.invariant());
	}

}
