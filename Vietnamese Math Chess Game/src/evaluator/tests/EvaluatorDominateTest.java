/**
 * 
 */
package evaluator.tests;

import mathchess.Board;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import evaluator.EvaluatorDominate;

/**
 * Test for class {@link EvaluatorDominate}
 *
 */
public class EvaluatorDominateTest extends EvaluatorAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		eval = new EvaluatorDominate();
	}

	/**
	 * Test method for {@link evaluator.EvaluatorDominate#setCoefs(int, int, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testSetCoefIntIntInt() throws Exception {
		((PlayerMathChess) bluePlayer).getListChessmen().get(4).update(5, 1);
		((Board) board).setState(5, 1, 5);
		((Board) board).setState(4, 0, 0);

		((PlayerMathChess) redPlayer).getListChessmen().get(4).update(3, 9);
		((Board) board).setState(4, 10, 0);
		((Board) board).setState(3, 9, 15);
		graphic.view();
		((PlayerMathChess) bluePlayer).calculateMove(board);
		System.out.println("Evaluator value (Blue): "
				+ eval.evaluate(bluePlayer, board));
		((PlayerMathChess) redPlayer).calculateMove(board);
		System.out.println("Evaluator value (Red): "
				+ eval.evaluate(redPlayer, board));
	}

	@Test
	public void testSetCoefs() {
		((EvaluatorDominate) eval).setCoefs(1, 2, 4);
	}
}
