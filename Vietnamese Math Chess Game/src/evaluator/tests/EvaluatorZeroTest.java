/**
 * 
 */
package evaluator.tests;

import mathchess.Board;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import evaluator.EvaluatorZero;

/**
 *Test for class {@link EvaluatorZero}
 *
 */
public class EvaluatorZeroTest extends EvaluatorAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		eval = new EvaluatorZero();
	}

	/**
	 * Test method for {@link evaluator.EvaluatorZero#evaluate(specifications.PlayerInterface, specifications.BoardInterface)}.
	 * @throws Exception 
	 */
	@Test
	public void testEvaluate() throws Exception {
		((PlayerMathChess) bluePlayer).getListChessmen().get(4).update(6, 7);
		// ((PlayerAbstract) bluePlayer).getListChessmans().get(5).update(5, 8);
		((Board) board).setState(6, 7, 5);
		// ((Board) board).setState(5, 8, 4);
		((Board) board).setState(4, 0, 0);
		// ((Board) board).setState(5, 0, 0);
		graphic.view();
		((PlayerMathChess) bluePlayer).calculateMove(board);
		((PlayerMathChess) redPlayer).calculateMove(board);
		System.out.println("Blue:" + eval.evaluate(bluePlayer, board));
		System.out.println("Red: " + eval.evaluate(redPlayer, board));
	}

}
