/**
 * 
 */
package evaluator.tests;

import static org.junit.Assert.*;

import mathchess.Board;
import mathchess.GraphicConsole;
import mathchess.HumanPlayer;
import mathchess.PlayerMathChess;

import org.junit.Before;
import org.junit.Test;

import specifications.BoardInterface;
import specifications.EvaluatorInterface;
import specifications.GraphicInterface;
import specifications.PlayerInterface;

import evaluator.EvaluatorDominate;
import evaluator.EvaluatorManager;
import evaluator.EvaluatorValue;
import evaluator.EvaluatorZero;

/**
 * Test for class {@link EvaluatorManager}
 *
 */
public class EvaluatorManagerTest {
	protected static BoardInterface board;
	protected static PlayerInterface bluePlayer;
	protected static PlayerInterface redPlayer;
	protected static GraphicInterface graphic;
	protected EvaluatorInterface eval;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		eval = new EvaluatorManager();
		board = new Board();
		((Board) board).initial();
		graphic = new GraphicConsole(board);
		bluePlayer = new HumanPlayer("Blue", true);
		redPlayer = new HumanPlayer("Red", false);
		((PlayerMathChess) bluePlayer).getListChessmen().get(4).update(6, 7);
		((PlayerMathChess) bluePlayer).getListChessmen().get(5).update(5, 8);
		((Board) board).setState(6, 7, 5);
		((Board) board).setState(5, 8, 4);
		((Board) board).setState(4, 0, 0);
		((Board) board).setState(5, 0, 0);
		graphic.view();
		((PlayerMathChess) bluePlayer).calculateMove(board);
	}

	/**
	 * Test method for {@link evaluator.EvaluatorManager#addEvaluator(specifications.EvaluatorInterface)}.
	 */
	@Test
	public void testAddEvaluator() {
		System.out.println("\n-->Empty: " + eval.evaluate(bluePlayer, board));

		EvaluatorValue evalValue = new EvaluatorValue();
		System.out.println("\nEvaluator value: "
				+ evalValue.evaluate(bluePlayer, board));

		((EvaluatorManager) eval).addEvaluator(new EvaluatorValue());
		System.out.println("\n-->Add Evaluator value: "
				+ eval.evaluate(bluePlayer, board));

		EvaluatorDominate dominate = new EvaluatorDominate();
		System.out.println("\nEvaluator dominate: "
				+ dominate.evaluate(bluePlayer, board));

		((EvaluatorManager) eval).addEvaluator(new EvaluatorDominate());
		System.out.println("\n-->Add Evaluator dominate: "
				+ eval.evaluate(bluePlayer, board));

		EvaluatorZero zero = new EvaluatorZero();
		System.out.println("\nEvaluator zero: "
				+ zero.evaluate(bluePlayer, board));

		((EvaluatorManager) eval).addEvaluator(new EvaluatorZero());
		System.out.println("\n-->Add Evaluator zero: "
				+ eval.evaluate(bluePlayer, board));
	}

	/**
	 * Test method for {@link evaluator.EvaluatorManager#addEvaluator(specifications.EvaluatorInterface)}.
	 */
	@Test
	public void testAddEvaluatorAlreadyExist() {
		((EvaluatorManager) eval).loadAllEvaluator();
		int evaluate = eval.evaluate(bluePlayer, board);
		System.out.println("\n-->Evaluate value before add: " + evaluate);
		((EvaluatorManager) eval).addEvaluator(new EvaluatorZero());
		int evaluate2 = eval.evaluate(bluePlayer, board);
		assertTrue(evaluate == evaluate2);
		System.out.println("\n-->Evaluate value after add: " + evaluate);
		
	}

	/**
	 * Test method for {@link evaluator.EvaluatorManager#evaluate(specifications.PlayerInterface, specifications.BoardInterface)}.
	 */
	@Test
	public void testEvaluate() {
		((EvaluatorManager) eval).loadAllEvaluator();
		System.out.println("Human1: " + eval.evaluate(bluePlayer, board));
		System.out.println("Human2: " + eval.evaluate(redPlayer, board));
	}

	/**
	 * Test method for {@link evaluator.EvaluatorManager#loadAllEvaluator()}.
	 */
	@Test
	public void testLoadAllEvaluator() {
		assertTrue(((EvaluatorManager) eval).getListEvaluators().isEmpty());
		((EvaluatorManager) eval).loadAllEvaluator();
		assertFalse(((EvaluatorManager) eval).getListEvaluators().isEmpty());
	}

	/**
	 * Test method for {@link evaluator.EvaluatorManager#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertFalse(eval.invariant());
		((EvaluatorManager) eval).loadAllEvaluator();
		assertTrue(eval.invariant());
	}

	/**
	 * Test method for {@link evaluator.EvaluatorManager#getListEvaluators()}.
	 */
	@Test
	public void testGetListEvaluators() {
		((EvaluatorManager) eval).loadAllEvaluator();
		for (int i = 0; i < ((EvaluatorManager) eval).getListEvaluators()
				.size(); i++) {
			System.out.println(((EvaluatorManager) eval).getListEvaluators()
					.get(i).toString());
		}
	}

}
