/**
 * 
 */
package evaluator.tests;

import static org.junit.Assert.*;
import mathchess.Board;
import mathchess.GraphicConsole;
import mathchess.HumanPlayer;
import mathchess.PlayerMathChess;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import evaluator.EvaluatorAbstract;

import specifications.BoardInterface;
import specifications.EvaluatorInterface;
import specifications.GraphicInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;

/**
 * Test for class {@link EvaluatorAbstract}
 *
 */
public abstract class EvaluatorAbstractTest {

	protected static BoardInterface board;
	protected static PlayerInterface bluePlayer;
	protected static PlayerInterface redPlayer;
	protected static GraphicInterface graphic;
	protected EvaluatorInterface eval;

	@BeforeClass 
	public static void setUpClass() throws Exception {
		board = new Board();
		((Board) board).initial();
		graphic = new GraphicConsole(board);
		bluePlayer = new HumanPlayer("Blue", true);
		redPlayer = new HumanPlayer("Red", false);
		((PlayerMathChess) bluePlayer).calculateMove(board);
		((PlayerMathChess) redPlayer).calculateMove(board);
		graphic.view();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		eval = null;
	}

	/**
	 * Test for {@link EvaluatorAbstract#evaluate(PlayerInterface, BoardInterface)}
	 * @throws GameExceptions 
	 * @throws Exception 
	 */
	@Test
	public void testEvaluate() throws GameExceptions, Exception {
		System.out.println("Human1: " + eval.evaluate(bluePlayer, board));
		System.out.println("Human2: " + eval.evaluate(redPlayer, board));
	}

	/**
	 * Test for {@link EvaluatorAbstract#setCoef(int)}
	 */
	@Test
	public void testSetCoef() {
		((EvaluatorAbstract) eval).setCoef(2);
		System.out.println("Human1: " + eval.evaluate(bluePlayer, board));
		System.out.println("Human2: " + eval.evaluate(redPlayer, board));
	}
 
	/**
	 * Test for method {@link EvaluatorAbstract#invariant()}
	 */
	@Test
	public void testInvariant() {  
		assertTrue(eval.invariant());
		((EvaluatorAbstract) eval).setCoef(0);
		assertFalse(eval.invariant());
	}

}
