package evaluator.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ EvaluatorDominateTest.class, EvaluatorManagerTest.class,
		EvaluatorValueTest.class, EvaluatorZeroTest.class,
		MiniMaxCalculatorTest.class })
public class AllEvaluatorsTests {

}
