/**
 * 
 */
package evaluator;

import java.util.ArrayList;
import java.util.HashMap;

import mathchess.Board;
import mathchess.PlayerMathChess;
import specifications.BoardInterface;
import specifications.HasInvariant;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import abstractions.PlayerAbstract;

/**
 * {@link MiniMaxCalculator} calculate the Minimax value of board
 * <br> The blue player will get the maximum value of board
 * <br> The red player will get the minimum value of board
 * <br>Use minimax alpha beta cut to calculate move of player
 */
public class MiniMaxCalculator implements HasInvariant {
	/**
	 * Max player always  try to maximum the value of board 
	 */
	PlayerInterface maxPlayer;
	/**
	 * Min player always try to minimum the value of board
	 */
	PlayerInterface minPlayer;
	/**
	 * The board on which calculator will calculate the value
	 */
	BoardInterface board;
	/**
	 * The {@link EvaluatorManager} manage evaluators will be used by an AIPlayer
	 */
	EvaluatorManager evalManager = new EvaluatorManager();
	/**
	 * List all the state of board which has calculated value.
	 */
	public static HashMap<String, Integer> listCalculatedStates = new HashMap<String, Integer>();

	/**
	 * List all the state of board which is visited while calculate the value of board
	 */
	ArrayList<String> listVisitedNode;

	/**
	 * Construct a minimaxCalculator.
	 * @param maxPlayer is blue player
	 * @param minPlayer is red player
	 * @param board
	 * @throws GameExceptions when two input players are same color
	 */
	public MiniMaxCalculator(PlayerInterface player1, PlayerInterface player2,
			BoardInterface board) throws GameExceptions {
		if (player1.isBlue() && !player2.isBlue()) {
			this.maxPlayer = player1;
			this.minPlayer = player2;
		} else if (!player1.isBlue() && player2.isBlue()) {
			this.maxPlayer = player2;
			this.minPlayer = player1;
		} else {
			throw new GameExceptions(this,
					"@MiniMaxCalculator#: Constructor error!");
		}
		this.board = board;
		listVisitedNode = new ArrayList<String>();
		evalManager.loadAllEvaluator();
	}

	/**
	 * Check a board state is already visited or not
	 * @param board
	 * @return true if the board state is already visited
	 * <li> false if the board state isn't visited.
	 */
	public boolean isVisited(BoardInterface board) {
		return listVisitedNode.contains(board.toString());
	}

	/**
	 * Reset list of visited node to create new calculator
	 */
	public void resetCaculator() {
		listVisitedNode.clear();
	}

	/**
	 * Add new visited node into the list of visited nodes
	 * @param board
	 */
	public void addNewVisitedNode(BoardInterface board) {
		listVisitedNode.add(board.toString());
	}

	/**
	 * Calculate the value of board at current state
	 * <li> Blue player is the max player
	 * <li> Red player is the min player
	 * <li> The value of board is evaluated by the value of blue player - the value of red player
	 * 
	 * @return
	 */
	public int calculateStateScore() {
		int maxEval = 0;
		int minEval = 0;
		for (int i = 0; i < evalManager.getListEvaluators().size(); i++) {
			maxEval = maxEval
					+ evalManager.getListEvaluators().get(i)
							.evaluate(maxPlayer, board);
			minEval = minEval
					+ evalManager.getListEvaluators().get(i)
							.evaluate(minPlayer, board);
		}
		return maxEval - minEval;
	}

	/**
	 * Expand the min node.
	 * <br> Calculate the Minimax score of board when the red player make move
	 * <br> Red player will get the move which make the minimax score of board is minimum
	 * @param deeper
	 * @param minCut
	 * @return 
	 * @throws Exception 
	 */
	public int expandMinNode(int deeper, int minCut) throws Exception {
		MessageConsole.debugMessage("Get move for Min player: "
				+ ((PlayerAbstract) minPlayer).getPlayerName());
		int minValue = Integer.MAX_VALUE;

		// Check the over game
		if (((PlayerMathChess) minPlayer).isWinner())
			return Integer.MIN_VALUE;
		else {
			ArrayList<MoveInterface> listAllPossibleMoves = getListPossibleMoves(minPlayer);

			for (int i = 0; i < listAllPossibleMoves.size(); i++) {

				MoveInterface currentMove = listAllPossibleMoves.get(i);
				int destMove = ((Board) board).getDestMove(currentMove);
				minPlayer.makeMove(currentMove, board);
				if (destMove != 0)
					((PlayerMathChess) maxPlayer)
							.updateChessmanCaught(destMove);
				String str = board.toString();
				if (!isVisited(board)) {
					addNewVisitedNode(board);
					int score = calculateScore(deeper, minValue, str, true);
					// Cut
					if (score > minCut) {
						//System.out.println("maxCut: " + maxCut +" score: " + score);
						undoLastMoveOfGame();
						return score;
					}

					if (score < minValue) {
						minValue = score;
					}
				}
				undoLastMoveOfGame();
			}
			//			System.out.println("max: " + maxValue);
			return minValue;
		}
	}

	/**
	 * Undo the last move of Game
	 * @throws GameExceptions
	 */
	private void undoLastMoveOfGame() throws GameExceptions {
		minPlayer.undoLastMove(board);
		maxPlayer.undoLastMove(board);
		board.undoLastMove();
	}

	/**
	 * Get list all possible moves of player
	 * @return
	 */
	private ArrayList<MoveInterface> getListPossibleMoves(PlayerInterface player) {
		ArrayList<MoveInterface> listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(((PlayerMathChess) player)
				.getListAllPossibleCaught());
		listAllPossibleMoves.addAll(((PlayerMathChess) player)
				.getListAllPossibleMoves());
		return listAllPossibleMoves;
	}

	/**
	 * Calculate score of board
	 * <li> If deeper = 0 calculate score of board at current state by {@link MiniMaxCalculator#calculateStateScore()}
	 * <li> if deeper !=0, go to the deeper node
	 * <li> if isMin is true, return the value of {@link MiniMaxCalculator#expandMaxNode(int, int)}
	 * <li> if isMin is false, return the value of {@link MiniMaxCalculator#expandMinNode(int, int)}
	 * @param deeper
	 * @param maxValue
	 * @return score of board
	 * @throws Exception 
	 */
	private int calculateScore(int deeper, int maxValue, String str,
			boolean isMin) throws Exception {
		int score;
		// Calculate the dominate value at depth =0
		if (deeper == 0) {
			if (listCalculatedStates.containsKey(str)) {
				score = listCalculatedStates.get(str);
			} else {
				((PlayerMathChess) minPlayer).calculateMove(board);
				((PlayerMathChess) maxPlayer).calculateMove(board);
				score = calculateStateScore();
				listCalculatedStates.put(str, score);
			}
		} else {
			((PlayerMathChess) minPlayer).calculateMove(board);
			((PlayerMathChess) maxPlayer).calculateMove(board);
			if (isMin) {
				score = expandMaxNode(deeper - 1, maxValue);
			} else {
				score = expandMinNode(deeper - 1, maxValue);
			}
		}
		return score;
	}

	/**
	 *Expand the max node.
	 * <br> Calculate the Minimax score of board when the blue player make move
	 * <br> Move player will get the move which make the minimax score of board is maximum
	 * @param deeper
	 * @param maxCut
	 * @return
	 * @throws Exception 
	 */
	public int expandMaxNode(int deeper, int maxCut) throws Exception {
		MessageConsole.debugMessage("Get move for Max player: "
				+ ((PlayerAbstract) maxPlayer).getPlayerName());
		int maxValue = Integer.MIN_VALUE;

		// Check the over game
		if (((PlayerMathChess) maxPlayer).isWinner())
			return Integer.MAX_VALUE;
		else {
			ArrayList<MoveInterface> listAllPossibleMoves = getListPossibleMoves(maxPlayer);

			for (int i = 0; i < listAllPossibleMoves.size(); i++) {

				MoveInterface currentMove = listAllPossibleMoves.get(i);
				int destMove = ((Board) board).getDestMove(currentMove);
				maxPlayer.makeMove(currentMove, board);
				if (destMove != 0)
					((PlayerMathChess) minPlayer)
							.updateChessmanCaught(destMove);

				String str = board.toString();
				if (!isVisited(board)) {
					addNewVisitedNode(board);
					int score = calculateScore(deeper, maxValue, str, false);
					if (score < maxCut) {
						undoLastMoveOfGame();
						return score;
					}
					if (score > maxValue) {
						maxValue = score;
					}
				}
				undoLastMoveOfGame();
			}
			//			System.out.println("min: " + minValue);
			return maxValue;
		}
	}

	/**
	 * Get list all board states are visited when calculate the minimax score of board
	 * @return the listVisitedNode
	 */
	public ArrayList<String> getListVisitedNode() {
		return listVisitedNode;
	}

	/**
	 * @return false if :
	 * <li> The board isn't invariant
	 * <li> The max player or min player isn't invariant
	 * <li>	The evalManager isn't invariant
	 * */
	@Override
	public boolean invariant() {
		return evalManager.invariant() && board.invariant();
	}
}
