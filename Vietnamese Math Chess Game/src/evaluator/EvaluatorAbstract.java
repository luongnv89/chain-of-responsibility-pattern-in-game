/**
 * 
 */
package evaluator;

import specifications.EvaluatorInterface;

/**
 * {@link EvaluatorAbstract} represent an evaluator of game. Each evaluator will have an {@link EvaluatorAbstract#id} 
 * to identify the evaluator and a {@link EvaluatorAbstract#coef} . 
 * The id to avoid using one evaluator many times at one evaluation.
 * The coef of result of current evaluator in the total result of evaluators
 *
 */
public abstract class EvaluatorAbstract implements EvaluatorInterface {
	/**
	 * the id of evaluator to identify the evaluator
	 */
	protected int id;
	/**
	 * the coef of result of current evaluator in the total result of evaluators
	 */
	protected int coef;

	/**
	 * @return the id
	 */
	protected int getId() {
		return id;
	}

	/**
	 * @return the coef
	 */
	protected int getCoef() {
		return coef;
	}

	/**
	 * Set the coef of evaluator
	 * @param coef the coef to set
	 */
	public void setCoef(int coef) {
		this.coef = coef;
	}

	/*
	 * @return false if id <0 or coef <1
	 */
	@Override
	public boolean invariant() {
		return (id >= 0 && coef >= 1);
	}

}
