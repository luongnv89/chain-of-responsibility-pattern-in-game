var searchData=
[
  ['gameabstract',['GameAbstract',['../classabstractions_1_1GameAbstract.html#a4bd1520599c850c72e1e480ebf94f856',1,'abstractions::GameAbstract']]],
  ['getcoordinate',['getCoordinate',['../interfacespecifications_1_1MoveInterface.html#a62ebfb43437651a8b6fb162b5909ece9',1,'specifications.MoveInterface.getCoordinate()'],['../classxoconnect4_1_1Move.html#ac32dcfa54dd2271668b748c95b0fe670',1,'xoconnect4.Move.getCoordinate()']]],
  ['getcurrentplayer',['getCurrentPlayer',['../classabstractions_1_1GameAbstract.html#a1ac077085695d7310325cb11963d427c',1,'abstractions::GameAbstract']]],
  ['getheight',['getHeight',['../classabstractions_1_1BoardAbstract.html#a4e5cc0cd78807322cebd16c29288dbd4',1,'abstractions.BoardAbstract.getHeight()'],['../interfacespecifications_1_1BoardInterface.html#afb6c02aa6dac2ea1f2a8fb8bec9a42a4',1,'specifications.BoardInterface.getHeight()']]],
  ['gethistorymoves',['getHistoryMoves',['../classxoconnect4_1_1Board.html#ab4f3f4f91bdcc80a44e56252d19067d0',1,'xoconnect4::Board']]],
  ['getlastmove',['getLastMove',['../classxoconnect4_1_1Board.html#ae44f46ec2df78bf0380223f0d2628c9c',1,'xoconnect4::Board']]],
  ['getlistallpossiblemoves',['getListAllPossibleMoves',['../classxoconnect4_1_1Board.html#a869f0b849481f5966bc9f71964b2506e',1,'xoconnect4::Board']]],
  ['getmove',['getMove',['../classrules_1_1Rule__BlockWin.html#ad4a628b4dc45237aaa26011f3066719f',1,'rules.Rule_BlockWin.getMove()'],['../classrules_1_1Rule__Manger.html#af7db09c46c43b8fda4ec642d8b879213',1,'rules.Rule_Manger.getMove()'],['../classrules_1_1Rule__Random.html#a6f3c34638e93e74f3d270732eeb508d1',1,'rules.Rule_Random.getMove()'],['../classrules_1_1Rule__Win.html#ab2867cdef47acc471aa9fa398eff3a05',1,'rules.Rule_Win.getMove()'],['../interfacespecifications_1_1PlayerInterface.html#abf8d430f52713067a8cac0d1ee2feae9',1,'specifications.PlayerInterface.getMove()'],['../interfacespecifications_1_1RuleInterface.html#af958e888888b47b66de728e9427a1628',1,'specifications.RuleInterface.getMove()'],['../classxoconnect4_1_1AIPlayer.html#a95a0f0956d7b4a81267acb66ee0805c6',1,'xoconnect4.AIPlayer.getMove()'],['../classxoconnect4_1_1HumanPlayer.html#a3d7d1db5385f1d5e47dcd1fda342b34c',1,'xoconnect4.HumanPlayer.getMove()']]],
  ['getplayername',['getPlayerName',['../classabstractions_1_1PlayerAbstract.html#ac0531741a864308ceb29c5b09139fced',1,'abstractions::PlayerAbstract']]],
  ['getrule',['getRule',['../classxoconnect4_1_1AIPlayer.html#ae32b5aa976066fc7ed71b761231b7768',1,'xoconnect4::AIPlayer']]],
  ['getrulebyname',['getRuleByName',['../classrules_1_1Rule__Manger.html#afe64e22813a7b2ece1fa934c9e369ec8',1,'rules::Rule_Manger']]],
  ['getrulename',['getRuleName',['../classabstractions_1_1RuleAbstract.html#a226aa8e418f0dd542e8a958d7456111a',1,'abstractions::RuleAbstract']]],
  ['getstate',['getState',['../classabstractions_1_1BoardAbstract.html#aa2faf800d8359a9ea03ce78980fdc289',1,'abstractions.BoardAbstract.getState()'],['../interfacespecifications_1_1BoardInterface.html#a440bd9c0bca4cc0146de2e1d1a5fcbb4',1,'specifications.BoardInterface.getState()']]],
  ['getwidth',['getWidth',['../classabstractions_1_1BoardAbstract.html#a106d12ef51b3e92cf77f34c8edac6b9d',1,'abstractions.BoardAbstract.getWidth()'],['../interfacespecifications_1_1BoardInterface.html#af329e558b00ed47ac850e4a7b362fdbd',1,'specifications.BoardInterface.getWidth()']]],
  ['getwinmove',['getWinMove',['../classabstractions_1_1RuleAbstract.html#aefd32b7cf27253ddcf136abad0ccfed9',1,'abstractions::RuleAbstract']]]
];
