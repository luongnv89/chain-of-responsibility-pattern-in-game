var searchData=
[
  ['hasinvariant',['HasInvariant',['../interfacespecifications_1_1HasInvariant.html',1,'specifications']]],
  ['historymoves',['historyMoves',['../classxoconnect4_1_1Board.html#a8a1c54ac76124bf3c07c58e17ce26327',1,'xoconnect4::Board']]],
  ['humanconnect4playinggametest',['HumanConnect4PlayingGameTest',['../classsimulations_1_1HumanConnect4PlayingGameTest.html',1,'simulations']]],
  ['humanplayer',['HumanPlayer',['../classxoconnect4_1_1HumanPlayer.html',1,'xoconnect4']]],
  ['humanplayerabstracttest',['HumanPlayerAbstractTest',['../classabstractions_1_1tests_1_1HumanPlayerAbstractTest.html',1,'abstractions::tests']]],
  ['humanplayinggameabstracttest',['HumanPlayingGameAbstractTest',['../classsimulations_1_1abstractions_1_1HumanPlayingGameAbstractTest.html',1,'simulations::abstractions']]],
  ['humanxoplayinggametest',['HumanXOPlayingGameTest',['../classsimulations_1_1HumanXOPlayingGameTest.html',1,'simulations']]]
];
