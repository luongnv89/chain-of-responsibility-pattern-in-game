var searchData=
[
  ['blue',['blue',['../classabstractions_1_1PlayerAbstract.html#a5f3f8da5f90fcab11618ef4e9f578efb',1,'abstractions.PlayerAbstract.blue()'],['../classabstractions_1_1tests_1_1PlayerAbstractTest.html#ab30122ca0fac9132cc31bf29a2335ee0',1,'abstractions.tests.PlayerAbstractTest.blue()']]],
  ['bluename',['blueName',['../classabstractions_1_1tests_1_1PlayerAbstractTest.html#accfa5b593947a7e62d0ee5fabfa5e1df',1,'abstractions::tests::PlayerAbstractTest']]],
  ['blueplayer',['bluePlayer',['../classabstractions_1_1GameAbstract.html#af5d6e5330940a5ae2200f486fc1a413a',1,'abstractions::GameAbstract']]],
  ['board',['board',['../classabstractions_1_1GameAbstract.html#a5a84fd9c82b64854ae11ece62451cb5d',1,'abstractions.GameAbstract.board()'],['../classabstractions_1_1GraphicAbstract.html#ab2018304c22a3f8e3c878daa0ff6dffb',1,'abstractions.GraphicAbstract.board()'],['../classabstractions_1_1tests_1_1PlayerAbstractTest.html#a6070d45b8d418901606d4f70aac1b0a2',1,'abstractions.tests.PlayerAbstractTest.board()']]]
];
