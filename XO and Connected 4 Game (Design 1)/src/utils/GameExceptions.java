/**
 * 
 */
package utils;

/**
 * {@link GameExceptions} manage the exception of Vietnammese Math Chess game
 *
 */
public class GameExceptions extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GameExceptions(Object obj, String exception) {
		System.out.println("Exception: " + obj.getClass().getName() + ": "
				+ exception);
	}
}
