package xoconnect4;

import java.util.ArrayList;

import specifications.MoveInterface;

public class XOBoard extends Board {

	private final static int DEFAULT_SIZE = 3;

	public XOBoard() {
		super(DEFAULT_SIZE, DEFAULT_SIZE);
		numberOfPicesToWin = 3;

	}

	public XOBoard(int width, int height) {
		super(width, height);
		numberOfPicesToWin = 3;
	}

	protected ArrayList<MoveInterface> calculatePossibleMove() {
		ArrayList<MoveInterface> allPossibleMoves = new ArrayList<MoveInterface>();
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				allPossibleMoves.add(new Move(i, j));
			}
		}
		return allPossibleMoves;
	}

}
