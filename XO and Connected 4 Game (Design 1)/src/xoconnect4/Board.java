package xoconnect4;

import java.util.ArrayList;

import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import abstractions.BoardAbstract;

/**
 * {@link Board} extends from {@link BoardAbstract} present a board in both XO game and Connect4 game
 * <br> All the moves have done in board will be contained in {@link Board#historyMoves}
 * <br> There is one thing difference in the board over state of XO game and Connect4 game.
 * <li> In the XO game, the board state will has 3 pieces next together , present by {@link Board#numberOfPicesToWin} = 3
 * <li> In the Connect4 game, the board state will has 4 pieces next together , present by {@link Board#numberOfPicesToWin} = 4
 * 
 * <br> In the XO game and Connect4 game, the value of each piece of board is in {0,1,2}.
 * <li> 0 : empty position
 * <li> 1 : move of player 1
 * <li> 2 : move of player 2
 */
public abstract class Board extends BoardAbstract {

	/**
	 * Contains all moves have done in game
	 */
	protected ArrayList<MoveInterface> historyMoves;

	/**
	 * List all possible moves on the board
	 * <br> There is some differences between XO game and Connect4 game.
	 * <li> In the XO game, the possible move is any empty position of board. 
	 * <li> In the Connect4 game, a possible move is the first empty position in a column of board.
	 * <br> So in the Connect4 game, in a board state, the number of possible moves always less than or equals the width of board 
	 */
	protected ArrayList<MoveInterface> listAllPossibleMoves;
	/**
	 * The number of pieces next together in the board over state
	 */
	protected int numberOfPicesToWin;

	public Board(int width, int height) {
		super(width, height);
		historyMoves = new ArrayList<>();
		listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(calculatePossibleMove());
	}

	/**
	 * Calculate all possible moves on board at the beginning of game.
	 * <li> In the XO Game, all pieces of board are the possible move at the beginning of game
	 * <li> In the Connect4 game, all the highest pieces of board are the possible move at the beginning of game
	 * @return
	 */
	protected abstract ArrayList<MoveInterface> calculatePossibleMove();

	/**
	 * Get list positions where the player can play on the board
	 * @return List all possible moves on the board
	 */
	public ArrayList<MoveInterface> getListAllPossibleMoves() {
		return listAllPossibleMoves;
	}

	/**
	 * Check the invariant of board
	 * 
	 * @return <li> false if there is some piece of board has value is out of range {0,1,2}
	 * <li> true in other case
	 */
	@Override
	public boolean invariant() {

		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				int value = getState(i, j);
				// Check the range of value of chessman
				if (value < 0 || value > 2) {
					MessageConsole
							.errorMessage("The value is out of range at (" + i
									+ ", " + j + "): " + value);
					return false;
				}
			}
		}

		return true;

	}

	/**
	 * In the XO game and Connect4 game, at the beginning of game, all pieces of board is empty
	 * */
	@Override
	public void initial() {
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				setState(i, j, 0);
			}
		}
	}

	@Override
	public boolean canMakeMove(MoveInterface move) {
		for (int i = 0; i < listAllPossibleMoves.size(); i++) {
			if (((Move) move).equal(listAllPossibleMoves.get(i)))
				return true;
		}
		return false;
	}

	/**
	 * Get the last move on board
	 * @return <li> Null if there isn't any move has done on board
	 * <li> The last move has done on board
	 */
	protected MoveInterface getLastMove() {
		if (historyMoves.isEmpty()) {
			MessageConsole
					.inforMessage("There isn't any move has done on board");
			return null;
		}
		return historyMoves.get(historyMoves.size() - 1);
	}

	/**
	 * Check over state of board
	 * @return
	 */
	public boolean isOver() {
		if (overInColumns())
			return true;
		if (overOnRows())
			return true;
		if (overOnDiagonalsLTRB())
			return true;
		if (overOnDiagonalsLBRT())
			return true;
		if (isFull()) {
			MessageConsole.inforMessage("The board is full. Game tie !");
			return true;
		}
		return false;
	}

	/**
	 * Check the full state of board
	 * @return <li> false if there is some empty position on board
	 * <li> true if all pieces on board has moved
	 */
	public boolean isFull() {
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++)
				if (isEmpty(i, j))
					return false;
		}
		return true;
	}

	/**
	 * Check over state on diagonal lines from left-bottom to right-top
	 * @return true if there is at least one diagonal line contains over state
	 * <li> false if there isn't any diagonal line contains over state 
	 */
	private boolean overOnDiagonalsLBRT() {
		// From left-right
		for (int i = 0; i < getWidth(); i++) {
			if (overOnDiagonalLBRT(i, getHeight() - 1))
				return true;
		}

		// From top-bottom
		for (int i = 0; i < getHeight(); i++) {
			if (overOnDiagonalLBRT(0, i))
				return true;
		}
		return false;
	}

	/**
	 * Check over on a diagonal line from left-bottom to right-top
	 * <li> The length of string represent state of board in diagonal line is min of (getWidth() - x, getHeight() - y) 
	 * @param x
	 * @param y
	 * @return true if the diagonal line contains over state
	 * <li> false if the diagonal line doesn't contains over state
	 */
	private boolean overOnDiagonalLBRT(int x, int y) {
		int maxStep = min(getWidth() - x, y);
		String string = getString(x, y, x + maxStep, y - maxStep);
		if (string.contains(patternToWin(true))
				|| string.contains(patternToWin(false)))
			return true;
		return false;
	}

	/**
	 * Check over state on diagonal lines from left-top to right-bottom
	 * @return true if there is at least one diagonal line contains over state
	 * <li> false if there isn't any diagonal line contains over state 
	 */
	private boolean overOnDiagonalsLTRB() {

		// From left-right
		for (int i = 0; i < getWidth(); i++) {
			if (overOnDiagonalLTRB(i, 0))
				return true;
		}
		// From top-bottom
		for (int i = 0; i < getHeight(); i++) {
			if (overOnDiagonalLTRB(0, i))
				return true;
		}
		return false;
	}

	/**
	 * Check over on a diagonal line from left-top to right-bottom
	 * <li> The length of string represent state of board in diagonal line is min of (getWidth() - x, getHeight() - y) 
	 * @param x
	 * @param y
	 * @return true if the diagonal line contains over state
	 * <li> false if the diagonal line doesn't contains over state
	 */
	private boolean overOnDiagonalLTRB(int x, int y) {
		int maxStep = min(getWidth() - x, getHeight() - y);
		String string = getString(x, y, x + maxStep, y + maxStep);
		if (string.contains(patternToWin(true))
				|| string.contains(patternToWin(false)))
			return true;
		return false;
	}

	/**
	 * Get the min number between a and b
	 * @param a
	 * @param b
	 * @return
	 */
	private int min(int a, int b) {
		return a > b ? b : a;
	}

	/**
	 * Check over state on rows
	 * @return true if there is at least one row contains over state
	 * <li> false if there isn't any row contains over state 
	 * 
	 */
	private boolean overOnRows() {
		for (int i = 0; i < getHeight(); i++) {
			if (overInRow(i))
				return true;
		}
		return false;
	}

	/**
	 * Check over state on a row
	 * @param row
	 * @return true if row contains over state
	 * <li> false if row doesn't contains over state
	 */
	private boolean overInRow(int row) {
		String string = getString(0, row, getWidth(), row);
		if (string.contains(patternToWin(true))
				|| string.contains(patternToWin(false)))
			return true;
		return false;
	}

	/**
	 * Check over state in columns
	 * @return true if there is at least one column contains over state
	 * <li> false if there isn't any column contains over state
	 */
	private boolean overInColumns() {
		for (int i = 0; i < getWidth(); i++) {
			if (overInColumn(i))
				return true;
		}
		return false;
	}

	/**
	 * Check over state in column
	 * @param column
	 * @return true if column contains over state
	 * <li> false if column doesn't contains over state
	 */
	private boolean overInColumn(int column) {
		String string = getString(column, 0, column, getHeight());
		if (string.contains(patternToWin(true))
				|| string.contains(patternToWin(false)))
			return true;
		return false;
	}

	/**
	 * Get string represent state of board from (x1,y1) to (x2,y2)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return <li> empty string if the path from (x1,y1) to (x2,y2) isn't a line
	 * <li> if x1=x2 return the string present the state of board in column x1 
	 * <li> if y1=y2 return the string present the state of board on row y1
	 * <li> if x2-x1=y2-y1 return the string present the state of board on diagonal from left-top to right-bottom starting at (x1,y1) to (x2,y2)
	 * <li> if x1 + y1=x2+y2 return the string present the state of board on diagonal from left-bottom to right-top starting at (x1,y1) to (x2,y2)
	 */
	private String getString(int x1, int y1, int x2, int y2) {
		String str = "";
		// in a column
		if (x1 == x2) {
			str = getStringInColumn(x1);
			return str;
		}

		// On a row
		if (y1 == y2) {
			str = getStringOnRow(y1);
			return str;
		}

		// On a diagonal line from left-top to right-bottom
		if (x2 - x1 == y2 - y1) {
			str = getStringOnDiagonalLTRB(x1, y1, x2, y2);
			return str;
		}

		// On a diagonal line from left-bottom to right-top
		if (x1 + y1 == x2 + y2) {
			str = getStringOnDiagonalLBRT(x1, y1, x2, y2);
			return str;
		}
		MessageConsole.debugMessage("@Board#getString(): Wrong direction!");
		return "";
	}

	/**
	 * Get String present the state of board from (x1,y1) to (x2,y2)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	private String getStringOnDiagonalLBRT(int x1, int y1, int x2, int y2) {
		String str = "";
		for (int x = x1, y = y1; x <= x2 && y >= y2; x++, y--) {
			if (insideBoard(x, y))
				str += getState(x, y);
		}
		return str;
	}

	/**
	 * Get String present the state of board from (x1,y1) to (x2,y2)
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	private String getStringOnDiagonalLTRB(int x1, int y1, int x2, int y2) {
		String str = "";
		for (int x = x1, y = y1; x <= x2 && y <= y2; x++, y++) {
			if (insideBoard(x, y))
				str += getState(x, y);
		}
		return str;
	}

	/**
	 * Get string present the state of board on row y1
	 * @param y1
	 * @return
	 */
	private String getStringOnRow(int y1) {
		String str = "";
		for (int i = 0; i < getWidth(); i++) {
			if (insideBoard(i, y1))
				str += getState(i, y1);
		}
		return str;
	}

	/**
	 * Get string present the state of board in column x1
	 * @param x1
	 * @return
	 */
	private String getStringInColumn(int x1) {
		String str = "";
		for (int i = 0; i < getHeight(); i++) {
			if (insideBoard(x1, i))
				str += getState(x1, i);
		}
		return str;
	}

	/**
	 * Get the string present the number of pieces next together in the over state of board
	 *  
	 * @param blue
	 * @return
	 * <li> In the XO game, the patternToWin is a string has 3 characters present 3 pieces next together of a player
	 * <li> In the Connect4 game, the patternToWin is a string has 4 characters present 4 pieces next together of a player
	 * <li> With the player 1, the patternToWin is a string presented by numberOfPicesToWin of "1"
	 * <li> With the player 2, the patternToWin is a string presented by numberOfPicesToWin of "2"
	 */
	private String patternToWin(boolean blue) {
		String pattern = "";
		for (int i = 0; i < numberOfPicesToWin; i++) {
			if (blue)
				pattern += String.valueOf(1);
			else
				pattern += String.valueOf(2);
		}
		return pattern;
	}

	/**
	 * @return the historyMoves
	 */
	public ArrayList<MoveInterface> getHistoryMoves() {
		return historyMoves;
	}

	/**
	 * @return <li> true if the listAllPossibleMoves contains the move which will be done 
	 * <br> - Remove the move will be done from listAllPossibleMoves
	 * <br> - Update the state of board
	 * <li> throws GameException if :
	 * <br> - Cannot find the move in listAllPossibleMoves
	 * <br> - {@link Board#canMakeMove(MoveInterface)} return false
	 * */
	@Override
	public boolean makeMove(MoveInterface move) throws GameExceptions {
		if (canMakeMove(move)) {
			for (int i = 0; i < listAllPossibleMoves.size(); i++) {
				if (((Move) move).equal(listAllPossibleMoves.get(i))) {
					listAllPossibleMoves.remove(i);
					setState(move.getCoordinate(0), move.getCoordinate(1),
							((Move) move).isBlue() ? 1 : 2);
					historyMoves.add(move);
					return true;
				}
			}
			throw new GameExceptions(this,
					"@XOBoard#makeMove: Cannot find move in the listAllPossibleMoves. Move: "
							+ move.toString());

		} else {
			throw new GameExceptions(this,
					"@XOBoard#makeMove: Cannot make move on board. Move: "
							+ move.toString());
		}
	}

	@Override
	public boolean undoLastMove() {
		MoveInterface move = getLastMove();
		if (move != null) {
			this.setState(move.getCoordinate(0), move.getCoordinate(1), 0);
			historyMoves.remove(move);
			listAllPossibleMoves.add(move);
			return true;
		} else {
			MessageConsole
					.inforMessage("@Board#undoLastMove: Cannot get the last move");
			return false;
		}
	}

}
