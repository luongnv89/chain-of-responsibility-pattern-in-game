/**
 * 
 */
package xoconnect4;

import specifications.BoardInterface;
import utils.MessageConsole;
import abstractions.GraphicAbstract;

/**
 * {@link GraphicConsole} extends from {@link GraphicAbstract} represent the state of board in console graphic 
 *
 */
public class GraphicConsole extends GraphicAbstract {

	public GraphicConsole(BoardInterface board) {
		super(board);
	}

	@Override
	public void view() {
		MessageConsole.inforMessage("Board state: ");
		for (int i = -1; i < board.getHeight(); i++) {
			if (i == -1) {
				showRowIndexes();
			} else {
				showBoardBody(i);
			}
			System.out.println();
		}
	}

	/**
	 * Show the body of board game
	 * @param i
	 */
	private void showBoardBody(int i) {
		for (int j = -1; j < board.getWidth(); j++) {

			if (j == -1) {
				if (i != board.getHeight())
					System.out.print(i + "   ");

			} else {
				int state = board.getState(j, i);
				System.out.print(showByValue(state));
			}
		}
	}

	/**
	 * Show the rows which contains indexes of board
	 */
	private void showRowIndexes() {
		for (int j = -1; j < board.getWidth(); j++) {
			if (j != -1)
				System.out.print(j + " ");
			else
				System.out.print(j + "  ");
		}
	}

	/**
	 * Represent a piece of board by state of them
	 * @param state
	 * @return
	 * <li> "x  " if the piece is an empty piece
	 * <li> the number represent for the value of chess-men of player
	 * <li> If the chess-men is a chess-man of red player, it will be represented by a number and a *
	 */
	private String showByValue(int state) {
		if (state == 0)
			return "- ";
		else if (state == 1) {
			return "X ";
		} else {
			return "O ";
		}
	}

}
