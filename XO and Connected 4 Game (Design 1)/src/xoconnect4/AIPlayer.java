/**
 * 
 */
package xoconnect4;

import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.RuleInterface;
import utils.GameExceptions;
import abstractions.PlayerAbstract;

/**
 *{@link AIPlayer} extends {@link PlayerAbstract} represent an AI player in Vietnammese Math Chess game
 *<br>AI player have a rule to calculate move
 */
public class AIPlayer extends PlayerAbstract {

	/**
	 * The rule of player which AI player will use to get the move
	 */
	RuleInterface rule;

	public AIPlayer(String playerName, boolean blue, RuleInterface rule) {
		super(playerName, blue);
		this.rule = rule;
		this.rule.setPlayer(this);
	}

	@Override
	public MoveInterface getMove(BoardInterface board) throws GameExceptions {
		if (rule.invariant())
			return rule.getMove(board);
		else {
			throw new GameExceptions(this,
					"@AIPlayer#getMove: Rule is invariant");
		}
	}

	@Override
	public boolean invariant() {
		return rule.invariant();
	}

	/**
	 * Get the rule of AI player
	 * @return
	 */
	public RuleInterface getRule() {
		return rule;
	}
}
