package xoconnect4;

import java.util.ArrayList;

import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;

public class Connect4Board extends Board {

	private final static int WIDTH = 7;
	private final static int HEIGHT = 6;

	public Connect4Board(int width, int height) {
		super(width, height);
		numberOfPicesToWin = 4;
	}

	public Connect4Board() {
		super(WIDTH, HEIGHT);
		numberOfPicesToWin = 4;
	}

	/** 
	 * 
	 * @return In the Connect4 game the invariant() method return false if:
	 * <br> There are some holes in the column of board
	 * */
	@Override
	public boolean invariant() {

		for (int i = 0; i < this.getWidth(); i++) {
			if (!validColumn(i)) {
				MessageConsole.errorMessage("Invalid at column: " + i);
				return false;
			}
		}
		return super.invariant();
	}

	/**
	 * Check a column of board is valid or not
	 * @param i
	 * @return false if:
	 * <br> there are some holes in column. For examples:
	 * <br>
	 * <pre>
	 * -					- <br>
	 * -					- <br>
	 * X					- <br>
	 * -					X<br>
	 * O					X<br>
	 * X					O<br>
	 *invalid column			valid column
	 *@return true if:
	 *<li> this is a empty column
	 *<li> There aren't any hole
	 */
	private boolean validColumn(int i) {
		int indexFound = -1;
		// Find the index of the first chessman
		for (int j = 0; j < this.getHeight(); j++) {
			if (this.getState(i, j) != 0) {
				indexFound = j;
				break;
			}
		}
		// If indexFound = -1 , that means this is a empty column
		if (indexFound == -1)
			return true;
		else {
			// Found the empty hole in the column
			for (int k = indexFound; k < this.getHeight(); k++) {
				if (this.getState(i, k) == 0) {
					MessageConsole
							.debugMessage("\nThe board isn't invariant. ");
					MessageConsole.debugMessage("Found the nonempty at (" + i
							+ ", " + indexFound + ")");
					MessageConsole.debugMessage("Found the empty at (" + i
							+ ", " + k + ")");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * @return <li> true if the listAllPossibleMoves contains the move which will be done 
	 * <br> - Remove the move will be done from listAllPossibleMoves
	 * <br> - if the column of move isn't full, add a new possible move in the column into list all possible moves 
	 * <br> - Update the state of board
	 * <li> throws GameException if :
	 * <br> - Cannot find the move in listAllPossibleMoves
	 * <br> - {@link Board#canMakeMove(MoveInterface)} return false
	 * */
	@Override
	public boolean makeMove(MoveInterface move) throws GameExceptions {
		if (super.makeMove(move)) {
			if (move.getCoordinate(1) > 0) {
				listAllPossibleMoves.add(new Move(move.getCoordinate(0), move
						.getCoordinate(1) - 1));
			}
			return true;
		} else {
			MessageConsole
					.inforMessage("@Connect4Board#makeMove: Cannot update list possible move");
			return false;
		}
	}

	@Override
	protected ArrayList<MoveInterface> calculatePossibleMove() {
		ArrayList<MoveInterface> allPossibleMoves = new ArrayList<MoveInterface>();
		for (int i = 0; i < getWidth(); i++) {
			allPossibleMoves.add(new Move(i, getHeight() - 1));
		}
		return allPossibleMoves;
	}

	@Override
	public boolean undoLastMove() {
		MoveInterface move = getLastMove();
		if (move != null) {
			this.setState(move.getCoordinate(0), move.getCoordinate(1), 0);
			historyMoves.remove(move);
			listAllPossibleMoves.add(move);
			MoveInterface moveRemoved = new Move(move.getCoordinate(0),
					move.getCoordinate(1) - 1);
			for (int i = 0; i < listAllPossibleMoves.size(); i++) {
				if (((Move) listAllPossibleMoves.get(i)).equal(moveRemoved))
					listAllPossibleMoves.remove(i);
			}
			return true;
		} else {
			MessageConsole
					.inforMessage("@Board#undoLastMove: Cannot get the last move");
			return false;
		}
	}
}
