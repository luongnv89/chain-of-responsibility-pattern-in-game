package xoconnect4;

import specifications.BoardInterface;
import specifications.PlayerInterface;
import abstractions.GameAbstract;

/**
 * Represent a math chess game
 *
 */
public class Game extends GameAbstract {

	public Game(BoardInterface board, PlayerInterface bluePlayer,
			PlayerInterface redPlayer) {
		super(board, bluePlayer, redPlayer);
		this.graphic = new GraphicConsole(board);
	}

	@Override
	public boolean isGameOver() {
		return ((Board) board).isOver();
	}

}
