package xoconnect4;

import specifications.MoveInterface;

/**
 * {@link Move} implement of {@link MoveInterface} present a move in game
 * 
 */
public class Move implements MoveInterface {

	/**
	 * x coordinate of move
	 */
	private int x;
	/**
	 * y coordinate of move
	 */
	private int y;
	/**
	 * <li>true if the move was made by the blue player
	 * <li>false if the move was made by the red player
	 */
	private boolean isBlue;

	
	
	/**
	 * Create a new move
	 * @param x
	 * @param y
	 * @param isBlue
	 */
	public Move(int x, int y, boolean isBlue) {
		super();
		this.x = x;
		this.y = y;
		this.isBlue = isBlue;
	}

	/**
	 * Create an move can be done
	 * @param x
	 * @param y
	 */
	public Move(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Set player for move
	 * @param isBlue the isBlue to set
	 */
	public void setBlue(boolean isBlue) {
		this.isBlue = isBlue;
	}

	/**
	 * Get the player of move
	 * @return the isBlue
	 */
	public boolean isBlue() {
		return isBlue;
	}

	@Override
	public String toString() {
		return "Move [x=" + x + ", y=" + y + ", isBlue=" + isBlue + "]";
	}

	@Override
	public int getCoordinate(int index) throws IllegalArgumentException {
		if (index != 0 && index != 1)
			throw new IllegalArgumentException(
					"@Move#getCoordinate: Index is invalid. Index must be 0 or 1. Input index: "
							+ index);
		if (index == 0)
			return x;
		else
			return y;
	}

	/**
	 * Check the invariant of move
	 * @return <li> false if x<0 or y<0
	 * <li> true in other case
	 * */
	@Override
	public boolean invariant() {
		return x >= 0 && y >= 0;
	}

	/**
	 * Compare with another move
	 * @param other
	 * @return <li> True if both of two coordinates of two move are equal
	 * <li> False in other case
	 */
	public boolean equal(MoveInterface other) {
		if (x != other.getCoordinate(0))
			return false;
		if (y != other.getCoordinate(1))
			return false;
		return true;
	}

}
