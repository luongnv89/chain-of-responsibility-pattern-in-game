/**
 * 
 */
package xoconnect4;

import java.util.Scanner;

import javax.sql.PooledConnection;

import org.hamcrest.core.IsInstanceOf;

import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.MessageConsole;
import abstractions.PlayerAbstract;

/**
 * Represent a human player.
 * It extends from {@link PlayerAbstract}.
 */
public class HumanPlayer extends PlayerAbstract {

	public HumanPlayer(String playerName, boolean blue) {
		super(playerName, blue);
	}

	@Override
	public MoveInterface getMove(BoardInterface board) {
		return readInput(board);
	}

	/**
	 * Read the input move of player
	 * @return 
	 */
	private MoveInterface readInput(BoardInterface board) {
		MoveInterface move = null;
		Scanner input = new Scanner(System.in);
		int x, y;
		boolean possibleMove = false;
		while (!possibleMove) {
			try {
				System.out.print("x = ");
				x = Integer.parseInt(input.nextLine());
				System.out.print("y = ");
				y = Integer.parseInt(input.nextLine());
				move = new Move(x, y);
				possibleMove = board.canMakeMove(move);
				if (!possibleMove) {
					MessageConsole.inforMessage("Please input again!");
				}
			} catch (NumberFormatException nFE) {
				possibleMove = false;
				MessageConsole.inforMessage("Error!" + nFE.toString());
				MessageConsole.inforMessage("Please input again!");
			}
		}
		return move;
	}

	@Override
	public boolean invariant() {
		return playerName != null;
	}

}
