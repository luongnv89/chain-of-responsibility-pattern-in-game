/**
 * 
 */
package simulations;

import org.junit.Before;

import simulations.abstractions.HumanPlayingGameAbstractTest;

import xoconnect4.XOBoard;

/**
 * Test <br>
 * <li>a human player plays with another human
 * <li>a human players plays with AI player.
 * 
 */
public class HumanXOPlayingGameTest extends HumanPlayingGameAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		board = new XOBoard();
		setUpPlayers();
	}

}
