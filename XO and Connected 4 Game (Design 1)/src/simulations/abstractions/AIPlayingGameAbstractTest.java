/**
 * 
 */
package simulations.abstractions;

import org.junit.Test;

import abstractions.GameAbstract;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.Game;

/**
 * 
 *Test for class {@link Game}
 */
public class AIPlayingGameAbstractTest extends PlayingGameAbstractTest {
	/** 
	 * Test method for {@link mathchess.objects.Game#startGame()}.
	 * <li> Game create by two AI Random player
	 * @throws GameExceptions 
	 */
	@Test
	public void testStartGameAIRandomvsAIRandom() throws GameExceptions {
		MessageConsole.disableAllMessages();
		game = new Game(board, blueAIRandomPlayer, redAIRandomPlayer);
		((GameAbstract) game).setCurrentPlayer(true);
		game.startGame();
	} 

	/** 
	 * Test method for {@link mathchess.objects.Game#startGame()}.
	 * <li> Game create by two AI Random player
	 * @throws GameExceptions 
	 */
	@Test
	public void testStartGameAIGoodvsAIGood() throws GameExceptions {
		MessageConsole.disableAllMessages();
		game = new Game(board, blueAIGoodPlayer, redAIGoodPlayer);
		((GameAbstract) game).setCurrentPlayer(true);
		game.startGame();
	}

	/** 
	 * Test method for {@link mathchess.objects.Game#startGame()}.
	 * <li> Game create by two AI Random player
	 * @throws GameExceptions 
	 */
	@Test
	public void testStartGameAIGoodvsAIRandom() throws GameExceptions {
		MessageConsole.disableAllMessages();
		game = new Game(board, blueAIGoodPlayer, redAIRandomPlayer);
		((GameAbstract) game).setCurrentPlayer(true);
		game.startGame();
	} 
}
