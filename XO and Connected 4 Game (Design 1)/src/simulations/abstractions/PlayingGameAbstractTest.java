/**
 * 
 */
package simulations.abstractions;

import org.junit.After;

import rules.Rule_Manger;
import specifications.BoardInterface;
import specifications.GameInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import xoconnect4.AIPlayer;
import xoconnect4.Game;
import xoconnect4.HumanPlayer;

/**
 * {@link PlayingGameAbstractTest} test for {@link Game#startGame()}
 *
 */
public abstract class PlayingGameAbstractTest {

	protected BoardInterface board;

	// Human player
	protected PlayerInterface blueHumanPlayer;
	protected PlayerInterface redHumanPlayer;

	// GoodRules player
	protected RuleInterface blueGoodRule;
	protected RuleInterface redGoodRule;
	protected PlayerInterface blueAIGoodPlayer;
	protected PlayerInterface redAIGoodPlayer;

	// Random rule player
	protected RuleInterface blueRandomRule;
	protected RuleInterface redRandomRule;
	protected PlayerInterface redAIRandomPlayer;
	protected PlayerInterface blueAIRandomPlayer;

	// Game
	protected GameInterface game;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;

		// Human player
		blueHumanPlayer = null;
		redHumanPlayer = null;

		// GoodRule player
		blueGoodRule = null;
		redGoodRule = null;
		blueAIGoodPlayer = null;
		redAIGoodPlayer = null;

		// Winfirst Random player
		blueRandomRule = null;
		redRandomRule = null;
		redAIRandomPlayer = null;
		blueAIRandomPlayer = null;

		// Game
		game = null;
	}

	public void setUpPlayers() {
		// AI - Blue - GoodRule
		blueGoodRule = new Rule_Manger();
		((Rule_Manger) blueGoodRule).setRules(2);
		blueAIGoodPlayer = new AIPlayer("blueAIGoodPlayer", true,
				blueGoodRule);

		// AI - red - GoodRule
		redGoodRule = new Rule_Manger();
		((Rule_Manger) redGoodRule).setRules(2);
		redAIGoodPlayer = new AIPlayer("redAIGoodPlayer", false,
				redGoodRule);

		// AI - Blue - Random
		blueRandomRule = new Rule_Manger();
		((Rule_Manger) blueRandomRule).setRules(0);
		blueAIRandomPlayer = new AIPlayer("blueAIRandomPlayer", true,
				blueRandomRule);

		// AI - red - Random
		redRandomRule = new Rule_Manger();
		((Rule_Manger) redRandomRule).setRules(0);
		redAIRandomPlayer = new AIPlayer("redAIRandomPlayer", false,
				redRandomRule);

		// Human blue player
		blueHumanPlayer = new HumanPlayer("blueHumanPlayer", true);
		// Human red player
		redHumanPlayer = new HumanPlayer("redHumanPlayer", false);
	}

}
