/**
 * 
 */
package simulations.abstractions;

import org.junit.Test;

import rules.Rule_Manger;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.AIPlayer;
import xoconnect4.Game;
import abstractions.GameAbstract;

/**
 * Test <br>
 * <li>a human player plays with another human
 * <li>a human players plays with AI player.
 * 
 */
public class HumanPlayingGameAbstractTest extends PlayingGameAbstractTest {

	/**
	 * Test method for {@link mathchess.objects.Game#startGame()}.
	 * <li> Game create with an AI Random player and a Human player
	 * @throws GameExceptions 
	 */
	@Test
	public void testStartGameAIRandomvsHuman() throws GameExceptions {
		MessageConsole.disableAllMessages();
		MessageConsole.setINFOR(true);
		((Rule_Manger) ((AIPlayer) blueAIRandomPlayer).getRule())
				.showRules(true);
		game = new Game(board, blueAIRandomPlayer, redHumanPlayer);
		((GameAbstract) game).setCurrentPlayer(true);
		game.startGame();
	}

	/**
	 * Test method for {@link mathchess.objects.Game#startGame()}.
	 * <li> Game create with an AI Good Rule player and a Human player
	 * @throws GameExceptions 
	 */
	@Test
	public void testStartGameAIGoodvsHuman() throws GameExceptions {
		MessageConsole.disableAllMessages();
		MessageConsole.setINFOR(true);
		((Rule_Manger) ((AIPlayer) blueAIGoodPlayer).getRule()).showRules(true);
		game = new Game(board, blueAIGoodPlayer, redHumanPlayer);
		((GameAbstract) game).setCurrentPlayer(true);
		game.startGame();
	}

	/**
	 * Test method for {@link mathchess.objects.Game#startGame()}.
	 * <li> Game create with a human player and another Human player
	 * @throws GameExceptions 
	 */
	@Test
	public void testStartGameHumanvsHuman() throws GameExceptions {
		game = new Game(board, blueHumanPlayer, redHumanPlayer);
		((GameAbstract) game).setCurrentPlayer(true);
		game.startGame();
	}

}
