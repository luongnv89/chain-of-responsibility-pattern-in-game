/**
 * 
 */
package simulations;

import org.junit.Before;

import simulations.abstractions.AIPlayingGameAbstractTest;

import xoconnect4.Connect4Board;

/**
 * Test AI players automatic play with each other.
 * Two AIs have different algorithms or rules.
 * 
 */
public class AIConnect4PlayingGameTest extends AIPlayingGameAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		board = new Connect4Board();
		setUpPlayers();
	}

}
