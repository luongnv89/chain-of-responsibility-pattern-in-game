/**
 * 
 */
package simulations;

import org.junit.Before;

import simulations.abstractions.AIPlayingGameAbstractTest;

import xoconnect4.XOBoard;

/**
 * Test AI players automatic play with each other.
 * Two AIs have different algorithms or rules.
 * 
 */
public class AIXOPlayingGameTest extends AIPlayingGameAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		board = new XOBoard();
		setUpPlayers();
	}
	
}
