package simulations;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import xo.tests.XOAIPlayerTest;
import xo.tests.XOBoardTest;
import xo.tests.XOGameTest;
import xo.tests.XOGraphicConsoleTest;
import xo.tests.XOHumanPlayerTest;
import xo.tests.XORule_BlockWinTest;
import xo.tests.XORule_MangerTest;
import xo.tests.XORule_RandomTest;
import xo.tests.XORule_WinTest;
import abstractions.tests.MoveTest;
import connect4.tests.Connect4AIPlayerTest;
import connect4.tests.Connect4BoardTest;
import connect4.tests.Connect4GameTest;
import connect4.tests.Connect4GraphicConsoleTest;
import connect4.tests.Connect4HumanPlayerTest;
import connect4.tests.Connect4Rule_BlockWinTest;
import connect4.tests.Connect4Rule_MangerTest;
import connect4.tests.Connect4Rule_RandomTest;
import connect4.tests.Connect4Rule_WinTest;

@RunWith(Suite.class)
@SuiteClasses({ XOAIPlayerTest.class, XOBoardTest.class, XOGameTest.class,
		XOGraphicConsoleTest.class, XOHumanPlayerTest.class,
		XORule_BlockWinTest.class, XORule_MangerTest.class,
		XORule_RandomTest.class, XORule_WinTest.class,
		Connect4AIPlayerTest.class, Connect4BoardTest.class,
		Connect4GameTest.class, Connect4GraphicConsoleTest.class,
		Connect4HumanPlayerTest.class, Connect4Rule_BlockWinTest.class,
		Connect4Rule_MangerTest.class, Connect4Rule_RandomTest.class,
		Connect4Rule_WinTest.class, MoveTest.class,
		AIConnect4PlayingGameTest.class, AIXOPlayingGameTest.class })
public class AllTests {

}
