/**
 * 
 */
package xo.tests;

import org.junit.Before;

import abstractions.tests.Rule_MangerAbstractTest;

import rules.Rule_Manger;
import xoconnect4.Move;
import xoconnect4.XOBoard;

/**
 *Test for {@link Rule_Manger} 
 *
 */
public class XORule_MangerTest extends Rule_MangerAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		rule = new Rule_Manger();
		setupGame();
		board.makeMove(new Move(0, 0, false));
		board.makeMove(new Move(0, 1, false));
	}
}
