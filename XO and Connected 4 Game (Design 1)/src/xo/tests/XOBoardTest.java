/**
 * 
 */
package xo.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import abstractions.tests.BoardTest;

import utils.GameExceptions;
import xoconnect4.Board;
import xoconnect4.Move;
import xoconnect4.XOBoard;

/**
 * Test for class {@link XOBoard}
 *
 *
 */
public class XOBoardTest extends BoardTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		WIDH_DEFAULT = 3;
		HEIGHT_DEFAULT = 3;
		board = new XOBoard(4, 5);
		defaultBoard = new XOBoard();
	}

	/**
	 * Test method for {@link xoconnect4.XOBoard#calculatePossibleMove()}.
	 */
	@Test
	public void testCalculatePossibleMove() {
		assertTrue(((Board) board).getListAllPossibleMoves().size() == 20);
	}

	/**
	 * Test method for {@link xoconnect4.XOBoard#XOBoard()}.
	 */
	@Test
	public void testXOBoard() {
		assertTrue(defaultBoard.getWidth() == WIDH_DEFAULT);
		assertTrue(defaultBoard.getHeight() == HEIGHT_DEFAULT);
	}

	/**
	 * Test method for {@link xoconnect4.XOBoard#XOBoard(int, int)}.
	 */
	@Test
	public void testXOBoardIntInt() {
		assertTrue(board.getWidth() == 4);
		assertTrue(board.getHeight() == 5);
	}

	/**
	 * Test method for {@link xoconnect4.XOBoard#makeMove(specifications.MoveInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testMakeMove() throws GameExceptions {
		assertTrue(board.invariant());
		assertTrue(((Board) board).getListAllPossibleMoves().size() == 20);
		assertTrue(board.isEmpty(0, 0));
		assertTrue(board.makeMove(new Move(0, 0, false)));
		assertTrue(board.invariant());
		assertTrue(board.getState(0, 0) == 2);
		assertTrue(((Board) board).getListAllPossibleMoves().size() == 19);
	}

	/**
	 * Test method for {@link xoconnect4.XOBoard#makeMove(specifications.MoveInterface)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testMakeMoveException() throws GameExceptions {
		assertTrue(board.invariant());
		assertTrue(((Board) board).getListAllPossibleMoves().size() == 20);
		assertTrue(board.isEmpty(0, 0));
		assertTrue(board.makeMove(new Move(0, 0, false)));
		assertTrue(board.invariant());
		board.makeMove(new Move(0, 0, true));

	}


	@Override
	@Test
	public void testIsOverInColumn() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());
		assertTrue(board.makeMove(new Move(0, 0, true)));
		assertTrue(board.makeMove(new Move(0, 1, true)));
		assertTrue(board.makeMove(new Move(0, 2, true)));
		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

	@Override
	@Test
	public void testIsOverOnRow() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());
		assertTrue(board.makeMove(new Move(0, 0, true)));
		assertTrue(board.makeMove(new Move(1, 0, true)));
		assertTrue(board.makeMove(new Move(2, 0, true)));
		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

	@Override
	@Test
	public void testIsOverOnDiagonalLTRB() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());
		assertTrue(board.makeMove(new Move(0, 0, true)));
		assertTrue(board.makeMove(new Move(1, 1, true)));
		assertTrue(board.makeMove(new Move(2, 2, true)));
		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

	@Override
	@Test
	public void testIsOverOnDiagonalLBRT() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());
		assertTrue(board.makeMove(new Move(0, 2, true)));
		assertTrue(board.makeMove(new Move(1, 1, true)));
		assertTrue(board.makeMove(new Move(2, 0, true)));
		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

}
