/**
 * 
 */
package xo.tests;

import org.junit.Before;

import abstractions.tests.RuleAbstractTest;

import rules.Rule_BlockWin;
import xoconnect4.Move;
import xoconnect4.XOBoard;

/**
 * Test for {@link Rule_BlockWin}
 *
 */
public class XORule_BlockWinTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		rule = new Rule_BlockWin();
		setupGame();
		board.makeMove(new Move(0, 0, false));
		board.makeMove(new Move(0, 1, false));
	}

}
