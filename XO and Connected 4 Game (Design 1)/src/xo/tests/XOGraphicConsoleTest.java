/**
 * 
 */
package xo.tests;

import org.junit.Before;

import abstractions.tests.GraphicConsoleAbstractTest;

import xoconnect4.GraphicConsole;
import xoconnect4.XOBoard;

/**
 *Test for {@link GraphicConsole} with {@link XOBoard}
 *
 */
public class XOGraphicConsoleTest extends GraphicConsoleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		graphic = new GraphicConsole(board);
	}
}
