/**
 * 
 */
package xo.tests;

import org.junit.Before;

import abstractions.tests.RuleAbstractTest;

import rules.Rule_Random;
import xoconnect4.XOBoard;

/**
 * 
 *
 */
public class XORule_RandomTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		rule = new Rule_Random();
		setupGame();
	}

}
