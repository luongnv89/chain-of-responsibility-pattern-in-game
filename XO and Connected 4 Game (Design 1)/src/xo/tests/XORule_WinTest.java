/**
 * 
 */
package xo.tests;

import org.junit.Before;

import abstractions.tests.RuleAbstractTest;

import rules.Rule_Win;
import xoconnect4.Move;
import xoconnect4.XOBoard;

/**
 * Test for {@link Rule_Win} with {@link XOBoard}
 *
 */
public class XORule_WinTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		rule = new Rule_Win();
		setupGame();
		bluePlayer.makeMove(new Move(0, 0), board);
		bluePlayer.makeMove(new Move(0, 1), board);
	}

}
