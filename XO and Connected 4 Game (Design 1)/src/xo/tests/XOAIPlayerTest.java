/**
 * 
 */
package xo.tests;

import org.junit.Before;

import abstractions.tests.AIPlayerAbstractTest;

import xoconnect4.XOBoard;

/**
 * extends {@link PlayerMathChess} class.
 * tests AI player behaviors.
 */
public class XOAIPlayerTest extends AIPlayerAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		setPlayer();
	}

}
