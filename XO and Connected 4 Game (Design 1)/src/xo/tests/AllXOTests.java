package xo.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ XOAIPlayerTest.class, XOBoardTest.class, XOGameTest.class,
		XOGraphicConsoleTest.class, XOHumanPlayerTest.class,
		XORule_BlockWinTest.class, XORule_MangerTest.class,
		XORule_RandomTest.class, XORule_WinTest.class })
public class AllXOTests {

}
