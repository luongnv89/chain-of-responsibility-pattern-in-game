/**
 * 
 */
package xo.tests;

import org.junit.Before;

import abstractions.tests.HumanPlayerAbstractTest;

import xoconnect4.XOBoard;

/**
 *Extends {@link PlayerMathChessTest} class.
 *Test a human player behaviors.
 */
public class XOHumanPlayerTest extends HumanPlayerAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		setPlayer();
	}

}
