/**
 * 
 */
package xo.tests;


import org.junit.Before;

import abstractions.tests.GameAbstractTest;

import simulations.AIXOPlayingGameTest;
import simulations.HumanXOPlayingGameTest;
import xoconnect4.Game;
import xoconnect4.XOBoard;

/**
 * test {@link Game} class.
 * test the validation of methods in the class. There is also another test class to test playing a game:
 * {@link AIXOPlayingGameTest} and {@link HumanXOPlayingGameTest}.
 */
public class XOGameTest extends GameAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new XOBoard();
		setUpPlayers();
	}

}
