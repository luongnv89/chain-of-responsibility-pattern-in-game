/**
 * 
 */
package specifications;

import utils.GameExceptions;

/**
 * {@link RuleInterface} Represent the interface of Rule in game which the AI player will use to calculate move
 *
 */
public interface RuleInterface extends HasInvariant {
	
	/**
	 * Set the player will use the rule
	 * @param player
	 */
	public void setPlayer(PlayerInterface player);
	
	/**
	 * Get a possible move for AI player
	 * @param board
	 * @return
	 * @throws GameExceptions
	 */
	public MoveInterface getMove(BoardInterface board) throws GameExceptions;

	
}
