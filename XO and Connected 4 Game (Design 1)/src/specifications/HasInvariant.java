package specifications;

public interface HasInvariant {
	/**
	 * Check the invariant of an object
	 * @return
	 */
	public boolean invariant();
}
