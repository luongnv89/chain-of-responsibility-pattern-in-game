package specifications;

import utils.GameExceptions;

/**
 * {@link PlayerInterface} present the interface of a player in game
 * 
 *
 */
public interface PlayerInterface extends HasInvariant {

	/**
	 * Get a new move for player
	 * <li> AI player will use some algorithms for calculate and return a best move for AI player
	 * <li> Human player will provide some value to create new move
	 * @return a possible move can be done
	 * @throws GameExceptions 
	 */
	public MoveInterface getMove(BoardInterface board) throws GameExceptions;

	/**
	 * Player make a move
	 * @param move
	 * @param board
	 * @return <li> True if the move has done successfully
	 * <li> false if the move hasn't done successfully
	 * @throws GameExceptions
	 */
	public boolean makeMove(MoveInterface move, BoardInterface board)
			throws GameExceptions;

	/**
	 * Player undo the last move of game
	 * @param board
	 * @return <li>True if player undid the last move successfully
	 * <li>True if player didn't undo the last move successfully
	 * @throws GameExceptions
	 */
	public boolean undoLastMove(BoardInterface board) throws GameExceptions;

	/**
	 * Check the color of player in the game which has two player again each other
	 * @return true if the player is the blue player in game
	 * <li> false if the player is the red player in game
	 */
	public boolean isBlue();

}
