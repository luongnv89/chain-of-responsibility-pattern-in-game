package specifications;

import utils.GameExceptions;

/**
 * {@link BoardInterface} represent the interface of a board game
 */
public interface BoardInterface extends HasInvariant {

	/**
	 * The board update state after one player made a move
	 * @param move The move has done
	 * @return <li> True if the board update successfully
	 * <li> False if the board update unsuccessfully
	 * @throws GameExceptions 
	 */
	public boolean makeMove(MoveInterface move) throws GameExceptions;

	/**
	 * Check a move can be done on the board
	 * @param move
	 * @return <li> True if the move can be done on the board
	 * <li> False in other case
	 */
	public boolean canMakeMove(MoveInterface move);

	/**
	 * Initial board state
	 */
	public void initial();

	/**
	 * Reset a board then all the states of board are 0.
	 */
	public void resetBoard();

	/**
	 * Get the width of a board
	 * @return
	 */
	public int getWidth();

	/**
	 * Get the height of a board
	 * @return
	 */
	public int getHeight();

	/**
	 * Get the state of board at (i,j) position.
	 * @param i
	 * @param j
	 * @return
	 */
	public int getState(int i, int j);

	/**
	 * The board update state when one player undoed the last move
	 * @return <li> True if the board update state successfully
	 * <li> False if the board update state unsuccessfully
	 */
	public boolean undoLastMove();

}
