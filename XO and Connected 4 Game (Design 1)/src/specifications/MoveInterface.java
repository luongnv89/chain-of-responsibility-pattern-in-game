/**
 * 
 */
package specifications;

/**
 * {@link MoveInterface} present the interface of a move in game
 */
public interface MoveInterface extends HasInvariant {

	/**
	 * Get the coordinate of move
	 * @param index index of coordinate
	 * @return <li> x coordinate of move if index = 0
	 * <li> y coordinate of move if index = 1
	 */
	public int getCoordinate(int index);
}
