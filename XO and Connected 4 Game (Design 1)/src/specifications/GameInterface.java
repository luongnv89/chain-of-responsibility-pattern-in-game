package specifications;

import utils.GameExceptions;


/**
 * Interface {@link GameInterface} represent the interface of a Game.
 * It allows to start or reset the game.
 */
public interface GameInterface extends HasInvariant {

	/**
	 * Start a game
	 * @throws GameExceptions 
	 */
	public void startGame() throws GameExceptions;

	/**
	 * Reset game to new game
	 */
	public void resetGame();

	/**
	 * Check the status of game is over or not
	 * @return
	 */
	public boolean isGameOver();

}
