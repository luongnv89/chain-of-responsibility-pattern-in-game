/**
 * 
 */
package specifications;


/**
 * {@link GraphicInterface} represent the interface of a graphic object of game which will represent the board state in game
 *
 */
public interface GraphicInterface extends HasInvariant {
	/**
	 * Represent the board game on the graphic output
	 */
	public void view();

	/**
	 * Reset graphic when game reset
	 */
	public void resetGraphic();
}
