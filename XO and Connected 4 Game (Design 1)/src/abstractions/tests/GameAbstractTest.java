/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;

import connect4.tests.Connect4GameTest;

import specifications.BoardInterface;
import specifications.PlayerInterface;
import xoconnect4.Game;
import xoconnect4.GraphicConsole;
import xoconnect4.HumanPlayer;
import abstractions.GameAbstract;

/**
 * Test {@link GameAbstract} class.
 * an abstract test class which is extended by {@link Connect4GameTest}.
 */
public abstract class GameAbstractTest {

	protected BoardInterface board;

	protected PlayerInterface blueHumanPlayer;
	protected PlayerInterface redHumanPlayer;

	protected GameAbstract defaultGame;
	protected GameAbstract gameKo;

	protected GraphicConsole graphic;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;
		defaultGame = null;
		gameKo = null;

	}

	/**
	 * Test {@link Game#Game(BoardInterface, PlayerInterface, PlayerInterface)}.
	 * checks invariant of games.
	 */
	@Test
	public void testConstructor() {
		assertTrue(defaultGame.invariant());
		assertFalse(gameKo.invariant());
	}

	/**
	 * Test method for {@link abstractions.GameAbstract#setCurrentPlayer(specifications.PlayerInterface)}.
	 */
	@Test
	public void testSetCurrentPlayer() {
		((GameAbstract) defaultGame).setCurrentPlayer(true);
		assertTrue(blueHumanPlayer.equals(((GameAbstract) defaultGame)
				.getCurrentPlayer()));
	}

	/**
	 * Test method for {@link abstractions.GameAbstract#resetGame()}.
	 * After reset game, the board becomes an initial board (defaultBoard).
	 */
	@Test
	public void testResetGame() {
		defaultGame.resetGame();
	}

	/**
	 * Test {@link GameAbstract#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(defaultGame.invariant());
		assertFalse(gameKo.invariant());
	}

	public void setUpPlayers() {
		graphic = new GraphicConsole(board);

		blueHumanPlayer = new HumanPlayer("blueHumanPlayer", true);
		redHumanPlayer = new HumanPlayer("redHumanPlayer", false);

		defaultGame = new Game(board, blueHumanPlayer, redHumanPlayer);
		((GameAbstract) defaultGame).setCurrentPlayer(true);

		gameKo = new Game(board, blueHumanPlayer, blueHumanPlayer);
	}

	/**
	 * Test method for {@link mathchess.objects.Game#isGameOver()}.
	 */
	@Test
	public void testIsGameOver() {
		assertFalse(defaultGame.isGameOver());
	}
}
