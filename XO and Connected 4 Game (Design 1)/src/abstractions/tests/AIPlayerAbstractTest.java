/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import rules.Rule_Manger;
import specifications.RuleInterface;
import utils.GameExceptions;
import xoconnect4.AIPlayer;

/**
 * 
 *Test for {@link AIPlayer}
 *
 */
public abstract class AIPlayerAbstractTest extends PlayerAbstractTest {

	/**
	 * Test method for {@link xoconnect4.AIPlayer#AIPlayer(java.lang.String, boolean, specifications.RuleInterface)}.
	 */
	@Test
	public void testAIPlayer() {
		assertTrue(blue.invariant());
	}

	/**
	 * Test method for {@link xoconnect4.AIPlayer#getMove(specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testGetMove() throws GameExceptions {
		assertTrue(blue.invariant());
		assertNotNull(blue.getMove(board));
	}

	/**
	 * Test method for {@link xoconnect4.AIPlayer#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(blue.invariant());
	}

	public void setPlayer() {
		RuleInterface rule = new Rule_Manger();
		((Rule_Manger) rule).setRules(2);
		blue = new AIPlayer(blueName, true, rule);

		RuleInterface rule2 = new Rule_Manger();
		((Rule_Manger) rule2).setRules(2);
		red = new AIPlayer(redName, false, rule2);
	}

}
