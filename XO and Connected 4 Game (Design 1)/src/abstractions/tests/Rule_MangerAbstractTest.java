/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import rules.Rule_Manger;
import rules.Rule_Random;
import rules.Rule_Win;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;
import xoconnect4.HumanPlayer;

/**
 * 
 *Test for class {@link Rule_Manger}
 *
 */
public class Rule_MangerAbstractTest extends RuleAbstractTest {

	/**
	 * Test method for {@link rules.Rule_Manger#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertFalse(rule.invariant());
		((Rule_Manger) rule).setRules(2);
		((Rule_Manger) rule).setPlayer(bluePlayer);
		assertTrue(rule.invariant());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#setPlayer(specifications.PlayerInterface)}.
	 */
	@Test
	public void testSetPlayer() {
		Rule_Manger ruleTest = new Rule_Manger();
		ruleTest.setRules(2);
		assertFalse(ruleTest.invariant());
		ruleTest.setPlayer(bluePlayer);
		assertTrue(ruleTest.invariant());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#setPlayer(specifications.PlayerInterface)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetPlayerHumanPlayer() {
		HumanPlayer human = new HumanPlayer("Human", false);
		Rule_Manger ruleTest = new Rule_Manger();
		ruleTest.setRules(2);
		assertFalse(ruleTest.invariant());
		ruleTest.setPlayer(human);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#Rule_Manger()}.
	 */
	@Test
	public void testRule_Manger() {
		assertFalse(rule.invariant());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#setRules(java.lang.String)}.
	 */
	@Test
	public void testSetRulesString() {
		((Rule_Manger) rule).setRules("rules/goodrules.rules");
		((Rule_Manger) rule).showRules(true);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#setRules(int)}.
	 */
	@Test
	public void testSetRulesInt() {
		((Rule_Manger) rule).setRules(0);
		((Rule_Manger) rule).showRules(true);

		((Rule_Manger) rule).setRules(2);
		((Rule_Manger) rule).showRules(true);

		((Rule_Manger) rule).setRules(3);
		((Rule_Manger) rule).showRules(true);

	}

	/**
	 * Test method for {@link rules.Rule_Manger#getMove(specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testGetMove() throws GameExceptions {
		((Rule_Manger) rule).setRules(2);
		MoveInterface move = rule.getMove(board);
		assertNotNull(move);
		System.out.println(move.toString());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#showRules(boolean)}.
	 */
	@Test
	public void testShowRules() {
		((Rule_Manger) rule).setRules(2);
		((Rule_Manger) rule).showRules(true);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#addRule(specifications.RuleInterface)}.
	 */
	@Test
	public void testAddRule() {
		((Rule_Manger) rule).setRules(2);
		((Rule_Manger) rule).showRules(true);
		((Rule_Manger) rule).addRule(new Rule_Win());
		((Rule_Manger) rule).showRules(true);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#removeRule(int)}.
	 */
	@Test
	public void testRemoveRule() {
		((Rule_Manger) rule).setRules(2);
		((Rule_Manger) rule).showRules(true);
		((Rule_Manger) rule).removeRule(0);
		((Rule_Manger) rule).showRules(true);
	}

	/**
	 * Test method for {@link rules.Rule_Manger#getRuleByName(java.lang.String)}.
	 */
	@Test
	public void testGetRuleByName() {
		((Rule_Manger) rule).setRules(2);
		System.out.println(((Rule_Manger) rule).getRuleByName(
				"If you can win then win").toString());
	}

	/**
	 * Test method for {@link rules.Rule_Manger#getRuleByName(java.lang.String)}.
	 */
	@Test
	public void testGetRuleByNameNull() {
		((Rule_Manger) rule).setRules(2);
		assertNull(((Rule_Manger) rule)
				.getRuleByName("If you can win then winn"));
	}

}
