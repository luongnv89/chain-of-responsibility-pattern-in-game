/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import xoconnect4.Move;


/**
 *Test for class {@link Move} 
 *
 */
public class MoveTest {

	Move move;
	Move moveBoolean;
	Move moveNoInvariant;

	@Before
	public void setUp() throws Exception {
		move = new Move(0, 0);
		moveBoolean = new Move(1, 1, true);
		moveNoInvariant = new Move(-1, -1);
	}

	/**
	 * Test method for {@link xoconnect4.Move#Move(int, int, boolean)}.
	 */
	@Test
	public void testMoveIntIntBoolean() {
		assertTrue(moveBoolean.invariant());
		assertTrue(moveBoolean.isBlue());
	}

	/**
	 * Test method for {@link xoconnect4.Move#Move(int, int)}.
	 */
	@Test
	public void testMoveIntInt() {
		assertTrue(move.invariant());
	}

	/**
	 * Test method for {@link xoconnect4.Move#setBlue(boolean)}.
	 */
	@Test
	public void testSetBlue() {
		move.setBlue(false);
		assertFalse(move.isBlue());
	}

	/**
	 * Test method for {@link xoconnect4.Move#getCoordinate(int)}.
	 */
	@Test
	public void testGetCoordinate0() {
		assertTrue(move.getCoordinate(0) == 0);
	}

	/**
	 * Test method for {@link xoconnect4.Move#getCoordinate(int)}.
	 */
	@Test
	public void testGetCoordinate1() {
		assertTrue(move.getCoordinate(1) == 0);
	}

	/**
	 * Test method for {@link xoconnect4.Move#getCoordinate(int)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCoordinateException() {
		assertTrue(move.getCoordinate(-1) == 0);
	}

	/**
	 * Test method for {@link xoconnect4.Move#getCoordinate(int)}.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCoordinateException2() {
		assertTrue(move.getCoordinate(2) == 0);
	}

	/**
	 * Test method for {@link xoconnect4.Move#isBlue()}.
	 */
	@Test
	public void testIsBlue() {
		assertTrue(moveBoolean.isBlue());
	}

	/**
	 * Test method for {@link xoconnect4.Move#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(moveBoolean.invariant());
		assertFalse(moveNoInvariant.invariant());
	}

	/**
	 * Test method for {@link xoconnect4.Move#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(move.toString());
	}

	@Test
	public void testEqual() {
		assertTrue(move.equal(move));
		assertFalse(move.equal(moveBoolean));
	}
}
