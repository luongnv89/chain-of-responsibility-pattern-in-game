/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import xoconnect4.HumanPlayer;

/**
 * 
 *Test for class {@link HumanPlayer}
 *
 */
public class HumanPlayerAbstractTest extends PlayerAbstractTest {

	/**
	 * Test method for {@link xoconnect4.HumanPlayer#HumanPlayer(java.lang.String, boolean)}.
	 */
	@Test
	public void testHumanPlayer() {
		assertTrue(blue.invariant());
	}

	/**
	 * Test method for {@link xoconnect4.HumanPlayer#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(blue.invariant());
	}

	@Override
	public void setPlayer() {
		blue = new HumanPlayer(blueName, true);
		red = new HumanPlayer(redName, false);
	}

}
