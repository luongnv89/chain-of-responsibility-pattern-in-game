/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;

import rules.Rule_Manger;
import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.AIPlayer;
import xoconnect4.GraphicConsole;
import abstractions.PlayerAbstract;
import abstractions.RuleAbstract;

/**
 * Test for {@link RuleAbstract}
 *
 */
public abstract class RuleAbstractTest {
	protected BoardInterface board;
	protected GraphicConsole graphic;
	protected RuleInterface rule;
	protected PlayerInterface bluePlayer;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;
		graphic = null;
		rule = null;
		bluePlayer = null;
	}

	/**
	 * Test method for {@link abstractions.RuleAbstract#getRuleName()}.
	 */
	@Test
	public void testGetRulesName() {
		System.out.println(((RuleAbstract) rule).getRuleName());
	}

	/**
	 * Test method for {@link abstractions.RuleAbstract#setPlayer(specifications.PlayerInterface)}.
	 */
	@Test
	public void testSetPlayer() {
		rule.setPlayer(bluePlayer);
	}

	/**
	 * Test method for {@link rules.Rule_Random#getMove(specifications.BoardInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testGetMove() throws GameExceptions {
		graphic.view();
		MoveInterface move = rule.getMove(board);
		if (move != null) {
			MessageConsole.inforMessage(((PlayerAbstract) bluePlayer)
					.getPlayerName() + " move: " + move.toString());
			bluePlayer.makeMove(move, board);
			graphic.view();
		}
	}

	public void setupGame() {
		graphic = new GraphicConsole(board);
		bluePlayer = new AIPlayer("AI Random", true, rule);
	}
}
