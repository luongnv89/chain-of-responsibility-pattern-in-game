/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import abstractions.BoardAbstract;

/**
 * Test for class {@link BoardAbstract}
 *
 */
public abstract class BoardAbstractTest {

	protected BoardAbstract board;
	protected BoardAbstract defaultBoard;

	protected int WIDH_DEFAULT;
	protected int HEIGHT_DEFAULT;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		board = null;
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#BoardAbstract(int, int)}.
	 */
	@Test
	public void testBoardAbstract() {
		assertTrue(board.invariant());
		assertTrue(defaultBoard.invariant());
		assertTrue(defaultBoard.getHeight() == HEIGHT_DEFAULT);
		assertTrue(defaultBoard.getWidth() == WIDH_DEFAULT);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#resetBoard()}.
	 */
	@Test
	public void testResetBoard() {
		board.setState(board.getWidth() - 1, board.getHeight() - 1, 1);
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == 1);
		board.resetBoard();
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == 0);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#isEmpty(int, int)}.
	 */
	@Test
	public void testIsEmpty() {
		assertTrue(board.isEmpty(board.getWidth() - 1, board.getHeight() - 1));
		board.setState(board.getWidth() - 1, board.getHeight() - 1, 1);
		assertFalse(board.isEmpty(board.getWidth() - 1, board.getHeight() - 1));
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#insideBoard(int, int)}.
	 */
	@Test
	public void testInsideBoard() {
		assertTrue(board.insideBoard(0, 0));
		assertTrue(board.insideBoard(board.getWidth() - 1,
				board.getHeight() - 1));
		assertTrue(board.insideBoard((board.getWidth() - 1) / 2,
				(board.getHeight() - 1) / 2));
		assertFalse(board.insideBoard(-1, 0));
		assertFalse(board.insideBoard(0, -1));
		assertFalse(board.insideBoard(board.getWidth(), 0));
		assertFalse(board.insideBoard(0, board.getHeight()));
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#setState(int, int, int)}.
	 */
	@Test
	public void testSetState() {
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == 0);
		board.setState(board.getWidth() - 1, board.getHeight() - 1, 2);
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == 2);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#getState(int, int)}.
	 */
	@Test
	public void testGetState() {
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == 0);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#getWidth()}.
	 */
	@Test
	public void testGetWidth() {
		assertTrue(defaultBoard.getWidth() == WIDH_DEFAULT);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#getHeight()}.
	 */
	@Test
	public void testGetHeight() {
		assertTrue(defaultBoard.getHeight() == HEIGHT_DEFAULT);
	}

	/**
	 * Test method for {@link abstractions.BoardAbstract#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(board.toString());
	}

}
