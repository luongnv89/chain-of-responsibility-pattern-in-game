/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.*;

import org.junit.Test;


import specifications.MoveInterface;
import utils.GameExceptions;
import xoconnect4.Board;
import xoconnect4.Move;


/**
 * Test for class {@link Board}
 *
 *
 */
public abstract class BoardTest extends BoardAbstractTest {

	/**
	 * Test method for {@link xoconnect4.Board#Board(int, int)}.
	 */
	@Test
	public void testBoard() {
		assertTrue(((Board) board).getHistoryMoves().isEmpty());
	}

	/**
	 * Test method for {@link xoconnect4.Board#calculatePossibleMove()}.
	 */
	@Test
	public void testCalculatePossibleMove() {
		assertFalse(((Board) board).getListAllPossibleMoves().isEmpty());
	}

	/**
	 * Test method for {@link xoconnect4.Board#getListAllPossibleMoves()}.
	 */
	@Test
	public void testGetListAllPossibleMoves() {
		for (int i = 0; i < ((Board) board).getListAllPossibleMoves().size(); i++) {
			System.out.println(((Board) board).getListAllPossibleMoves().get(i)
					.toString());
		}
	}

	/**
	 * Test method for {@link xoconnect4.Board#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(board.invariant());
		board.setState(board.getWidth() - 1, board.getHeight() - 1, -1);
		assertFalse(board.invariant());
		board.resetBoard();
		assertTrue(board.invariant());
		board.setState(board.getWidth() - 1, board.getHeight() - 1, 3);
		assertFalse(board.invariant());
	}

	/**
	 * Test method for {@link xoconnect4.Board#initial()}.
	 */
	@Test
	public void testInitial() {
		assertTrue(board.invariant());
	}

	/**
	 * Test method for {@link xoconnect4.Board#undoLastMove()}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testUndoLastMove() throws GameExceptions {
		int x = board.getWidth() - 1;
		int y = board.getHeight() - 1;
		MoveInterface move = new Move(x, y);
		((Move) move).setBlue(true);

		// Before make move
		assertFalse(board.undoLastMove());
		assertTrue(board.isEmpty(x, y));
		assertTrue(((Board) board).getHistoryMoves().isEmpty());
		// After made move
		assertTrue(board.makeMove(move));
		assertTrue(board.getState(x, y) == 1);
		assertTrue(((Board) board).getHistoryMoves().size() == 1);
		// After undo move
		assertTrue(board.undoLastMove());
		assertTrue(board.isEmpty(x, y));
		assertTrue(((Board) board).getHistoryMoves().isEmpty());
	}

	/**
	 * Test method for {@link xoconnect4.Board#canMakeMove(specifications.MoveInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testCanMakeMove() throws GameExceptions {
		int x = board.getWidth() - 1;
		int y = board.getHeight() - 1;
		MoveInterface move = new Move(x, y);
		((Move) move).setBlue(true);

		// Before make move
		assertTrue(board.canMakeMove(move));
		// After made move
		assertTrue(board.makeMove(move));
		assertTrue(board.getState(x, y) == 1);
		assertFalse(board.canMakeMove(move));

		assertFalse(board.canMakeMove(new Move(-1, 0)));
		assertFalse(board.canMakeMove(new Move(0, -1)));
		assertFalse(board.canMakeMove(new Move(board.getWidth(), board
				.getHeight())));
	}

	/**
	 * Test method for {@link xoconnect4.Board#getHistoryMoves()}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testGetHistoryMoves() throws GameExceptions {
		assertTrue(((Board) board).getHistoryMoves().isEmpty());
		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 1, true)));
		assertTrue(((Board) board).getHistoryMoves().size() == 1);
	}

	/**
	 * Test method for {@link xoconnect4.Board#isOver()}.
	 * @throws GameExceptions 
	 */
	@Test
	public abstract void testIsOverInColumn() throws GameExceptions;

	/**
	 * Test method for {@link xoconnect4.Board#isOver()}.
	 * @throws GameExceptions 
	 */
	@Test
	public abstract void testIsOverOnRow() throws GameExceptions;

	/**
	 * Test method for {@link xoconnect4.Board#isOver()}.
	 * @throws GameExceptions 
	 */
	@Test
	public abstract void testIsOverOnDiagonalLTRB() throws GameExceptions;

	/**
	 * Test method for {@link xoconnect4.Board#isOver()}.
	 * @throws GameExceptions 
	 */
	@Test
	public abstract void testIsOverOnDiagonalLBRT() throws GameExceptions;

}
