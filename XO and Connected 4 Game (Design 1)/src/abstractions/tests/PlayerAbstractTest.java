/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;

import specifications.BoardInterface;
import utils.GameExceptions;
import xoconnect4.HumanPlayer;
import xoconnect4.Move;
import abstractions.PlayerAbstract;

/**
 * Test {@link PlayerAbstract} class.
 * an abstract class which is extended by {@link PlayerMathChessTest}.
 */
public abstract class PlayerAbstractTest {

	/**
	 * players for tests
	 */
	protected PlayerAbstract blue;
	protected PlayerAbstract red;
	protected PlayerAbstract blueKo;

	/**
	 * names for tests
	 */
	protected String blueName = "Blue";
	protected String redName = "Red";

	/**
	* board for test
	*/
	protected BoardInterface board;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		blue = null;
		red = null;
		blueKo = null;
		board = null;
	}

	/**
	 * Test method for
	 * {@link abstractions.PlayerAbstract#PlayerAbstract(java.lang.String, boolean)}
	 */
	@Test
	public void testConstrcutor() {
		assertTrue(blue.invariant());
		assertTrue(blue.isBlue());
		assertEquals(blueName.hashCode(), blue.getPlayerName().hashCode());

		assertTrue(red.invariant());
		assertFalse(red.isBlue());
		assertEquals(redName.hashCode(), red.getPlayerName().hashCode());
	}

	/**
	 * test {@link PlayerAbstract#isBlue()}.
	 */
	@Test
	public void testIsBlue() {
		assertTrue(blue.isBlue());
		assertFalse(red.isBlue());
	}

	/**
	 * Test method for {@link abstractions.PlayerAbstract#getPlayerName()}.
	 */
	@Test
	public void testGetPlayerName() {
		assertEquals(blueName.hashCode(), blue.getPlayerName().hashCode());
		assertEquals(redName.hashCode(), red.getPlayerName().hashCode());
	}

	/**
	 * Test method for {@link abstractions.PlayerAbstract#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(blue.toString());
	}

	public void setUpHumanPlayers() throws GameExceptions {
		blue = new HumanPlayer(blueName, true);
		red = new HumanPlayer(redName, false);
		blueKo = new HumanPlayer(blueName, true);
		blue.makeMove(new Move(board.getWidth() - 1, board.getHeight() - 1,
				true), board);
		blue.makeMove(new Move(board.getWidth() - 2, board.getHeight() - 1,
				true), board);
		blue.makeMove(new Move(board.getWidth() - 1, board.getHeight() - 2,
				true), board);

	}

	/**
	 * Setup player
	 */
	public abstract void setPlayer();
}
