/**
 * 
 */
package abstractions.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;


import xoconnect4.GraphicConsole;
import xoconnect4.XOBoard;

/**
 *Test for {@link GraphicConsole} with {@link XOBoard}
 *
 */
public class GraphicConsoleAbstractTest extends GraphicAbstractTest {


	/**
	 * Test method for {@link mathchess.objects.GraphicConsole#GraphicConsole(specifications.BoardInterface)}.
	 */
	@Test
	public void testGraphicConsole() {
		assertTrue(graphic.invariant());
	}

	/**
	 * Test method for {@link mathchess.objects.GraphicConsole#view()}.
	 */
	@Test
	public void testView() {
		graphic.view();
	}

}
