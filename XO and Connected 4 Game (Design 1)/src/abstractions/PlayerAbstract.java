package abstractions;

import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;
import xoconnect4.Board;
import xoconnect4.Move;

/**
 * {@link PlayerAbstract} represent a player abstraction in a game <li>
 * {@link PlayerAbstract#playerName} The name of player <li>
 * {@link PlayerAbstract#blue} The color represent for player in a game which
 * has two player against each other <li>true if the player is a blue player in
 * a game <li>false if the player is a red player in a game
 */
public abstract class PlayerAbstract implements PlayerInterface {
	/**
	 * The name of player
	 */
	protected String playerName;
	/**
	 * The color represent for player in a game which has two player against each other
	 * <li> true if the player is a blue player in a game
	 * <li> false if the player is a red player in a game
	 */
	protected boolean blue;

	/**
	 * Create a new player with name and color <li>initial the beginning state
	 * of player
	 * 
	 * @param playerName
	 * @param blue
	 */
	public PlayerAbstract(String playerName, boolean blue) {
		this.playerName = playerName;
		this.blue = blue;
	}

	/**
	 * Get the name of player
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}

	public boolean isBlue() {
		return blue;
	}

	/**
	 * <li> Set player for move
	 * @return {@link Board#makeMove(MoveInterface)}
	 * */
	@Override
	public boolean makeMove(MoveInterface move, BoardInterface board)
			throws GameExceptions {
		((Move) move).setBlue(isBlue());
		return board.makeMove(move);
	}

	@Override
	public boolean undoLastMove(BoardInterface board) throws GameExceptions {
		return board.undoLastMove();
	}
}
