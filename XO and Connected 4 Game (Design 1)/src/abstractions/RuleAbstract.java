package abstractions;

import java.util.ArrayList;

import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.Board;
import xoconnect4.Move;

/**
 * {@link RuleAbstract} implements {@link RuleInterface} present abstract of rule of AI player
 *
 */
public abstract class RuleAbstract implements RuleInterface {

	/**
	 * The name of rule
	 */
	protected String ruleName;
	/**
	 * The player use rule
	 */
	protected PlayerInterface player;

	@Override
	public boolean invariant() {
		return ruleName != null && player != null;
	}

	/**
	 * Get the name of rule
	 * @return
	 */
	public String getRuleName() {
		return ruleName;
	}

	public void setPlayer(PlayerInterface player) {
		this.player = player;
	}

	/**
	 * Get win move for isBlue player
	 * @param board
	 * @param isBlue
	 * @return <li> null if there isn't any win move 
	 * <li> a move can lead the player to win
	 * @throws GameExceptions
	 */
	protected MoveInterface getWinMove(BoardInterface board, boolean isBlue)
			throws GameExceptions {
		ArrayList<MoveInterface> listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(((Board) board).getListAllPossibleMoves());
		for (int i = 0; i < listAllPossibleMoves.size(); i++) {
			Move move = (Move) listAllPossibleMoves.get(i);
			move.setBlue(isBlue);
			// Find the win move of enemy
			if (board.makeMove(move)) {
				if (((Board) board).isOver()) {
					MessageConsole.inforMessage("Move block win: "
							+ move.toString());
					board.undoLastMove();
					return move;
				}
				board.undoLastMove();
			}
		}
		return null;
	}
}
