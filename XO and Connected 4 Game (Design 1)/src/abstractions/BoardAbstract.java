package abstractions;

import specifications.BoardInterface;

/**
 * {@link BoardAbstract} implements from {@link BoardInterface} represent a abstract of board game
 * <br> The state of board is presented by a 2D Integer array.
 */
public abstract class BoardAbstract implements BoardInterface {

	/**
	 * Present the state of board
	 */
	private int[][] states;

	/**
	 * The width of board
	 */
	private int width;

	/**
	 * The height of board
	 */
	private int height;

	/**
	 * Constructor a board with the height and the width as integers.
	 * @param width
	 * @param height
	 */
	public BoardAbstract(int width, int height) {
		this.width = width;
		this.height = height;
		this.states = new int[width][height];
		this.initial();
	}

	public void resetBoard() {
		initial();
	}

	/**
	 * Check empty of (x,y)
	 * @param x
	 * @param y
	 * @return true if the state of board at (x,y) is empty
	 * <li> false in other case
	 */
	public boolean isEmpty(int x, int y) {
		return this.getState(x, y) == 0;
	}

	/**
	 * Check valid of (x,y)
	 * @param x
	 * @param y
	 * @return true if (x,y) inside the board
	 * <li> false if (x,y) outside the board
	 */
	public boolean insideBoard(int x, int y) {
		return x >= 0 && x < getWidth() && y >= 0 && y < getHeight();
	}

	/**
	 * Update state of board at (x,y)
	 * @param x
	 * @param y
	 * @param value
	 */
	public void setState(int x, int y, int value) {
		states[x][y] = value;
	}

	public int getState(int x, int y) {
		return states[x][y];
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	/**
	 * just display states on the board as an array of values of each position.
	 */
	public String toString() {
		String str = "";
		for (int i = 0; i < getHeight(); i++) {
			for (int j = 0; j < getWidth(); j++) {
				str = str + String.valueOf(this.getState(j, i));
			}
			str += "\n";
		}
		return str;
	}

}
