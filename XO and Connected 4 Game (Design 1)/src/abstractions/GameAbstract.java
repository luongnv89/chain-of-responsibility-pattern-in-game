package abstractions;

import specifications.BoardInterface;
import specifications.GameInterface;
import specifications.GraphicInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.Board;
import xoconnect4.Game;

/**
 * An abstract class which is extended by {@link Game}.
 * It allows to start a game and reset the game. To reset a game,
 * we reset the board and players of this game back to the initial state.
 * It also allows to get the current player {@link GameAbstract#getCurrentPlayer()} at the specific state.
 * Defines invariant() method when the board and players invariant, two players are different.
 */
public abstract class GameAbstract implements GameInterface {

	/**
	 * The board game
	 */
	protected BoardInterface board;

	/**
	 * The graphic will represent the board game
	 */
	protected GraphicInterface graphic;

	/**
	 * Blue player in game
	 */
	protected PlayerInterface bluePlayer;
	/**
	 * The red player in game
	 */
	protected PlayerInterface redPlayer;
	/**
	 * The current player in game
	 */
	protected PlayerInterface currentPlayer;

	/**
	 * @param board
	 * @param bluePlayer
	 * @param redPlayer
	 */
	public GameAbstract(BoardInterface board, PlayerInterface bluePlayer,
			PlayerInterface redPlayer) {
		this.board = board;
		this.bluePlayer = bluePlayer;
		this.redPlayer = redPlayer;
	}

	/**
	 * Set current player of game, to set the player will go first when game start
	 * <li> if blueFirst is true, the blue player will be the current player of game
	 * <li> if blueFirst is false, the red player will be the current player of game
	 * @param blueFirst
	 */
	public void setCurrentPlayer(boolean blueFirst) {
		if (blueFirst) {
			this.currentPlayer = bluePlayer;
		} else {
			this.currentPlayer = redPlayer;
		}
	}

	/**
	 * Get the current player of game
	 * @return
	 */
	public PlayerInterface getCurrentPlayer() {
		return currentPlayer;
	}

	public void resetGame() {
		board.resetBoard();
		graphic.resetGraphic();

	}

	public void startGame() throws GameExceptions {
		graphic.view();
		while (!isGameOver() && this.invariant()) {
			MessageConsole.inforMessage("Number of possible moves: "
					+ ((Board) board).getListAllPossibleMoves().size());
			MessageConsole.inforMessage(((PlayerAbstract) currentPlayer)
					.getPlayerName()
					+ " go: ("
					+ (currentPlayer.isBlue() ? "blue" : "red") + ")");
			MoveInterface move = currentPlayer.getMove(board);
			if (board.canMakeMove(move)) {
				currentPlayer.makeMove(move, board);
				MessageConsole.inforMessage(((PlayerAbstract) currentPlayer)
						.getPlayerName() + " made move: " + move.toString());
				graphic.view();
				currentPlayer = nextPlayer();
			} else {
				throw (new GameExceptions(this,
						((PlayerAbstract) currentPlayer).getPlayerName()
								+ " Invalid move: " + move.toString()));
			}
		}
		if (!((Board) board).isFull()) {
			MessageConsole.inforMessage(((PlayerAbstract) currentPlayer)
					.getPlayerName() + " win!");
		} else {
			MessageConsole.inforMessage("Game tie!");
		}
	}

	/**
	 * Change the turn to next player
	 * @return the next player
	 */
	private PlayerInterface nextPlayer() {
		if (this.getCurrentPlayer() == bluePlayer) {
			return redPlayer;
		} else
			return bluePlayer;
	}

	/**
	 *  @return false 
	 *  <li>If the position of chess-man is out of the board
	 *  <li> There is two Chess-men at the same position 
	 *  <br>(In this case, at that position, the state of board is only one value 
	 *  <br> -> the first condition will return false 
	 *  <br>=> we don't need to check this case)
	 * */
	@Override
	public boolean invariant() {
		// Check invariant of board
		if (!this.board.invariant()) {
			MessageConsole
					.errorMessage("@GameAbstract#invariant: Board is invalid");
			return false;
		}

		// Check invariant of blue Player on board
		if (!bluePlayer.invariant()) {
			MessageConsole.debugMessage(((PlayerAbstract) this.bluePlayer)
					.getPlayerName() + " is invalid");
			return false;
		}

		// Check invariant of red Player on board
		if (!redPlayer.invariant()) {
			MessageConsole.debugMessage(((PlayerAbstract) this.redPlayer)
					.getPlayerName() + " is invalid");
			return false;
		}

		// Check graphic
		if (!graphic.invariant()) {
			MessageConsole
					.errorMessage("@GameAbstract#invariant: Graphic is invalid");
			return false;
		}

		// Check kind of player
		if (this.bluePlayer.isBlue() == this.redPlayer.isBlue()) {
			MessageConsole.debugMessage("Two player are same color");
			return false;
		}
		return true;
	}

}
