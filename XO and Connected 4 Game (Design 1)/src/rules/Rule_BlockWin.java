package rules;

import java.util.ArrayList;

import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.Board;
import abstractions.RuleAbstract;

/**
 * {@link Rule_BlockWin} extends from {@link RuleAbstract} present a rule of AI player
 * <br> The rule will find the move can block the enemy of player go to win
 *
 */
public class Rule_BlockWin extends RuleAbstract {

	public Rule_BlockWin() {
		this.ruleName = "If you can block a win then block a win";
	}

	/**
	 * The rule look at all possible move of board
	 * @return <li> A move can block the enemy of player go to win
	 * <li> null if there isn't any move can block the enemy of player go to win
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws GameExceptions {
		ArrayList<MoveInterface> listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(((Board) board).getListAllPossibleMoves());
		if (listAllPossibleMoves.isEmpty()) {
			MessageConsole.inforMessage("There isn't any possible move");
			return null;
		} else {
			return getWinMove(board, !player.isBlue());
		}
	}

}
