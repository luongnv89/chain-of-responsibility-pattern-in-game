/**
 * 
 */
package rules;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import specifications.BoardInterface;
import specifications.MoveInterface;
import specifications.PlayerInterface;
import specifications.RuleInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.HumanPlayer;
import abstractions.PlayerAbstract;
import abstractions.RuleAbstract;

/**
 * {@link Rule_Manger} extends {@link RuleAbstract} manager all rules will be used by AI player
 *
 */
public class Rule_Manger extends RuleAbstract {

	/**
	 * List rules will be used by AI player
	 * <li> The manager will look at all rules in ordering to get the move for AI player
	 */
	ArrayList<RuleInterface> listRules;
	/**
	 * All rules of AI player which AI player can use
	 */
	ArrayList<RuleInterface> allRules;

	/**
	 * Create a rule manager
	 */
	public Rule_Manger() {
		ruleName = "RuleManager";
		listRules = new ArrayList<RuleInterface>();
		allRules = new ArrayList<RuleInterface>();
		loadRules();
		// setRules(0)
	}

	/**
	 * set rule from text file
	 * @param ruleFilePath
	 */
	public void setRules(String ruleFilePath) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(ruleFilePath));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				RuleInterface rule = getRuleByName(line);
				addRule(rule);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set rules for AI player 
	 * @param index
	 * <li> index = 0, get randomly number of rules and set randomly ordering of rules for AI player
	 * <li> index = 1, choose and set rules for AI player manually
	 * <li> index = 2, set rules for player in order: Rule_Win -> Rule_BlockWin -> Rule_Random
	 * <li> index = 3, set rules for player only : Rule_Random
	 */
	public void setRules(int index) {
		switch (index) {
		case 0:
			setRandomly();
			break;
		case 1:
			setManually();
			break;
		case 2:
			setGoodRules();
			break;
		case 3:
			setBadRules();
			break;
		default:
			throw new IllegalArgumentException(
					"@Rule_Manager#setRules(int): Invalid input: " + index);
		}
	}

	/**
	 * Set the bad rule for AI player
	 * <bre> There is only {@link Rule_Random} in listRules 
	 */
	private void setBadRules() {
		listRules.clear();
		Rule_Random rule = new Rule_Random();
		rule.setPlayer(player);
		addRule(rule);
	}

	/**
	 * Set a good rule ordering for AIplayer
	 * <li> The first rule is {@link Rule_Win}
	 * <li> The second rule is {@link Rule_BlockWin}
	 * <li> The final rule is {@link Rule_Random}
	 */
	private void setGoodRules() {
		listRules.clear();

		Rule_Win win = new Rule_Win();
		win.setPlayer(player);
		addRule(win);

		Rule_BlockWin block = new Rule_BlockWin();
		block.setPlayer(player);
		addRule(block);

		Rule_Random rule = new Rule_Random();
		rule.setPlayer(player);
		addRule(rule);

	}

	/**
	 * Set manually rule and order of rules
	 */
	private void setManually() {
		listRules.clear();
		System.out.println("Total rules of AI player:");
		showRules(false);

		int totalRules = allRules.size();

		// Get the number of rules
		int numberOfUseRules = getNumberOfUseRules(totalRules);
		Scanner input = new Scanner(System.in);
		// Get the list index of rules
		ArrayList<Integer> listRuleIndex = new ArrayList<Integer>();
		for (int i = 0; i < numberOfUseRules; i++) {
			int ruleIndex = -1;
			while (ruleIndex < 0 || ruleIndex > totalRules - 1) {
				System.out
						.println("[ "
								+ i
								+ " ] Index of rule will be added (Each rule can be selected only one time): ");
				try {
					ruleIndex = Integer.parseInt(input.nextLine());
					if (!listRuleIndex.contains(ruleIndex)) {
						listRuleIndex.add(ruleIndex);
					} else {
						System.out.println("Rule has selected: " + ruleIndex);
						ruleIndex = -1;
					}
				} catch (NumberFormatException e) {
					System.out.println(e.toString());
					ruleIndex = -1;
				}
			}
		}

		// Add list rules
		for (int i = 0; i < listRuleIndex.size(); i++) {
			addRule(allRules.get(listRuleIndex.get(i)));
		}

	}

	/**
	 * Get the number of ruled will use
	 * @param totalRules
	 * @return
	 */
	private int getNumberOfUseRules(int totalRules) {
		Scanner input = new Scanner(System.in);
		int numberOfUseRules = -1;
		while (numberOfUseRules < 1 || numberOfUseRules > totalRules) {
			System.out
					.println("Number of rules will be used (must be possitive and less than "
							+ totalRules + " :");
			try {
				numberOfUseRules = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e) {
				System.out.println(e.toString());
				numberOfUseRules = -1;
			}
		}
		return numberOfUseRules;
	}

	/**
	 * <li> Get randomly number of rules will be use
	 * <li> Set randomly order of rules
	 */
	private void setRandomly() {
		listRules.clear();
		Random ran = new Random();
		int numberOfRules = ran.nextInt(allRules.size());
		if (numberOfRules == 0)
			numberOfRules = 1;
		ArrayList<RuleInterface> allrules = new ArrayList<RuleInterface>();
		allrules.addAll(allRules);
		for (int i = 0; i < numberOfRules; i++) {
			int index = ran.nextInt(allrules.size());
			RuleInterface rule = allrules.get(index);
			addRule(rule);
			allrules.remove(index);
		}
		addRule(new Rule_Random());
	}

	/**
	 * Load all the rules of AI player
	 */
	private void loadRules() {

		allRules.add(new Rule_Win());

		allRules.add(new Rule_BlockWin());

		allRules.add(new Rule_Random());
	}

	/**
	 * Set the AI player will use the rule
	 * @param player the player to set
	 */
	public void setPlayer(PlayerInterface player)
			throws IllegalArgumentException {
		if (player instanceof HumanPlayer) {
			throw (new IllegalArgumentException(
					"Cannot apply a playing rule for a Human player: "
							+ ((PlayerAbstract) player).getPlayerName()));
		} else {
			this.player = player;
			for (int i = 0; i < listRules.size(); i++) {
				listRules.get(i).setPlayer(player);
			}
		}
	}

	/**
	 * The Rule_manager will look at in ordering rules in the listRules until one of rule give a possible move
	 * <li> If all rules cannot get any possible move, return null
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws GameExceptions {
		for (int i = 0; i < listRules.size(); i++) {
			MoveInterface move = listRules.get(i).getMove(board);
			if (move != null) {
				MessageConsole.inforMessage("Rule: "
						+ ((RuleAbstract) listRules.get(i)).getRuleName());
				return move;
			}
		}
		return null;
	}

	/**
	 * @return false when:
	 * <br> there isn't any rule to manage
	 * <br> there rules isn't unique
	 * */
	@Override
	public boolean invariant() {
		if (listRules.isEmpty()) {
			MessageConsole
					.inforMessage("There isn't any rule to apply to get move");
			return false;
		}

		return super.invariant();
	}

	/**
	 * @param useRule
	 * <li> useRule is true, show the list using rules of AI player
	 * <li> useRule is false, show all rules of AI player
	 */
	public void showRules(boolean useRule) {
		ArrayList<RuleInterface> showRules = useRule ? listRules : allRules;
		for (int i = 0; i < showRules.size(); i++) {
			System.out.println("[" + i + "]: "
					+ ((RuleAbstract) showRules.get(i)).getRuleName());
		}
	}

	/**
	 * Add new rule in the list rules of AI player
	 * 
	 * @param rule
	 */
	public void addRule(RuleInterface rule) {
		if (listRules.contains(rule)) {
			System.out.println("The rule already exist!");
		} else {
			rule.setPlayer(player);
			listRules.add(rule);
		}

	}

	/**
	 * Remove the rule at index from list rules of AI player
	 * @param index
	 */
	public void removeRule(int index) {
		this.listRules.remove(index);
	}

	/**
	 * Get the rule in listRules by name of rule
	 * @param ruleName
	 * @return <li> A rule has name equals input name
	 * <li> null if there isn't any move has name like input name
	 */
	public RuleInterface getRuleByName(String ruleName) {
		for (int i = 0; i < allRules.size(); i++) {
			if (((RuleAbstract) allRules.get(i)).getRuleName().equals(ruleName)) {
				return allRules.get(i);
			}
		}
		System.out.println("Cannot found the rule with name = " + ruleName);
		return null;
	}

}
