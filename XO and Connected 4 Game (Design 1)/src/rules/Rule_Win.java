package rules;

import java.util.ArrayList;

import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.Board;
import abstractions.RuleAbstract;

/**
 * {@link Rule_Win} extends from {@link RuleAbstract} present a rule of AI player
 * <br> Rule will find the move can lead player go to win
 *
 */
public class Rule_Win extends RuleAbstract {

	public Rule_Win() {
		this.ruleName = "If you can win then win";
	}

	/**
	 * The rule look at all possible move of board
	 * @return <li> A move can lead player go to win
	 * <li> null if there isn't any move can lead player go to win
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws GameExceptions {
		ArrayList<MoveInterface> listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(((Board) board).getListAllPossibleMoves());
		if (listAllPossibleMoves.isEmpty()) {
			MessageConsole.inforMessage("There isn't any possible move");
			return null;
		} else {
			return getWinMove(board, player.isBlue());
		}
	}
}
