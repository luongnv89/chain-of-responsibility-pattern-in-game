package rules;

import java.util.ArrayList;
import java.util.Random;

import abstractions.RuleAbstract;

import specifications.BoardInterface;
import specifications.MoveInterface;
import utils.GameExceptions;
import utils.MessageConsole;
import xoconnect4.Board;
import xoconnect4.Move;

/**
 * {@link Rule_Random} extends {@link RuleAbstract} present a rule of AI player
 * <br> The Rule_Random get a possible move for AI player randomly
 */
public class Rule_Random extends RuleAbstract {

	public Rule_Random() {
		this.ruleName = "Choose one of the remaining free positions randomly";
	}

	/**
	 * The rule will look at all possible move on board.
	 * <li> Choose a move randomly, and check the move is safe or not
	 * <br> A safe move : After the move has done, player won't be lost
	 * <br> @return <li>a safe possible move
	 * <li> null if there isn't any possible move
	 * 
	 * */
	@Override
	public MoveInterface getMove(BoardInterface board) throws GameExceptions {
		ArrayList<MoveInterface> listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(((Board) board).getListAllPossibleMoves());
		if (listAllPossibleMoves.isEmpty()) {
			MessageConsole.inforMessage("There isn't any possible move");
			return null;
		} else {
			Random random = new Random();
			boolean safe = false;
			MoveInterface move = null;
			while (!safe && !listAllPossibleMoves.isEmpty()) {
				int index = random.nextInt(listAllPossibleMoves.size());
				move = listAllPossibleMoves.get(index);
				listAllPossibleMoves.remove(index);
				safe = moveSafe(move, board);
			}
			return move;
		}
	}

	/**
	 * Check a move is safe or not
	 * <br> check the possible move of enemy after the checking move has done
	 * @param move
	 * @param board
	 * @return <li> true if after move, there isn't any possible move
	 * <li> false if there is some move can lead the enemy player go to win
	 * <li> true if there isn't any move can lead the enemy player go to win
	 * @throws GameExceptions if the checking move cannot be done 
	 */
	private boolean moveSafe(MoveInterface move, BoardInterface board)
			throws GameExceptions {

		player.makeMove(move, board);

		ArrayList<MoveInterface> listAllPossibleMoves = new ArrayList<MoveInterface>();
		listAllPossibleMoves.addAll(((Board) board).getListAllPossibleMoves());
		if (listAllPossibleMoves.isEmpty()) {
			MessageConsole
					.inforMessage("@Rule_Random#moveSafe:There isn't any possible move");
			player.undoLastMove(board);
			return true;
		} else {
			for (int i = 0; i < listAllPossibleMoves.size(); i++) {
				Move enemyMove = (Move) listAllPossibleMoves.get(i);
				enemyMove.setBlue(!player.isBlue());
				if (board.makeMove(enemyMove)) {
					// Check the over state
					if (((Board) board).isOver()) {
						board.undoLastMove();
						player.undoLastMove(board);
						return false;
					}
					board.undoLastMove();
				}
			}
		}

		player.undoLastMove(board);
		return true;
	}
}
