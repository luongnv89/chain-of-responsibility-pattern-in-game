/**
 * 
 */
package connect4.tests;

import org.junit.Before;

import abstractions.tests.RuleAbstractTest;

import rules.Rule_BlockWin;
import xoconnect4.Connect4Board;
import xoconnect4.Move;

/**
 * Test for {@link Rule_BlockWin}
 *
 */
public class Connect4Rule_BlockWinTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		rule = new Rule_BlockWin();
		setupGame();
		board.makeMove(new Move(0, board.getHeight() - 1, false));
		board.makeMove(new Move(0, board.getHeight() - 2, false));
		board.makeMove(new Move(0, board.getHeight() - 3, false));
	} 

}
