/**
 * 
 */
package connect4.tests;

import org.junit.Before;

import abstractions.tests.AIPlayerAbstractTest;

import xoconnect4.Connect4Board;

/**
 * extends {@link PlayerMathChess} class.
 * tests AI player behaviors.
 */
public class Connect4AIPlayerTest extends AIPlayerAbstractTest {

	/** 
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		setPlayer();
	}

}
