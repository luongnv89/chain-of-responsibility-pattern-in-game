/**
 * 
 */
package connect4.tests;

import org.junit.Before;

import abstractions.tests.HumanPlayerAbstractTest;

import xoconnect4.Connect4Board;

/**
 *Extends {@link PlayerMathChessTest} class.
 *Test a human player behaviors.
 */
public class Connect4HumanPlayerTest extends HumanPlayerAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		setPlayer();
	}

}
