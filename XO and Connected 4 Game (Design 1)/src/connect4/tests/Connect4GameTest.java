/**
 * 
 */
package connect4.tests;


import org.junit.Before;

import abstractions.tests.GameAbstractTest;

import simulations.AIConnect4PlayingGameTest;
import simulations.HumanConnect4PlayingGameTest;
import xoconnect4.Connect4Board;
import xoconnect4.Game;

/**
 * test {@link Game} class.
 * test the validation of methods in the class. There is also another test class to test playing a game:
 * {@link AIConnect4PlayingGameTest} and {@link HumanConnect4PlayingGameTest}.
 */
public class Connect4GameTest extends GameAbstractTest {
 
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		setUpPlayers();
	}

}
