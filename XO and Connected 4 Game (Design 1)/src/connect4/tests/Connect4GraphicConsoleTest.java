/**
 * 
 */
package connect4.tests;

import org.junit.Before;

import abstractions.tests.GraphicAbstractTest;

import xoconnect4.Connect4Board;
import xoconnect4.GraphicConsole;

/**
 * Test for {@link GraphicConsole} with {@link Connect4Board}
 *
 */
public class Connect4GraphicConsoleTest extends GraphicAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		graphic = new GraphicConsole(board);
	}

}
