/**
 * 
 */
package connect4.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import abstractions.tests.BoardTest;


import utils.GameExceptions;
import xoconnect4.Board;
import xoconnect4.Connect4Board;
import xoconnect4.Move;

/**
 * Test for class {@link Connect4Board}
 *
 *
 */
public class Connect4BoardTest extends BoardTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		WIDH_DEFAULT = 7;
		HEIGHT_DEFAULT = 6;
		board = new Connect4Board(9, 10);
		defaultBoard = new Connect4Board();
	}

	/**
	 * Test method for {@link xoconnect4.Connect4Board#calculatePossibleMove()}.
	 */
	@Test
	public void testCalculatePossibleMove() {
		assertTrue(((Board) board).getListAllPossibleMoves().size() == 9);
	}

	/**
	 * Test method for {@link xoconnect4.Connect4Board#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(board.invariant());
		board.setState(0, 0, 1);
		assertFalse(board.invariant());
	}

	/**
	 * Test method for {@link xoconnect4.Connect4Board#Connect4Board(int, int)}.
	 */
	@Test
	public void testConnect4BoardIntInt() {
		assertTrue(board.getWidth() == 9);
		assertTrue(board.getHeight() == 10);
	}

	/**
	 * Test method for {@link xoconnect4.Connect4Board#Connect4Board()}.
	 */
	@Test
	public void testConnect4Board() {
		assertTrue(defaultBoard.getWidth() == WIDH_DEFAULT);
		assertTrue(defaultBoard.getHeight() == HEIGHT_DEFAULT);
	}

	/**
	 * Test method for {@link xoconnect4.Connect4Board#makeMove(specifications.MoveInterface)}.
	 * @throws GameExceptions 
	 */
	@Test
	public void testMakeMove() throws GameExceptions {
		assertTrue(board.invariant());
		assertTrue(((Board) board).getListAllPossibleMoves().size() == 9);
		assertTrue(board.isEmpty(board.getWidth() - 1, board.getHeight() - 1));
		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 1, false)));
		assertTrue(board.invariant());
		assertTrue(board.getState(board.getWidth() - 1, board.getHeight() - 1) == 2);
		assertTrue(((Board) board).getListAllPossibleMoves().size() == 9);
	}

	/**
	 * Test method for {@link xoconnect4.Connect4Board#makeMove(specifications.MoveInterface)}.
	 * @throws GameExceptions 
	 */
	@Test(expected = GameExceptions.class)
	public void testMakeMoveException() throws GameExceptions {
		assertTrue(board.invariant());
		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 1, false)));
		assertTrue(board.invariant());
		board.makeMove(new Move(board.getWidth() - 1, board.getHeight() - 1,
				false));
	}

	@Override
	@Test
	public void testIsOverInColumn() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());
		assertTrue(board.makeMove(new Move(0, board.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(0, board.getHeight() - 2, true)));
		assertTrue(board.makeMove(new Move(0, board.getHeight() - 3, true)));
		assertTrue(board.makeMove(new Move(0, board.getHeight() - 4, true)));
		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

	@Override
	@Test
	public void testIsOverOnRow() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());
		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 2, board
				.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 3, board
				.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 4, board
				.getHeight() - 1, true)));
		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

	@Override
	@Test
	public void testIsOverOnDiagonalLTRB() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());

		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 2, board
				.getHeight() - 1, false)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 3, board
				.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 4, board
				.getHeight() - 1, true)));

		assertTrue(board.makeMove(new Move(board.getWidth() - 2, board
				.getHeight() - 2, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 3, board
				.getHeight() - 2, false)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 4, board
				.getHeight() - 2, true)));

		assertTrue(board.makeMove(new Move(board.getWidth() - 3, board
				.getHeight() - 3, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 4, board
				.getHeight() - 3, false)));

		assertTrue(board.makeMove(new Move(board.getWidth() - 4, board
				.getHeight() - 4, true)));

		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

	@Override
	@Test
	public void testIsOverOnDiagonalLBRT() throws GameExceptions {
		assertTrue(board.invariant());
		assertFalse(((Board) board).isOver());

		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 2, board
				.getHeight() - 1, false)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 3, board
				.getHeight() - 1, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 4, board
				.getHeight() - 1, true)));

		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 2, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 2, board
				.getHeight() - 2, false)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 3, board
				.getHeight() - 2, true)));

		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 3, true)));
		assertTrue(board.makeMove(new Move(board.getWidth() - 2, board
				.getHeight() - 3, false)));

		assertTrue(board.makeMove(new Move(board.getWidth() - 1, board
				.getHeight() - 4, true)));

		assertTrue(board.invariant());
		assertTrue(((Board) board).isOver());

	}

}
