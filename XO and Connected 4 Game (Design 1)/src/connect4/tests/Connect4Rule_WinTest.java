/**
 * 
 */
package connect4.tests;

import org.junit.Before;

import abstractions.tests.RuleAbstractTest;

import rules.Rule_Win;
import xoconnect4.Connect4Board;
import xoconnect4.Move;

/**
 * Test for {@link Rule_Win} with {@link Connect4Board}
 *
 */
public class Connect4Rule_WinTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		rule = new Rule_Win();
		setupGame();
		bluePlayer.makeMove(new Move(0, board.getHeight() - 1), board);
		bluePlayer.makeMove(new Move(0, board.getHeight() - 2), board);
		bluePlayer.makeMove(new Move(0, board.getHeight() - 3), board);
	} 

}
