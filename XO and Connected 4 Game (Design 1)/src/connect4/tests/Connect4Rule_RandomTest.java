/**
 * 
 */
package connect4.tests;

import org.junit.Before;

import abstractions.tests.RuleAbstractTest;

import rules.Rule_Random;
import xoconnect4.Connect4Board;

/**
 * Test for {@link Rule_Random} with {@link Connect4Board}
 *
 */
public class Connect4Rule_RandomTest extends RuleAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		rule = new Rule_Random();
		setupGame();
	}

}
