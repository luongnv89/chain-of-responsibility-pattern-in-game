/**
 * 
 */
package connect4.tests;

import org.junit.Before;

import abstractions.tests.Rule_MangerAbstractTest;

import rules.Rule_Manger;
import xoconnect4.Connect4Board;
import xoconnect4.Move;

/**
 *Test for {@link Rule_Manger} 
 *
 */
public class Connect4Rule_MangerTest extends Rule_MangerAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		board = new Connect4Board();
		rule = new Rule_Manger();
		setupGame();
		board.makeMove(new Move(0, board.getHeight() - 1, false));
		board.makeMove(new Move(0, board.getHeight() - 2, false));
	}

}
