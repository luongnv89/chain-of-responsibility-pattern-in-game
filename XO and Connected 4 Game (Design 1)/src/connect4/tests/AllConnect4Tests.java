package connect4.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Connect4AIPlayerTest.class, Connect4BoardTest.class,
		Connect4GameTest.class, Connect4GraphicConsoleTest.class,
		Connect4HumanPlayerTest.class, Connect4Rule_BlockWinTest.class,
		Connect4Rule_MangerTest.class, Connect4Rule_RandomTest.class,
		Connect4Rule_WinTest.class })
public class AllConnect4Tests {

}
